%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           VALVOLA PNEUMATICA 4.5                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alberto Margari (2011)
% by Barbara Bertani (2015)
% by Fabio Bernardini (2016)
% by F. M. Marchese (2011-17)
%
% Tested under MatLab R2013b
%


%%
function varargout = Main(varargin)
% Modello M-file for Modello.fig
%      Modello, by itself, creates a new Modello or raises the existing
%      singleton*.
%
%      H = Modello returns the handle to a new Modello or the handle to
%      the existing singleton*.
%
%      Modello('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Modello.m with the given input arguments.
%
%      Modello('Property','Value',...) creates a new Modello or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
%   IMPORTANTE: necessita m'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Modello

% Last Modified by GUIDE v2.5 22-Nov-2016 16:40:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before Modello is made visible.
function Main_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Modello (see VARARGIN)


global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def =[
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      -10      % F_min
      1        % F, F_cur
      10       % F_max
      0        % B_min
      5       % B, B_cur
      10      % B_max
      0        % m_min
      0.2        % m, m_cur
      1       % m_max
      -10      % v0_min
      0        % v0, v0_cur
      10       % v0_max
      -10      % x0_min
      0        % x0, x0_cur
      10       % x0_max
      0        % k_min
      20       % k, k_cur
      50       % k_max
      ];
    
% Par degli slider
global Fsld Bsld msld ksld x0sld v0sld;

Fsld.stmin = 0.1;
Fsld.stmax = 1;
Fsld.Llim = -Inf;
Fsld.Hlim = +Inf;

Bsld.stmin = 0.1;
Bsld.stmax = 1;
Bsld.Llim = eps;
Bsld.Hlim = +Inf;

msld.stmin = 0.05;
msld.stmax = 0.1;
msld.Llim = eps;
msld.Hlim = +Inf;

ksld.stmin = 1;
ksld.stmax = 10;
ksld.Llim = eps;
ksld.Hlim = +Inf;

x0sld.stmin = 0.1;
x0sld.stmax = 1;
x0sld.Llim = -Inf;
x0sld.Hlim = +Inf;

v0sld.stmin = 0.1;
v0sld.stmax = 1;
v0sld.Llim = -Inf;
v0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);


% Choose default command line output for Modello
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');



% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%SLIDER B
%%
% --- Executes on slider movement.
function B_Callback(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
Slider_sld_Callback(handles, handles.B, handles.B_cur);


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Reset testo punto di equilibrio
set(handles.punto_eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function B_min_Callback(hObject, eventdata, handles)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Hints: get(hObject,'String') returns contents of B_min as text
%        str2double(get(hObject,'String')) returns contents of B_min as a double


% --- Executes during object creation, after setting all properties.
function B_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function B_cur_Callback(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.punto_eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of B_cur as text
%        str2double(get(hObject,'String')) returns contents of B_cur as a double


% --- Executes during object creation, after setting all properties.
function B_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function B_max_Callback(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);
% Hints: get(hObject,'String') returns contents of B_max as text
%        str2double(get(hObject,'String')) returns contents of B_max as a double


% --- Executes during object creation, after setting all properties.
function B_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%SLIDER m
%%
% --- Executes on slider movement.
function m_Callback(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.m, handles.m_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function m_min_Callback(hObject, eventdata, handles)
% hObject    handle to m_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);
% Hints: get(hObject,'String') returns contents of m_min as text
%        str2double(get(hObject,'String')) returns contents of m_min as a double


% --- Executes during object creation, after setting all properties.
function m_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function m_cur_Callback(hObject, eventdata, handles)
% hObject    handle to m_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);

% Hints: get(hObject,'String') returns contents of m_cur as text
%        str2double(get(hObject,'String')) returns contents of m_cur as a double
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function m_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function m_max_Callback(hObject, eventdata, handles)
% hObject    handle to m_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);
% Hints: get(hObject,'String') returns contents of M_max as text
%        str2double(get(hObject,'String')) returns contents of m_max as a double


% --- Executes during object creation, after setting all properties.
function m_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%SLIDER k
%%
% --- Executes on slider movement.
function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.k, handles.k_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.punto_eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function k_min_Callback(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);
% Hints: get(hObject,'String') returns contents of k_min as text
%        str2double(get(hObject,'String')) returns contents of k_min as a double


% --- Executes during object creation, after setting all properties.
function k_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function k_cur_Callback(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Hints: get(hObject,'String') returns contents of k_cur as text
%        str2double(get(hObject,'String')) returns contents of k_cur as a double
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function k_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function k_max_Callback(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);
% Hints: get(hObject,'String') returns contents of k_max as text
%        str2double(get(hObject,'String')) returns contents of m_max as a double


% --- Executes during object creation, after setting all properties.
function k_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER F
% --- Executes on slider movement.
function F_Callback(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.F, handles.F_cur);

val = get(handles.F, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Past instruction:
% evalin('base', ['input_param(' num2str(get(handles.IngressoPar, 'Value')) ') = ' num2str(get(hObject, 'Value')) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function F_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function F_min_Callback(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

%Hints: get(hObject,'String') returns contents of F_min as text
%        str2double(get(hObject,'String')) returns contents of F_min as a double


% --- Executes during object creation, after setting all properties.
function F_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function F_cur_Callback(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~iscell(get(handles.IngressoPar, 'String'))
    set(handles.IngressoPar, 'String', {get(handles.IngressoPar, 'String')});
end
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};

% if strcmp(param_name, 'Ampiezza [N]') == 0
%     if str2double(get(hObject, 'String')) < 0
%         set(hObject, 'String', '0');
%     end
%     if strcmp(param_name, 'Duty Cycle [%]') == 1
%         if str2double(get(hObject, 'String')) > 100
%             set(hObject, 'String', '100');
%         end
%     elseif strcmp(param_name, 'Frequenza�[Hz]') == 1
%         if str2double(get(hObject, 'String')) > 1000
%             set(hObject, 'String', '1000');
%         end
%     end
% end
% if str2double(get(hObject, 'String')) < get(handles.F, 'Min')
%     set(handles.F_min, 'String', get(hObject, 'String'));
%     set(handles.F, 'Min', str2double(get(hObject, 'String')));
% end
% if str2double(get(hObject, 'String')) > get(handles.F, 'Max')
%     set(handles.F_max, 'String', get(hObject, 'String'));
%     set(handles.F, 'Max', str2double(get(hObject, 'String')));
% end
% set (handles.F,'Value',str2double(get(hObject,'String')));
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of actF as text
%        str2double(get(hObject,'String')) returns contents of F_cur as a double


% --- Executes during object creation, after setting all properties.
function F_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function F_max_Callback(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);


% Hints: get(hObject,'String') returns contents of F_max as text
%        str2double(get(hObject,'String')) returns contents of F_max as a double


% --- Executes during object creation, after setting all properties.
function F_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%SLIDER x0
% --- Executes on slider movement.
function x0_Callback(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.x0, handles.x0_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function x0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function x0_min_Callback(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);

% Hints: get(hObject,'String') returns contents of x0_min as text
%        str2double(get(hObject,'String')) returns contents of x0_min as a double


% --- Executes during object creation, after setting all properties.
function x0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);

% Hints: get(hObject,'String') returns contents of x0_cur as text
%        str2double(get(hObject,'String')) returns contents of x0_cur as a double


% --- Executes during object creation, after setting all properties.
function x0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_max_Callback(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);
% Hints: get(hObject,'String') returns contents of x0_max as text
%        str2double(get(hObject,'String')) returns contents of x0_max as a double


% --- Executes during object creation, after setting all properties.
function x0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% SLIDER v0
% --- Executes on slider movement.
function v0_Callback(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.v0, handles.v0_cur);
%
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function v0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%%
function v0_min_Callback(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_min_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_min as text
%        str2double(get(hObject,'String')) returns contents of v0_min as a double


% --- Executes during object creation, after setting all properties.
function v0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function v0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_cur_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_cur as text
%        str2double(get(hObject,'String')) returns contents of v0_cur as a double


% --- Executes during object creation, after setting all properties.
function v0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function v0_max_Callback(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld ksld x0sld v0sld;
Slider_max_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_max as text
%        str2double(get(hObject,'String')) returns contents of v0_max as a double


% --- Executes during object creation, after setting all properties.
function v0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;



%%
% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
axis equal;




%%
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);


id_ingresso = get(hObject, 'Value');

new_String = cell(1);

new_String{1} = 'Ampiezza [N]';

switch id_ingresso
    case {1, 2}
        set(handles.IngressoPar, 'Value', 1);
        set(handles.IngressoPar, 'Enable', 'off');
    case 3
        new_String{2} = 'Frequenza�[Hz]';
        if get(handles.IngressoPar, 'Value') > 2
            set(handles.IngressoPar, 'Value', 2);
        end
        set(handles.IngressoPar, 'Enable', 'on');
    case 5
        new_String{2} = 'Frequenza [Hz]';
        new_String{3} = 'Duty Cycle [%]';
        if get(handles.IngressoPar, 'Value') > 3
            set(handles.IngressoPar, 'Value', 3);
        end
        set(handles.IngressoPar, 'Enable', 'on');
    case {4, 6}
        new_String{2} = 'Frequenza [Hz]';
        if get(handles.IngressoPar, 'Value') > 2
            set(handles.IngressoPar, 'Value', 2);
        end
        set(handles.IngressoPar, 'Enable', 'on');
end

set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'String', new_String);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);


set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
 

%Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);


% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);


set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3)); 

global Fsld Bsld msld ksld x0sld v0sld;

% slider F
set(handles.F_min, 'Value', def(4));
set(handles.F_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.F_cur, 'Value', def(5));
set(handles.F_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.F_max, 'Value', def(6));
set(handles.F_max, 'String', num2str(def(6), '%.1f'));

set(handles.F, 'Min',   def(4)); 
set(handles.F, 'Value', def(5));
set(handles.F, 'Max',   def(6)); 
majorstep = Fsld.stmax / (def(6)-def(4));
minorstep = Fsld.stmin / (def(6)-def(4));
set(handles.F, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.F, 'Min') get(handles.F, 'Value') get(handles.F, 'Max')]);
evalin('base', ['input_params(''Ampiezza [N]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% slider B
set(handles.B_min, 'Value', def(7));
set(handles.B_min, 'String', num2str(def(7),  '%.1f')); 
set(handles.B_cur, 'Value', def(8));
set(handles.B_cur, 'String', num2str(def(8), '%.2f')); 
set(handles.B_max, 'Value', def(9));
set(handles.B_max, 'String', num2str(def(9), '%.1f'));

set(handles.B, 'Min',   def(7)); 
set(handles.B, 'Value', def(8));
set(handles.B, 'Max',   def(9)); 
majorstep = Bsld.stmax / (def(9)-def(7));
minorstep = Bsld.stmin / (def(9)-def(7));
set(handles.B, 'SliderStep', [minorstep majorstep]);

% slider m
set(handles.m_min, 'Value', def(10));
set(handles.m_min, 'String', num2str(def(10),  '%.1f')); 
set(handles.m_cur, 'Value', def(11));
set(handles.m_cur, 'String', num2str(def(11), '%.2f')); 
set(handles.m_max, 'Value', def(12));
set(handles.m_max, 'String', num2str(def(12), '%.1f'));

set(handles.m, 'Min',   def(10)); 
set(handles.m, 'Value', def(11));
set(handles.m, 'Max',   def(12)); 
majorstep = msld.stmax / (def(12)-def(10));
minorstep = msld.stmin / (def(12)-def(10));
set(handles.m, 'SliderStep', [minorstep majorstep]);

%slider v0
set(handles.v0_min, 'Value', def(13));
set(handles.v0_min, 'String', num2str(def(13), '%.1f'));
set(handles.v0_cur, 'Value', def(14));
set(handles.v0_cur, 'String', num2str(def(14), '%.2f'));
set(handles.v0_max, 'Value', def(15));
set(handles.v0_max, 'String', num2str(def(15), '%.1f'));

set(handles.v0, 'Min',   def(13)); 
set(handles.v0, 'Value', def(14));
set(handles.v0, 'Max',   def(15)); 
majorstep = v0sld.stmax / (def(15)-def(13));
minorstep = v0sld.stmin / (def(15)-def(13));
set(handles.v0, 'SliderStep', [minorstep majorstep]);


%slider x0
set(handles.x0_min, 'Value', def(16));
set(handles.x0_min, 'String', num2str(def(16), '%.1f'));
set(handles.x0_cur, 'Value', def(17));
set(handles.x0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.x0_max, 'Value', def(18));
set(handles.x0_max, 'String', num2str(def(18), '%.1f'));

set(handles.x0, 'Min',   def(16)); 
set(handles.x0, 'Value', def(17));
set(handles.x0, 'Max',   def(18)); 
majorstep = x0sld.stmax / (def(18)-def(16));
minorstep = x0sld.stmin / (def(18)-def(16));
set(handles.x0, 'SliderStep', [minorstep majorstep]);

%slider k
set(handles.k_min, 'Value', def(19));
set(handles.k_min, 'String', num2str(def(19), '%.1f'));
set(handles.k_cur, 'Value', def(20));
set(handles.k_cur, 'String', num2str(def(20), '%.2f'));
set(handles.k_max, 'Value', def(21));
set(handles.k_max, 'String', num2str(def(21), '%.1f'));

set(handles.k, 'Min',   def(19)); 
set(handles.k, 'Value', def(20));
set(handles.k, 'Max',   def(21)); 
majorstep = ksld.stmax / (def(21)-def(19));
minorstep = ksld.stmin / (def(21)-def(19));
set(handles.k, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to esegui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)global uscita;
clc

global Uscita;

% Chiudo il modello simulink senza salvare
bdclose(Uscita);

% Leggo la variabile di uscita del sistema (y)
Uscita = 'Valvola_Pneumatica';

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');


% Leggo i dati inseriti dall'utente
x0 = get(handles.x0, 'value');
v0 = get(handles.v0, 'value');
B = get(handles.B, 'value');
m = get(handles.m, 'value');
k = get(handles.k, 'value');
F = ampiezza(2);

% Controllo sui dati nulli
if m == 0, m = eps; end

% Controllo sui dati nulli
if frequenza == 0, frequenza = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza == 1000, frequenza = 999.999; end, end
if dutycycle < 0.0001, dutycycle = 0.0001; end
if dutycycle == 100, dutycycle = 100-0.0001; end


% Costante di tempo
F_mat = [0, 1;
        -k/m, -B/m];
Tau = TimeConstantLTI(F_mat);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'v0', v0;'B', B; 'm', m; 'k', k; 'F', F; 'x0', x0};
for i = 1 : size(vars, 1)
  name = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end


% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end

% Legge qual � il nuovo segnale in input da simulare
val = get(handles.IngressoTipo,'Value');
switch val
  case 1
      add_block('Simulink/Sources/Step', [Uscita,'/input'], 'Time', num2str(.5*Tau));
  case 2
      add_block('Simulink/Sources/Step', [Uscita,'/step1'], 'Time', num2str(.5*Tau));
      add_block('Simulink/Sources/Step', [Uscita,'/step2'], 'Time', num2str(.5*Tau+min(0.25*Tau, 0.01)));
      add_block('Simulink/Math Operations/Sum', [Uscita,'/input'], 'Inputs', '+-');
  case 3
      add_block('Simulink/Sources/Pulse Generator', [Uscita,'/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4
      add_block('Simulink/Sources/Sine Wave', [Uscita,'/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5
      add_block('Simulink/Sources/Pulse Generator', [Uscita,'/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6
      add_block('Simulink/Sources/Repeating Sequence', [Uscita,'/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();


%%
% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



% --- Executes on button press in Punto_eq.
function Punto_eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');

% Setto la tensione in ingresso a 'Step'
set(handles.IngressoTipo,'Value',1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.F_min, 'Value',  ampiezza(1));
set(handles.F_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.F_cur, 'Value',  ampiezza(2));
set(handles.F_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.F_max, 'Value',  ampiezza(3));
set(handles.F_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.F, 'Min',   ampiezza(1));
set(handles.F, 'Value', ampiezza(2));
set(handles.F, 'Max',   ampiezza(3));


% leggo i dati inseriti dall'utente
k = get(handles.k, 'Value');
m = get(handles.m, 'Value');
B = get(handles.B, 'Value');
F = get(handles.F, 'Value');
u = F;

% Controllo sui dati nulli
if m == 0, m = eps; end

F_mat = [0, 1;
        -k/m, -B/m];
G = [0; 1/m];

[x, stb] = PuntoEquilibrioLTI2(F_mat, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['F = ', num2str(u(1), '%.2f'), ' N']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: F = %.1f N', u(1));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  x = %.2f m', x(1));
str22 = sprintf('\n  v = %.2f m/s', x(2));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str22, str3, endstr);
set(handles.punto_eq_txt, 'String', str);



%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % Save della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
 Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Fsld Bsld msld ksld x0sld v0sld;
set(handles.B_min, 'Value',  B_min);
set(handles.B_min, 'String', num2str(B_min, '%.1f'));
set(handles.B_cur, 'Value',  B_cur);
set(handles.B_cur, 'String', num2str(B_cur, '%.2f'));
set(handles.B_max, 'Value',  B_max);
set(handles.B_max, 'String', num2str(B_max, '%.1f'));

set(handles.B, 'Min',   B_min);
set(handles.B, 'Value', B_cur);
set(handles.B, 'Max',   B_max);
majorstep = Bsld.stmax / (B_max-B_min);
minorstep = Bsld.stmin / (B_max-B_min);
set(handles.B, 'SliderStep', [minorstep majorstep]);

set(handles.m_min, 'Value',  m_min);
set(handles.m_min, 'String', num2str(m_min, '%.1f'));
set(handles.m_cur, 'Value',  m_cur);
set(handles.m_cur, 'String', num2str(m_cur, '%.2f'));
set(handles.m_max, 'Value',  m_max);
set(handles.m_max, 'String', num2str(m_max, '%.1f'));

set(handles.m, 'Min',   m_min);
set(handles.m, 'Value', m_cur);
set(handles.m, 'Max',   m_max);
majorstep = msld.stmax / (m_max-m_min);
minorstep = msld.stmin / (m_max-m_min);
set(handles.m, 'SliderStep', [minorstep majorstep]);

set(handles.k_min, 'Value',  k_min);
set(handles.k_min, 'String', num2str(k_min, '%.1f'));
set(handles.k_cur, 'Value',  k_cur);
set(handles.k_cur, 'String', num2str(k_cur, '%.2f'));
set(handles.k_max, 'Value',  k_max);
set(handles.k_max, 'String', num2str(k_max, '%.1f'));

set(handles.k, 'Min',   k_min);
set(handles.k, 'Value', k_cur);
set(handles.k, 'Max',   k_max);
majorstep = ksld.stmax / (k_max-k_min);
minorstep = ksld.stmin / (k_max-k_min);
set(handles.k, 'SliderStep', [minorstep majorstep]);

set(handles.x0_min, 'Value',  x0_min);
set(handles.x0_min, 'String', num2str(x0_min, '%.1f'));
set(handles.x0_cur, 'Value',  x0_cur);
set(handles.x0_cur, 'String', num2str(x0_cur, '%.2f'));
set(handles.x0_max, 'Value',  x0_max);
set(handles.x0_max, 'String', num2str(x0_max, '%.1f'));

set(handles.x0, 'Min',   x0_min);
set(handles.x0, 'Value', x0_cur);
set(handles.x0, 'Max',   x0_max);
majorstep = x0sld.stmax / (x0_max-x0_min);
minorstep = x0sld.stmin / (x0_max-x0_min);
set(handles.x0, 'SliderStep', [minorstep majorstep]);

set(handles.v0_min, 'Value',  v0_min);
set(handles.v0_min, 'String', num2str(v0_min, '%.1f'));
set(handles.v0_cur, 'Value',  v0_cur);
set(handles.v0_cur, 'String', num2str(v0_cur, '%.2f'));
set(handles.v0_max, 'Value',  v0_max);
set(handles.v0_max, 'String', num2str(v0_max, '%.1f'));

set(handles.v0, 'Min',   v0_min);
set(handles.v0, 'Value', v0_cur);
set(handles.v0, 'Max',   v0_max);
majorstep = v0sld.stmax / (v0_max-v0_min);
minorstep = v0sld.stmin / (v0_max-v0_min);
set(handles.v0, 'SliderStep', [minorstep majorstep]);


% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.F_min, 'Value',  F_min);
set(handles.F_min, 'String', num2str(F_min, '%.1f'));
set(handles.F_cur, 'Value',  F_cur);
set(handles.F_cur, 'String', num2str(F_cur, '%.2f'));
set(handles.F_max, 'Value',  F_max);
set(handles.F_max, 'String', num2str(F_max, '%.1f'));

set(handles.F, 'Min',   F_min);
set(handles.F, 'Value', F_cur);
set(handles.F, 'Max',   F_max);
majorstep = Fsld.stmax / (F_max-F_min);
minorstep = Fsld.stmin / (F_max-F_min);
set(handles.F, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% --- Executes on button press in Save.
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello coeff. attrito viscoso
B_min = get(handles.B_min, 'Value');
B_cur = get(handles.B_cur, 'Value');
B_max = get(handles.B_max, 'Value');

% pannello massa
m_min = get(handles.m_min, 'Value');
m_cur = get(handles.m_cur, 'Value');
m_max = get(handles.m_max, 'Value');

% pannello coeff. elastico
k_min = get(handles.k_min, 'Value');
k_cur = get(handles.k_cur, 'Value');
k_max = get(handles.k_max, 'Value');


% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

F_min = get(handles.F_min, 'Value');
F_cur = get(handles.F_cur, 'Value');
F_max = get(handles.F_max, 'Value');

% pannello stato iniziale
v0_min = get(handles.v0_min, 'Value');
v0_cur = get(handles.v0_cur, 'Value');
v0_max = get(handles.v0_max, 'Value');

x0_min = get(handles.x0_min, 'Value');
x0_cur = get(handles.x0_cur, 'Value');
x0_max = get(handles.x0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'F_min = %f;\n', F_min);
fprintf(fid, 'F_cur = %f;\n', F_cur);
fprintf(fid, 'F_max = %f;\n', F_max);

fprintf(fid, 'B_min = %f;\n', B_min);
fprintf(fid, 'B_cur = %f;\n', B_cur);
fprintf(fid, 'B_max = %f;\n', B_max);

fprintf(fid, 'k_min = %f;\n', k_min);
fprintf(fid, 'k_cur = %f;\n', k_cur);
fprintf(fid, 'k_max = %f;\n', k_max);


fprintf(fid, 'm_min = %f;\n', m_min);
fprintf(fid, 'm_cur = %f;\n', m_cur);
fprintf(fid, 'm_max = %f;\n', m_max);

fprintf(fid, 'v0_min = %f;\n', v0_min);
fprintf(fid, 'v0_cur = %f;\n', v0_cur);
fprintf(fid, 'v0_max = %f;\n', v0_max);

fprintf(fid, 'x0_min = %f;\n', x0_min);
fprintf(fid, 'x0_cur = %f;\n', x0_cur);
fprintf(fid, 'x0_max = %f;\n', x0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)

val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end




