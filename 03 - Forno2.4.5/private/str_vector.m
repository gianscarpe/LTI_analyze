%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                      STR_VECTOR                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function str_vector = str_vector(vector) 
%
% vector     row vector as expressed in matlab
%
% str_vector    return displayable rappresentation of the vector
%                distinguishing complex and real values
%       
%
% by G. Scarpellini (2018)
%
% Tested under MatLab R2013b
%


function str_vector = str_vector(vector)    
    i = 1;
    str_vector = '';
    while (i <= length(vector))
        v = vector(i);
        if (not(isreal(v)))
            i = i+1;
        end
        str = str_complex(v);
        if (i ~= length(vector))
            str = strcat(str, ', ');
        end
        str_vector = strcat(str_vector, str);
        % salta il polo successivo, essendo il correte il primo dei due
        % poli complessi coniugati
        i = i+1;
    end
end