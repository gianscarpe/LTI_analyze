%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               F O R N O   C O N   P A R E T E  2   4.1                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Giorgio Codara (2016)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 01-Dec-2016 16:04:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      0        % q_min
      100      % q, q_cur
      300      % q_max
      -20      % thetae_min
      20       % thetae, thetae_cur
      40       % thetae_max
      1        % Ci_min
      300      % Ci, Ci_cur
      1000     % Ci_max
      0        % Kip_min
      83       % Kip, Kip_cur
      100      % Kip_max
      1        % Cp_min
      150       % Cp, Cp_cur
      1000    % Cp_max
      0        % Kpe_min
      85       % Kpe, Kpe_cur
      100      % Kpe_max
      -20      % thetai0_min
      20       % thetai0, thetai0_cur
      300      % thetai0_max
      -20      % thetap0_min
      20       % thetap0, thetap0_cur
      300      % thetap0_max
      ];

% Par degli slider
global qsld thetaesld Cisld Kipsld Cpsld Kpesld thetai0sld thetap0sld;

qsld.stmin = 1;
qsld.stmax = 10;
qsld.Llim = eps;
qsld.Hlim = +Inf;

thetaesld.stmin = 1;
thetaesld.stmax = 10;
thetaesld.Llim = eps;
thetaesld.Hlim = +Inf;

Cisld.stmin = 10;
Cisld.stmax = 100;
Cisld.Llim = eps;
Cisld.Hlim = +Inf;

Kipsld.stmin = 1;
Kipsld.stmax = 10;
Kipsld.Llim = eps;
Kipsld.Hlim = +Inf;

Cpsld.stmin = 10;
Cpsld.stmax = 100;
Cpsld.Llim = eps;
Cpsld.Hlim = +Inf;

Kpesld.stmin = 1;
Kpesld.stmax = 10;
Kpesld.Llim = eps;
Kpesld.Hlim = +Inf;

thetai0sld.stmin = 1;
thetai0sld.stmax = 10;
thetai0sld.Llim = eps;
thetai0sld.Hlim = +Inf;

thetap0sld.stmin = 1;
thetap0sld.stmax = 10;
thetap0sld.Llim = eps;
thetap0sld.Hlim = +Inf;
    
evalin('base', 'input_params = containers.Map();');
    
Load_Defaults(handles);  

% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');
% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.Dialog);


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;

% Hint: place code in OpeningFcn to populate Schema


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im2 = imread('Modello.jpg');
image(im2);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

global Uscita;

% Chiusura del modello simulink senza salvare
bdclose(Uscita);

% Leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = 'thetai';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
thetai0 = get(handles.thetai0, 'Value');
thetap0 = get(handles.thetap0, 'Value');
thetae = get(handles.thetae, 'Value');
q = ampiezza(2);
Ci = get(handles.Ci, 'Value');
Cp = get(handles.Cp, 'Value');
Kip = get(handles.Kip, 'Value');
Kpe = get(handles.Kpe, 'Value');

% Controllo sui dati nulli
if Ci == 0, Ci = eps; end
if Cp == 0, Cp = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [-Kip/Ci, Kip/Ci; 
      Kip/Cp, (-Kip-Kpe)/Cp];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(10, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'thetai0', thetai0; 'thetap0', thetap0; 'q', q; 'thetae', thetae; 'Ci', Ci; 'Kip', Kip; 'Cp', Cp; 'Kpe', Kpe};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il modello in Simulink
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if ( size(h)==[1,1])
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita,'/step1']);
        delete_block([Uscita,'/step2']);
    end
end


% Legge qual'� il nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

%Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([Uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');%salva il sistema

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');

% Setto la tensione in ingresso a 'Step'
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.q_min, 'Value',  ampiezza(1));
set(handles.q_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.q_cur, 'Value',  ampiezza(2));
set(handles.q_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.q_max, 'Value',  ampiezza(3));
set(handles.q_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.q, 'Min',   ampiezza(1));
set(handles.q, 'Value', ampiezza(2));
set(handles.q, 'Max',   ampiezza(3));

% Leggo i dati inseriti dall'utente
thetae = get(handles.thetae, 'Value');
q = get(handles.q, 'Value');
Ci = get(handles.Ci, 'Value');
Cp = get(handles.Cp, 'Value');
Kip = get(handles.Kip, 'Value');
Kpe = get(handles.Kpe, 'Value');
u = [q; thetae];

%controllo sui dati nulli
if Ci == 0, Ci = eps; end
if Cp == 0, Cp = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
%[thetai0; thetap0] = [(-Kip/Ci), (Kip/Ci); (Kip/Cp), ((-Kip-Kpe)/Cp);]*[thetai0; thetap0] + [(1/Ci), 0; 0, (Kpe/Cp)]*[q; thetae]

% Preparazione matrici 
F = [-Kip/Ci, Kip/Ci; 
      Kip/Cp, (-Kip-Kpe)/Cp];
G = [1/Ci, 0; 0, Kpe/Cp];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['q = ', num2str(u(1), '%.2f'), ' W , thetae = ', num2str(u(2), '%.2f'), ' �C']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza degli ingressi: q = %.2f W , thetae = %.2f �C', u(1), u(2));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  thetai = %.2f �C', x(1));
str22 = sprintf('\n  thetap = %.2f �C', x(2));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str22, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);




%% SLIDER Ci
% --- Executes on slider movement.
function Ci_Callback(hObject, eventdata, handles)
% hObject    handle to Ci (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Ci, handles.Ci_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Ci_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ci (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Ci_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Ci_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cisld;
Slider_cur_Callback(handles, handles.Ci, handles.Ci_min, handles.Ci_cur, handles.Ci_max, Cisld.stmin, Cisld.stmax, Cisld.Llim, Cisld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Ci_cur as text
%        str2double(get(hObject,'String')) returns contents of Ci_cur as a double


% --- Executes during object creation, after setting all properties.
function Ci_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ci_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Ci_max_Callback(hObject, eventdata, handles)
% hObject    handle to Ci_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cisld;
Slider_max_Callback(handles, handles.Ci, handles.Ci_min, handles.Ci_cur, handles.Ci_max, Cisld.stmin, Cisld.stmax, Cisld.Llim, Cisld.Hlim);
% Hints: get(hObject,'String') returns contents of Ci_max as text
%        str2double(get(hObject,'String')) returns contents of Ci_max as a double


% --- Executes during object creation, after setting all properties.
function Ci_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ci_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Ci_min_Callback(hObject, eventdata, handles)
% hObject    handle to Ci_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cisld;
Slider_min_Callback(handles, handles.Ci, handles.Ci_min, handles.Ci_cur, handles.Ci_max, Cisld.stmin, Cisld.stmax, Cisld.Llim, Cisld.Hlim);
% Hints: get(hObject,'String') returns contents of Ci_min as text
%        str2double(get(hObject,'String')) returns contents of Ci_min as a double


% --- Executes during object creation, after setting all properties.
function Ci_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ci_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Kip
% --- Executes on slider movement.
function Kip_Callback(hObject, eventdata, handles)
% hObject    handle to Kip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Kip, handles.Kip_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Kip_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Kip_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Kip_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kipsld;
Slider_cur_Callback(handles, handles.Kip, handles.Kip_min, handles.Kip_cur, handles.Kip_max, Kipsld.stmin, Kipsld.stmax, Kipsld.Llim, Kipsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Kip_cur as text
%        str2double(get(hObject,'String')) returns contents of Kip_cur as a double


% --- Executes during object creation, after setting all properties.
function Kip_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kip_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Kip_max_Callback(hObject, eventdata, handles)
% hObject    handle to Kip_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kipsld;
Slider_max_Callback(handles, handles.Kip, handles.Kip_min, handles.Kip_cur, handles.Kip_max, Kipsld.stmin, Kipsld.stmax, Kipsld.Llim, Kipsld.Hlim);
% Hints: get(hObject,'String') returns contents of Kip_max as text
%        str2double(get(hObject,'String')) returns contents of Kip_max as a double


% --- Executes during object creation, after setting all properties.
function Kip_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kip_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Kip_min_Callback(hObject, eventdata, handles)
% hObject    handle to Kip_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kipsld;
Slider_min_Callback(handles, handles.Kip, handles.Kip_min, handles.Kip_cur, handles.Kip_max, Kipsld.stmin, Kipsld.stmax, Kipsld.Llim, Kipsld.Hlim);
% Hints: get(hObject,'String') returns contents of Kip_min as text
%        str2double(get(hObject,'String')) returns contents of Kip_min as a double


% --- Executes during object creation, after setting all properties.
function Kip_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kip_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Cp
% --- Executes on slider movement.
function Cp_Callback(hObject, eventdata, handles)
% hObject    handle to Cp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Cp, handles.Cp_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Cp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Cp_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Cp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cpsld;
Slider_cur_Callback(handles, handles.Cp, handles.Cp_min, handles.Cp_cur, handles.Cp_max, Cpsld.stmin, Cpsld.stmax, Cpsld.Llim, Cpsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Cp_cur as text
%        str2double(get(hObject,'String')) returns contents of Cp_cur as a double


% --- Executes during object creation, after setting all properties.
function Cp_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Cp_max_Callback(hObject, eventdata, handles)
% hObject    handle to Cp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cpsld;
Slider_max_Callback(handles, handles.Cp, handles.Cp_min, handles.Cp_cur, handles.Cp_max, Cpsld.stmin, Cpsld.stmax, Cpsld.Llim, Cpsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cp_max as text
%        str2double(get(hObject,'String')) returns contents of Cp_max as a double


% --- Executes during object creation, after setting all properties.
function Cp_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Cp_min_Callback(hObject, eventdata, handles)
% hObject    handle to Cp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cpsld;
Slider_min_Callback(handles, handles.Cp, handles.Cp_min, handles.Cp_cur, handles.Cp_max, Cpsld.stmin, Cpsld.stmax, Cpsld.Llim, Cpsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cp_min as text
%        str2double(get(hObject,'String')) returns contents of Cp_min as a double


% --- Executes during object creation, after setting all properties.
function Cp_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Kpe
% --- Executes on slider movement.
function Kpe_Callback(hObject, eventdata, handles)
% hObject    handle to Kpe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Kpe, handles.Kpe_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Kpe_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kpe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Kpe_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Kpe_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kpesld;
Slider_cur_Callback(handles, handles.Kpe, handles.Kpe_min, handles.Kpe_cur, handles.Kpe_max, Kpesld.stmin, Kpesld.stmax, Kpesld.Llim, Kpesld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Kpe_cur as text
%        str2double(get(hObject,'String')) returns contents of Kpe_cur as a double


% --- Executes during object creation, after setting all properties.
function Kpe_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kpe_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Kpe_max_Callback(hObject, eventdata, handles)
% hObject    handle to Kpe_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kpesld;
Slider_max_Callback(handles, handles.Kpe, handles.Kpe_min, handles.Kpe_cur, handles.Kpe_max, Kpesld.stmin, Kpesld.stmax, Kpesld.Llim, Kpesld.Hlim);
% Hints: get(hObject,'String') returns contents of Kpe_max as text
%        str2double(get(hObject,'String')) returns contents of Kpe_max as a double


% --- Executes during object creation, after setting all properties.
function Kpe_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kpe_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Kpe_min_Callback(hObject, eventdata, handles)
% hObject    handle to Kpe_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kpesld;
Slider_min_Callback(handles, handles.Kpe, handles.Kpe_min, handles.Kpe_cur, handles.Kpe_max, Kpesld.stmin, Kpesld.stmax, Kpesld.Llim, Kpesld.Hlim);

% Hints: get(hObject,'String') returns contents of Kpe_min as text
%        str2double(get(hObject,'String')) returns contents of Kpe_min as a double


% --- Executes during object creation, after setting all properties.
function Kpe_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kpe_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [W]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    new_String{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    new_String{2} = 'Frequenza [Hz]'; 
    new_String{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    new_String{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q_min, 'Value',  values(1));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_cur, 'Value',  values(2));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_max, 'Value',  values(3));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));
set(handles.q, 'Min',   values(1));
set(handles.q, 'Value', values(2));
set(handles.q, 'Max',   values(3));

% Hints: contents = cellstr(get(hObject,'String')) returns IngressoTipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoTipo


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q_min, 'Value',  values(1));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_cur, 'Value',  values(2));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_max, 'Value',  values(3));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));
set(handles.q, 'Min',   values(1));
set(handles.q, 'Max',   values(3));
set(handles.q, 'Value', values(2));
% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function q_Callback(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.q, handles.q_cur);

val = get(handles.q, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function q_cur_Callback(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_cur_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_cur as text
%        str2double(get(hObject,'String')) returns contents of q_cur as a double


% --- Executes during object creation, after setting all properties.
function q_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q_max_Callback(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_max_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_max as text
%        str2double(get(hObject,'String')) returns contents of q_max as a double


% --- Executes during object creation, after setting all properties.
function q_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function q_min_Callback(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_min_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_min as text
%        str2double(get(hObject,'String')) returns contents of q_min as a double


% --- Executes during object creation, after setting all properties.
function q_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function thetae_Callback(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetae, handles.thetae_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetae_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function thetae_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_cur_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of thetae_cur as text
%        str2double(get(hObject,'String')) returns contents of thetae_cur as a double


% --- Executes during object creation, after setting all properties.
function thetae_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetae_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_max_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_max as text
%        str2double(get(hObject,'String')) returns contents of thetae_max as a double


% --- Executes during object creation, after setting all properties.
function thetae_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetae_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_min_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_min as text
%        str2double(get(hObject,'String')) returns contents of thetae_min as a double


% --- Executes during object creation, after setting all properties.
function thetae_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function thetai0_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetai0, handles.thetai0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetai0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function thetai0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_cur_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_cur as text
%        str2double(get(hObject,'String')) returns contents of thetai0_cur as a double


% --- Executes during object creation, after setting all properties.
function thetai0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetai0_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_max_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_max as text
%        str2double(get(hObject,'String')) returns contents of thetai0_max as a double


% --- Executes during object creation, after setting all properties.
function thetai0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetai0_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_min_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_min as text
%        str2double(get(hObject,'String')) returns contents of thetai0_min as a double


% --- Executes during object creation, after setting all properties.
function thetai0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on slider movement.
function thetap0_Callback(hObject, eventdata, handles)
% hObject    handle to thetap0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetap0, handles.thetap0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetap0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetap0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function thetap0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetap0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetap0sld;
Slider_cur_Callback(handles, handles.thetap0, handles.thetap0_min, handles.thetap0_cur, handles.thetap0_max, thetap0sld.stmin, thetap0sld.stmax, thetap0sld.Llim, thetap0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetap0_cur as text
%        str2double(get(hObject,'String')) returns contents of thetap0_cur as a double


% --- Executes during object creation, after setting all properties.
function thetap0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetap0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetap0_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetap0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetap0sld;
Slider_max_Callback(handles, handles.thetap0, handles.thetap0_min, handles.thetap0_cur, handles.thetap0_max, thetap0sld.stmin, thetap0sld.stmax, thetap0sld.Llim, thetap0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetap0_max as text
%        str2double(get(hObject,'String')) returns contents of thetap0_max as a double


% --- Executes during object creation, after setting all properties.
function thetap0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetap0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function thetap0_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetap0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetap0sld;
Slider_min_Callback(handles, handles.thetap0, handles.thetap0_min, handles.thetap0_cur, handles.thetap0_max, thetap0sld.stmin, thetap0sld.stmax, thetap0sld.Llim, thetap0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetap0_min as text
%        str2double(get(hObject,'String')) returns contents of thetap0_min as a double


% --- Executes during object creation, after setting all properties.
function thetap0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetap0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
%
function Load_Defaults(handles)

global def;
global qsld thetaesld Cisld Kipsld Cpsld Kpesld thetai0sld thetap0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider q
set(handles.q_min, 'Value', def(4));
set(handles.q_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.q_cur, 'Value', def(5));
set(handles.q_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.q_max, 'Value', def(6));
set(handles.q_max, 'String', num2str(def(6), '%.1f'));

set(handles.q, 'Min',   def(4)); 
set(handles.q, 'Value', def(5));
set(handles.q, 'Max',   def(6)); 
majorstep = qsld.stmax / (def(6)-def(4));
minorstep = qsld.stmin / (def(6)-def(4));
set(handles.q, 'SliderStep', [minorstep majorstep]);

evalin('base', ['input_params(''Ampiezza [W]'') = ' mat2str([get(handles.q, 'Min') get(handles.q, 'Value') get(handles.q, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% slider thetae
set(handles.thetae_min, 'Value', def(7));
set(handles.thetae_min, 'String', num2str(def(7),  '%.1f')); 
set(handles.thetae_cur, 'Value', def(8));
set(handles.thetae_cur, 'String', num2str(def(8), '%.2f')); 
set(handles.thetae_max, 'Value', def(9));
set(handles.thetae_max, 'String', num2str(def(9), '%.1f'));

set(handles.thetae, 'Min',   def(7)); 
set(handles.thetae, 'Value', def(8));
set(handles.thetae, 'Max',   def(9)); 
majorstep = thetaesld.stmax / (def(9)-def(7));
minorstep = thetaesld.stmin / (def(9)-def(7));
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

% slider Ci
set(handles.Ci_min, 'Value',  def(10));
set(handles.Ci_min, 'String', num2str(def(10), '%.1f'));
set(handles.Ci_cur, 'Value',  def(11));
set(handles.Ci_cur, 'String', num2str(def(11), '%.2f'));
set(handles.Ci_max, 'Value',  def(12));
set(handles.Ci_max, 'String', num2str(def(12), '%.1f'));

set(handles.Ci, 'Min',   def(10));
set(handles.Ci, 'Value', def(11));
set(handles.Ci, 'Max',   def(12));
majorstep = Cisld.stmax / (def(12)-def(10));
minorstep = Cisld.stmin / (def(12)-def(10));
set(handles.Ci, 'SliderStep', [minorstep majorstep]);

% slider Kip
set(handles.Kip_min, 'Value',  def(13));
set(handles.Kip_min, 'String', num2str(def(13), '%.1f'));
set(handles.Kip_cur, 'Value',  def(14));
set(handles.Kip_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Kip_max, 'Value',  def(15));
set(handles.Kip_max, 'String', num2str(def(15), '%.1f'));

set(handles.Kip, 'Min',   def(13));
set(handles.Kip, 'Value', def(14));
set(handles.Kip, 'Max',   def(15));
majorstep = Kipsld.stmax / (def(15)-def(13));
minorstep = Kipsld.stmin / (def(15)-def(13));
set(handles.Kip, 'SliderStep', [minorstep majorstep]);

% slider Cp
set(handles.Cp_min, 'Value',  def(16));
set(handles.Cp_min, 'String', num2str(def(16), '%.1f'));
set(handles.Cp_cur, 'Value',  def(17));
set(handles.Cp_cur, 'String', num2str(def(17), '%.2f'));
set(handles.Cp_max, 'Value',  def(18));
set(handles.Cp_max, 'String', num2str(def(18), '%.1f'));

set(handles.Cp, 'Min',   def(16));
set(handles.Cp, 'Value', def(17));
set(handles.Cp, 'Max',   def(18));
majorstep = Cpsld.stmax / (def(18)-def(16));
minorstep = Cpsld.stmin / (def(18)-def(16));
set(handles.Cp, 'SliderStep', [minorstep majorstep]);

% slider Kpe
set(handles.Kpe_min, 'Value',  def(19));
set(handles.Kpe_min, 'String', num2str(def(19), '%.1f'));
set(handles.Kpe_cur, 'Value',  def(20));
set(handles.Kpe_cur, 'String', num2str(def(20), '%.2f'));
set(handles.Kpe_max, 'Value',  def(21));
set(handles.Kpe_max, 'String', num2str(def(21), '%.1f'));

set(handles.Kpe, 'Min',   def(19));
set(handles.Kpe, 'Value', def(20));
set(handles.Kpe, 'Max',   def(21));
majorstep = Kpesld.stmax / (def(21)-def(19));
minorstep = Kpesld.stmin / (def(21)-def(19));
set(handles.Kpe, 'SliderStep', [minorstep majorstep]);

% slider thetai0
set(handles.thetai0_min, 'Value', def(22));
set(handles.thetai0_min, 'String', num2str(def(22),  '%.1f')); 
set(handles.thetai0_cur, 'Value', def(23));
set(handles.thetai0_cur, 'String', num2str(def(23), '%.2f')); 
set(handles.thetai0_max, 'Value', def(24));
set(handles.thetai0_max, 'String', num2str(def(24), '%.1f'));

set(handles.thetai0, 'Min',   def(22)); 
set(handles.thetai0, 'Value', def(23));
set(handles.thetai0, 'Max',   def(24)); 
majorstep = thetai0sld.stmax / (def(24)-def(22));
minorstep = thetai0sld.stmin / (def(24)-def(22));
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

% slider thetap0
set(handles.thetap0_min, 'Value', def(25));
set(handles.thetap0_min, 'String', num2str(def(25),  '%.1f')); 
set(handles.thetap0_cur, 'Value', def(26));
set(handles.thetap0_cur, 'String', num2str(def(26), '%.2f')); 
set(handles.thetap0_max, 'Value', def(27));
set(handles.thetap0_max, 'String', num2str(def(27), '%.1f'));

set(handles.thetap0, 'Min',   def(25)); 
set(handles.thetap0, 'Value', def(26));
set(handles.thetap0, 'Max',   def(27)); 
majorstep = thetap0sld.stmax / (def(27)-def(25));
minorstep = thetap0sld.stmin / (def(27)-def(25));
set(handles.thetap0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


%% GESTION CONFIGURAZIONI
% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end
% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global qsld thetaesld Cisld Kipsld Cpsld Kpesld thetai0sld thetap0sld;
set(handles.Ci_min, 'Value',  Ci_min);
set(handles.Ci_min, 'String', num2str(Ci_min, '%.1f'));
set(handles.Ci_cur, 'Value',  Ci_cur);
set(handles.Ci_cur, 'String', num2str(Ci_cur, '%.1f'));
set(handles.Ci_max, 'Value',  Ci_max);
set(handles.Ci_max, 'String', num2str(Ci_max, '%.1f'));

set(handles.Ci, 'Min',   Ci_min);
set(handles.Ci, 'Value', Ci_cur);
set(handles.Ci, 'Max',   Ci_max);
majorstep = Cisld.stmax / (Ci_max-Ci_min);
minorstep = Cisld.stmin / (Ci_max-Ci_min);
set(handles.Ci, 'SliderStep', [minorstep majorstep]);

set(handles.Kip_min, 'Value',  Kip_min);
set(handles.Kip_min, 'String', num2str(Kip_min, '%.1f'));
set(handles.Kip_cur, 'Value',  Kip_cur);
set(handles.Kip_cur, 'String', num2str(Kip_cur, '%.1f'));
set(handles.Kip_max, 'Value',  Kip_max);
set(handles.Kip_max, 'String', num2str(Kip_max, '%.1f'));

set(handles.Kip, 'Min',   Kip_min);
set(handles.Kip, 'Value', Kip_cur);
set(handles.Kip, 'Max',   Kip_max);
majorstep = Kipsld.stmax / (Kip_max-Kip_min);
minorstep = Kipsld.stmin / (Kip_max-Kip_min);
set(handles.Kip, 'SliderStep', [minorstep majorstep]);

set(handles.Cp_min, 'Value',  Cp_min);
set(handles.Cp_min, 'String', num2str(Cp_min, '%.1f'));
set(handles.Cp_cur, 'Value',  Cp_cur);
set(handles.Cp_cur, 'String', num2str(Cp_cur, '%.1f'));
set(handles.Cp_max, 'Value',  Cp_max);
set(handles.Cp_max, 'String', num2str(Cp_max, '%.1f'));

set(handles.Cp, 'Min',   Cp_min);
set(handles.Cp, 'Value', Cp_cur);
set(handles.Cp, 'Max',   Cp_max);
majorstep = Cpsld.stmax / (Cp_max-Cp_min);
minorstep = Cpsld.stmin / (Cp_max-Cp_min);
set(handles.Cp, 'SliderStep', [minorstep majorstep]);

set(handles.Kpe_min, 'Value',  Kpe_min);
set(handles.Kpe_min, 'String', num2str(Kpe_min, '%.1f'));
set(handles.Kpe_cur, 'Value',  Kpe_cur);
set(handles.Kpe_cur, 'String', num2str(Kpe_cur, '%.1f'));
set(handles.Kpe_max, 'Value',  Kpe_max);
set(handles.Kpe_max, 'String', num2str(Kpe_max, '%.1f'));

set(handles.Kpe, 'Min',   Kpe_min);
set(handles.Kpe, 'Value', Kpe_cur);
set(handles.Kpe, 'Max',   Kpe_max);
majorstep = Kpesld.stmax/ (Kpe_max-Kpe_min);
minorstep = Kpesld.stmin / (Kpe_max-Kpe_min);
set(handles.Kpe, 'SliderStep', [minorstep majorstep]);

set(handles.thetai0_min, 'Value',  thetai0_min);
set(handles.thetai0_min, 'String', num2str(thetai0_min, '%.1f'));
set(handles.thetai0_cur, 'Value',  thetai0_cur);
set(handles.thetai0_cur, 'String', num2str(thetai0_cur, '%.1f'));
set(handles.thetai0_max, 'Value',  thetai0_max);
set(handles.thetai0_max, 'String', num2str(thetai0_max, '%.1f'));

set(handles.thetai0, 'Min',   thetai0_min);
set(handles.thetai0, 'Value', thetai0_cur);
set(handles.thetai0, 'Max',   thetai0_max);
majorstep = thetai0sld.stmax / (thetai0_max-thetai0_min);
minorstep = thetai0sld.stmin / (thetai0_max-thetai0_min);
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

set(handles.thetap0_min, 'Value',  thetap0_min);
set(handles.thetap0_min, 'String', num2str(thetap0_min, '%.1f'));
set(handles.thetap0_cur, 'Value',  thetap0_cur);
set(handles.thetap0_cur, 'String', num2str(thetap0_cur, '%.1f'));
set(handles.thetap0_max, 'Value',  thetap0_max);
set(handles.thetap0_max, 'String', num2str(thetap0_max, '%.1f'));

set(handles.thetap0, 'Min',   thetap0_min);
set(handles.thetap0, 'Value', thetap0_cur);
set(handles.thetap0, 'Max',   thetap0_max);
majorstep = thetap0sld.stmax / (thetap0_max-thetap0_min);
minorstep = thetap0sld.stmin / (thetap0_max-thetap0_min);
set(handles.thetap0, 'SliderStep', [minorstep majorstep]);

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.q_min, 'Value',  q_min);
set(handles.q_min, 'String', num2str(q_min, '%.1f'));
set(handles.q_cur, 'Value',  q_cur);
set(handles.q_cur, 'String', num2str(q_cur, '%.1f'));
set(handles.q_max, 'Value',  q_max);
set(handles.q_max, 'String', num2str(q_max, '%.1f'));

set(handles.q, 'Min',   q_min);
set(handles.q, 'Value', q_cur);
set(handles.q, 'Max',   q_max);
majorstep = qsld.stmax / (q_max-q_min);
minorstep = qsld.stmin / (q_max-q_min);
set(handles.q, 'SliderStep', [minorstep majorstep]);

set(handles.thetae_min, 'Value',  thetae_min);
set(handles.thetae_min, 'String', num2str(thetae_min, '%.1f'));
set(handles.thetae_cur, 'Value',  thetae_cur);
set(handles.thetae_cur, 'String', num2str(thetae_cur, '%.1f'));
set(handles.thetae_max, 'Value',  thetae_max);
set(handles.thetae_max, 'String', num2str(thetae_max, '%.1f'));

set(handles.thetae, 'Min',   thetae_min);
set(handles.thetae, 'Value', thetae_cur);
set(handles.thetae, 'Max',   thetae_max);
majorstep = thetaesld.stmax / (thetae_max-thetae_min);
minorstep = thetaesld.stmin / (thetae_max-thetae_min);
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello capacit� termica interna
Ci_min = get(handles.Ci_min, 'Value');
Ci_cur = get(handles.Ci_cur, 'Value');
Ci_max = get(handles.Ci_max, 'Value');

% pannello conducibilit� interno-parete
Kip_min = get(handles.Kip_min, 'Value');
Kip_cur = get(handles.Kip_cur, 'Value');
Kip_max = get(handles.Kip_max, 'Value');

% pannello capacit� termica parete
Cp_min = get(handles.Cp_min, 'Value');
Cp_cur = get(handles.Cp_cur, 'Value');
Cp_max = get(handles.Cp_max, 'Value');

% pannello conducibilit� parete-esterno
Kpe_min = get(handles.Kpe_min, 'Value');
Kpe_cur = get(handles.Kpe_cur, 'Value');
Kpe_max = get(handles.Kpe_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

q_min = get(handles.q_min, 'Value');
q_cur = get(handles.q_cur, 'Value');
q_max = get(handles.q_max, 'Value');

thetae_min = get(handles.thetae_min, 'Value');
thetae_cur = get(handles.thetae_cur, 'Value');
thetae_max = get(handles.thetae_max, 'Value');

% pannello stato iniziale
thetai0_min = get(handles.thetai0_min, 'Value');
thetai0_cur = get(handles.thetai0_cur, 'Value');
thetai0_max = get(handles.thetai0_max, 'Value');

thetap0_min = get(handles.thetap0_min, 'Value');
thetap0_cur = get(handles.thetap0_cur, 'Value');
thetap0_max = get(handles.thetap0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'q_min = %f;\n', q_min);
fprintf(fid, 'q_cur = %f;\n', q_cur);
fprintf(fid, 'q_max = %f;\n', q_max);

fprintf(fid, 'thetae_min = %f;\n', thetae_min);
fprintf(fid, 'thetae_cur = %f;\n', thetae_cur);
fprintf(fid, 'thetae_max = %f;\n', thetae_max);

fprintf(fid, 'Ci_min = %f;\n', Ci_min);
fprintf(fid, 'Ci_cur = %f;\n', Ci_cur);
fprintf(fid, 'Ci_max = %f;\n', Ci_max);

fprintf(fid, 'Kip_min = %f;\n', Kip_min);
fprintf(fid, 'Kip_cur = %f;\n', Kip_cur);
fprintf(fid, 'Kip_max = %f;\n', Kip_max);

fprintf(fid, 'Cp_min = %f;\n', Cp_min);
fprintf(fid, 'Cp_cur = %f;\n', Cp_cur);
fprintf(fid, 'Cp_max = %f;\n', Cp_max);

fprintf(fid, 'Kpe_min = %f;\n', Kpe_min);
fprintf(fid, 'Kpe_cur = %f;\n', Kpe_cur);
fprintf(fid, 'Kpe_max = %f;\n', Kpe_max);

fprintf(fid, 'thetai0_min = %f;\n', thetai0_min);
fprintf(fid, 'thetai0_cur = %f;\n', thetai0_cur);
fprintf(fid, 'thetai0_max = %f;\n', thetai0_max);

fprintf(fid, 'thetap0_min = %f;\n', thetap0_min);
fprintf(fid, 'thetap0_cur = %f;\n', thetap0_cur);
fprintf(fid, 'thetap0_max = %f;\n', thetap0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 263.00537109375, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 3, ...
    'uipanel', 21, ...
    'slider', 11, ...
    'edit', 32, ...
    'popupmenu', 5, ...
    'pushbutton', 7, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\03 - Forno2.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\03 - Forno2.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','FORNO E PARETE',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 26.4 165.571428571429 35.15],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[2.71428571428571 7.6 55 8],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.71428571428571 0.65 51 6],...
'Box','on',...
'CameraPosition',[253.5 75.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[64.956780104712 21.3463962962963 47.4684162303665 14.5543611111111],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 506.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 150.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[252.082633053221 184.875 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-55.4859943977591 77.3749999999999 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-46.9817927170868 -517.625 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[116.714285714286 16.15 46.7142857142857 15.35],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel7';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Flusso di calore q [J/s]',...
'Clipping','on',...
'Position',[1.14285714285714 5.9 44 8.35],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'q';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('q_Callback',hObject,eventdata,guidata(hObject)),...
'Max',300,...
'Position',[1.57142857142857 2.4 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q');

appdata = [];
appdata.lastValidTag = 'q_max';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[31.5714285714286 0.699999999999999 10 1],...
'String','300.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_max');

appdata = [];
appdata.lastValidTag = 'q_min';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.699999999999999 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_min');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.57142857142857 5.7 40 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h13 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.57142857142857 3.95 40 1.3],...
'String','Ampiezza [W]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'q_cur';

h14 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.5714285714286 0.699999999999999 10 1],...
'String','100.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_cur');

appdata = [];
appdata.lastValidTag = 'uipanel9';

h15 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Temperatura esterna thetae [�C]',...
'Clipping','on',...
'Position',[1.14285714285714 0.7 44 4.75],...
'Tag','uipanel9',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'thetae';

h16 = uicontrol(...
'Parent',h15,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('thetae_Callback',hObject,eventdata,guidata(hObject)),...
'Max',40,...
'Min',-20,...
'Position',[1.71428571428571 2.3 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae');

appdata = [];
appdata.lastValidTag = 'thetae_cur';

h17 = uicontrol(...
'Parent',h15,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.7142857142857 0.6 10 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_cur');

appdata = [];
appdata.lastValidTag = 'thetae_max';

h18 = uicontrol(...
'Parent',h15,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[31.7142857142857 0.6 10 1],...
'String','40.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_max');

appdata = [];
appdata.lastValidTag = 'thetae_min';

h19 = uicontrol(...
'Parent',h15,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.71428571428571 0.6 10 1],...
'String','-20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_min');

appdata = [];
appdata.lastValidTag = 'uipanel12';

h20 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[67.7142857142857 10.7 46.5714285714286 20.8],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel13';

h21 = uipanel(...
'Parent',h20,...
'Units','characters',...
'Title','Capacit� termica interna Ci [J/�C]',...
'UserData',[],...
'Clipping','on',...
'Position',[1.14285714285714 14.85 44 4.7],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Ci';

h22 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Ci_Callback',hObject,eventdata,guidata(hObject)),...
'Max',1000,...
'Min',1,...
'Position',[2 2.21538461538461 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',700,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Ci_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Ci');

appdata = [];
appdata.lastValidTag = 'Ci_cur';

h23 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Ci_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[17 0.71538461538461 10 1],...
'String','700.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Ci_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Ci_cur');

appdata = [];
appdata.lastValidTag = 'Ci_max';

h24 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Ci_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[32 0.71538461538461 10 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Ci_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Ci_max');

appdata = [];
appdata.lastValidTag = 'Ci_min';

h25 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Ci_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[2 0.71538461538461 10 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Ci_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Ci_min');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h26 = uipanel(...
'Parent',h20,...
'Units','characters',...
'Title','Conducibilit� interno-parete Kip [�C/W]',...
'Clipping','on',...
'Position',[1.14285714285714 10.2 44 4.5],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Kip';

h27 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Kip_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Position',[2 2.2 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kip_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kip');

appdata = [];
appdata.lastValidTag = 'Kip_cur';

h28 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kip_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[17 0.5 10 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kip_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kip_cur');

appdata = [];
appdata.lastValidTag = 'Kip_max';

h29 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kip_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[32 0.5 10 1],...
'String','100.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kip_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kip_max');

appdata = [];
appdata.lastValidTag = 'Kip_min';

h30 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kip_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[2 0.5 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kip_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kip_min');

appdata = [];
appdata.lastValidTag = 'uipanel15';

h31 = uipanel(...
'Parent',h20,...
'Units','characters',...
'Title','Capacit� termica parete Cp [J/�C]',...
'Clipping','on',...
'Position',[1 5.4 44 4.5],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Cp';

h32 = uicontrol(...
'Parent',h31,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Cp_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10000,...
'Min',1,...
'Position',[1.85714285714286 2.11538461538461 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cp_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cp');

appdata = [];
appdata.lastValidTag = 'Cp_cur';

h33 = uicontrol(...
'Parent',h31,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cp_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.8571428571429 0.46538461538461 10 1],...
'String','50.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cp_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cp_cur');

appdata = [];
appdata.lastValidTag = 'Cp_max';

h34 = uicontrol(...
'Parent',h31,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cp_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[31.8571428571428 0.46538461538461 10 1],...
'String','10000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cp_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cp_max');

appdata = [];
appdata.lastValidTag = 'Cp_min';

h35 = uicontrol(...
'Parent',h31,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cp_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.85714285714286 0.46538461538461 10 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cp_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cp_min');

appdata = [];
appdata.lastValidTag = 'uipanel16';

h36 = uipanel(...
'Parent',h20,...
'Units','characters',...
'Title','Conducibilit� parete-esterno Kpe [�C/W]',...
'Clipping','on',...
'Position',[1.28571428571429 0.549999999999995 44 4.5],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Kpe';

h37 = uicontrol(...
'Parent',h36,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Kpe_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Position',[2 2.2 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kpe_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kpe');

appdata = [];
appdata.lastValidTag = 'Kpe_cur';

h38 = uicontrol(...
'Parent',h36,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kpe_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[17 0.499999999999999 10 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kpe_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kpe_cur');

appdata = [];
appdata.lastValidTag = 'Kpe_max';

h39 = uicontrol(...
'Parent',h36,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kpe_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[32 0.499999999999999 10 1],...
'String','100.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kpe_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kpe_max');

appdata = [];
appdata.lastValidTag = 'Kpe_min';

h40 = uicontrol(...
'Parent',h36,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kpe_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[2 0.499999999999999 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kpe_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kpe_min');

appdata = [];
appdata.lastValidTag = 'Run';

h41 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[18.2857142857143 1.7 24.5714285714286 1.9],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel17';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[117.285714285714 0.9 46.5714285714286 3.65],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h43 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.57142857142857 0.95 40 1.3],...
'String','thetai',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h44 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[77.4285714285714 1.85 23.7142857142857 1.8],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel18';

h45 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[58.7142857142857 3.8 56.8571428571429 6.3],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h46 = uicontrol(...
'Parent',h45,...
'Units','characters',...
'HorizontalAlignment','left',...
'Position',[1.28571428571429 0.299999999999998 54 4.85],...
'String',blanks(0),...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel19';

h47 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[2.71428571428571 16 63.4285714285714 19],...
'Tag','uipanel19',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h48 = axes(...
'Parent',h47,...
'Units','characters',...
'Position',[1.57142857142857 0.55 60.1428571428571 17],...
'Box','on',...
'CameraPosition',[268 202.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[45.8028996057022 4.85304507042253 33.4713497118593 3.30889436619718],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 535.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 404.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h49 = get(h48,'xlabel');

set(h49,...
'Parent',h48,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[266.729216152019 437.176470588235 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h50 = get(h48,'ylabel');

set(h50,...
'Parent',h48,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-49.6959619952494 204.282352941176 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h51 = get(h48,'zlabel');

set(h51,...
'Parent',h48,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-40.8004750593824 -33.364705882353 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel6';

h52 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[116.714285714286 4.7 47 11.35],...
'Tag','uipanel6',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel5';

h53 = uipanel(...
'Parent',h52,...
'Units','characters',...
'Title','Temperatura parete thetap0 [�C]',...
'Clipping','on',...
'Position',[1.42857142857143 0.499999999999999 44 4.75],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'thetap0';

h54 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('thetap0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',300,...
'Min',-20,...
'Position',[1.42857142857143 2.35 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetap0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetap0');

appdata = [];
appdata.lastValidTag = 'thetap0_cur';

h55 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetap0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.4285714285714 0.7 10 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetap0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetap0_cur');

appdata = [];
appdata.lastValidTag = 'thetap0_max';

h56 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetap0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[31.4285714285714 0.7 10 1],...
'String','300.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetap0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetap0_max');

appdata = [];
appdata.lastValidTag = 'thetap0_min';

h57 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetap0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.7 10 1],...
'String','-20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetap0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetap0_min');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h58 = uipanel(...
'Parent',h52,...
'Units','characters',...
'Title','Temperatura interna thetai0 [�C]',...
'Clipping','on',...
'Position',[1.14285714285714 5.65 44 4.55],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'thetai0';

h59 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('thetai0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',300,...
'Min',-20,...
'Position',[1.57142857142857 2.2 40 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0');

appdata = [];
appdata.lastValidTag = 'thetai0_cur';

h60 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.5714285714286 0.5 10 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_cur');

appdata = [];
appdata.lastValidTag = 'thetai0_max';

h61 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[31.5714285714286 0.5 10 1],...
'String','300.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_max');

appdata = [];
appdata.lastValidTag = 'thetai0_min';

h62 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.5 10 1],...
'String','-20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_min');

appdata = [];
appdata.lastValidTag = 'uipanel20';

h63 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazioni',...
'Clipping','on',...
'Position',[67.7142857142857 31.85 97 3.15],...
'Tag','uipanel20',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h64 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.5 0.499999999999999 18 1.45],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h65 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[22 0.499999999999999 8 1.45],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h66 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[32 0.499999999999999 8 1.45],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h67 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[65.8000000000001 0.599999999999999 20 1.3],...
'String','nome file',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h68 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[86.8000000000001 0.499999999999999 8 1.45],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


