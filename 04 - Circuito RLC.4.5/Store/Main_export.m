%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       C I R C U I T O    R L C   4.1                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alessandro Degli Agosti (2008)
% by Dany Thach (2014)
% by Alessandro Pagani (2016)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
function varargout = Main_export(varargin)
% Modello M-file for Modello.fig
%      Modello, by itself, creates a new Modello or raises the existing
%      singleton*.
%
%      H = Modello returns the handle to a new Modello or the handle to
%      the existing singleton*.
%
%      Modello('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Modello.M with the given input arguments.
%
%      Modello('Property','Value',...) creates a new Modello or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_export_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
%   IMPORTANTE: necessita l'installazione del Image Processing Toolbox
%   per la funzione imshow()

% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Modello

% Last Modified by GUIDE v2.5 29-Nov-2016 17:00:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Modello is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Modello (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      -10      % vi_min
      5        % vi, vi_cur
      10       % vi_max
      0.0      % R_min
      1        % R, R_cur
      10       % R_max
      1        % L_min
      20       % L, L_cur
      500      % L_max
      1        % C_min
      500      % C, C_cur
      1000     % C_max
      -10      % vc0_min
      0        % vc0, vc0_cur
      10       % vc0_max
      -2       % i0_min
      0        % i0, i0_cur
      2        % i0_max
      ];

% Par degli slider
global visld Rsld Lsld Csld vc0sld i0sld;

visld.stmin = 0.1;
visld.stmax = 1;
visld.Llim = -Inf;
visld.Hlim = +Inf;

Rsld.stmin = 0.1;
Rsld.stmax = 1;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Lsld.stmin = 1;
Lsld.stmax = 10;
Lsld.Llim = eps;
Lsld.Hlim = +Inf;

Csld.stmin = 10;
Csld.stmax = 100;
Csld.Llim = eps;
Csld.Hlim = +Inf;

vc0sld.stmin = 0.1;
vc0sld.stmax = 1;
vc0sld.Llim = -Inf;
vc0sld.Hlim = +Inf;

i0sld.stmin = 0.1;
i0sld.stmax = 1;
i0sld.Llim = -Inf;
i0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);

% Choose default command line output for Modello
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%%
% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to esegui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)global uscita;
clc

global Uscita;

% Chiusura del modello simulink senza salvare
bdclose(Uscita);

% leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = 'Tensione_Condensatore';
    case 2
        Uscita = 'Energia';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
R   = get(handles.R, 'Value');
L   = get(handles.L, 'Value')/1000;
C   = get(handles.C, 'Value')/1000000;
vi  = ampiezza(2);
vc0 = get(handles.vc0, 'Value');
i0  = get(handles.i0, 'Value');

% Controllo sui dati nulli
if L == 0, L = eps; end
if C == 0, C = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end

% Calcolo costanti di tempo
F = [-R/L -1/L; 1/C 0];

Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'R', R; 'L', L;  'C', C; 'vi', vi; 'vc0', vc0; 'i0', i0};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il modello in Simulink
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita,'Name','input');
if ( size(h)==[1,1])
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita,'/step1']);
        delete_block([Uscita,'/step2']);
    end
end

% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo,'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;


% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val ~= 2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,65,85,95]');
    set_param([Uscita,'/step2'], 'Position', '[20,115,85,145]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = get(hObject,'String') returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Settaggio della tensione in ingresso a 'Step'
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
set(handles.IngressoTipo,'Value',1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.vi_min, 'Value',  ampiezza(1));
set(handles.vi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.vi_cur, 'Value',  ampiezza(2));
set(handles.vi_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.vi_max, 'Value',  ampiezza(3));
set(handles.vi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.vi, 'Min',   ampiezza(1));
set(handles.vi, 'Value', ampiezza(2));
set(handles.vi, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
R = get(handles.R, 'Value');
L = get(handles.L, 'Value')/1000;
C = get(handles.C, 'Value')/1000000;
vi= get(handles.vi, 'Value');
vc0=get(handles.vc0, 'Value');
i0 =get(handles.i0, 'Value');
u = vi;

% Controllo sui dati nulli
if L == 0, L = eps; end
if C == 0, C = eps; end

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [-R/L -1/L; 1/C 0];
G = [1/L; 0];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['vi = ', num2str(u(1), '%.2f'), ' V']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: vi = %.2f V', u(1));
str1 = sprintf('\nlo stato:');
str2 = sprintf('\n  i = %.1f A', x(1));
str21 = sprintf('\n  vc = %.1f V', x(2));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str2, str21, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);


%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER L
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.L, handles.L_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function L_min_Callback(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Lsld;
Slider_min_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function L_cur_Callback(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Lsld;
Slider_cur_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function L_max_Callback(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Lsld;
Slider_max_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER C
% --- Executes on slider movement.
function C_Callback(hObject, eventdata, handles)
% hObject    handle to C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.C, handles.C_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function C_min_Callback(hObject, eventdata, handles)
% hObject    handle to C_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Csld;
Slider_min_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function C_cur_Callback(hObject, eventdata, handles)
% hObject    handle to C_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Csld;
Slider_cur_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function C_max_Callback(hObject, eventdata, handles)
% hObject    handle to C_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Csld;
Slider_max_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [V]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.vi_min, 'Value',  values(1));
set(handles.vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.vi_cur, 'Value',  values(2));
set(handles.vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.vi_max, 'Value',  values(3));
set(handles.vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.vi, 'Min',   values(1));
set(handles.vi, 'Value', values(2));
set(handles.vi, 'Max',   values(3));


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.vi_min, 'Value',  values(1));
set(handles.vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.vi_cur, 'Value',  values(2));
set(handles.vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.vi_max, 'Value',  values(3));
set(handles.vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.vi, 'Min',   values(1));
set(handles.vi, 'Max',   values(3));
set(handles.vi, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function vi_Callback(hObject, eventdata, handles)
% hObject    handle to vi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.vi, handles.vi_cur);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function vi_min_Callback(hObject, eventdata, handles)
% hObject    handle to vi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global visld;
Slider_min_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, visld.stmin, visld.stmax, visld.Llim, visld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function vi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to vi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global visld;
Slider_cur_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, visld.stmin, visld.stmax, visld.Llim, visld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function vi_max_Callback(hObject, eventdata, handles)
% hObject    handle to vi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global visld;
Slider_max_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, visld.stmin, visld.stmax, visld.Llim, visld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function vc0_Callback(hObject, eventdata, handles)
% hObject    handle to vc0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.vc0, handles.vc0_cur);


% --- Executes during object creation, after setting all properties.
function vc0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function vc0_min_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_min_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function vc0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_cur_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function vc0_max_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_max_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function i0_Callback(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.i0, handles.i0_cur);


% --- Executes during object creation, after setting all properties.
function i0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function i0_min_Callback(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global i0sld;
Slider_min_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, i0sld.stmin, i0sld.stmax, i0sld.Llim, i0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function i0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global i0sld;
Slider_cur_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, i0sld.stmin, i0sld.stmax, i0sld.Llim, i0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function i0_max_Callback(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global i0sld;
Slider_max_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, i0sld.stmin, i0sld.stmax, i0sld.Llim, i0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
%
function Load_Defaults(handles)

global def;
global visld Rsld Lsld Csld vc0sld i0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [V]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider vi
set(handles.vi_min, 'Value', def(4));
set(handles.vi_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.vi_cur, 'Value', def(5));
set(handles.vi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.vi_max, 'Value', def(6));
set(handles.vi_max, 'String', num2str(def(6), '%.1f'));

set(handles.vi, 'Min',   def(4)); 
set(handles.vi, 'Value', def(5));
set(handles.vi, 'Max',   def(6)); 
majorstep = visld.stmax / (def(6)-def(4));
minorstep = visld.stmin / (def(6)-def(4));
set(handles.vi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.vi, 'Min') get(handles.vi, 'Value') get(handles.vi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [V]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider L
set(handles.L_min, 'Value',  def(10));
set(handles.L_min, 'String', num2str(def(10), '%.1f'));
set(handles.L_cur, 'Value',  def(11));
set(handles.L_cur, 'String', num2str(def(11), '%.2f'));
set(handles.L_max, 'Value',  def(12));
set(handles.L_max, 'String', num2str(def(12), '%.1f'));

set(handles.L, 'Min',   def(10));
set(handles.L, 'Value', def(11));
set(handles.L, 'Max',   def(12));
majorstep = Lsld.stmax / (def(12)-def(10));
minorstep = Lsld.stmin / (def(12)-def(10));
set(handles.L, 'SliderStep', [minorstep majorstep]);

% slider C
set(handles.C_min, 'Value',  def(13));
set(handles.C_min, 'String', num2str(def(13), '%.1f'));
set(handles.C_cur, 'Value',  def(14));
set(handles.C_cur, 'String', num2str(def(14), '%.2f'));
set(handles.C_max, 'Value',  def(15));
set(handles.C_max, 'String', num2str(def(15), '%.1f'));

set(handles.C, 'Min',   def(13));
set(handles.C, 'Value', def(14));
set(handles.C, 'Max',   def(15));
majorstep = Csld.stmax / (def(15)-def(13));
minorstep = Csld.stmin / (def(15)-def(13));
set(handles.C, 'SliderStep', [minorstep majorstep]);

% slider vc0
set(handles.vc0_min, 'Value', def(16));
set(handles.vc0_min, 'String', num2str(def(16), '%.1f'));
set(handles.vc0_cur, 'Value', def(17));
set(handles.vc0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.vc0_max, 'Value', def(18));
set(handles.vc0_max, 'String', num2str(def(18), '%.1f'));

set(handles.vc0, 'Min',   def(16)); 
set(handles.vc0, 'Value', def(17));
set(handles.vc0, 'Max',   def(18)); 
majorstep = vc0sld.stmax / (def(18)-def(16));
minorstep = vc0sld.stmin / (def(18)-def(16));
set(handles.vc0, 'SliderStep', [minorstep majorstep]);

% slider i0
set(handles.i0_min, 'Value', def(19));
set(handles.i0_min, 'String', num2str(def(19), '%.1f'));
set(handles.i0_cur, 'Value', def(20));
set(handles.i0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.i0_max, 'Value', def(21));
set(handles.i0_max, 'String', num2str(def(21), '%.1f'));

set(handles.i0, 'Min',   def(19)); 
set(handles.i0, 'Value', def(20));
set(handles.i0, 'Max',   def(21)); 
majorstep = i0sld.stmax / (def(21)-def(19));
minorstep = i0sld.stmin / (def(21)-def(19));
set(handles.i0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

bdclose('all');
 
% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global visld Rsld Lsld Csld vc0sld i0sld;

% Aggiornamento degli slider
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

set(handles.L_min, 'Value',  L_min);
set(handles.L_min, 'String', num2str(L_min, '%.1f'));
set(handles.L_cur, 'Value',  L_cur);
set(handles.L_cur, 'String', num2str(L_cur, '%.2f'));
set(handles.L_max, 'Value',  L_max);
set(handles.L_max, 'String', num2str(L_max, '%.1f'));

set(handles.L, 'Min',   L_min);
set(handles.L, 'Value', L_cur);
set(handles.L, 'Max',   L_max);
majorstep = Lsld.stmax / (L_max-L_min);
minorstep = Lsld.stmin / (L_max-L_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

set(handles.C_min, 'Value',  C_min);
set(handles.C_min, 'String', num2str(C_min, '%.1f'));
set(handles.C_cur, 'Value',  C_cur);
set(handles.C_cur, 'String', num2str(C_cur, '%.2f'));
set(handles.C_max, 'Value',  C_max);
set(handles.C_max, 'String', num2str(C_max, '%.1f'));

set(handles.C, 'Min',   C_min);
set(handles.C, 'Value', C_cur);
set(handles.C, 'Max',   C_max);
majorstep = Csld.stmax / (C_max-C_min);
minorstep = Csld.stmin / (C_max-C_min);
set(handles.C, 'SliderStep', [minorstep majorstep]);

set(handles.vc0_min, 'Value',  vc0_min);
set(handles.vc0_min, 'String', num2str(vc0_min, '%.1f'));
set(handles.vc0_cur, 'Value',  vc0_cur);
set(handles.vc0_cur, 'String', num2str(vc0_cur, '%.2f'));
set(handles.vc0_max, 'Value',  vc0_max);
set(handles.vc0_max, 'String', num2str(vc0_max, '%.1f'));

set(handles.vc0, 'Min',   vc0_min);
set(handles.vc0, 'Value', vc0_cur);
set(handles.vc0, 'Max',   vc0_max);
majorstep = vc0sld.stmax / (vc0_max-vc0_min);
minorstep = vc0sld.stmin / (vc0_max-vc0_min);
set(handles.vc0, 'SliderStep', [minorstep majorstep]);

set(handles.i0_min, 'Value',  i0_min);
set(handles.i0_min, 'String', num2str(i0_min, '%.1f'));
set(handles.i0_cur, 'Value',  i0_cur);
set(handles.i0_cur, 'String', num2str(i0_cur, '%.2f'));
set(handles.i0_max, 'Value',  i0_max);
set(handles.i0_max, 'String', num2str(i0_max, '%.1f'));

set(handles.i0, 'Min',   i0_min);
set(handles.i0, 'Value', i0_cur);
set(handles.i0, 'Max',   i0_max);
majorstep = i0sld.stmax / (i0_max-i0_min);
minorstep = i0sld.stmin / (i0_max-i0_min);
set(handles.i0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [V]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.vi_min, 'Value',  vi_min);
set(handles.vi_min, 'String', num2str(vi_min, '%.1f'));
set(handles.vi_cur, 'Value',  vi_cur);
set(handles.vi_cur, 'String', num2str(vi_cur, '%.2f'));
set(handles.vi_max, 'Value',  vi_max);
set(handles.vi_max, 'String', num2str(vi_max, '%.1f'));

set(handles.vi, 'Min',   vi_min);
set(handles.vi, 'Value', vi_cur);
set(handles.vi, 'Max',   vi_max);
majorstep = visld.stmax / (vi_max-vi_min);
minorstep = visld.stmin / (vi_max-vi_min);
set(handles.vi, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello induttore
L_min = get(handles.L_min, 'Value');
L_cur = get(handles.L_cur, 'Value');
L_max = get(handles.L_max, 'Value');

% pannello condensatore
C_min = get(handles.C_min, 'Value');
C_cur = get(handles.C_cur, 'Value');
C_max = get(handles.C_max, 'Value');

% pannello stato iniziale
vc0_min = get(handles.vc0_min, 'Value');
vc0_cur = get(handles.vc0_cur, 'Value');
vc0_max = get(handles.vc0_max, 'Value');

i0_min = get(handles.i0_min, 'Value');
i0_cur = get(handles.i0_cur, 'Value');
i0_max = get(handles.i0_max, 'Value');

% pannello Uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

vi_min = get(handles.vi_min, 'Value');
vi_cur = get(handles.vi_cur, 'Value');
vi_max = get(handles.vi_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'vi_min = %f;\n', vi_min);
fprintf(fid, 'vi_cur = %f;\n', vi_cur);
fprintf(fid, 'vi_max = %f;\n', vi_max);

% Salvataggio Uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'L_min = %f;\n', L_min);
fprintf(fid, 'L_cur = %f;\n', L_cur);
fprintf(fid, 'L_max = %f;\n', L_max);

fprintf(fid, 'C_min = %f;\n', C_min);
fprintf(fid, 'C_cur = %f;\n', C_cur);
fprintf(fid, 'C_max = %f;\n', C_max);

fprintf(fid, 'vc0_min = %f;\n', vc0_min);
fprintf(fid, 'vc0_cur = %f;\n', vc0_cur);
fprintf(fid, 'vc0_max = %f;\n', vc0_max);

fprintf(fid, 'i0_min = %f;\n', i0_min);
fprintf(fid, 'i0_cur = %f;\n', i0_cur);
fprintf(fid, 'i0_max = %f;\n', i0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 339.008544921875, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 11, ...
    'uipanel', 27, ...
    'slider', 13, ...
    'edit', 39, ...
    'popupmenu', 5, ...
    'pushbutton', 6, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\04 - Circuito RLC.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\04 - Circuito RLC.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','CIRCUITO RLC',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[0 2.05 148.285714285714 31.15],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[65 10.9 40.5714285714286 16.25],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h3 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Resistenza R [ohm]',...
'Clipping','on',...
'Position',[1.85714285714286 10.55 37 4.65],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R';

h4 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.15 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R');

appdata = [];
appdata.lastValidTag = 'R_min';

h5 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_min');

appdata = [];
appdata.lastValidTag = 'R_cur';

h6 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.55 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_cur');

appdata = [];
appdata.lastValidTag = 'R_max';

h7 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_max');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h8 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Induttore L [mHenry]',...
'Clipping','on',...
'Position',[1.85714285714286 5.7 37 4.55],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'L';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('L_Callback',hObject,eventdata,guidata(hObject)),...
'Max',500,...
'Min',1,...
'Position',[1.28571428571429 2.05 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.00200400801603206 0.0200400801603206],...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L');

appdata = [];
appdata.lastValidTag = 'L_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.45 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_min');

appdata = [];
appdata.lastValidTag = 'L_cur';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.45 9 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_cur');

appdata = [];
appdata.lastValidTag = 'L_max';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.45 9 1],...
'String','500.0',...
'Style','edit',...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_max');

appdata = [];
appdata.lastValidTag = 'uipanel4';

h13 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Condensatore C [microF]',...
'Clipping','on',...
'Position',[1.57142857142857 0.5 37 4.75],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'C';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('C_Callback',hObject,eventdata,guidata(hObject)),...
'Max',1000,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('C_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','C');

appdata = [];
appdata.lastValidTag = 'C_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('C_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.6 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('C_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','C_min');

appdata = [];
appdata.lastValidTag = 'C_cur';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('C_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.6 9 1],...
'String','500.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('C_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','C_cur');

appdata = [];
appdata.lastValidTag = 'C_max';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('C_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.6 9 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('C_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','C_max');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h18 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.71428571428571 0.950000000000001 29.7142857142857 9.1],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h19 = axes(...
'Parent',h18,...
'Units','characters',...
'Position',[1.57142857142857 0.45 26.1428571428571 7.4],...
'Box','on',...
'CameraPosition',[93.5 69 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[6.43609550561799 1.06874514736454 4.70330056179776 0.728689873203098],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 186.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 137.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h20 = get(h19,'xlabel');

set(h20,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[92.483606557377 162.956081081081 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h21 = get(h19,'ylabel');

set(h21,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-39.6475409836066 70.3885135135135 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h22 = get(h19,'zlabel');

set(h22,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-25.4180327868852 -410.037162162162 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel13';

h23 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[107.571428571429 17.85 40.5714285714286 9.3],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel7';

h24 = uipanel(...
'Parent',h23,...
'Units','characters',...
'Title','Tensione vi [V]',...
'Clipping','on',...
'Position',[1.28571428571429 0.4 37 7.65],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'vi';

h25 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('vi_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.28571428571429 2.15 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vi_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','vi');

appdata = [];
appdata.lastValidTag = 'vi_min';

h26 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vi_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vi_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','vi_min');

appdata = [];
appdata.lastValidTag = 'vi_cur';

h27 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vi_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.55 9 1],...
'String','5.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vi_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','vi_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h28 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.28571428571429 5.15 34 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'vi_max';

h29 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vi_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vi_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','vi_max');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h30 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.28571428571429 3.55 34 1.3],...
'String','Ampiezza [V]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'uipanel8';

h31 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[107.285714285714 2.95 40.5714285714286 3.1],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h32 = uicontrol(...
'Parent',h31,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Position',[2.28571428571429 0.55 36 1.3],...
'String',{  'Tensione condensatore'; 'Energia del sistema' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'Run';

h33 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[114.714285714286 0.55 28 2],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h34 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[65.4285714285714 1.1 28 2],...
'String','Punto di Equilibrio',...
'UserData',[],...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel15';

h35 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[47.1428571428571 3.65 58.7142857142857 6.6],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h36 = uicontrol(...
'Parent',h35,...
'Units','characters',...
'FontName','Courier',...
'FontSize',9,...
'HorizontalAlignment','left',...
'Position',[1.28571428571429 0.35 55.8571428571429 4.85],...
'String',blanks(0),...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel18';

h37 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.28571428571429 10.55 60.4285714285714 20.55],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h38 = axes(...
'Parent',h37,...
'Units','characters',...
'Position',[1.71428571428571 0.7 56.2857142857143 18.6],...
'Box','on',...
'CameraPosition',[185 176 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[13.1152421964991 2.87559598148166 9.5842154512878 1.96063362373749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 369.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 351.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h39 = get(h38,'xlabel');

set(h39,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[184.531725888325 377.447580645161 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h40 = get(h38,'ylabel');

set(h40,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-36.493654822335 177.415322580645 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h41 = get(h38,'zlabel');

set(h41,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-21.508883248731 -20.7298387096774 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[107.285714285714 6.25 40.5714285714286 11.4],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h43 = uipanel(...
'Parent',h42,...
'Units','characters',...
'Title','Tensione vc0 [V]',...
'Clipping','on',...
'Position',[1.28571428571429 5.4 37 4.75],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'vc0';

h44 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('vc0_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Max',10,...
'Min',-10,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vc0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','vc0');

appdata = [];
appdata.lastValidTag = 'vc0_min';

h45 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vc0_min_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.649999999999998 9 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vc0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','vc0_min');

appdata = [];
appdata.lastValidTag = 'vc0_cur';

h46 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vc0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.649999999999998 9 1],...
'String','0.0',...
'Style','edit',...
'Value',0.5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vc0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','vc0_cur');

appdata = [];
appdata.lastValidTag = 'vc0_max';

h47 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('vc0_max_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.649999999999998 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('vc0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','vc0_max');

appdata = [];
appdata.lastValidTag = 'uipanel12';

h48 = uipanel(...
'Parent',h42,...
'Units','characters',...
'Title','Corrente i0 [A]',...
'Clipping','on',...
'Position',[1.28571428571429 0.45 37 4.7],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'i0';

h49 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('i0_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Max',2,...
'Min',-2,...
'Position',[1.28571428571429 2.2 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.025 0.25],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','i0');

appdata = [];
appdata.lastValidTag = 'i0_min';

h50 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_min_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','-2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','i0_min');

appdata = [];
appdata.lastValidTag = 'i0_cur';

h51 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','i0_cur');

appdata = [];
appdata.lastValidTag = 'i0_max';

h52 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_max_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.55 9 1],...
'String','2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','i0_max');

appdata = [];
appdata.lastValidTag = 'uipanel26';

h53 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'UserData',[],...
'Clipping','on',...
'Position',[65 27.65 83 3.5],...
'Tag','uipanel26',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h54 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[23.7142857142857 0.7 8.57142857142857 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h55 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[72 0.7 8.57142857142857 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h56 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.28571428571429 0.7 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h57 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[49.5714285714286 0.8 21 1.1],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h58 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[33.7142857142857 0.7 8.57142857142857 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


