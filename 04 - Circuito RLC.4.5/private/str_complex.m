%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                      STR_COMPLEX                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function str_complex = str_complex(value)  
%
% value          double variable (either real or complex)
%
% str_complex    return displayable rappresentation of the variable
%       
%
% by G. Scarpellini (2018)
%
% Tested under MatLab R2013b
%

function str_complex = str_complex(value)    
   r = real(value);
   im = imag(value);
   if (r == 0 && im ~= 0), r = ''; end
   if (im == 0)
       str_complex = sprintf(' %.1f', r);
   else
       str_complex = sprintf(' %.1f � i %.1f', r, im);
   end
end