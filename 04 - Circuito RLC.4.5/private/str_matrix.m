%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                      STR_MATRIX                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function str_matrix = str_matrix(matrix)  
%
% matrix         matrix vector as expressed in matlab
% n_tab          tabulation space characters for row i > 1 
%
% str_complex    return displayable rappresentation of the matrix
%       
%
% by G. Scarpellini (2018)
%
% Tested under MatLab R2013b
%
function str_matrix = str_matrix(matrix, n_tab)
    str_matrix = '';
    sep_i = ' |';
    space = repmat(' ', n_tab +1, 1);
    sep_f = sprintf('|');

    for i = 1 : size(matrix, 1)
        str = '';
        for j = 1 : size(matrix, 2)
            max_digits = max([floor(log10(abs(matrix(:, j)))); 0])+1;
            value  = matrix(i, j);
            n_digits = max([floor(log10(abs(value))), 0])+1;
            left_space = repmat(' ', 1, max_digits - n_digits);
            str = sprintf('%s %s%+.1f ', str, left_space, value);
        end
        str_row = sprintf('%s%s%s',sep_i, str, sep_f);
        str_matrix = sprintf(strcat(str_matrix, str_row));
        sep_i = sprintf('\n%s|', space);
    end

end