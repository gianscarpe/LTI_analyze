% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
Vi_min = -10.000000;
Vi_cur = 5.000000;
Vi_max = 10.000000;

% Uscita
Uscita = 1;

% Parametri
R_min = 0.000000;
R_cur = 1.100000;
R_max = 10.000000;
L_min = 1.000000;
L_cur = 21.000000;
L_max = 500.000000;
C_min = 1.000000;
C_cur = 500.000000;
C_max = 1000.000000;
Vc0_min = -10.000000;
Vc0_cur = 0.000000;
Vc0_max = 10.000000;
i0_min = -2.000000;
i0_cur = 0.000000;
i0_max = 2.000000;
