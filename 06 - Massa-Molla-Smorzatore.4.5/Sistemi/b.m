% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
F_min = -20.000000;
F_cur = 3.061230;
F_max = 20.000000;

% Uscita
Uscita = 2;

% Parametri
M_min = 1.000000;
M_cur = 3.739130;
M_max = 10.000000;
K_min = 0.000000;
K_cur = 15.800000;
K_max = 20.000000;
B_min = 0.000000;
B_cur = 0.300000;
B_max = 10.000000;
x0_min = -10.000000;
x0_cur = 0.769230;
x0_max = 10.000000;
v0_min = -10.000000;
v0_cur = 0.871800;
v0_max = 10.000000;
