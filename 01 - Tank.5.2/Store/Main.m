%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             T A N K    5.1.0                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Giorgio Codara (2016)
% by Riccardo Candrina e Fabio Nava (2017)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      h0 = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 13-Dec-2017 16:46:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

%Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      0        % phii_min
      1        % phii, phii_cur
      10       % phii_max
      100     % R_min
      1000     % R, R_cur
      10000    % R_max
      1        % rho_pred
      600      % rho_min
      1000     % rho, rho_cur
      16000    % rho_max
      0.1        % A_min
      1        % A, A_cur
      10       % A_max
      0        % h0_min
      0        % h0, h0_cur
      10       % h0_max
      ];
    
% Par degli slider
global phiisld Rsld rhosld Asld h0sld;

phiisld.stmin = 0.1;
phiisld.stmax = 1;
phiisld.Llim = -Inf;
phiisld.Hlim = +Inf;

Rsld.stmin = 100;
Rsld.stmax = 1000;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

rhosld.stmin = 100;
rhosld.stmax = 1000;
rhosld.Llim = eps;
rhosld.Hlim = +Inf;

Asld.stmin = 0.1;
Asld.stmax = 1;
Asld.Llim = eps;
Asld.Hlim = +Inf;

h0sld.stmin = 0.1;
h0sld.stmax = 1;
h0sld.Llim = -Inf;
h0sld.Hlim = +Inf;    

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
  
% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Creazione costante globale
global ver;
ver = '5.1.0';

% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Scheme_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Scheme (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%ConfigLoad jpg
im = imread('Scheme.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Scheme


% --- Executes during object creation, after setting all properties.
function Model_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Model (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%ConfigLoad jpg
im2 = imread('Model.jpg');
image(im2);
axis off;
% Hint: place code in OpeningFcn to populate Model


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

bdclose(Uscita);

% Lettura della variabile di Output del sistema
val1 = get(handles.Output, 'Value');
switch val1
    case 1
        Uscita = 'Flow';
    case 2
        Uscita = 'Height';
    case 3
        Uscita = 'Pressure';
end;

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Amplitude [m^3/s]'')');

if get(handles.InputType, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequency�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequency [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
h0   = get(handles.h0, 'Value');
phii = ampiezza(2);
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');
g    = 9.81;

% Controllo sui dati nulli
if   A == 0,   A = eps; end
if rho == 0, rho = eps; end
if   R == 0,   R = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.InputType, 'Value') == 3, 
    if frequenza(2) == 1000, 
        frequenza(2) = 999.999; 
    end, 
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [-rho*g / (R*A)];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilita'
vars = {'g', g; 'h0', h0; 'phii', phii; 'A', A; 'R', R; 'rho', rho};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Apre il sistema da simulare
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
hdl = find_system(Uscita, 'Name', 'input');
if size(hdl)==[1, 1]
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita, '/step1']);
        delete_block([Uscita, '/step2']);
    end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.InputType, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;


% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([Uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



%% OUTPUT
% --- Executes on selection change in Output.
function Output_Callback(hObject, eventdata, handles)
% hObject    handle to Output (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Output contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Output


% --- Executes during object creation, after setting all properties.
function Output_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Output (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in St_State.
function St_State_Callback(hObject, eventdata, handles)
% hObject    handle to St_State (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Reimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Amplitude [m^3/s]'')');

% Settaggio del flusso in ingresso a 'Step'
set(handles.InputType,'Value',1);
set(handles.InputPar, 'Value', 1);
set(handles.InputPar, 'Enable', 'off');
set(handles.phii_min, 'Value',  ampiezza(1));
set(handles.phii_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.phii_cur, 'Value',  ampiezza(2));
set(handles.phii_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.phii_max, 'Value',  ampiezza(3));
set(handles.phii_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.phii, 'Min',   ampiezza(1));
set(handles.phii, 'Value', ampiezza(2));
set(handles.phii, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
g    = 9.81;
h0   = get(handles.h0, 'Value');
phii = get(handles.phii, 'Value');
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');
u = phii;

% Controllo sui dati
if A == 0, A = eps; end
if rho == 0, rho = eps; end
if R == 0, R = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [-rho*g / (R*A)];
G = [1 / A];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.St_State_txt, 'String', ...
    {'The steady state doesn''t exist given the input: ';...
    ['phii = ', num2str(u(1), '%.1f'), ' m^3/s']});
  return
end

% Preparazione testo da visualizzare
str = sprintf('Given the input: phii = %.1f m^3/s', u(1));
str1 = sprintf('\nthe state:');
str21 = sprintf('\n  h = %.3f m', x(1));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n is asymptotically stable (osc.)');
  case -1.1
    str3 = sprintf('\n is asymptotically stable');
  case  0.1
    str3 = sprintf('\n is simply stable');
  case  0.2
    str3 = sprintf('\n is simply stable (osc.)');
  case +1.1
    str3 = sprintf('\n is weakly unstable');
  case +1.2
    str3 = sprintf('\n is weakly unstable (osc.)');
  case +2.1
    str3 = sprintf('\n is strongly unstable');
  case +2.2
    str3 = sprintf('\n is strongly unstable (osc.)');
  otherwise
    str1 = sprintf('\nregarding the state:');
    str3 = sprintf('\n the stability is NOT determinable');
end

endstr = '.';
str = strcat(str, str1, str21, str3, endstr);
set(handles.St_State_txt, 'String', str);



%% SLIDER A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.A, handles.A_cur);

%Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object deletion, before destroying properties.
function A_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);
% Hints: get(hObject,'String') returns contents of R_min as text
%        str2double(get(hObject,'String')) returns contents of R_min as a double


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);
% Hints: get(hObject,'String') returns contents of R_max as text
%        str2double(get(hObject,'String')) returns contents of R_max as a double


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of R_cur as text
%        str2double(get(hObject,'String')) returns contents of R_cur as a double


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R, handles.R_cur);

%Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);
% Hints: get(hObject,'String') returns contents of R_min as text
%        str2double(get(hObject,'String')) returns contents of R_min as a double


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);
% Hints: get(hObject,'String') returns contents of R_max as text
%        str2double(get(hObject,'String')) returns contents of R_max as a double


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of R_cur as text
%        str2double(get(hObject,'String')) returns contents of R_cur as a double


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER RHO
% --- Executes on slider movement.
function rho_Callback(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.rho, handles.rho_cur);

%Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function rho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function rho_min_Callback(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_min_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);
% Hints: get(hObject,'String') returns contents of rho_min as text
%        str2double(get(hObject,'String')) returns contents of rho_min as a double


% --- Executes during object creation, after setting all properties.
function rho_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rho_max_Callback(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_max_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);
% Hints: get(hObject,'String') returns contents of rho_max as text
%        str2double(get(hObject,'String')) returns contents of rho_max as a double


% --- Executes during object creation, after setting all properties.
function rho_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rho_cur_Callback(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_cur_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.St_State_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of rho_cur as text
%        str2double(get(hObject,'String')) returns contents of rho_cur as a double


% --- Executes during object creation, after setting all properties.
function rho_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in rho_pred.
function rho_pred_Callback(hObject, eventdata, handles)
% hObject    handle to rho_pred (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(hObject,'Value');
switch val
case 1
    set(handles.rho_cur,'String','1000');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 2
    set(handles.rho_cur,'String','670');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 3
    set(handles.rho_cur,'String','760');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 4
    set(handles.rho_cur,'String','920');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 5
    set(handles.rho_cur,'String','960');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 6
    set(handles.rho_cur,'String','13595');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 7
    set(handles.rho_cur,'Enable','on');
    set(handles.rho_min,'Enable','on');
    set(handles.rho_max,'Enable','on');
    set(handles.rho,'Enable','on');
end
if str2double(get(handles.rho_cur, 'String')) < str2double(get(handles.rho_min, 'String'))
    set(handles.rho_min, 'String', get(handles.rho_cur, 'String'));
    set(handles.rho, 'Min', get(handles.rho, 'Value'));
end
if str2double(get(handles.rho_cur, 'String')) > str2double(get(handles.rho_max, 'String'))
    set(handles.rho_max, 'String', get(handles.rho_cur, 'String'));
    set(handles.rho, 'Max', get(handles.rho, 'Value'));
end
% Hints: contents = cellstr(get(hObject,'String')) returns rho_pred contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rho_pred


% --- Executes during object creation, after setting all properties.
function rho_pred_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_pred (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in InputType.
function InputType_Callback(hObject, eventdata, handles)
% hObject    handle to InputType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;

list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Amplitude [m^3/s]';
par = get(handles.InputPar, 'Value');
set(handles.InputPar, 'Enable', 'on');

switch id_ingresso
  case {1, 2}
    set(handles.InputPar, 'Value', 1);
    set(handles.InputPar, 'Enable', 'off');
  case 3
    new_String{2} = 'Frequency�[Hz]';
    set(handles.InputPar, 'Value', 2);
  case 5
    new_String{2} = 'Frequency [Hz]'; 
    new_String{3} = 'Duty Cycle [%]'; 
    set(handles.InputPar, 'Value', 3);
  case {4, 6}
    new_String{2} = 'Frequency [Hz]'; 
    set(handles.InputPar, 'Value', 2);
end

set(handles.InputPar, 'String', new_String);
set(handles.InputType, 'Value', id_ingresso);
set(handles.InputPar, 'Value', 1);

parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.phii_min, 'Value',  values(1));
set(handles.phii_min, 'String', num2str(values(1), '%.1f'));
set(handles.phii_cur, 'Value',  values(2));
set(handles.phii_cur, 'String', num2str(values(2), '%.1f'));
set(handles.phii_max, 'Value',  values(3));
set(handles.phii_max, 'String', num2str(values(3), '%.1f'));
set(handles.phii, 'Min',   values(1));
set(handles.phii, 'Value', values(2));
set(handles.phii, 'Max',   values(3));
% Hints: contents = cellstr(get(hObject,'String')) returns InputType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from InputType


% --- Executes during object creation, after setting all properties.
function InputType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in InputPar.
function InputPar_Callback(hObject, eventdata, handles)
% hObject    handle to InputPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.phii, 'Min',   values(1));
set(handles.phii, 'Max',   values(3));
set(handles.phii, 'Value', values(2));
set(handles.phii_min, 'Value',  values(1));
set(handles.phii_min, 'String', num2str(values(1), '%.1f'));
set(handles.phii_cur, 'Value',  values(2));
set(handles.phii_cur, 'String', num2str(values(2), '%.1f'));
set(handles.phii_max, 'Value',  values(3));
set(handles.phii_max, 'String', num2str(values(3), '%.1f'));

% Hints: contents = cellstr(get(hObject,'String')) returns InputPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from InputPar


% --- Executes during object creation, after setting all properties.
function InputPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% PHI DI INGRESSO
% --- Executes on slider movement.
function phii_Callback(hObject, eventdata, handles)
% hObject    handle to phii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.phii, handles.phii_cur);

val = get(handles.phii, 'Value');
parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function phii_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function phii_min_Callback(hObject, eventdata, handles)
% hObject    handle to phii_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_min_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_min as text
%        str2double(get(hObject,'String')) returns contents of phii_min as a double


% --- Executes during object creation, after setting all properties.
function phii_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function phii_max_Callback(hObject, eventdata, handles)
% hObject    handle to phii_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_max_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_max as text
%        str2double(get(hObject,'String')) returns contents of phii_max as a double


% --- Executes during object creation, after setting all properties.
function phii_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function phii_cur_Callback(hObject, eventdata, handles)
% hObject    handle to phii_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_cur_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.InputPar, 'String');
param_name = parametri{get(handles.InputPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_cur as text
%        str2double(get(hObject,'String')) returns contents of phii_cur as a double


% --- Executes during object creation, after setting all properties.
function phii_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function h0_Callback(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h0, handles.h0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function h0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function h0_min_Callback(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_min_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_min as text
%        str2double(get(hObject,'String')) returns contents of h0_min as a double


% --- Executes during object creation, after setting all properties.
function h0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function h0_max_Callback(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_max_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_max as text
%        str2double(get(hObject,'String')) returns contents of h0_max as a double


% --- Executes during object creation, after setting all properties.
function h0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function h0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_cur_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_cur as text
%        str2double(get(hObject,'String')) returns contents of h0_cur as a double


% --- Executes during object creation, after setting all properties.
function h0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global phiisld Rsld rhosld Asld h0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Amplitude [m^3/s]';
IngressoParval = get(handles.InputPar, 'Value');
set(handles.InputPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.InputPar, 'Value', 1);
    set(handles.InputPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequency�[Hz]';
    set(handles.InputPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequency [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.InputPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequency [Hz]'; 
    set(handles.InputPar, 'Value', 2);
end

set(handles.InputPar, 'String', IngressoParstr);
set(handles.InputType, 'Value', def(1));
set(handles.InputPar, 'Value', def(2));

% Output
set(handles.Output, 'Value', def(3));

% slider phii
set(handles.phii_min, 'Value', def(4));
set(handles.phii_min, 'String', num2str(def(4), '%.1f')); 
set(handles.phii_cur, 'Value', def(5));
set(handles.phii_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.phii_max, 'Value', def(6));
set(handles.phii_max, 'String', num2str(def(6), '%.1f'));

set(handles.phii, 'Min',   def(4)); 
set(handles.phii, 'Value', def(5));
set(handles.phii, 'Max',   def(6)); 
majorstep = phiisld.stmax / (def(6)-def(4));
minorstep = phiisld.stmin / (def(6)-def(4));
set(handles.phii, 'SliderStep', [minorstep majorstep]);

evalin('base', ['input_params(''Amplitude [m^3/s]'') = ' mat2str([get(handles.phii, 'Min') get(handles.phii, 'Value') get(handles.phii, 'Max')]) ';']);
evalin('base', 'input_params(''Frequency [Hz]'') = [0 100 10000];'); % Frequency dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequency�[Hz]'') = [0 100 500];'); % Frequency del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider rho
set(handles.rho_pred, 'Value', def(10));

set(handles.rho_min, 'Value',  def(11));
set(handles.rho_min, 'String', num2str(def(11), '%.1f'));
set(handles.rho_cur, 'Value',  def(12));
set(handles.rho_cur, 'String', num2str(def(12), '%.2f'));
set(handles.rho_max, 'Value',  def(13));
set(handles.rho_max, 'String', num2str(def(13), '%.1f'));

set(handles.rho, 'Min',   def(11));
set(handles.rho, 'Value', def(12));
set(handles.rho, 'Max',   def(13));
majorstep = rhosld.stmax / (def(13)-def(11));
minorstep = rhosld.stmin / (def(13)-def(11));
set(handles.rho, 'SliderStep', [minorstep majorstep]);

% slider A
set(handles.A_min, 'Value',  def(14));
set(handles.A_min, 'String', num2str(def(14), '%.1f'));
set(handles.A_cur, 'Value',  def(15));
set(handles.A_cur, 'String', num2str(def(15), '%.2f'));
set(handles.A_max, 'Value',  def(16));
set(handles.A_max, 'String', num2str(def(16), '%.1f'));

set(handles.A, 'Min',   def(14));
set(handles.A, 'Value', def(15));
set(handles.A, 'Max',   def(16));
majorstep = Asld.stmax / (def(16)-def(14));
minorstep = Asld.stmin / (def(16)-def(14));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider h0
set(handles.h0_min, 'Value', def(17));
set(handles.h0_min, 'String', num2str(def(17), '%.1f'));
set(handles.h0_cur, 'Value', def(18));
set(handles.h0_cur, 'String', num2str(def(18), '%.2f'));
set(handles.h0_max, 'Value', def(19));
set(handles.h0_max, 'String', num2str(def(19), '%.1f'));

set(handles.h0, 'Min',   def(17)); 
set(handles.h0, 'Value', def(18));
set(handles.h0, 'Max',   def(19)); 
majorstep = h0sld.stmax / (def(19)-def(17));
minorstep = h0sld.stmin / (def(19)-def(17));
set(handles.h0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'file name');


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello scheme simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Chiusura delle finestre di Bode e Nyquist
close(findobj('type', 'figure', '-regexp', 'name', '(TANK)*Bode Diagram'));
close(findobj('type', 'figure', '-regexp', 'name', '(TANK)*Nyquist Diagram'));

% Pulizia workspace
evalin('base', 'clear');



%% GESTIONE CONFIGURAZIONI
% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end
% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global phiisld Rsld rhosld Asld h0sld;
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

set(handles.rho_pred, 'Value', rho_pred);

set(handles.rho_min, 'Value',  rho_min);
set(handles.rho_min, 'String', num2str(rho_min, '%.1f'));
set(handles.rho_cur, 'Value',  rho_cur);
set(handles.rho_cur, 'String', num2str(rho_cur, '%.2f'));
set(handles.rho_max, 'Value',  rho_max);
set(handles.rho_max, 'String', num2str(rho_max, '%.1f'));

set(handles.rho, 'Min',   rho_min);
set(handles.rho, 'Value', rho_cur);
set(handles.rho, 'Max',   rho_max);
majorstep = rhosld.stmax / (rho_max-rho_min);
minorstep = rhosld.stmin / (rho_max-rho_min);
set(handles.rho, 'SliderStep', [minorstep majorstep]);

set(handles.h0_min, 'Value',  h0_min);
set(handles.h0_min, 'String', num2str(h0_min, '%.1f'));
set(handles.h0_cur, 'Value',  h0_cur);
set(handles.h0_cur, 'String', num2str(h0_cur, '%.2f'));
set(handles.h0_max, 'Value',  h0_max);
set(handles.h0_max, 'String', num2str(h0_max, '%.1f'));

set(handles.h0, 'Min',   h0_min);
set(handles.h0, 'Value', h0_cur);
set(handles.h0, 'Max',   h0_max);
majorstep = h0sld.stmax / (h0_max-h0_min);
minorstep = h0sld.stmin / (h0_max-h0_min);
set(handles.h0, 'SliderStep', [minorstep majorstep]);

% ingressi
new_String = cell(1);
new_String{1} = 'Amplitude [m^3/s]';
set(handles.InputPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.InputPar, 'Enable', 'off');
  case 3
      new_String{2} = 'Frequency�[Hz]';
  case 5
      new_String{2} = 'Frequency [Hz]';
      new_String{3} = 'Duty Cycle [%]';
  case {4, 6}
      new_String{2} = 'Frequency [Hz]';
end


set(handles.InputPar,  'String', new_String);
set(handles.InputType, 'Value',  IngressoTipo);
set(handles.InputPar,  'Value',  IngressoPar);

set(handles.phii_min, 'Value',  phii_min);
set(handles.phii_min, 'String', num2str(phii_min, '%.1f'));
set(handles.phii_cur, 'Value',  phii_cur);
set(handles.phii_cur, 'String', num2str(phii_cur, '%.2f'));
set(handles.phii_max, 'Value',  phii_max);
set(handles.phii_max, 'String', num2str(phii_max, '%.1f'));

set(handles.phii, 'Min',   phii_min);
set(handles.phii, 'Value', phii_cur);
set(handles.phii, 'Max',   phii_max);
majorstep = phiisld.stmax / (phii_max-phii_min);
minorstep = phiisld.stmin / (phii_max-phii_min);
set(handles.phii, 'SliderStep', [minorstep majorstep]);

set(handles.Output, 'Value', Uscita);



% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

%Gestione nomefile vuoto
if isempty(name)
    errordlg('Empty file name!');
    return;
end

if exist('Sistemi', 'dir') == 0
    mkdir('Sistemi');
end
cd('Sistemi');

% Controllo se il nome della configurazione da salvare sia gi� presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
    choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
    switch choice
        case 'Yes'
            delete(namem);
        case 'No'
            cd('..');
            return
    end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello resistenza valvola
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello Area base
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello Peso specifico
rho_pred = get(handles.rho_pred, 'Value');
rho_min = get(handles.rho_min, 'Value');
rho_cur = get(handles.rho_cur, 'Value');
rho_max = get(handles.rho_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.InputType, 'Value');
IngressoPar = get(handles.InputPar, 'Value');

phii_min = get(handles.phii_min, 'Value');
phii_cur = get(handles.phii_cur, 'Value');
phii_max = get(handles.phii_max, 'Value');

% pannello stato iniziale
h0_min = get(handles.h0_min, 'Value');
h0_cur = get(handles.h0_cur, 'Value');
h0_max = get(handles.h0_max, 'Value');

Uscita = get(handles.Output, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'InputType = %f;\n', IngressoTipo);
fprintf(fid, 'InputPar = %f;\n', IngressoPar);
fprintf(fid, 'Output = %f;\n', Uscita);

fprintf(fid, 'phii_min = %f;\n', phii_min);
fprintf(fid, 'phii_cur = %f;\n', phii_cur);
fprintf(fid, 'phii_max = %f;\n', phii_max);

fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'rho_pred = %f;\n', rho_pred);
fprintf(fid, 'rho_min = %f;\n', rho_min);
fprintf(fid, 'rho_cur = %f;\n', rho_cur);
fprintf(fid, 'rho_max = %f;\n', rho_max);

fprintf(fid, 'h0_min = %f;\n', h0_min);
fprintf(fid, 'h0_cur = %f;\n', h0_cur);
fprintf(fid, 'h0_max = %f;\n', h0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


%% BODE
% --- Executes on button press in Bode.
function Bode_Callback(hObject, eventdata, handles)
% hObject    handle to Bode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc

global ver;

% Lettura dei dati inseriti dall'utente
g    = 9.81;
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if A == 0, A = eps; end
if rho == 0, rho = eps; end
if R == 0, R = eps; end

% Creazione matrici della forma canonica secondo il model del sistema
F = -rho*g/(R*A);
G = 1/A;
H = rho*g/R;

% Crea una nuova finestra
figure('Name', ['TANK ver. ' ver ' - Bode Diagram'], 'NumberTitle', 'off', 'pos', [0 150 600 600]);

% Creazione del sistema e calcolo dei poli
SYS = ss(F,G,H,0);
P = abs(pole(SYS));

% Cambia le opzioni del plot
opts = bodeoptions;
aString = num2str(A);
rString = num2str(R);
rhoString = num2str(rho);
pString = sprintf('%.2f',P);
opts.Title.String = ({'Bode Diagram',
                        ['A = ' aString ' m^2; R = ' rString ' Pa*s/m^3; rho = ' rhoString ' Kg/m^3; p = ' pString ' rad/s']});
opts.Title.FontSize = 12;
opts.XLabel.FontSize = 12;
opts.YLabel.FontSize = 12;
opts.TickLabel.FontSize = 12;

% Plot del sistema
bode(tf(SYS),opts);

grid on;


%% NYQUIST
% --- Executes on button press in Nyquist.
function Nyquist_Callback(hObject, eventdata, handles)
% hObject    handle to Nyquist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global ver;

% Lettura dei dati inseriti dall'utente
g    = 9.81;
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if A == 0, A = eps; end
if rho == 0, rho = eps; end
if R == 0, R = eps; end

% Creazione matrici della forma canonica secondo il model del sistema
F = -rho*g/(R*A);
G = 1/A;
H = rho*g/R;

% Crea una nuova finestra
figure('Name', ['TANK ver. ' ver ' - Nyquist Diagram'], 'NumberTitle', 'off', 'pos' ,[600 150 600 600]);

% Creazione del sistema e calcolo dei poli
SYS = ss(F,G,H,0);
P = abs(pole(SYS));

% Cambia le opzioni del plot
opts = nyquistoptions;
aString = num2str(A);
pString = sprintf('%.2f',P);
rString = num2str(R);
rhoString = num2str(rho);
opts.Title.String = ({'Nyquist Diagram',
                        ['A = ' aString ' m^2; R = ' rString ' Pa*s/m^3; rho = ' rhoString ' Kg/m^3; p = ' pString ' rad/s']});
opts.Title.FontSize = 12;
opts.XLabel.FontSize = 12;
opts.YLabel.FontSize = 12;

% Creazione e plot del sistema
nyquist(tf(SYS),opts);

grid on;


%% CLOSE DIAGRAMS
% --- Executes on button press in CloseDiagrams.
function CloseDiagrams_Callback(hObject, eventdata, handles)
% hObject    handle to CloseDiagrams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Chiusura delle finestre di Bode e Nyquist
close(findobj('type', 'figure', '-regexp', 'name', '(TANK)*Bode Diagram'));
close(findobj('type', 'figure', '-regexp', 'name', '(TANK)*Nyquist Diagram'));
