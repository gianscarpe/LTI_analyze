%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         C I S T E R N A    4.1                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Giorgio Codara (2016)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      h0 = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 01-Dec-2016 15:55:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

%Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      0        % phii_min
      1        % phii, phii_cur
      10       % phii_max
      100     % R_min
      1000     % R, R_cur
      10000    % R_max
      1        % rho_pred
      600      % rho_min
      1000     % rho, rho_cur
      16000    % rho_max
      0.1        % A_min
      1        % A, A_cur
      10       % A_max
      0        % h0_min
      0        % h0, h0_cur
      10       % h0_max
      ];
    
% Par degli slider
global phiisld Rsld rhosld Asld h0sld;

phiisld.stmin = 0.1;
phiisld.stmax = 1;
phiisld.Llim = -Inf;
phiisld.Hlim = +Inf;

Rsld.stmin = 100;
Rsld.stmax = 1000;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

rhosld.stmin = 100;
rhosld.stmax = 1000;
rhosld.Llim = eps;
rhosld.Hlim = +Inf;

Asld.stmin = 0.1;
Asld.stmax = 1;
Asld.Llim = eps;
Asld.Hlim = +Inf;

h0sld.stmin = 0.1;
h0sld.stmax = 1;
h0sld.Llim = -Inf;
h0sld.Hlim = +Inf;    

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
  
% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Schema


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

%ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

bdclose(Uscita);

% Lettura della variabile di Uscita del sistema
val1 = get(handles.Uscita, 'Value');
switch val1
    case 1
        Uscita = 'Flussi';
    case 2
        Uscita = 'Altezza';
    case 3
        Uscita = 'Pressione';
end;

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [m^3/s]'')');

if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
h0   = get(handles.h0, 'Value');
phii = ampiezza(2);
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');
g    = 9.81;

% Controllo sui dati nulli
if   A == 0,   A = eps; end
if rho == 0, rho = eps; end
if   R == 0,   R = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, 
    if frequenza(2) == 1000, 
        frequenza(2) = 999.999; 
    end, 
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [-rho*g / (R*A)];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilita'
vars = {'g', g; 'h0', h0; 'phii', phii; 'A', A; 'R', R; 'rho', rho};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Apre il sistema da simulare
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
hdl = find_system(Uscita, 'Name', 'input');
if size(hdl)==[1, 1]
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita, '/step1']);
        delete_block([Uscita, '/step2']);
    end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;


% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([Uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Reimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [m^3/s]'')');

% Settaggio del flusso in ingresso a 'Step'
set(handles.IngressoTipo,'Value',1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.phii_min, 'Value',  ampiezza(1));
set(handles.phii_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.phii_cur, 'Value',  ampiezza(2));
set(handles.phii_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.phii_max, 'Value',  ampiezza(3));
set(handles.phii_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.phii, 'Min',   ampiezza(1));
set(handles.phii, 'Value', ampiezza(2));
set(handles.phii, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
g    = 9.81;
h0   = get(handles.h0, 'Value');
phii = get(handles.phii, 'Value');
A    = get(handles.A, 'Value');
R    = get(handles.R, 'Value');
rho  = get(handles.rho, 'Value');
u = phii;

% Controllo sui dati
if A == 0, A = eps; end
if rho == 0, rho = eps; end
if R == 0, R = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [-rho*g / (R*A)];
G = [1 / A];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['phii = ', num2str(u(1), '%.1f'), ' m^3/s']});
  return
end

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: phii = %.1f m^3/s', u(1));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  h = %.3f m', x(1));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);



%% SLIDER A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.A, handles.A_cur);

%Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object deletion, before destroying properties.
function A_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);
% Hints: get(hObject,'String') returns contents of R_min as text
%        str2double(get(hObject,'String')) returns contents of R_min as a double


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);
% Hints: get(hObject,'String') returns contents of R_max as text
%        str2double(get(hObject,'String')) returns contents of R_max as a double


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of R_cur as text
%        str2double(get(hObject,'String')) returns contents of R_cur as a double


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R, handles.R_cur);

%Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);
% Hints: get(hObject,'String') returns contents of R_min as text
%        str2double(get(hObject,'String')) returns contents of R_min as a double


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);
% Hints: get(hObject,'String') returns contents of R_max as text
%        str2double(get(hObject,'String')) returns contents of R_max as a double


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of R_cur as text
%        str2double(get(hObject,'String')) returns contents of R_cur as a double


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER RHO
% --- Executes on slider movement.
function rho_Callback(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.rho, handles.rho_cur);

%Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function rho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function rho_min_Callback(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_min_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);
% Hints: get(hObject,'String') returns contents of rho_min as text
%        str2double(get(hObject,'String')) returns contents of rho_min as a double


% --- Executes during object creation, after setting all properties.
function rho_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rho_max_Callback(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_max_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);
% Hints: get(hObject,'String') returns contents of rho_max as text
%        str2double(get(hObject,'String')) returns contents of rho_max as a double


% --- Executes during object creation, after setting all properties.
function rho_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rho_cur_Callback(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_cur_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of rho_cur as text
%        str2double(get(hObject,'String')) returns contents of rho_cur as a double


% --- Executes during object creation, after setting all properties.
function rho_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in rho_pred.
function rho_pred_Callback(hObject, eventdata, handles)
% hObject    handle to rho_pred (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(hObject,'Value');
switch val
case 1
    set(handles.rho_cur,'String','1000');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 2
    set(handles.rho_cur,'String','670');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 3
    set(handles.rho_cur,'String','760');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 4
    set(handles.rho_cur,'String','920');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 5
    set(handles.rho_cur,'String','960');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 6
    set(handles.rho_cur,'String','13595');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 7
    set(handles.rho_cur,'Enable','on');
    set(handles.rho_min,'Enable','on');
    set(handles.rho_max,'Enable','on');
    set(handles.rho,'Enable','on');
end
if str2double(get(handles.rho_cur, 'String')) < str2double(get(handles.rho_min, 'String'))
    set(handles.rho_min, 'String', get(handles.rho_cur, 'String'));
    set(handles.rho, 'Min', get(handles.rho, 'Value'));
end
if str2double(get(handles.rho_cur, 'String')) > str2double(get(handles.rho_max, 'String'))
    set(handles.rho_max, 'String', get(handles.rho_cur, 'String'));
    set(handles.rho, 'Max', get(handles.rho, 'Value'));
end
% Hints: contents = cellstr(get(hObject,'String')) returns rho_pred contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rho_pred


% --- Executes during object creation, after setting all properties.
function rho_pred_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_pred (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;

list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [m^3/s]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    new_String{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    new_String{2} = 'Frequenza [Hz]'; 
    new_String{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    new_String{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.phii_min, 'Value',  values(1));
set(handles.phii_min, 'String', num2str(values(1), '%.1f'));
set(handles.phii_cur, 'Value',  values(2));
set(handles.phii_cur, 'String', num2str(values(2), '%.1f'));
set(handles.phii_max, 'Value',  values(3));
set(handles.phii_max, 'String', num2str(values(3), '%.1f'));
set(handles.phii, 'Min',   values(1));
set(handles.phii, 'Value', values(2));
set(handles.phii, 'Max',   values(3));
% Hints: contents = cellstr(get(hObject,'String')) returns IngressoTipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoTipo


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.phii, 'Min',   values(1));
set(handles.phii, 'Max',   values(3));
set(handles.phii, 'Value', values(2));
set(handles.phii_min, 'Value',  values(1));
set(handles.phii_min, 'String', num2str(values(1), '%.1f'));
set(handles.phii_cur, 'Value',  values(2));
set(handles.phii_cur, 'String', num2str(values(2), '%.1f'));
set(handles.phii_max, 'Value',  values(3));
set(handles.phii_max, 'String', num2str(values(3), '%.1f'));

% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% PHI DI INGRESSO
% --- Executes on slider movement.
function phii_Callback(hObject, eventdata, handles)
% hObject    handle to phii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.phii, handles.phii_cur);

val = get(handles.phii, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function phii_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function phii_min_Callback(hObject, eventdata, handles)
% hObject    handle to phii_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_min_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_min as text
%        str2double(get(hObject,'String')) returns contents of phii_min as a double


% --- Executes during object creation, after setting all properties.
function phii_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function phii_max_Callback(hObject, eventdata, handles)
% hObject    handle to phii_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_max_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_max as text
%        str2double(get(hObject,'String')) returns contents of phii_max as a double


% --- Executes during object creation, after setting all properties.
function phii_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function phii_cur_Callback(hObject, eventdata, handles)
% hObject    handle to phii_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global phiisld;
Slider_cur_Callback(handles, handles.phii, handles.phii_min, handles.phii_cur, handles.phii_max, phiisld.stmin, phiisld.stmax, phiisld.Llim, phiisld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.phii, 'Min') get(handles.phii, 'Value'), get(handles.phii, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of phii_cur as text
%        str2double(get(hObject,'String')) returns contents of phii_cur as a double


% --- Executes during object creation, after setting all properties.
function phii_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to phii_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function h0_Callback(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h0, handles.h0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function h0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function h0_min_Callback(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_min_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_min as text
%        str2double(get(hObject,'String')) returns contents of h0_min as a double


% --- Executes during object creation, after setting all properties.
function h0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function h0_max_Callback(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_max_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_max as text
%        str2double(get(hObject,'String')) returns contents of h0_max as a double


% --- Executes during object creation, after setting all properties.
function h0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function h0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_cur_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);
% Hints: get(hObject,'String') returns contents of h0_cur as text
%        str2double(get(hObject,'String')) returns contents of h0_cur as a double


% --- Executes during object creation, after setting all properties.
function h0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global phiisld Rsld rhosld Asld h0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [m^3/s]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider phii
set(handles.phii_min, 'Value', def(4));
set(handles.phii_min, 'String', num2str(def(4), '%.1f')); 
set(handles.phii_cur, 'Value', def(5));
set(handles.phii_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.phii_max, 'Value', def(6));
set(handles.phii_max, 'String', num2str(def(6), '%.1f'));

set(handles.phii, 'Min',   def(4)); 
set(handles.phii, 'Value', def(5));
set(handles.phii, 'Max',   def(6)); 
majorstep = phiisld.stmax / (def(6)-def(4));
minorstep = phiisld.stmin / (def(6)-def(4));
set(handles.phii, 'SliderStep', [minorstep majorstep]);

evalin('base', ['input_params(''Ampiezza [m^3/s]'') = ' mat2str([get(handles.phii, 'Min') get(handles.phii, 'Value') get(handles.phii, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider rho
set(handles.rho_pred, 'Value', def(10));

set(handles.rho_min, 'Value',  def(11));
set(handles.rho_min, 'String', num2str(def(11), '%.1f'));
set(handles.rho_cur, 'Value',  def(12));
set(handles.rho_cur, 'String', num2str(def(12), '%.2f'));
set(handles.rho_max, 'Value',  def(13));
set(handles.rho_max, 'String', num2str(def(13), '%.1f'));

set(handles.rho, 'Min',   def(11));
set(handles.rho, 'Value', def(12));
set(handles.rho, 'Max',   def(13));
majorstep = rhosld.stmax / (def(13)-def(11));
minorstep = rhosld.stmin / (def(13)-def(11));
set(handles.rho, 'SliderStep', [minorstep majorstep]);

% slider A
set(handles.A_min, 'Value',  def(14));
set(handles.A_min, 'String', num2str(def(14), '%.1f'));
set(handles.A_cur, 'Value',  def(15));
set(handles.A_cur, 'String', num2str(def(15), '%.2f'));
set(handles.A_max, 'Value',  def(16));
set(handles.A_max, 'String', num2str(def(16), '%.1f'));

set(handles.A, 'Min',   def(14));
set(handles.A, 'Value', def(15));
set(handles.A, 'Max',   def(16));
majorstep = Asld.stmax / (def(16)-def(14));
minorstep = Asld.stmin / (def(16)-def(14));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider h0
set(handles.h0_min, 'Value', def(17));
set(handles.h0_min, 'String', num2str(def(17), '%.1f'));
set(handles.h0_cur, 'Value', def(18));
set(handles.h0_cur, 'String', num2str(def(18), '%.2f'));
set(handles.h0_max, 'Value', def(19));
set(handles.h0_max, 'String', num2str(def(19), '%.1f'));

set(handles.h0, 'Min',   def(17)); 
set(handles.h0, 'Value', def(18));
set(handles.h0, 'Max',   def(19)); 
majorstep = h0sld.stmax / (def(19)-def(17));
minorstep = h0sld.stmin / (def(19)-def(17));
set(handles.h0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



%% GESTIONE CONFIGURAZIONI
% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end
% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global phiisld Rsld rhosld Asld h0sld;
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

set(handles.rho_pred, 'Value', rho_pred);

set(handles.rho_min, 'Value',  rho_min);
set(handles.rho_min, 'String', num2str(rho_min, '%.1f'));
set(handles.rho_cur, 'Value',  rho_cur);
set(handles.rho_cur, 'String', num2str(rho_cur, '%.2f'));
set(handles.rho_max, 'Value',  rho_max);
set(handles.rho_max, 'String', num2str(rho_max, '%.1f'));

set(handles.rho, 'Min',   rho_min);
set(handles.rho, 'Value', rho_cur);
set(handles.rho, 'Max',   rho_max);
majorstep = rhosld.stmax / (rho_max-rho_min);
minorstep = rhosld.stmin / (rho_max-rho_min);
set(handles.rho, 'SliderStep', [minorstep majorstep]);

set(handles.h0_min, 'Value',  h0_min);
set(handles.h0_min, 'String', num2str(h0_min, '%.1f'));
set(handles.h0_cur, 'Value',  h0_cur);
set(handles.h0_cur, 'String', num2str(h0_cur, '%.2f'));
set(handles.h0_max, 'Value',  h0_max);
set(handles.h0_max, 'String', num2str(h0_max, '%.1f'));

set(handles.h0, 'Min',   h0_min);
set(handles.h0, 'Value', h0_cur);
set(handles.h0, 'Max',   h0_max);
majorstep = h0sld.stmax / (h0_max-h0_min);
minorstep = h0sld.stmin / (h0_max-h0_min);
set(handles.h0, 'SliderStep', [minorstep majorstep]);

% ingressi
new_String = cell(1);
new_String{1} = 'Ampiezza [m^3/s]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      new_String{2} = 'Frequenza�[Hz]';
  case 5
      new_String{2} = 'Frequenza [Hz]';
      new_String{3} = 'Duty Cycle [%]';
  case {4, 6}
      new_String{2} = 'Frequenza [Hz]';
end


set(handles.IngressoPar,  'String', new_String);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.phii_min, 'Value',  phii_min);
set(handles.phii_min, 'String', num2str(phii_min, '%.1f'));
set(handles.phii_cur, 'Value',  phii_cur);
set(handles.phii_cur, 'String', num2str(phii_cur, '%.2f'));
set(handles.phii_max, 'Value',  phii_max);
set(handles.phii_max, 'String', num2str(phii_max, '%.1f'));

set(handles.phii, 'Min',   phii_min);
set(handles.phii, 'Value', phii_cur);
set(handles.phii, 'Max',   phii_max);
majorstep = phiisld.stmax / (phii_max-phii_min);
minorstep = phiisld.stmin / (phii_max-phii_min);
set(handles.phii, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);



% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

%Gestione nomefile vuoto
if isempty(name)
    errordlg('Empty file name!');
    return;
end

if exist('Sistemi', 'dir') == 0
    mkdir('Sistemi');
end
cd('Sistemi');

% Controllo se il nome della configurazione da salvare sia gi� presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
    choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
    switch choice
        case 'Yes'
            delete(namem);
        case 'No'
            cd('..');
            return
    end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello resistenza valvola
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello Area base
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello Peso specifico
rho_pred = get(handles.rho_pred, 'Value');
rho_min = get(handles.rho_min, 'Value');
rho_cur = get(handles.rho_cur, 'Value');
rho_max = get(handles.rho_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

phii_min = get(handles.phii_min, 'Value');
phii_cur = get(handles.phii_cur, 'Value');
phii_max = get(handles.phii_max, 'Value');

% pannello stato iniziale
h0_min = get(handles.h0_min, 'Value');
h0_cur = get(handles.h0_cur, 'Value');
h0_max = get(handles.h0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'phii_min = %f;\n', phii_min);
fprintf(fid, 'phii_cur = %f;\n', phii_cur);
fprintf(fid, 'phii_max = %f;\n', phii_max);

fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'rho_pred = %f;\n', rho_pred);
fprintf(fid, 'rho_min = %f;\n', rho_min);
fprintf(fid, 'rho_cur = %f;\n', rho_cur);
fprintf(fid, 'rho_max = %f;\n', rho_max);

fprintf(fid, 'h0_min = %f;\n', h0_min);
fprintf(fid, 'h0_cur = %f;\n', h0_cur);
fprintf(fid, 'h0_max = %f;\n', h0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 246.008056640625, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 4, ...
    'uipanel', 18, ...
    'slider', 8, ...
    'edit', 23, ...
    'popupmenu', 8, ...
    'pushbutton', 6, ...
    'text', 5), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\01 - Cisterna.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\01 - Cisterna.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','CISTERNA',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 36.15 140.714285714286 28.55],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.14285714285714 2.05 31.7142857142857 7.2],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.28571428571429 0.6 28.1428571428571 5.4],...
'Box','on',...
'CameraPosition',[122.5 70 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[9.7983665961945 1.83713003029915 7.16034482029598 1.25258865702215],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 244.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 139.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[121.261421319797 174.893518518518 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-48.4238578680203 71.9305555555555 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-23.6522842639594 -522.680555555556 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel5';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[56.1428571428571 8.15 41 16.8],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel3';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Area base A [m^2]',...
'Clipping','on',...
'Position',[1.28571428571429 11.35 38 4.3],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'A';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('A_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',1,...
'Position',[1.42857142857143 2 35 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0111111111111111 0.111111111111111],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'DeleteFcn',@(hObject,eventdata)Main_export('A_DeleteFcn',hObject,eventdata,guidata(hObject)),...
'Tag','A');

appdata = [];
appdata.lastValidTag = 'A_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.55 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_min');

appdata = [];
appdata.lastValidTag = 'A_max';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[27.4285714285714 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_max');

appdata = [];
appdata.lastValidTag = 'A_cur';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.4285714285714 0.55 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_cur');

appdata = [];
appdata.lastValidTag = 'uipanel6';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Resistenza valvola R [Pa�s/m^3]',...
'Clipping','on',...
'Position',[1.28571428571429 6.7 38 4.4],...
'Tag','uipanel6',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10000,...
'Min',1000,...
'Position',[1.42857142857143 2.05 35 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0111111111111111 0.111111111111111],...
'Value',1000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R');

appdata = [];
appdata.lastValidTag = 'R_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.5 9 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_min');

appdata = [];
appdata.lastValidTag = 'R_max';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_max_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[27.4285714285714 0.5 9 1],...
'String','10000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_max');

appdata = [];
appdata.lastValidTag = 'R_cur';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.4285714285714 0.5 9 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_cur');

appdata = [];
appdata.lastValidTag = 'uipanel9';

h18 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Peso specifico fluido rho [Kg/m^3]',...
'Clipping','on',...
'Position',[1.42857142857143 0.4 38 6],...
'Tag','uipanel9',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'rho';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('rho_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Max',16000,...
'Min',600,...
'Position',[1.28571428571429 2 35 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',1000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho');

appdata = [];
appdata.lastValidTag = 'rho_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_min_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','600.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_min');

appdata = [];
appdata.lastValidTag = 'rho_max';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_max_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'HorizontalAlignment','right',...
'Position',[27.2857142857143 0.55 9 1],...
'String','16000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_max');

appdata = [];
appdata.lastValidTag = 'rho_cur';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_cur_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[14.2857142857143 0.55 9 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_cur');

appdata = [];
appdata.lastValidTag = 'rho_pred';

h23 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_pred_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Position',[1.28571428571429 3.4 35 1.3],...
'String',{  'Acqua a 4�C - 1000'; 'Etere di petrolio a 0�C - 670'; 'Benzina - 760'; 'Olio d''oliva - 920'; 'Vino - 960'; 'Mercurio a 0�C - 13.595'; 'Personalizza...' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_pred_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','rho_pred');

appdata = [];
appdata.lastValidTag = 'Run';

h24 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[106.571428571429 3.25 25 1.65],...
'String','Run',...
'UserData',[],...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h25 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'UserData',[],...
'Clipping','on',...
'Position',[97.8571428571428 15.5 41 9.45],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h26 = uipanel(...
'Parent',h25,...
'Units','characters',...
'Title','Flusso in ingresso phii [m^3/s]',...
'Clipping','on',...
'Position',[1.14285714285714 0.399999999999999 38 7.8],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'phii';

h27 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('phii_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.14285714285714 2 35 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('phii_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','phii');

appdata = [];
appdata.lastValidTag = 'phii_min';

h28 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('phii_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.5 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('phii_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','phii_min');

appdata = [];
appdata.lastValidTag = 'phii_max';

h29 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('phii_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[27.1428571428571 0.5 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('phii_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','phii_max');

appdata = [];
appdata.lastValidTag = 'phii_cur';

h30 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('phii_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.1428571428571 0.5 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('phii_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','phii_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h31 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Position',[1.14285714285714 5.2 35 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h32 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.14285714285714 3.55 35 1.3],...
'String','Ampiezza [m^3/s]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h33 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[64.8571428571428 0.55 25 1.45],...
'String','Punto di Equilibrio',...
'UserData',[],...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel15';

h34 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equlibrio',...
'Clipping','on',...
'Position',[56.1428571428571 2.5 41.4285714285714 5.15],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h35 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'HorizontalAlignment','left',...
'Position',[1 0.449999999999999 39 3.45],...
'String',blanks(0),...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel16';

h36 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.14285714285714 9.55 53.8571428571429 18.9],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h37 = axes(...
'Parent',h36,...
'Units','characters',...
'Position',[1 0.66923076923077 51.2 17],...
'Box','on',...
'CameraPosition',[228.5 223.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[15.7271583130934 3.09201814882033 11.4929233826452 2.10819419237749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 456.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 446.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h38 = get(h37,'xlabel');

set(h38,...
'Parent',h37,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[227.86312849162 482.573529411765 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h39 = get(h37,'ylabel');

set(h39,...
'Parent',h37,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-49.8128491620112 225.467647058824 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h40 = get(h37,'zlabel');

set(h40,...
'Parent',h37,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-21.7905027932961 -30.3264705882353 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h41 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'UserData',[],...
'Clipping','on',...
'Position',[97.8571428571428 5.75 41 3.2],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h42 = uicontrol(...
'Parent',h41,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.4 0.576923076923078 37 1.3],...
'String',{  'Flussi'; 'Altezza'; 'Pressione' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel17';

h43 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[56.1428571428571 25.35 82.7142857142857 3.15],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h44 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.499999999999999 8.57142857142857 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h45 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.499999999999999 8 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h46 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[50.1428571428571 0.6 20 1.1],...
'String','nome file',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h47 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[71.4285714285714 0.499999999999999 9 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h48 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2 0.499999999999999 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'uipanel4';

h49 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[97.8571428571428 9.35 41 5.9],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h50 = uipanel(...
'Parent',h49,...
'Units','characters',...
'Title','Altezza liquido h0 [m]',...
'Clipping','on',...
'Position',[1.42857142857143 0.4 38 4.3],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'h0';

h51 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('h0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.42857142857143 2.1 35 1],...
'String',{  'Slider' },...
'Style','slider',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0');

appdata = [];
appdata.lastValidTag = 'h0_min';

h52 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.5 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0_min');

appdata = [];
appdata.lastValidTag = 'h0_max';

h53 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[27.4285714285714 0.5 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0_max');

appdata = [];
appdata.lastValidTag = 'h0_cur';

h54 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.4285714285714 0.5 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0_cur');


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


