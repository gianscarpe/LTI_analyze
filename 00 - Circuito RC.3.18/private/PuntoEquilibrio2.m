%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                P U N T O  D I  E Q U I L I B R I O  2                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function [x, stb] = PuntoEquilibrio2(dx_sym, x_sym)
%
% dx_sym  vector of state equations (n x 1)
% x_sym   vector of state variables (n x 1)
%
% x     solutions vector (n x ?)
% stb   stability values (1 x ?, 1 for each solution)
%
% by F. M. Marchese (2016)
%
% Tested under MatLab R2013b
%


function [x, stb] = PuntoEquilibrio2(dx_sym, x_symb)
  x   = NaN;
  stb = NaN;
  
  if nargin ~= 2 || isempty(dx_sym) || isempty(x_symb)
    fprintf('PuntoEquilibrio2: wrong parameter(s) number!\n');
    fprintf('Use: PuntoEquilibrio2(dx_sym, x_sym)\n');    
    return;
  end
  
  % Preparazione delle istruzioni in funzione dei simboli 
  % presenti nelle equazioni
  [n, ~] = size(x_symb);
  str1 = 'syms'; 
  str2 = 'x_sym = ['; 
  str3 = 'X = solve(dx_sym == 0';
  str5 = 'x1 = double([';
  for i = 1 : n
    str1 = [str1 ' '  x_symb(i, :)];
    str2 = [str2 x_symb(i, :) '; ' ];
    str3 = [str3 ', ' x_symb(i, :)];
    str5 = [str5 'X.' x_symb(i, :) '(i); '];
  end
  str2 = [str2 '];'];
  str3 = [str3 ');'];
  str5 = [str5 ']);'];
  
  % Exec instr: 'syms Theta Omega'
  eval(str1);
  
  % Exec instr: 'x_sym = [Theta; Omega];'
  eval(str2);
  
  % Exec instr: 'X = solve(dx_sym == 0, Theta, Omega);'
  eval(str3);
  
  % Numero soluzioni calcolate da MatLab
  % Exec instr: 'numsol = numel(X.Theta);'
  str4 = ['numsol = numel(X.' x_symb(1, :) ');'];
  eval(str4);
    
  % Calcolo dello jacobiano simbolico (linearizzazione)
  Jac_sym = jacobian(dx_sym, x_sym);
  
  x = []; stb = [];
  for i = 1 : numsol
    % Soluzione numerica
    % Exec instr: 'x1 = double([X.Theta(i); X.Omega(i)]);'
    eval(str5);
    
    % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
    % al punto di equilibrio), per calcolare gli autovalori 
    % e la stabilita'�del sistema intornno al punto di equilibrio
    F = double(subs(Jac_sym, x_sym, x1));  
    
    % Vettore delle soluzioni + stabilit�
    x = [x x1];
    stb = [stb StabilityLTI(F)];
  end
end




