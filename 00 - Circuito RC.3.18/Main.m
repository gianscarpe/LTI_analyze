%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       C I R C U I T O  R C    3.18                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Dalila Virgolino e Marco Zanotti (2015)
% by F. M. Marchese (2008-17)
%
% Tested under MatLab R2013b
%


%% INIT
%
function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property', 'Value', ...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK', hObject, ...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to sim (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 21-Jun-2018 09:47:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      -10      % vi_min
      3        % vi, vi_cur
      10       % vi_max
      0.01     % R_min
      500      % R, R_cur
      1000     % R_max
      1        % C_min
      50       % C, C_cur
      100      % C_max
      -10      % vc0_min
      0        % vc0, vc0_cur
      10       % vc0_max
      ];

% Par degli slider
global visld Rsld Csld vc0sld;

visld.stmin = 0.1;
visld.stmax = 1;
visld.Llim = -Inf;
visld.Hlim = +Inf;

Rsld.stmin = 10;
Rsld.stmax = 100;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Csld.stmin = 0.1;
Csld.stmax = 1;
Csld.Llim = eps;
Csld.Hlim = +Inf;

vc0sld.stmin = 0.1;
vc0sld.stmax = 1;
vc0sld.Llim = -Inf;
vc0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);


% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)  
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.Dialog);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

global Uscita;

% Chiusura dello modello simulink (if any) senza salvare
bdclose(Uscita);

% Lettura della variabile di Uscita del sistema (y)
val1 = get(handles.Uscita, 'Value');
switch val1
  case 1
    Uscita = 'Tensione_Circuito_RC';
  case 2
    Uscita = 'Energia_Circuito_RC';
end;

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
vc0 = get(handles.vc0, 'Value');
vi  = ampiezza(2);
R   = get(handles.R, 'Value');
C   = get(handles.C, 'Value'); C = C * 10^-6; 

% Controllo sui dati nulli per evitare singolarita' (1/0)
if R == 0, R = eps; end
if C == 0, C = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [-1/(R*C)];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esportazione di tutte le variabili nel Workspace per permettere 
% a simulink di averne accesso
% neccessario??
vars = {'vc0', vc0; 'vi', vi; 'R', R; 'C', C};
for i = 1:size(vars, 1)
  name  = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end


% Apertura del sistema da simulare
open_system(Uscita);


% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
    add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
    add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
    add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
    add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
    add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
    add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
    add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
    add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
    set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
    set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject, 'String')) returns Uscita contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% ANALISI INFO DEL SISTEMA
% --- Executes on button press in Analisi.
function Analisi_Callback(hObject, eventdata, handles)
% hObject    handle to Analisi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.vi_min, 'Value',  ampiezza(1));
set(handles.vi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.vi_cur, 'Value',  ampiezza(2));
set(handles.vi_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.vi_max, 'Value',  ampiezza(3));
set(handles.vi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.vi, 'Min',   ampiezza(1));
set(handles.vi, 'Value', ampiezza(2));
set(handles.vi, 'Max',   ampiezza(3));
val1 = get(handles.Uscita,'Value');
switch val1
    case 2
        set(handles.Uscita, 'Value', 1);
end;

% Lettura dei dati inseriti dall'utente
vi = get(handles.vi, 'Value'); u = vi;
R  = get(handles.R,  'Value');
C  = get(handles.C,  'Value'); C = C * 10^-6; 

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if R <= 0, R = eps; end
if C <= 0, C = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [-1/(R*C)];
G = [+1/(R*C)];
H = [+1]; 

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
str_equilibrio = '';
str1 = '';
str21 = '';
str3 = '';
endstr = '.';
if isnan(stb)
  str_equilibrio = ...
  sprintf('\nNon esiste uno stato di equilibrio con l''ingresso: ', ...
  'vi = ', num2str(u(1), '%.2f'), ' V');
else
    % Preparazione testo da visualizzare
    str1 = sprintf('\n lo stato di equilibrio �:');
    str21 = sprintf('\n vc = %.1f V', x(1));
    str_equilibrio = sprintf(strcat('\n', str_equilibrio, str1, str21));
    % Stabilita'
    switch stb
      case -1.2
        str3 = sprintf('\n\n il sistema e'' asintoticamente stabile (osc.)');
      case -1.1
        str3 = sprintf('\n\n il sistema e'' asintoticamente stabile');
      case  0.1
        str3 = sprintf('\n\n il sistema e'' semplicemente stabile');
      case  0.2
        str3 = sprintf('\n\n il sistema e'' semplicemente stabile (osc.)');
      case +1.1
        str3 = sprintf('\n\n il sistema e'' debolmente instabile');
      case +1.2
        str3 = sprintf('\n\n il sistema e'' debolmente instabile (osc.)');
      case +2.1
        str3 = sprintf('\n\n il sistema e'' fortemente instabile');
      case +2.2
        str3 = sprintf('\n\n il sistema e'' fortemente instabile (osc.)');
        otherwise
        str3 = sprintf('\n\n la stabilit� NON e'' determinabile');
    end
end

autovalori = eig(F);
str1 = str_vector(autovalori);

str_autovalori = sprintf( ...
    '\n\n autoval(F) = [ %s ]', str1);

str_stabilita = sprintf(strcat( ...
    sprintf('\n\nIn presenza dell''ingresso vi = %.1f V:', x(1)), ...
    str_equilibrio, str_autovalori, str3));

tab_space = 3;
intr_F = '\n\nF = ';
str_F = strcat(intr_F, str_matrix(F, tab_space));
intr_G = '\n\nG = ';
str_G = strcat(intr_G, str_matrix(G, tab_space));
intr_H = '\n\nH = ';
str_H = strcat(intr_H, str_matrix(H, tab_space));

% Autovalori

sys = ss(F, G, H, 1);
sys_zpk = zpk(sys);
sys_tf = tf(sys);

% % Prova con sistema con poli complessi
% s = tf('s');
% sys_tf = 4 * (s^2 + 2 *s + 4) / ((s^2  +1 * s +2)*(s^2  +3 * s +3));
% sys_zpk = zpk(sys_tf);
% sys = ss(sys_tf);


% Osservabilit� / ricostruibilit�
obsv_matrix = obsv(sys);
Osservabile = (rank(obsv_matrix) == length(G));


str_obsv_property = '';
if (Osservabile)
    str_obsv_property = strcat('\n\nIl sistema � osservabile ', ...
        '(ed essendo lineare, ricostruibile)');
else
    str_obsv_property = strcat('\n\nIl sistema � non osservabile ', ...
         '(ed essendo lineare, non ricostruibile)');
end

intr = sprintf('\n\nMo = ');
tab_space = 4;
str_matrix_obsv = str_matrix(obsv_matrix, tab_space);
str_obsv = strcat(intr, str_matrix_obsv, ...
    str_obsv_property);


% Raggiungibilit� / controllabilit�
ctrl_matrix = ctrb(sys);
Controllabile = (rank(ctrl_matrix) == length(H));
intr = sprintf('\n\nMc =');
tab_space = 4;
str_ctrl = '';
str_matrix_ctrl = str_matrix(ctrl_matrix, tab_space);

if (Controllabile)
    str_ctrl_property = strcat('\n\nIl sistema � controllabile ', ...
        '(ed essendo lineare, raggiungibile)');
else
    str_ctrl_property = strcat('\n\nIl sistema � non controllabile ', ...
         '(ed essendo lineare, non raggiungibile)');
end

str_ctrl = strcat(intr, str_matrix_ctrl, ...
    str_ctrl_property);


% Zeri / poli / gain
zeri = cell2mat(sys_zpk.z);
str_zeri = sprintf('\n\nZeri : [ %s ]\n', ...
    str_vector(zeri));


[fr_poli, smorz_poli, poli] = damp(sys);

str_poli = sprintf('\n\nPoli : [ %s ]\n', ...
    str_vector(poli));

str_analisi_poli = sprintf('\n\nAnalisi dei poli:');
i = 1;

fr_risonanza = fr_poli  .* sqrt(1 - 2 * smorz_poli .^ 2);
omega = abs(imag(poli));
tau = -1 ./ real(poli);
while (i <= length(poli))
    polo = poli(i);
    str_polo_value = str_complex(polo);
    if (isreal(polo))
        str = sprintf('\nPolo reale: %s', str_polo_value);
    else
        fr_r = fr_risonanza(i);

        str1 = sprintf('\nPolo complesso:%s', str_polo_value);
        str2 = sprintf('\n  Pulsazione naturale: %.1f rad/sec', ...
            2 * pi * fr_poli(i));
        str3 = sprintf('\n  Frequenza naturale: %.1f Hz', fr_poli(i));       
        str4 = sprintf('\n  Smorzamento: %.1f', smorz_poli(i));
        if (isreal(fr_r))
            str5 = sprintf('\n  Pulsazione di risonanza: %.2f rad/sec', ...
            2 * pi * fr_risonanza(i));
            str6 = sprintf('\n  Frequenza di risonanza: %.2f Hz', ...
            fr_risonanza(i));
        else
            str5 = sprintf('\n  Privo di risonanza');
            str6 = '';
        end
        str7 = sprintf('\n  Pulsazione omega: %.1f rad/sec', 2 * pi * ...
            omega(i));
        str8 = sprintf('\n  Frequenza omega: %.1f Hz', omega(i));
        if (tau(i) ~= inf)
            str9 = sprintf('\n  Costante di tempo : %.2f sec', tau(i));
        else
            str9 = sprintf('\n  Impossibile calcolare la costante\n  di tempo: Presente una singolarit�');
        end
        str = strcat(str1, str2, str3, str4, str5, str6, str7, str8, str9);
        
        i = i+1; 
        % salta il polo successivo, essendo il correte il primo dei due
        % poli complessi coniugati
    end
    
    str_analisi_poli = strcat(str_analisi_poli, str);
    i = i +1;
end

guadagno = sys_zpk.k;
str_guadagno = sprintf('\n\nGuadagno : %.1f\n', guadagno);


% FdT
intro = sprintf('\nFdT = ');

num = sys_tf.num;
den = sys_tf.den;
num_str = str_polynomial(num, 's');
den_str = str_polynomial(den, 's');
% Si mostra SEMPRE il denominatore (anche quando questo � = 1) per
% chiarezza

if (isempty(num_str))
    num_str = '1';
end
if (isempty(den_str))
    den_str = '1';
end


% Separatore num/den
fraction_length = (max(length(num_str), length(den_str)) +1);
    sep_fraction = repmat('-', [1, fraction_length]);

% Spazi di tab per centrare num / den
tab_num = '';
tab_den = '';
if (length(num_str) < length(den_str)); tab_num =  repmat(' ', 1,floor((length(den_str) - length(num_str)) / 2)); end;

if (length(den_str) < length(num_str)); tab_den =  repmat(' ', 1,floor((length(num_str) - length(den_str)) / 2)); end;

% Spazio per correggere la posizione di num e den
tab_space = 6;
str_space = repmat(' ', tab_space, 1);

% Stringa finale
str_fdt = sprintf('\n\n%s%s%s%s%s\n%s%s%s', str_space, tab_num, num_str, intro, ...
        sep_fraction, str_space, tab_den, den_str);



response = sprintf(strcat( str_F, str_G, str_H, ...
    str_stabilita ,...
    str_ctrl, str_obsv, str_fdt, str_guadagno, str_zeri, str_poli, ...
    str_analisi_poli ));
set(handles.Analisi_txt, 'String', response);



%% SLIDER C
% --- Executes on slider movement.
function C_Callback(hObject, eventdata, handles)
% hObject    handle to C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.C, handles.C_cur);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function C_min_Callback(hObject, eventdata, handles)
% hObject    handle to C_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
global Csld;
Slider_min_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function C_max_Callback(hObject, eventdata, handles)
% hObject    handle to C_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Csld;
Slider_max_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function C_cur_Callback(hObject, eventdata, handles)
% hObject    handle to C_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Csld;
Slider_cur_Callback(handles, handles.C, handles.C_min, handles.C_cur, handles.C_max, Csld.stmin, Csld.stmax, Csld.Llim, Csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function C_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to C_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');

  
% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [V]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.vi_min, 'Value',  values(1));
set(handles.vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.vi_cur, 'Value',  values(2));
set(handles.vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.vi_max, 'Value',  values(3));
set(handles.vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.vi, 'Min',   values(1));
set(handles.vi, 'Value', values(2));
set(handles.vi, 'Max',   values(3));


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.vi_min, 'Value',  values(1));
set(handles.vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.vi_cur, 'Value',  values(2));
set(handles.vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.vi_max, 'Value',  values(3));
set(handles.vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.vi, 'Min',   values(1));
set(handles.vi, 'Max',   values(3));
set(handles.vi, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function vi_Callback(hObject, eventdata, handles)
% hObject    handle to vi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.vi, handles.vi_cur);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function vi_max_Callback(hObject, eventdata, handles)
% hObject    handle to vi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global visld;
Slider_max_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, visld.stmin, visld.stmax, visld.Llim, visld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function vi_min_Callback(hObject, eventdata, handles)
% hObject    handle to vi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vi0sld;
Slider_min_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, vi0sld.stmin, vi0sld.stmax, vi0sld.Llim, vi0sld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function vi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to vi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global visld;
Slider_cur_Callback(handles, handles.vi, handles.vi_min, handles.vi_cur, handles.vi_max, visld.stmin, visld.stmax, visld.Llim, visld.Hlim);

minval = get(handles.vi_min, 'Value');
curval = get(handles.vi, 'Value');
maxval = get(handles.vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function vi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%%  SLIDER STATO INIZIALE
% --- Executes on slider movement.
function vc0_Callback(hObject, eventdata, handles)
% hObject    handle to vc0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.vc0, handles.vc0_cur);


% --- Executes during object creation, after setting all properties.
function vc0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function vc0_min_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_min_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function vc0_max_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_max_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function vc0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to vc0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vc0sld;
Slider_cur_Callback(handles, handles.vc0, handles.vc0_min, handles.vc0_cur, handles.vc0_max, vc0sld.stmin, vc0sld.stmax, vc0sld.Llim, vc0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function vc0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vc0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global visld Rsld Csld vc0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [V]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider vi
set(handles.vi_min, 'Value', def(4));
set(handles.vi_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.vi_cur, 'Value', def(5));
set(handles.vi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.vi_max, 'Value', def(6));
set(handles.vi_max, 'String', num2str(def(6), '%.1f'));

set(handles.vi, 'Min',   def(4)); 
set(handles.vi, 'Value', def(5));
set(handles.vi, 'Max',   def(6)); 
majorstep = visld.stmax / (def(6)-def(4));
minorstep = visld.stmin / (def(6)-def(4));
set(handles.vi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.vi, 'Min') get(handles.vi, 'Value') get(handles.vi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [V]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider C
set(handles.C_min, 'Value',  def(10));
set(handles.C_min, 'String', num2str(def(10), '%.1f'));
set(handles.C_cur, 'Value',  def(11));
set(handles.C_cur, 'String', num2str(def(11), '%.2f'));
set(handles.C_max, 'Value',  def(12));
set(handles.C_max, 'String', num2str(def(12), '%.1f'));

set(handles.C, 'Min',   def(10));
set(handles.C, 'Value', def(11));
set(handles.C, 'Max',   def(12));
majorstep = Csld.stmax / (def(12)-def(10));
minorstep = Csld.stmin / (def(12)-def(10));
set(handles.C, 'SliderStep', [minorstep majorstep]);

% slider vc0
set(handles.vc0_min, 'Value', def(13));
set(handles.vc0_min, 'String', num2str(def(13), '%.1f'));
set(handles.vc0_cur, 'Value', def(14));
set(handles.vc0_cur, 'String', num2str(def(14), '%.2f'));
set(handles.vc0_max, 'Value', def(15));
set(handles.vc0_max, 'String', num2str(def(15), '%.1f'));

set(handles.vc0, 'Min',   def(13)); 
set(handles.vc0, 'Value', def(14));
set(handles.vc0, 'Max',   def(15)); 
majorstep = vc0sld.stmax / (def(15)-def(13));
minorstep = vc0sld.stmin / (def(15)-def(13));
set(handles.vc0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello modello simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');





%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Analisi_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global visld Rsld Csld vc0sld;

% Aggiornamento degli slider
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

set(handles.C_min, 'Value',  C_min);
set(handles.C_min, 'String', num2str(C_min, '%.1f'));
set(handles.C_cur, 'Value',  C_cur);
set(handles.C_cur, 'String', num2str(C_cur, '%.2f'));
set(handles.C_max, 'Value',  C_max);
set(handles.C_max, 'String', num2str(C_max, '%.1f'));

set(handles.C, 'Min',   C_min);
set(handles.C, 'Value', C_cur);
set(handles.C, 'Max',   C_max);
majorstep = Csld.stmax / (C_max-C_min);
minorstep = Csld.stmin / (C_max-C_min);
set(handles.C, 'SliderStep', [minorstep majorstep]);

set(handles.vc0_min, 'Value',  vc0_min);
set(handles.vc0_min, 'String', num2str(vc0_min, '%.1f'));
set(handles.vc0_cur, 'Value',  vc0_cur);
set(handles.vc0_cur, 'String', num2str(vc0_cur, '%.2f'));
set(handles.vc0_max, 'Value',  vc0_max);
set(handles.vc0_max, 'String', num2str(vc0_max, '%.1f'));

set(handles.vc0, 'Min',   vc0_min);
set(handles.vc0, 'Value', vc0_cur);
set(handles.vc0, 'Max',   vc0_max);
majorstep = vc0sld.stmax / (vc0_max-vc0_min);
minorstep = vc0sld.stmin / (vc0_max-vc0_min);
set(handles.vc0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [V]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.vi_min, 'Value',  vi_min);
set(handles.vi_min, 'String', num2str(vi_min, '%.1f'));
set(handles.vi_cur, 'Value',  vi_cur);
set(handles.vi_cur, 'String', num2str(vi_cur, '%.2f'));
set(handles.vi_max, 'Value',  vi_max);
set(handles.vi_max, 'String', num2str(vi_max, '%.1f'));

set(handles.vi, 'Min',   vi_min);
set(handles.vi, 'Value', vi_cur);
set(handles.vi, 'Max',   vi_max);
majorstep = visld.stmax / (vi_max-vi_min);
minorstep = visld.stmin / (vi_max-vi_min);
set(handles.vi, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);



% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello condensatore
C_min = get(handles.C_min, 'Value');
C_cur = get(handles.C_cur, 'Value');
C_max = get(handles.C_max, 'Value');

% pannello stato iniziale
vc0_min = get(handles.vc0_min, 'Value');
vc0_cur = get(handles.vc0_cur, 'Value');
vc0_max = get(handles.vc0_max, 'Value');

% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

vi_min = get(handles.vi_min, 'Value');
vi_cur = get(handles.vi_cur, 'Value');
vi_max = get(handles.vi_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'vi_min = %f;\n', vi_min);
fprintf(fid, 'vi_cur = %f;\n', vi_cur);
fprintf(fid, 'vi_max = %f;\n', vi_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'C_min = %f;\n', C_min);
fprintf(fid, 'C_cur = %f;\n', C_cur);
fprintf(fid, 'C_max = %f;\n', C_max);

fprintf(fid, 'vc0_min = %f;\n', vc0_min);
fprintf(fid, 'vc0_cur = %f;\n', vc0_cur);
fprintf(fid, 'vc0_max = %f;\n', vc0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject, 'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject, 'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end
