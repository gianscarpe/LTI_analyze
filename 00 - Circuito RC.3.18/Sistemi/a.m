% Ingressi
IngressoTipo = 2;
IngressoPar = 1;
vi_min = -10.000000;
vi_cur = 1.000000;
vi_max = 10.000000;

% Uscita
Uscita = 1;

% Parametri
R_min = 0.010000;
R_cur = 500.000000;
R_max = 1000.000000;
C_min = 1.000000;
C_cur = 50.000000;
C_max = 100.000000;
vc0_min = -10.000000;
vc0_cur = 0.000000;
vc0_max = 10.000000;
