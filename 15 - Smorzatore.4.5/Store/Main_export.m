%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             SMORZATORE 4.1                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alberto Margari (2011)
% by Barbara Bertani (2015)
% by Fabio Bernardini(2016)
% by F. M. Marchese (2011-16)
%
% Tested unedr MatLab R2013b
%


%%
function varargout = Main_export(varargin)
% Modello M-file for Modello.fig
%      Modello, by itself, creates a new Modello or raises the existing
%      singleton*.
%
%      H = Modello returns the handle to a new Modello or the handle to
%      the existing singleton*.
%
%      Modello('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Modello.M with the given input arguments.
%
%      Modello('Property','Value',...) creates a new Modello or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFunction gets called.  An
%      unrecognized property name or fnvalid value makes property application
%      stop.  All inputs are passed to Main_export_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
%   IMPORTANTE: necessita m'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Modello

% Last Modified by GUIDE v2.5 01-Dec-2016 16:39:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before Modello is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Modello (see VARARGIN)


global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def =[
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      -10      % F_min
      1        % F, F_cur
      10       % F_max
      0        % B_min
      50       % B, B_cur
      100      % B_max
      0        % m_min 10
      5        % m, m_cur 11
      10       % m_max 12
      -10      % v0_min 13
      0        % v0, v0_cur 14
      10       % v0_max 15
      -10      % x0_min 16
      0        % x0, v0_cur 17
      10       % x0_max 18
      ];

% Par degli slider
global Fsld Bsld msld x0sld v0sld;

Fsld.stmin = 0.1;
Fsld.stmax = 1;
Fsld.Llim = -Inf;
Fsld.Hlim = +Inf;

Bsld.stmin = 1;
Bsld.stmax = 10;
Bsld.Llim = eps;
Bsld.Hlim = +Inf;

msld.stmin = 0.1;
msld.stmax = 1;
msld.Llim = eps;
msld.Hlim = +Inf;

x0sld.stmin = 0.1;
x0sld.stmax = 1;
x0sld.Llim = -Inf;
x0sld.Hlim = +Inf;

v0sld.stmin = 0.1;
v0sld.stmax = 1;
v0sld.Llim = -Inf;
v0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);

% Choose default command line output for Modello
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Modello wait for user response (see UIRESUME)
% uiwait(handles.Dialog);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Modello



% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%%RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to esegui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)global uscita;
clc

global uscita;

% Chiudo il modello simulink senza salvare
bdclose(uscita);

% Leggo la variabile di uscita del sistema (y)

uscita = 'Smorzatore';

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');


% Leggo i dati inseriti dall'utente
x0 = get(handles.x0, 'Value');
v0 = get(handles.v0, 'Value');
B = get(handles.B, 'Value');
m = get(handles.m, 'Value');
F = ampiezza(2);

% Controllo sui dati nulli per evitare singolarita' (1/0)
if B == 0, B = eps; end
if m == 0, m = eps; end

% Controllo sui dati nulli
if frequenza == 0, frequenza = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza == 1000, frequenza = 999.999; end, end
if dutycycle < 0.0001, dutycycle = 0.0001; end
if dutycycle == 100, dutycycle = 100-0.0001; end

% Calcolo costanti di tempo
mat_F = [0, 1;
         0, -B/m];
Tau = TimeConstantLTI(mat_F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'x0', x0; 'v0', v0; 'B', B; 'm', m; 'F', F};
for i = 1 : size(vars,1)
  name = vars(i,1);
  value = vars(i,2);
  assignin('base', name{1}, value{1}); 
end


% Apre il sistema da simulare
open_system(uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(uscita,'Name','input');
if ( size(h)==[1,1])
    delete_line(uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(uscita);
    if numel(find(strcmp(system_blocks, [uscita '/step1']))) > 0
        delete_line(uscita, 'step1/1', 'input/1');
        delete_line(uscita, 'step2/1', 'input/2');
    end
    delete_block([uscita,'/input']);
    if numel(find(strcmp(system_blocks, [uscita '/step1']))) > 0
        delete_block([uscita,'/step1']);
        delete_block([uscita,'/step2']);
    end
end

% Legge qual � il nuovo segnale in input da simulare
val = get(handles.IngressoTipo,'Value');
switch val
    case 1
        add_block('Simulink/Sources/Step', [uscita,'/input'], 'Time', num2str(.5*Tau));
    case 2
        add_block('Simulink/Sources/Step', [uscita,'/step1'], 'Time', num2str(.5*Tau));
        add_block('Simulink/Sources/Step', [uscita,'/step2'], 'Time', num2str(.5*Tau+min(0.25*Tau, 0.01)));
        add_block('Simulink/Math Operations/Sum', [uscita,'/input'], 'Inputs', '+-');
    case 3
        add_block('Simulink/Sources/Pulse Generator', [uscita,'/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
    case 4
        add_block('Simulink/Sources/Sine Wave', [uscita,'/input'], 'Frequency', num2str(2*pi*frequenza(2)));
    case 5
        add_block('Simulink/Sources/Pulse Generator', [uscita,'/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
    case 6
        add_block('Simulink/Sources/Repeating Sequence', [uscita,'/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(uscita, 'StopTime', num2str(6/frequenza(2)));
end


% Modifico lo sfondo e la posizione del blocco inserito
set_param([uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(uscita);

% Avvia la simulazione
sim(uscita);

ViewerAutoscale();


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject, 'String')) returns Uscita contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_eq.
function Punto_eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');

set(handles.IngressoTipo,'Value',1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.F_min, 'Value',  ampiezza(1));
set(handles.F_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.F_cur, 'Value',  ampiezza(2));
set(handles.F_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.F_max, 'Value',  ampiezza(3));
set(handles.F_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.F, 'Min',   ampiezza(1));
set(handles.F, 'Value', ampiezza(2));
set(handles.F, 'Max',   ampiezza(3));

% Leggo i dati inseriti dall'utente
m = get(handles.m, 'Value');
B = get(handles.B, 'Value');
F = get(handles.F, 'Value');
g = 9.81;
u = F;

% Controllo sui dati nulli
if B <= 0, B = eps; end
if m <= 0, m = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)

% Preparazione matrici 
F_mat = [0, 1;
         0, -B/m];
G = [0; +1/m];

[x, stb] = PuntoEquilibrioLTI2(F_mat, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['F = ', num2str(u(1), '%.2f'), ' N']});
  return
end

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: F = %.1f N', u(1));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  x = %.2f m', x(1));
str22 = sprintf('\n  v = %.2f m/s', x(2));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str22, str3, endstr);
set(handles.punto_eq_txt, 'String', str);



%SLIDER B
%%
% --- Executes on slider movement.
function B_Callback(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
Slider_sld_Callback(handles, handles.B, handles.B_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Reset testo punto di equilibrio
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function B_min_Callback(hObject, eventdata, handles)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_min_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Hints: get(hObject,'String') returns contents of B_min as text
%        str2double(get(hObject,'String')) returns contents of B_min as a double


% --- Executes during object creation, after setting all properties.
function B_min_CreateFcn(hObject, eventdata, ~)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function B_cur_Callback(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_cur_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.punto_eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of B_cur as text
%        str2double(get(hObject,'String')) returns contents of B_cur as a double


% --- Executes during object creation, after setting all properties.
function B_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function B_max_Callback(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_max_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);
% Hints: get(hObject,'String') returns contents of B_max as text
%        str2double(get(hObject,'String')) returns contents of B_max as a double


% --- Executes during object creation, after setting all properties.
function B_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%SLIDER m
%%
% --- Executes on slider movement.
function m_Callback(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.m, handles.m_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function m_min_Callback(hObject, eventdata, handles)
% hObject    handle to m_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_min_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);
% Hints: get(hObject,'String') returns contents of m_min as text
%        str2double(get(hObject,'String')) returns contents of m_min as a double


% --- Executes during object creation, after setting all properties.
function m_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function m_cur_Callback(hObject, eventdata, handles)
% hObject    handle to m_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_cur_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);

% Hints: get(hObject,'String') returns contents of m_cur as text
%        str2double(get(hObject,'String')) returns contents of m_cur as a double
set(handles.punto_eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function m_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function m_max_Callback(hObject, eventdata, handles)
% hObject    handle to m_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_max_Callback(handles, handles.m, handles.m_min, handles.m_cur, handles.m_max, msld.stmin, msld.stmax, msld.Llim, msld.Hlim);
% Hints: get(hObject,'String') returns contents of M_max as text
%        str2double(get(hObject,'String')) returns contents of m_max as a double


% --- Executes during object creation, after setting all properties.
function m_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns IngressoTipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoTipo
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [N]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');


switch id_ingresso
    case {1, 2}
        set(handles.IngressoPar, 'Value', 1);
        set(handles.IngressoPar, 'Enable', 'off');
    case 3
        new_String{2} = 'Frequenza�[Hz]';
        if get(handles.IngressoPar, 'Value') > 2
            set(handles.IngressoPar, 'Value', 2);
        end
        set(handles.IngressoPar, 'Enable', 'on');
    case 5
        new_String{2} = 'Frequenza [Hz]';
        new_String{3} = 'Duty Cycle [%]';
        if get(handles.IngressoPar, 'Value') > 3
            set(handles.IngressoPar, 'Value', 3);
        end
        set(handles.IngressoPar, 'Enable', 'on');
    case {4, 6}
        new_String{2} = 'Frequenza [Hz]';
        if get(handles.IngressoPar, 'Value') > 2
            set(handles.IngressoPar, 'Value', 2);
        end
        set(handles.IngressoPar, 'Enable', 'on');
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);


parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);


parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% SLIDER F
% --- Executes on slider movement.
function F_Callback(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.F, handles.F_cur);

val = get(handles.F, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Past instruction:
% evalin('base', ['input_param(' num2str(get(handles.IngressoPar, 'Value')) ') = ' num2str(get(hObject, 'Value')) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function F_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function F_min_Callback(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_min_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

%Hints: get(hObject,'String') returns contents of F_min as text
%        str2double(get(hObject,'String')) returns contents of F_min as a double


% --- Executes during object creation, after setting all properties.
function F_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function F_cur_Callback(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_cur_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);% Hints: get(hObject,'String') returns contents of actF as text
%        str2double(get(hObject,'String')) returns contents of F_cur as a double


% --- Executes during object creation, after setting all properties.
function F_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function F_max_Callback(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld Bsld msld x0sld v0sld;
Slider_max_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);


% Hints: get(hObject,'String') returns contents of F_max as text
%        str2double(get(hObject,'String')) returns contents of F_max as a double


% --- Executes during object creation, after setting all properties.
function F_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






%% SLIDER v0
% --- Executes on slider movement.
function v0_Callback(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.v0, handles.v0_cur);
%
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function v0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%%
function v0_min_Callback(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_min_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_min as text
%        str2double(get(hObject,'String')) returns contents of v0_min as a double


% --- Executes during object creation, after setting all properties.
function v0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function v0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_cur_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_cur as text
%        str2double(get(hObject,'String')) returns contents of v0_cur as a double


% --- Executes during object creation, after setting all properties.
function v0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function v0_max_Callback(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_max_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);
% Hints: get(hObject,'String') returns contents of v0_max as text
%        str2double(get(hObject,'String')) returns contents of v0_max as a double


% --- Executes during object creation, after setting all properties.
function v0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%SLIDER x0
% --- Executes on slider movement.
function x0_Callback(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.x0, handles.x0_cur);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function x0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function x0_min_Callback(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_min_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);

% Hints: get(hObject,'String') returns contents of x0_min as text
%        str2double(get(hObject,'String')) returns contents of x0_min as a double


% --- Executes during object creation, after setting all properties.
function x0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_cur_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);

% Hints: get(hObject,'String') returns contents of x0_cur as text
%        str2double(get(hObject,'String')) returns contents of x0_cur as a double


% --- Executes during object creation, after setting all properties.
function x0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_max_Callback(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_max_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);
% Hints: get(hObject,'String') returns contents of x0_max as text
%        str2double(get(hObject,'String')) returns contents of x0_max as a double


% --- Executes during object creation, after setting all properties.
function x0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Fsld Bsld msld x0sld v0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]';
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3)); 


% slider F
set(handles.F_min, 'Value', def(4));
set(handles.F_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.F_cur, 'Value', def(5));
set(handles.F_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.F_max, 'Value', def(6));
set(handles.F_max, 'String', num2str(def(6), '%.1f'));

set(handles.F, 'Min',   def(4)); 
set(handles.F, 'Value', def(5));
set(handles.F, 'Max',   def(6)); 
majorstep = Fsld.stmax / (def(6)-def(4));
minorstep = Fsld.stmin / (def(6)-def(4));
set(handles.F, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.F, 'Min') get(handles.F, 'Value') get(handles.F, 'Max')]);
evalin('base', ['input_params(''Ampiezza [N]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% slider B
set(handles.B_min, 'Value', def(7));
set(handles.B_min, 'String', num2str(def(7),  '%.1f')); 
set(handles.B_cur, 'Value', def(8));
set(handles.B_cur, 'String', num2str(def(8), '%.2f')); 
set(handles.B_max, 'Value', def(9));
set(handles.B_max, 'String', num2str(def(9), '%.1f'));

set(handles.B, 'Min',   def(7)); 
set(handles.B, 'Value', def(8));
set(handles.B, 'Max',   def(9)); 
majorstep = Bsld.stmax / (def(9)-def(7));
minorstep = Bsld.stmin / (def(9)-def(7));
set(handles.B, 'SliderStep', [minorstep majorstep]);

% slider m
set(handles.m_min, 'Value', def(10));
set(handles.m_min, 'String', num2str(def(10),  '%.1f')); 
set(handles.m_cur, 'Value', def(11));
set(handles.m_cur, 'String', num2str(def(11), '%.2f')); 
set(handles.m_max, 'Value', def(12));
set(handles.m_max, 'String', num2str(def(12), '%.1f'));

set(handles.m, 'Min',   def(10)); 
set(handles.m, 'Value', def(11));
set(handles.m, 'Max',   def(12)); 
majorstep = msld.stmax / (def(12)-def(10));
minorstep = msld.stmin / (def(12)-def(10));
set(handles.m, 'SliderStep', [minorstep majorstep]);

%slider v0
set(handles.v0_min, 'Value', def(13));
set(handles.v0_min, 'String', num2str(def(13), '%.1f'));
set(handles.v0_cur, 'Value', def(14));
set(handles.v0_cur, 'String', num2str(def(14), '%.2f'));
set(handles.v0_max, 'Value', def(15));
set(handles.v0_max, 'String', num2str(def(15), '%.1f'));

set(handles.v0, 'Min',   def(13)); 
set(handles.v0, 'Value', def(14));
set(handles.v0, 'Max',   def(15)); 
majorstep = v0sld.stmax / (def(15)-def(13));
minorstep = v0sld.stmin / (def(15)-def(13));
set(handles.v0, 'SliderStep', [minorstep majorstep]);


%slider x0

set(handles.x0_min, 'Value', def(16));
set(handles.x0_min, 'String', num2str(def(16), '%.1f'));
set(handles.x0_cur, 'Value', def(17));
set(handles.x0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.x0_max, 'Value', def(18));
set(handles.x0_max, 'String', num2str(def(18), '%.1f'));

set(handles.x0, 'Min',   def(16)); 
set(handles.x0, 'Value', def(17));
set(handles.x0, 'Max',   def(18)); 
majorstep = x0sld.stmax / (def(18)-def(16));
minorstep = x0sld.stmin / (def(18)-def(16));
set(handles.x0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % Save della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Fsld Bsld msld x0sld v0sld;
set(handles.B_min, 'Value',  B_min);
set(handles.B_min, 'String', num2str(B_min, '%.1f'));
set(handles.B_cur, 'Value',  B_cur);
set(handles.B_cur, 'String', num2str(B_cur, '%.2f'));
set(handles.B_max, 'Value',  B_max);
set(handles.B_max, 'String', num2str(B_max, '%.1f'));

set(handles.B, 'Min',   B_min);
set(handles.B, 'Value', B_cur);
set(handles.B, 'Max',   B_max);
majorstep = Bsld.stmax / (B_max-B_min);
minorstep = Bsld.stmin / (B_max-B_min);
set(handles.B, 'SliderStep', [minorstep majorstep]);

set(handles.m_min, 'Value',  m_min);
set(handles.m_min, 'String', num2str(m_min, '%.1f'));
set(handles.m_cur, 'Value',  m_cur);
set(handles.m_cur, 'String', num2str(m_cur, '%.2f'));
set(handles.m_max, 'Value',  m_max);
set(handles.m_max, 'String', num2str(m_max, '%.1f'));

set(handles.m, 'Min',   m_min);
set(handles.m, 'Value', m_cur);
set(handles.m, 'Max',   m_max);
majorstep = msld.stmax / (m_max-m_min);
minorstep = msld.stmin / (m_max-m_min);
set(handles.m, 'SliderStep', [minorstep majorstep]);

set(handles.x0_min, 'Value',  x0_min);
set(handles.x0_min, 'String', num2str(x0_min, '%.1f'));
set(handles.x0_cur, 'Value',  x0_cur);
set(handles.x0_cur, 'String', num2str(x0_cur, '%.2f'));
set(handles.x0_max, 'Value',  x0_max);
set(handles.x0_max, 'String', num2str(x0_max, '%.1f'));

set(handles.x0, 'Min',   x0_min);
set(handles.x0, 'Value', x0_cur);
set(handles.x0, 'Max',   x0_max);
majorstep = x0sld.stmax / (x0_max-x0_min);
minorstep = x0sld.stmin / (x0_max-x0_min);
set(handles.x0, 'SliderStep', [minorstep majorstep]);

set(handles.v0_min, 'Value',  v0_min);
set(handles.v0_min, 'String', num2str(v0_min, '%.1f'));
set(handles.v0_cur, 'Value',  v0_cur);
set(handles.v0_cur, 'String', num2str(v0_cur, '%.2f'));
set(handles.v0_max, 'Value',  v0_max);
set(handles.v0_max, 'String', num2str(v0_max, '%.1f'));

set(handles.v0, 'Min',   v0_min);
set(handles.v0, 'Value', v0_cur);
set(handles.v0, 'Max',   v0_max);
majorstep = v0sld.stmax / (v0_max-v0_min);
minorstep = v0sld.stmin / (v0_max-v0_min);
set(handles.v0, 'SliderStep', [minorstep majorstep]);

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.F_min, 'Value',  F_min);
set(handles.F_min, 'String', num2str(F_min, '%.1f'));
set(handles.F_cur, 'Value',  F_cur);
set(handles.F_cur, 'String', num2str(F_cur, '%.2f'));
set(handles.F_max, 'Value',  F_max);
set(handles.F_max, 'String', num2str(F_max, '%.1f'));

set(handles.F, 'Min',   F_min);
set(handles.F, 'Value', F_cur);
set(handles.F, 'Max',   F_max);
majorstep = Fsld.stmax / (F_max-F_min);
minorstep = Fsld.stmin / (F_max-F_min);
set(handles.F, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% --- Executes on button press in Save.
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello coeff. attrito viscoso
B_min = get(handles.B_min, 'Value');
B_cur = get(handles.B_cur, 'Value');
B_max = get(handles.B_max, 'Value');

% pannello massa
m_min = get(handles.m_min, 'Value');
m_cur = get(handles.m_cur, 'Value');
m_max = get(handles.m_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

F_min = get(handles.F_min, 'Value');
F_cur = get(handles.F_cur, 'Value');
F_max = get(handles.F_max, 'Value');

% pannello stato iniziale
v0_min = get(handles.v0_min, 'Value');
v0_cur = get(handles.v0_cur, 'Value');
v0_max = get(handles.v0_max, 'Value');

x0_min = get(handles.x0_min, 'Value');
x0_cur = get(handles.x0_cur, 'Value');
x0_max = get(handles.x0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'F_min = %f;\n', F_min);
fprintf(fid, 'F_cur = %f;\n', F_cur);
fprintf(fid, 'F_max = %f;\n', F_max);

fprintf(fid, 'B_min = %f;\n', B_min);
fprintf(fid, 'B_cur = %f;\n', B_cur);
fprintf(fid, 'B_max = %f;\n', B_max);

fprintf(fid, 'm_min = %f;\n', m_min);
fprintf(fid, 'm_cur = %f;\n', m_cur);
fprintf(fid, 'm_max = %f;\n', m_max);

fprintf(fid, 'v0_min = %f;\n', v0_min);
fprintf(fid, 'v0_cur = %f;\n', v0_cur);
fprintf(fid, 'v0_max = %f;\n', v0_max);

fprintf(fid, 'x0_min = %f;\n', x0_min);
fprintf(fid, 'x0_cur = %f;\n', x0_cur);
fprintf(fid, 'x0_max = %f;\n', x0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)

val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end




%% GESTIONE STABILITA'
%
%%!! TODO: da sostituire con funzione + generale
%
function Smorzatore = Stability(F)

% Autovalori di F per valutare la stabilita' dell'equilibrio
autovals = eig(F);

if all(imag(autovals) == 0) 
  % tutti gli autovalori reali
  if all(real(autovals) <  0), Smorzatore = -1.1; return; end   % stabile
  if all(real(autovals) <= 0), Smorzatore =  0.1; return; end   % sempl. stabile
  Smorzatore = +1.1;                                            % instabile
else
  % alcuni autovalori coppie complesse coniugate
  if all(real(autovals) <  0), Smorzatore = -1.2; return; end   % stabile (oscill.)
  if all(real(autovals) <= 0), Smorzatore =  0.2; return; end   % sempl. stabile (oscill.)
  Smorzatore = +1.2;                                            % instabile (oscill.)
end
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 245.01123046875, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 10, ...
    'uipanel', 22, ...
    'slider', 9, ...
    'edit', 27, ...
    'popupmenu', 6, ...
    'pushbutton', 6, ...
    'text', 3), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\15 - Smorzatore.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\15 - Smorzatore.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','SMORZATORE',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 32.95 130.285714285714 28.5],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[56.7142857142857 13.05 37.1428571428571 11.7],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h3 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Coeff. attrito viscoso B [Kg/s]',...
'Clipping','on',...
'Position',[1.85714285714286 5.45 33 5],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'B';

h4 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('B_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Position',[1.42857142857143 2.4 30 1.1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B');

appdata = [];
appdata.lastValidTag = 'B_min';

h5 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.75 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_min');

appdata = [];
appdata.lastValidTag = 'B_cur';

h6 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[11.8571428571429 0.75 9 1],...
'String','50.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_cur');

appdata = [];
appdata.lastValidTag = 'B_max';

h7 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[22.4285714285714 0.75 9 1],...
'String','100.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_max');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h8 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Massa m [Kg]',...
'Clipping','on',...
'Position',[1.85714285714286 0.5 33 4.7],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'm';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('m_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.1 30 1.1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('m_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','m');

appdata = [];
appdata.lastValidTag = 'm_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('m_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('m_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','m_min');

appdata = [];
appdata.lastValidTag = 'm_cur';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('m_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[11.7142857142857 0.55 9 1],...
'String','5.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('m_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','m_cur');

appdata = [];
appdata.lastValidTag = 'm_max';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('m_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[22.2857142857143 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('m_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','m_max');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h13 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[0.857142857142857 1.65 32.1428571428571 6.85],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h14 = axes(...
'Parent',h13,...
'Units','characters',...
'Position',[1 0.530769230769228 29.4285714285714 5.1],...
'Box','on',...
'CameraPosition',[95.5 43.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'FontSize',8,...
'Layer','top',...
'LooseInset',[5.88244406196213 0.698262853231051 4.2987091222031 0.476088309021171],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 190.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 86.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h15 = get(h14,'xlabel');

set(h15,...
'Parent',h14,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[95.0388349514563 108 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h16 = get(h14,'ylabel');

set(h16,...
'Parent',h14,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-23.9417475728155 44.764705882353 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h17 = get(h14,'zlabel');

set(h17,...
'Parent',h14,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-13.7961165048544 -354.039215686274 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel13';

h18 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[94.7142857142857 14.3 34.1428571428571 10.45],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel7';

h19 = uipanel(...
'Parent',h18,...
'Units','characters',...
'Title','Forza applicata F [N]',...
'Clipping','on',...
'Position',[0.857142857142857 0.649999999999996 30.8571428571428 8.4],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'F';

h20 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('F_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.28571428571429 2.39615384615385 28 1.1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('F_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','F');

appdata = [];
appdata.lastValidTag = 'F_min';

h21 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('F_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.646153846153846 8 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('F_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','F_min');

appdata = [];
appdata.lastValidTag = 'F_cur';

h22 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('F_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[11.2857142857143 0.646153846153846 8 1],...
'String','5.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('F_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','F_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h23 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.28571428571429 5.84615384615385 28 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'F_max';

h24 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('F_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[21.2857142857143 0.646153846153846 8 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('F_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','F_max');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h25 = uicontrol(...
'Parent',h19,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.28571428571429 4.14615384615385 28 1.3],...
'String','Ampiezza [N]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'Run';

h26 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[99.7142857142857 0.55 23.1428571428571 1.9],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_eq';

h27 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[64.2857142857143 0.45 22.5714285714286 1.6],...
'String','Punto di equilibrio',...
'Tag','Punto_eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel17';

h28 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'UserData',[],...
'Clipping','on',...
'Position',[0.857142857142857 8.8 55.2857142857143 19.65],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h29 = axes(...
'Parent',h28,...
'Units','characters',...
'Position',[1.14285714285714 0.623076923076909 52.2857142857143 17.8],...
'Box','on',...
'CameraPosition',[211.5 192 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'FontSize',8,...
'Layer','top',...
'LooseInset',[7.852 2.22538461538462 5.738 1.51730769230769],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 422.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 383.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h30 = get(h29,'xlabel');

set(h30,...
'Parent',h29,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[210.92349726776 410.933988764045 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h31 = get(h29,'ylabel');

set(h31,...
'Parent',h29,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-38.1256830601093 193.613764044944 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h32 = get(h29,'zlabel');

set(h32,...
'Parent',h29,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-18.5245901639344 -23.7064606741573 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel19';

h33 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[56.7142857142857 24.9 71.8571428571428 3.55],...
'Tag','uipanel19',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h34 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[23.1428571428571 0.715384615384614 8.57142857142857 1.4],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h35 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[61 0.715384615384614 9 1.4],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h36 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.85714285714286 0.765384615384614 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h37 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[40.7142857142857 0.865384615384614 20 1.1],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h38 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[32 0.715384615384614 8 1.4],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel14';

h39 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[94.7142857142857 2.6 33.7142857142857 11.35],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h40 = uipanel(...
'Parent',h39,...
'Units','characters',...
'Title','Velocit� iniziale v0 [m/s]',...
'Clipping','on',...
'Position',[1.14285714285714 0.400000000000001 30.8571428571428 4.65],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'v0';

h41 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('v0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.12307692307692 28 1.1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0');

appdata = [];
appdata.lastValidTag = 'v0_min';

h42 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.52307692307692 8 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_min');

appdata = [];
appdata.lastValidTag = 'v0_cur';

h43 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[11.1428571428571 0.52307692307692 8 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_cur');

appdata = [];
appdata.lastValidTag = 'v0_max';

h44 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[21.1428571428571 0.52307692307692 8 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_max');

appdata = [];
appdata.lastValidTag = 'uipanel15';

h45 = uipanel(...
'Parent',h39,...
'Units','characters',...
'Title','Spostamento iniziale x0 [m]',...
'Clipping','on',...
'Position',[1.14285714285714 5.4 31 4.8],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'x0';

h46 = uicontrol(...
'Parent',h45,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('x0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1 2.2 28 1.1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0');

appdata = [];
appdata.lastValidTag = 'x0_min';

h47 = uicontrol(...
'Parent',h45,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1 0.549999999999998 8 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_min');

appdata = [];
appdata.lastValidTag = 'x0_cur';

h48 = uicontrol(...
'Parent',h45,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[11 0.549999999999998 8 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_cur');

appdata = [];
appdata.lastValidTag = 'x0_max';

h49 = uicontrol(...
'Parent',h45,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[21 0.549999999999998 8 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_max');

appdata = [];
appdata.lastValidTag = 'uipanel8';

h50 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'UserData',[],...
'Clipping','on',...
'Position',[56.7142857142857 8.65 37.1428571428571 3.55],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h51 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Enable','off',...
'Position',[2 0.9 33 1.3],...
'String','Spostamento finale',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel16';

h52 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di equilibrio',...
'UserData',[],...
'Clipping','on',...
'Position',[56.7142857142857 2.45 37.1428571428571 6.05],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'punto_eq_txt';

h53 = uicontrol(...
'Parent',h52,...
'Units','characters',...
'CData',[],...
'HorizontalAlignment','left',...
'Position',[1.57142857142857 0.5 33.7142857142857 4.25],...
'Style','text',...
'UserData',[],...
'Tag','punto_eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


