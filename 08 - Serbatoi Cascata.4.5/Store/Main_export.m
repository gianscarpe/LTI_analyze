%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               S E R B A T O I   I N   C A S C A T A    4.1              %
%                             (liquidi)                                   % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alessandro Degli Agosti (2008)
% by Dany Thach (2014)
% by Alessandro Pagani (2016)
% by F. M. Marchese (2008-17)
%
% Tested under MatLab R2013b
%


%% INIT
function varargout = Main_export(varargin)
% Main_export M-file for Main_export.fig
%      Main_export, by itself, creates a new Main_export or raises the existing
%      singleton*.
%
%      H = Main_export returns the handle to a new Main_export or the handle to
%      the existing singleton*.
%
%      Main_export('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Main_export.M with the given input arguments.
%
%      Main_export('Property','Value',...) creates a new Main_export or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_export_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
%   IMPORTANTE: necessita l'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 04-Oct-2017 14:52:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main_export (see VARARGIN)
% Pulisce la Command Windows

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo
      1        % IngressoPar
      1        % Uscita
      0.0      % P_min
      1        % P, P_cur
      10       % P_max
      1        % A_min
      1        % A, A_cur
      10       % A_max
      0.0      % Rho_min
      1        % Rho, Rho_cur
      10       % Rho_max
      0.01     % R1_min
      4.5      % R1, R1_cur
      10       % R1_max
      0.01     % R2_min
      5        % R2, R2_cur
      10       % R2_max
      0.0      % h1_min
      0.0      % h1, h1_cur
      10       % h1_max
      0.0      % h2_min
      0.0      % h2, h2_cur
      10       % h2_max
      ];

% Par degli slider
global Psld Asld Rhosld R1sld R2sld h1sld h2sld;

Psld.stmin = 0.1;
Psld.stmax = 1;
Psld.Llim = -Inf;
Psld.Hlim = +Inf;

Asld.stmin = 0.1;
Asld.stmax = 1;
Asld.Llim = eps;
Asld.Hlim = +Inf;

Rhosld.stmin = 0.1;
Rhosld.stmax = 1;
Rhosld.Llim = eps;
Rhosld.Hlim = +Inf;

R1sld.stmin = 0.1;
R1sld.stmax = 1;
R1sld.Llim = eps;
R1sld.Hlim = +Inf;

R2sld.stmin = 0.1;
R2sld.stmax = 1;
R2sld.Llim = eps;
R2sld.Hlim = +Inf;

h1sld.stmin = 0.1;
h1sld.stmax = 1;
h1sld.Llim = eps;
h1sld.Hlim = +Inf;

h2sld.stmin = 0.1;
h2sld.stmax = 1;
h2sld.Llim = eps;
h2sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);

% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Schema


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Modello


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)global uscita;
clc

global Uscita;

% Chiusura del modello simulink senza salvare
bdclose(Uscita);

% Lettura della variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = 'SerbatoioCascata2';
    case 2
        Uscita = 'SerbatoioCascata2';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [m^3/s]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
A   = get(handles.A, 'Value');
delta = get(handles.Rho, 'Value');
R1  = get(handles.R1, 'Value');
qi   = ampiezza(2);
R2  = get(handles.R2, 'Value');
h10 = get(handles.h1, 'Value');
h20 = get(handles.h2, 'Value');
g = 9.81;

% Controllo sui dati nulli
if A == 0, A = eps; end
if R1 == 0, R1 = eps; end
if R2 == 0, R2 = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo nel punto di eq.
F = [-delta*g/(A*R1) 0; delta*g/(A*R1) -delta*g/(A*R2)];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'A', A; 'delta', delta; 'R1', R1; 'qi', qi; 'R2', R2; 'h10', h10; 'h20', h20};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il modello in Simulink
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if ( size(h)==[1,1])
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita, '/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita, '/step1']);
        delete_block([Uscita, '/step2']);
    end
end

% Lettura del nuovo segnale in iNput da simulare
val = get(handles.IngressoTipo,'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,55,105,84]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,30,85,60]');
    set_param([Uscita,'/step2'], 'Position', '[20,80,85,110]');
    set_param([Uscita,'/input'], 'Position', '[95,60,115,80]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

ampiezza = evalin('base', 'input_params(''Ampiezza [m^3/s]'')');

% Settaggio della tensione in ingresso a 'Step'
set(handles.IngressoTipo,'Value',1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.P_min, 'Value',  ampiezza(1));
set(handles.P_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.P_cur, 'Value',  ampiezza(2));
set(handles.P_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.P_max, 'Value',  ampiezza(3));
set(handles.P_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.P, 'Min',   ampiezza(1));
set(handles.P, 'Value', ampiezza(2));
set(handles.P, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
A     = get(handles.A, 'Value');
delta = get(handles.Rho, 'Value');
R1    = get(handles.R1, 'Value');
P     = get(handles.P, 'Value');
R2    = get(handles.R2, 'Value');
g     = 9.81;
u = P;

% Controllo sui dati nulli
if A == 0, A = eps; end
if R1 == 0, R1 = eps; end
if R2 == 0, R2 = eps; end


% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [-delta*g/(A*R1) 0; delta*g/(A*R1) -delta*g/(A*R2)];
G = [1/A; 0];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['P = ', num2str(u(1), '%.2f'), ' m^3/s']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: qi = %.2f m^3/s', u(1));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  h1 = %.2f m', x(1));
str22 = sprintf('\n  h2 = %.2f m', x(2));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr= '.';
str = strcat(str, str1, str21, str22, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);


%% SLIDER A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.A, handles.A_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER RHO
% --- Executes on slider movement.
function Rho_Callback(hObject, eventdata, handles)
% hObject    handle to Rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Rho, handles.Rho_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Rho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function Rho_min_Callback(hObject, eventdata, handles)
% hObject    handle to Rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rhosld;
Slider_min_Callback(handles, handles.Rho, handles.Rho_min, handles.Rho_cur, handles.Rho_max, Rhosld.stmin, Rhosld.stmax, Rhosld.Llim, Rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Rho_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Rho_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rhosld;
Slider_cur_Callback(handles, handles.Rho, handles.Rho_min, handles.Rho_cur, handles.Rho_max, Rhosld.stmin, Rhosld.stmax, Rhosld.Llim, Rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Rho_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Rho_max_Callback(hObject, eventdata, handles)
% hObject    handle to Rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Rhosld;
Slider_max_Callback(handles, handles.Rho, handles.Rho_min, handles.Rho_cur, handles.Rho_max, Rhosld.stmin, Rhosld.stmax, Rhosld.Llim, Rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Rho_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER R1
% --- Executes on slider movement.
function R1_Callback(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R1, handles.R1_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R1_min_Callback(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_min_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R1_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_cur_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');



% --- Executes during object creation, after setting all properties.
function R1_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_max_Callback(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_max_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');



% --- Executes during object creation, after setting all properties.
function R1_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER R2
% --- Executes on slider movement.
function R2_Callback(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R2, handles.R2_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R2_min_Callback(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_min_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R2_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_cur_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');



% --- Executes during object creation, after setting all properties.
function R2_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R2_max_Callback(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_max_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [m^3/s]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.P_min, 'Value',  values(1));
set(handles.P_min, 'String', num2str(values(1), '%.1f'));
set(handles.P_cur, 'Value',  values(2));
set(handles.P_cur, 'String', num2str(values(2), '%.2f'));
set(handles.P_max, 'Value',  values(3));
set(handles.P_max, 'String', num2str(values(3), '%.1f'));
set(handles.P, 'Min',   values(1));
set(handles.P, 'Value', values(2));
set(handles.P, 'Max',   values(3));


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.P_min, 'Value',  values(1));
set(handles.P_min, 'String', num2str(values(1), '%.1f'));
set(handles.P_cur, 'Value',  values(2));
set(handles.P_cur, 'String', num2str(values(2), '%.2f'));
set(handles.P_max, 'Value',  values(3));
set(handles.P_max, 'String', num2str(values(3), '%.1f'));
set(handles.P, 'Min',   values(1));
set(handles.P, 'Max',   values(3));
set(handles.P, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function P_Callback(hObject, eventdata, handles)
% hObject    handle to P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.P, handles.P_cur);

minval = get(handles.P_min, 'Value');
curval = get(handles.P, 'Value');
maxval = get(handles.P_max, 'Value');

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function P_CreateFcn(hObject, eventdata, handles)
% hObject    handle to P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function P_min_Callback(hObject, eventdata, handles)
% hObject    handle to P_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Psld;
Slider_min_Callback(handles, handles.P, handles.P_min, handles.P_cur, handles.P_max, Psld.stmin, Psld.stmax, Psld.Llim, Psld.Hlim);

minval = get(handles.P_min, 'Value');
curval = get(handles.P, 'Value');
maxval = get(handles.P_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function P_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to P_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function P_cur_Callback(hObject, eventdata, handles)
% hObject    handle to P_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Psld;
Slider_cur_Callback(handles, handles.P, handles.P_min, handles.P_cur, handles.P_max, Psld.stmin, Psld.stmax, Psld.Llim, Psld.Hlim);

minval = get(handles.P_min, 'Value');
curval = get(handles.P, 'Value');
maxval = get(handles.P_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function P_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to P_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function P_max_Callback(hObject, eventdata, handles)
% hObject    handle to P_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Psld;
Slider_max_Callback(handles, handles.P, handles.P_min, handles.P_cur, handles.P_max, Psld.stmin, Psld.stmax, Psld.Llim, Psld.Hlim);

minval = get(handles.P_min, 'Value');
curval = get(handles.P, 'Value');
maxval = get(handles.P_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function P_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to P_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function h1_Callback(hObject, eventdata, handles)
% hObject    handle to h1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h1, handles.h1_cur);


% --- Executes during object creation, after setting all properties.
function h1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function h1_min_Callback(hObject, eventdata, handles)
% hObject    handle to h1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h1sld;
Slider_min_Callback(handles, handles.h1, handles.h1_min, handles.h1_cur, handles.h1_max, h1sld.stmin, h1sld.stmax, h1sld.Llim, h1sld.Hlim);

% --- Executes during object creation, after setting all properties.
function h1_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h1_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h1sld;
Slider_cur_Callback(handles, handles.h1, handles.h1_min, handles.h1_cur, handles.h1_max, h1sld.stmin, h1sld.stmax, h1sld.Llim, h1sld.Hlim);

% --- Executes during object creation, after setting all properties.
function h1_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h1_max_Callback(hObject, eventdata, handles)
% hObject    handle to h1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h1sld;
Slider_max_Callback(handles, handles.h1, handles.h1_min, handles.h1_cur, handles.h1_max, h1sld.stmin, h1sld.stmax, h1sld.Llim, h1sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h1_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function h2_Callback(hObject, eventdata, handles)
% hObject    handle to h2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h2, handles.h2_cur);


% --- Executes during object creation, after setting all properties.
function h2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function h2_min_Callback(hObject, eventdata, handles)
% hObject    handle to h2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h2sld;
Slider_min_Callback(handles, handles.h2, handles.h2_min, handles.h2_cur, handles.h2_max, h2sld.stmin, h2sld.stmax, h2sld.Llim, h2sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h2_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h2_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h2sld;
Slider_cur_Callback(handles, handles.h2, handles.h2_min, handles.h2_cur, handles.h2_max, h2sld.stmin, h2sld.stmax, h2sld.Llim, h2sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h2_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h2_max_Callback(hObject, eventdata, handles)
% hObject    handle to h2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h2sld;
Slider_max_Callback(handles, handles.h2, handles.h2_min, handles.h2_cur, handles.h2_max, h2sld.stmin, h2sld.stmax, h2sld.Llim, h2sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h2_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% --- Executes when user attempts to close Dialog.
function Load_Defaults(handles)

global def;
global Psld Asld Rhosld R1sld R2sld h1sld h2sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [m^3/s]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider P
set(handles.P_min, 'Value', def(4));
set(handles.P_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.P_cur, 'Value', def(5));
set(handles.P_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.P_max, 'Value', def(6));
set(handles.P_max, 'String', num2str(def(6), '%.1f'));

set(handles.P, 'Min',   def(4)); 
set(handles.P, 'Value', def(5));
set(handles.P, 'Max',   def(6)); 
majorstep = Psld.stmax / (def(6)-def(4));
minorstep = Psld.stmin / (def(6)-def(4));
set(handles.P, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.P, 'Min') get(handles.P, 'Value') get(handles.P, 'Max')]);
evalin('base', ['input_params(''Ampiezza [m^3/s]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider A
set(handles.A_min, 'Value',  def(7));
set(handles.A_min, 'String', num2str(def(7), '%.1f'));
set(handles.A_cur, 'Value',  def(8));
set(handles.A_cur, 'String', num2str(def(8), '%.2f'));
set(handles.A_max, 'Value',  def(9));
set(handles.A_max, 'String', num2str(def(9), '%.1f'));

set(handles.A, 'Min',   def(7));
set(handles.A, 'Value', def(8));
set(handles.A, 'Max',   def(9));
majorstep = Asld.stmax / (def(9)-def(7));
minorstep = Asld.stmin / (def(9)-def(7));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider Rho
set(handles.Rho_min, 'Value',  def(10));
set(handles.Rho_min, 'String', num2str(def(10), '%.1f'));
set(handles.Rho_cur, 'Value',  def(11));
set(handles.Rho_cur, 'String', num2str(def(11), '%.2f'));
set(handles.Rho_max, 'Value',  def(12));
set(handles.Rho_max, 'String', num2str(def(12), '%.1f'));

set(handles.Rho, 'Min',   def(10));
set(handles.Rho, 'Value', def(11));
set(handles.Rho, 'Max',   def(12));
majorstep = Rhosld.stmax / (def(12)-def(10));
minorstep = Rhosld.stmin / (def(12)-def(10));
set(handles.Rho, 'SliderStep', [minorstep majorstep]);

% slider R1
set(handles.R1_min, 'Value',  def(13));
set(handles.R1_min, 'String', num2str(def(13), '%.1f'));
set(handles.R1_cur, 'Value',  def(14));
set(handles.R1_cur, 'String', num2str(def(14), '%.2f'));
set(handles.R1_max, 'Value',  def(15));
set(handles.R1_max, 'String', num2str(def(15), '%.1f'));

set(handles.R1, 'Min',   def(13));
set(handles.R1, 'Value', def(14));
set(handles.R1, 'Max',   def(15));
majorstep = R1sld.stmax / (def(15)-def(13));
minorstep = R1sld.stmin / (def(15)-def(13));
set(handles.R1, 'SliderStep', [minorstep majorstep]);

% slider R2
set(handles.R2_min, 'Value', def(16));
set(handles.R2_min, 'String', num2str(def(16), '%.1f'));
set(handles.R2_cur, 'Value', def(17));
set(handles.R2_cur, 'String', num2str(def(17), '%.2f'));
set(handles.R2_max, 'Value', def(18));
set(handles.R2_max, 'String', num2str(def(18), '%.1f'));

set(handles.R2, 'Min',   def(16)); 
set(handles.R2, 'Value', def(17));
set(handles.R2, 'Max',   def(18)); 
majorstep = R2sld.stmax / (def(18)-def(16));
minorstep = R2sld.stmin / (def(18)-def(16));
set(handles.R2, 'SliderStep', [minorstep majorstep]);

% slider h1
set(handles.h1_min, 'Value', def(19));
set(handles.h1_min, 'String', num2str(def(19), '%.1f'));
set(handles.h1_cur, 'Value', def(20));
set(handles.h1_cur, 'String', num2str(def(20), '%.2f'));
set(handles.h1_max, 'Value', def(21));
set(handles.h1_max, 'String', num2str(def(21), '%.1f'));

set(handles.h1, 'Min',   def(19)); 
set(handles.h1, 'Value', def(20));
set(handles.h1, 'Max',   def(21)); 
majorstep = h1sld.stmax / (def(21)-def(19));
minorstep = h1sld.stmin / (def(21)-def(19));
set(handles.h1, 'SliderStep', [minorstep majorstep]);

% slider h2
set(handles.h2_min, 'Value', def(22));
set(handles.h2_min, 'String', num2str(def(22), '%.1f'));
set(handles.h2_cur, 'Value', def(23));
set(handles.h2_cur, 'String', num2str(def(23), '%.2f'));
set(handles.h2_max, 'Value', def(24));
set(handles.h2_max, 'String', num2str(def(24), '%.1f'));

set(handles.h2, 'Min',   def(22)); 
set(handles.h2, 'Value', def(23));
set(handles.h2, 'Max',   def(24)); 
majorstep = h2sld.stmax / (def(24)-def(22));
minorstep = h2sld.stmin / (def(24)-def(22));
set(handles.h2, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiudo il modello simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Psld Asld Rhosld R1sld R2sld h1sld h2sld;

% Aggiornamento degli slider
set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

set(handles.Rho_min, 'Value',  Rho_min);
set(handles.Rho_min, 'String', num2str(Rho_min, '%.1f'));
set(handles.Rho_cur, 'Value',  Rho_cur);
set(handles.Rho_cur, 'String', num2str(Rho_cur, '%.2f'));
set(handles.Rho_max, 'Value',  Rho_max);
set(handles.Rho_max, 'String', num2str(Rho_max, '%.1f'));

set(handles.Rho, 'Min',   Rho_min);
set(handles.Rho, 'Value', Rho_cur);
set(handles.Rho, 'Max',   Rho_max);
majorstep = Rhosld.stmax / (Rho_max-Rho_min);
minorstep = Rhosld.stmin / (Rho_max-Rho_min);
set(handles.Rho, 'SliderStep', [minorstep majorstep]);

set(handles.R1_min, 'Value',  R1_min);
set(handles.R1_min, 'String', num2str(R1_min, '%.1f'));
set(handles.R1_cur, 'Value',  R1_cur);
set(handles.R1_cur, 'String', num2str(R1_cur, '%.2f'));
set(handles.R1_max, 'Value',  R1_max);
set(handles.R1_max, 'String', num2str(R1_max, '%.1f'));

set(handles.R1, 'Min',   R1_min);
set(handles.R1, 'Value', R1_cur);
set(handles.R1, 'Max',   R1_max);
majorstep = R1sld.stmax / (R1_max-R1_min);
minorstep = R1sld.stmin / (R1_max-R1_min);
set(handles.R1, 'SliderStep', [minorstep majorstep]);

set(handles.R2_min, 'Value',  R2_min);
set(handles.R2_min, 'String', num2str(R2_min, '%.1f'));
set(handles.R2_cur, 'Value',  R2_cur);
set(handles.R2_cur, 'String', num2str(R2_cur, '%.2f'));
set(handles.R2_max, 'Value',  R2_max);
set(handles.R2_max, 'String', num2str(R2_max, '%.1f'));

set(handles.R2, 'Min',   R2_min);
set(handles.R2, 'Value', R2_cur);
set(handles.R2, 'Max',   R2_max);
majorstep = R2sld.stmax / (R2_max-R2_min);
minorstep = R2sld.stmin / (R2_max-R2_min);
set(handles.R2, 'SliderStep', [minorstep majorstep]);

set(handles.h1_min, 'Value',  h1_min);
set(handles.h1_min, 'String', num2str(h1_min, '%.1f'));
set(handles.h1_cur, 'Value',  h1_cur);
set(handles.h1_cur, 'String', num2str(h1_cur, '%.2f'));
set(handles.h1_max, 'Value',  h1_max);
set(handles.h1_max, 'String', num2str(h1_max, '%.1f'));

set(handles.h1, 'Min',   h1_min);
set(handles.h1, 'Value', h1_cur);
set(handles.h1, 'Max',   h1_max);
majorstep = h1sld.stmax / (h1_max-h1_min);
minorstep = h1sld.stmin / (h1_max-h1_min);
set(handles.h1, 'SliderStep', [minorstep majorstep]);

set(handles.h2_min, 'Value',  h2_min);
set(handles.h2_min, 'String', num2str(h2_min, '%.1f'));
set(handles.h2_cur, 'Value',  h2_cur);
set(handles.h2_cur, 'String', num2str(h2_cur, '%.2f'));
set(handles.h2_max, 'Value',  h2_max);
set(handles.h2_max, 'String', num2str(h2_max, '%.1f'));

set(handles.h2, 'Min',   h2_min);
set(handles.h2, 'Value', h2_cur);
set(handles.h2, 'Max',   h2_max);
majorstep = h2sld.stmax / (h2_max-h2_min);
minorstep = h2sld.stmin / (h2_max-h2_min);
set(handles.h2, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [m^3/s]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.P_min, 'Value',  P_min);
set(handles.P_min, 'String', num2str(P_min, '%.1f'));
set(handles.P_cur, 'Value',  P_cur);
set(handles.P_cur, 'String', num2str(P_cur, '%.2f'));
set(handles.P_max, 'Value',  P_max);
set(handles.P_max, 'String', num2str(P_max, '%.1f'));

set(handles.P, 'Min',   P_min);
set(handles.P, 'Value', P_cur);
set(handles.P, 'Max',   P_max);
majorstep = Psld.stmax / (P_max-P_min);
minorstep = Psld.stmin / (P_max-P_min);
set(handles.P, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello sezione serbatoio
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello densit� liquido
Rho_min = get(handles.Rho_min, 'Value');
Rho_cur = get(handles.Rho_cur, 'Value');
Rho_max = get(handles.Rho_max, 'Value');

% pannello resistenza valvola 1
R1_min = get(handles.R1_min, 'Value');
R1_cur = get(handles.R1_cur, 'Value');
R1_max = get(handles.R1_max, 'Value');

% pannello resistenza valvola 2
R2_min = get(handles.R2_min, 'Value');
R2_cur = get(handles.R2_cur, 'Value');
R2_max = get(handles.R2_max, 'Value');

% pannello stato iniziale
h1_min = get(handles.h1_min, 'Value');
h1_cur = get(handles.h1_cur, 'Value');
h1_max = get(handles.h1_max, 'Value');

h2_min = get(handles.h2_min, 'Value');
h2_cur = get(handles.h2_cur, 'Value');
h2_max = get(handles.h2_max, 'Value');

% pannello Uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

P_min = get(handles.P_min, 'Value');
P_cur = get(handles.P_cur, 'Value');
P_max = get(handles.P_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'P_min = %f;\n', P_min);
fprintf(fid, 'P_cur = %f;\n', P_cur);
fprintf(fid, 'P_max = %f;\n', P_max);

% Salvataggio Uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'Rho_min = %f;\n', Rho_min);
fprintf(fid, 'Rho_cur = %f;\n', Rho_cur);
fprintf(fid, 'Rho_max = %f;\n', Rho_max);

fprintf(fid, 'R1_min = %f;\n', R1_min);
fprintf(fid, 'R1_cur = %f;\n', R1_cur);
fprintf(fid, 'R1_max = %f;\n', R1_max);

fprintf(fid, 'R2_min = %f;\n', R2_min);
fprintf(fid, 'R2_cur = %f;\n', R2_cur);
fprintf(fid, 'R2_max = %f;\n', R2_max);

fprintf(fid, 'h1_min = %f;\n', h1_min);
fprintf(fid, 'h1_cur = %f;\n', h1_cur);
fprintf(fid, 'h1_max = %f;\n', h1_max);

fprintf(fid, 'h2_min = %f;\n', h2_min);
fprintf(fid, 'h2_cur = %f;\n', h2_cur);
fprintf(fid, 'h2_max = %f;\n', h2_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 255.001220703125, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 4, ...
    'uipanel', 18, ...
    'slider', 9, ...
    'edit', 26, ...
    'popupmenu', 5, ...
    'pushbutton', 6, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\08 - Serbatoi Cascata.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\08 - Serbatoi Cascata.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','SERBATOI IN CASCATA (liquidi)',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[0 -12.2 161.857142857143 32.9],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'UserData',[],...
'Clipping','on',...
'Position',[77 7.15 40.5 21.75],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h3 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Sezione del serbatoio A [m^2]',...
'Clipping','on',...
'Position',[1.25 15.75 37 4.75],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'A';

h4 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('A_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',1,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0111111111111111 0.111111111111111],...
'Value',2,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A');

appdata = [];
appdata.lastValidTag = 'A_min';

h5 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.8 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_min');

appdata = [];
appdata.lastValidTag = 'A_cur';

h6 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.8 9 1],...
'String','2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_cur');

appdata = [];
appdata.lastValidTag = 'A_max';

h7 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.8 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_max');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h8 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Densit� del liquido delta [kg/dm^3]',...
'Clipping','on',...
'Position',[1.25 10.75 37 4.75],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Rho';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Rho_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Rho_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Rho');

appdata = [];
appdata.lastValidTag = 'Rho_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Rho_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.65 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Rho_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Rho_min');

appdata = [];
appdata.lastValidTag = 'Rho_cur';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Rho_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.65 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Rho_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Rho_cur');

appdata = [];
appdata.lastValidTag = 'Rho_max';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Rho_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.65 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Rho_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Rho_max');

appdata = [];
appdata.lastValidTag = 'uipanel9';

h13 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Resistenza Valvola R1 [Kg/m^6 s]',...
'Clipping','on',...
'Position',[1.25 5.75 37 4.75],...
'Tag','uipanel9',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R1';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R1_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',0.01,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',0.4,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1');

appdata = [];
appdata.lastValidTag = 'R1_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.65 9 1],...
'String','0.01',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_min');

appdata = [];
appdata.lastValidTag = 'R1_cur';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.65 9 1],...
'String','0.4',...
'Style','edit',...
'Value',0.4,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_cur');

appdata = [];
appdata.lastValidTag = 'R1_max';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.65 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_max');

appdata = [];
appdata.lastValidTag = 'uipanel10';

h18 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Resistenza Valvola R2 [Kg/m^6 s]',...
'Clipping','on',...
'Position',[1.25 0.75 37 4.75],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R2';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R2_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',0.01,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',0.2,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2');

appdata = [];
appdata.lastValidTag = 'R2_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.7 9 1],...
'String','0.01',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_min');

appdata = [];
appdata.lastValidTag = 'R2_cur';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.7 9 1],...
'String','0.2',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_cur');

appdata = [];
appdata.lastValidTag = 'R2_max';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.7 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_max');

appdata = [];
appdata.lastValidTag = 'uipanel5';

h23 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[119.571428571429 19.35 40.5714285714286 9.55],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel6';

h24 = uipanel(...
'Parent',h23,...
'Units','characters',...
'Title','Portata ingresso qi [m^3/s]',...
'Clipping','on',...
'Position',[1.28571428571429 0.5 37 7.8],...
'Tag','uipanel6',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'P_min';

h25 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('P_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('P_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','P_min');

appdata = [];
appdata.lastValidTag = 'P_cur';

h26 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('P_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.55 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('P_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','P_cur');

appdata = [];
appdata.lastValidTag = 'P_max';

h27 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('P_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('P_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','P_max');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h28 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.25 5.3 34 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'P';

h29 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('P_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.3 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('P_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','P');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h30 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.25 3.7 34 1.3],...
'String','Ampiezza [m^3/s]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'uipanel8';

h31 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.42857142857143 0.45 31.8571428571428 11.6],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h32 = axes(...
'Parent',h31,...
'Units','characters',...
'Position',[1.57142857142857 0.599999999999999 28.1428571428571 9.75],...
'Box','on',...
'CameraPosition',[124.5 128 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[4.901 0.834875392464677 3.5815 0.569233222135007],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 248.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 255.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h33 = get(h32,'xlabel');

set(h33,...
'Parent',h32,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[123.241116751269 291.461538461538 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h34 = get(h32,'ylabel');

set(h34,...
'Parent',h32,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-49.2258883248731 129.307692307692 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h35 = get(h32,'zlabel');

set(h35,...
'Parent',h32,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-29.0837563451777 -572.923076923077 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel7';

h36 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Uscita',...
'UserData',[],...
'Clipping','on',...
'Position',[119.857142857143 3.5 40.5714285714286 3.3],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h37 = uicontrol(...
'Parent',h36,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.42857142857143 0.65 36 1.3],...
'String','Portata di uscita',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'Run';

h38 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[131.142857142857 0.8 21.5714285714286 2],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel14';

h39 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[56.5 0.4 61 6.25],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h40 = uicontrol(...
'Parent',h39,...
'Units','characters',...
'FontName','Courier',...
'FontSize',9,...
'HorizontalAlignment','left',...
'Position',[1.25 0.25 59 4.75],...
'String',blanks(0),...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h41 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[46.1428571428571 6.9 28 2],...
'String','Punto di Equilibrio',...
'UserData',[],...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel16';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.42857142857143 13.1 74.2857142857143 19.75],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h43 = axes(...
'Parent',h42,...
'Units','characters',...
'Position',[1.42857142857143 0.5 71 18.1],...
'Box','on',...
'CameraPosition',[270.5 192 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[13.1152421964991 2.87559598148166 9.5842154512878 1.96063362373749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 540.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 383.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h44 = get(h43,'xlabel');

set(h44,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[269.413480885312 412.595303867403 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h45 = get(h43,'ylabel');

set(h45,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-42.4175050301811 193.587016574586 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h46 = get(h43,'zlabel');

set(h46,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-23.9466800804829 -21.189226519337 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel13';

h47 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'UserData',[],...
'Clipping','on',...
'Position',[119.285714285714 7.05 40.5714285714286 11.55],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h48 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Livello serbatoio 2 h20 [m]',...
'Clipping','on',...
'Position',[1.14285714285714 0.449999999999999 37 4.75],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'h2';

h49 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('h2_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.25 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h2_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h2');

appdata = [];
appdata.lastValidTag = 'h2_min';

h50 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h2_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.7 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h2_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h2_min');

appdata = [];
appdata.lastValidTag = 'h2_cur';

h51 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h2_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.5714285714286 0.7 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h2_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h2_cur');

appdata = [];
appdata.lastValidTag = 'h2_max';

h52 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h2_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.1428571428571 0.7 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h2_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h2_max');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h53 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Livello serbatoio 1 h10 [m]',...
'Clipping','on',...
'Position',[1.28571428571429 5.6 37 4.7],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'h1';

h54 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('h1_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 2.2 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h1_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h1');

appdata = [];
appdata.lastValidTag = 'h1_min';

h55 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h1_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h1_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h1_min');

appdata = [];
appdata.lastValidTag = 'h1_cur';

h56 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h1_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.7142857142857 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h1_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h1_cur');

appdata = [];
appdata.lastValidTag = 'h1_max';

h57 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h1_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.2857142857143 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h1_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h1_max');

appdata = [];
appdata.lastValidTag = 'uipanel17';

h58 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'UserData',[],...
'Clipping','on',...
'Position',[77 29.4 83 3.5],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h59 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[23.7142857142857 0.7 8.57142857142857 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h60 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[72 0.7 8.57142857142857 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h61 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.28571428571429 0.7 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h62 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[49.5714285714286 0.8 21 1.1],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h63 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[33.7142857142857 0.7 8.57142857142857 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


