% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
P_min = 0.000000;
P_cur = 1.000000;
P_max = 10.000000;

% Uscita
Uscita = 1;

% Parametri
A_min = 1.000000;
A_cur = 5.000000;
A_max = 10.000000;
Rho_min = 0.000000;
Rho_cur = 1.000000;
Rho_max = 10.000000;
R1_min = 0.010000;
R1_cur = 0.400000;
R1_max = 10.000000;
R2_min = 0.010000;
R2_cur = 9.778000;
R2_max = 10.000000;
h1_min = 0.000000;
h1_cur = 0.000000;
h1_max = 10.000000;
h2_min = 0.000000;
h2_cur = 0.000000;
h2_max = 10.000000;
