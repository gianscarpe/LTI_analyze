% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
Fm_min = -50.000000;
Fm_cur = 4.000000;
Fm_max = 50.000000;

% Uscita
Uscita = 1;

% Parametri
K_min = 0.000000;
K_cur = 2.000000;
K_max = 10.000000;
tipoMolla = 1.000000;
Alpha_min = 0.000000;
Alpha_cur = 0.000000;
Alpha_max = 10.000000;
M_min = 0.000000;
M_cur = 1.100000;
M_max = 10.000000;
S0_min = -10.000000;
S0_cur = 0.100000;
S0_max = 10.000000;
V0_min = -5.000000;
V0_cur = 0.100000;
V0_max = 5.000000;
