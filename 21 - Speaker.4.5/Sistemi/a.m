% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
Va_min = -15.000000;
Va_cur = 3.000000;
Va_max = 15.000000;

% Uscita
Uscita = 1;

% Parametri
R_min = 0.000000;
R_cur = 8.000000;
R_max = 50.000000;
L_min = 0.000000;
L_cur = 492.386000;
L_max = 1000.000000;
B_min = -10.000000;
B_cur = 1.000000;
B_max = 10.000000;
M_min = 0.000000;
M_cur = 0.050000;
M_max = 0.100000;
b_min = 0.000000;
b_cur = 10.000000;
b_max = 200.000000;
l_min = 0.000000;
l_cur = 1.000000;
l_max = 5.000000;
k_min = 0.000000;
k_cur = 10.000000;
k_max = 100.000000;
x0_min = -0.300000;
x0_cur = 0.000000;
x0_max = 0.300000;
v0_min = -10.000000;
v0_cur = 1.000000;
v0_max = 10.000000;
i0_min = 0.000000;
i0_cur = 0.000000;
i0_max = 10.000000;
