%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             S P E A K E R    3.17                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 23-Nov-2017 13:29:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)
clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      -15      % Va_min        4
      3        % Va, Va_cur    5
      15       % Va_max        6
      0.00     % R_min         7
      8        % R, R_cur      8
      50.00    % R_max         9
      0        % L_min         10
      100      % L, L_cur      11
      1000     % L_max         12
      -10      % B_min         13
      1        % B, B_cur      14
      10       % B_max         15
      0.00     % M_min         16
      0.05     % M, M_cur      17
      0.1      % M_max         18
      0.00     % b_min         19
      10       % b, b_cur      20
      200      % b_max         21
      0        % l_min         22
      1        % l, l_cur      23
      5        % l_max         24
      0.00     % k_min         25
      10       % k, k_cur      26
      100      % k_max         27
      -0.3     % x0_min        28
      0.00     % x0, x0_cur    29
      0.3      % x0_max        30
      -10      % v0_min        31
      0.0      % v0, v0_cur    32
      10       % v0_max        33
      0        % i0_min        34
      0        % i0, i0_cur    35
      10       % i0_max        36
      ];

% Par degli slider
global Rsld Lsld Bsld Msld bsld lsld ksld x0sld v0sld i0sld Vasld;


Rsld.stmin = 0.1;
Rsld.stmax = 1;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Lsld.stmin = 0.1;
Lsld.stmax = 1;
Lsld.Llim = -Inf;
Lsld.Hlim = +Inf;

Bsld.stmin = 0.1;
Bsld.stmax = 1;
Bsld.Llim = -Inf;
Bsld.Hlim = +Inf;

Msld.stmin = 0.01;
Msld.stmax = 0.01;
Msld.Llim = eps;
Msld.Hlim = +Inf;

bsld.stmin = 0.1;
bsld.stmax = 1;
bsld.Llim = eps;
bsld.Hlim = +Inf;

lsld.stmin = 0.1;
lsld.stmax = 1;
lsld.Llim = eps;
lsld.Hlim = +Inf;

ksld.stmin = 0.1;
ksld.stmax = 1;
ksld.Llim = eps;
ksld.Hlim = +Inf;

x0sld.stmin = 0.01;
x0sld.stmax = 1;
x0sld.Llim = -Inf;
x0sld.Hlim = +Inf;

v0sld.stmin = 0.1;
v0sld.stmax = 1;
v0sld.Llim = -Inf;
v0sld.Hlim = +Inf;

i0sld.stmin = 0.1;
i0sld.stmax = 1;
i0sld.Llim = -Inf;
i0sld.Hlim = +Inf;

Vasld.stmin = 0.1;
Vasld.stmax = 1;
Vasld.Llim = -Inf;
Vasld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles); 
% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Uscita;

% Chiusura dello schema simulink (if any) senza salvare
bdclose(Uscita);

Uscita = 'Velocita';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
Va = get(handles.Va, 'Value'); u=Va;
R  = get(handles.R,  'Value');
L  = get(handles.L,  'Value'); L=L/1000;
B  = get(handles.B,  'Value');
M  = get(handles.M,  'Value');
b  = get(handles.b,  'Value');
l  = get(handles.l,  'Value');
k  = get(handles.k,  'Value');
x0  = get(handles.x0,  'Value');
v0  = get(handles.v0,  'Value');
i0  = get(handles.i0,  'Value');

% Controllo sui dati nulli per evitare singolarita' (1/0)
if R == 0, R = eps; end
if M <= 0, M = eps; end
if l <= 0, l = eps; end
if b <= 0, b = eps; end
if k <= 0, k = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end

% Costante di tempo
F = [0 1 0; -k/M -b/M (B*l)/M; 0 (B*l)/L -R/L];
G = [0; 0; 1/L];
H = [0 1 0];

Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esportazione di tutte le variabili nel Workspace per permettere 
% a simulink di averne accesso

vars = {'Va', Va; 'x0', x0; 'v0', v0; 'i0', i0; 'R', R; 'L', L; 'B', B; 'M', M; 'l', l; 'b', b; 'k', k};
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end


% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
    add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
    add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
    add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
    add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
    add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
    add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
    add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
    add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();



%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.Va_min, 'Value',  ampiezza(1));
set(handles.Va_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.Va_cur, 'Value',  ampiezza(2));
set(handles.Va_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.Va_max, 'Value',  ampiezza(3));
set(handles.Va_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.Va, 'Min',   ampiezza(1));
set(handles.Va, 'Value', ampiezza(2));
set(handles.Va, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
Va = get(handles.Va, 'Value'); u = Va;
R  = get(handles.R,  'Value');
L  = get(handles.L,  'Value'); L = L/1000;
B  = get(handles.B,  'Value');
M  = get(handles.M,  'Value');
b  = get(handles.b,  'Value');
l  = get(handles.l,  'Value');
k  = get(handles.k,  'Value');

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if R <= 0, R = eps; end
if M <= 0, M = eps; end
if b <= 0, b = eps; end
if l <= 0, l = eps; end
if k <= 0, k = eps; end

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)
% Preparazione matrici 
F = [0 1 0; -k/M -b/M (B*l)/M; 0 (B*l)/L -R/L];
G = [0; 0; 1/L];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['Va = ', num2str(u(1), '%.2f'), ' V']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: Va = %.1f V', u(1));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  x = %.3f m', x(1));
str22 = sprintf('\n  v = %.2f m/s', x(2));
str23 = sprintf('\n  i = %.3f A', x(3));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str3 = '';
end

endstr = '.';
str = strcat(str, str1, str21, str22, str23, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Rsld Lsld Bsld Msld bsld lsld ksld x0sld v0sld i0sld Vasld;

% Aggiornamento degli slider
%R
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

%L
set(handles.L_min, 'Value',  L_min);
set(handles.L_min, 'String', num2str(L_min, '%.1f'));
set(handles.L_cur, 'Value',  L_cur);
set(handles.L_cur, 'String', num2str(L_cur, '%.2f'));
set(handles.L_max, 'Value',  L_max);
set(handles.L_max, 'String', num2str(L_max, '%.1f'));

set(handles.L, 'Min',   L_min);
set(handles.L, 'Value', L_cur);
set(handles.L, 'Max',   L_max);
majorstep = Lsld.stmax / (L_max-L_min);
minorstep = Lsld.stmin / (L_max-L_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

%B
set(handles.B_min, 'Value',  B_min);
set(handles.B_min, 'String', num2str(B_min, '%.1f'));
set(handles.B_cur, 'Value',  B_cur);
set(handles.B_cur, 'String', num2str(B_cur, '%.2f'));
set(handles.B_max, 'Value',  B_max);
set(handles.B_max, 'String', num2str(B_max, '%.1f'));

set(handles.B, 'Min',   B_min);
set(handles.B, 'Value', B_cur);
set(handles.B, 'Max',   B_max);
majorstep = Bsld.stmax / (B_max-B_min);
minorstep = Bsld.stmin / (B_max-B_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

%M
set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

%b
set(handles.b_min, 'Value',  b_min);
set(handles.b_min, 'String', num2str(b_min, '%.1f'));
set(handles.b_cur, 'Value',  b_cur);
set(handles.b_cur, 'String', num2str(b_cur, '%.2f'));
set(handles.b_max, 'Value',  b_max);
set(handles.b_max, 'String', num2str(b_max, '%.1f'));

set(handles.b, 'Min',   b_min);
set(handles.b, 'Value', b_cur);
set(handles.b, 'Max',   b_max);
majorstep = bsld.stmax / (b_max-b_min);
minorstep = bsld.stmin / (b_max-b_min);
set(handles.b, 'SliderStep', [minorstep majorstep]);

%l
set(handles.l_min, 'Value',  l_min);
set(handles.l_min, 'String', num2str(l_min, '%.1f'));
set(handles.l_cur, 'Value',  l_cur);
set(handles.l_cur, 'String', num2str(l_cur, '%.2f'));
set(handles.l_max, 'Value',  l_max);
set(handles.l_max, 'String', num2str(l_max, '%.1f'));

set(handles.l, 'Min',   l_min);
set(handles.l, 'Value', l_cur);
set(handles.l, 'Max',   l_max);
majorstep = lsld.stmax / (l_max-l_min);
minorstep = lsld.stmin / (l_max-l_min);
set(handles.l, 'SliderStep', [minorstep majorstep]);

%k
set(handles.k_min, 'Value',  k_min);
set(handles.k_min, 'String', num2str(k_min, '%.1f'));
set(handles.k_cur, 'Value',  k_cur);
set(handles.k_cur, 'String', num2str(k_cur, '%.2f'));
set(handles.k_max, 'Value',  k_max);
set(handles.k_max, 'String', num2str(k_max, '%.1f'));

set(handles.k, 'Min',   k_min);
set(handles.k, 'Value', k_cur);
set(handles.k, 'Max',   k_max);
majorstep = ksld.stmax / (k_max-k_min);
minorstep = ksld.stmin / (k_max-k_min);
set(handles.k, 'SliderStep', [minorstep majorstep]);

%x0
set(handles.x0_min, 'Value',  x0_min);
set(handles.x0_min, 'String', num2str(x0_min, '%.1f'));
set(handles.x0_cur, 'Value',  x0_cur);
set(handles.x0_cur, 'String', num2str(x0_cur, '%.2f'));
set(handles.x0_max, 'Value',  x0_max);
set(handles.x0_max, 'String', num2str(x0_max, '%.1f'));

set(handles.x0, 'Min',   x0_min);
set(handles.x0, 'Value', x0_cur);
set(handles.x0, 'Max',   x0_max);
majorstep = x0sld.stmax / (x0_max-x0_min);
minorstep = x0sld.stmin / (x0_max-x0_min);
set(handles.x0, 'SliderStep', [minorstep majorstep]);

%v0
set(handles.v0_min, 'Value',  v0_min);
set(handles.v0_min, 'String', num2str(v0_min, '%.1f'));
set(handles.v0_cur, 'Value',  v0_cur);
set(handles.v0_cur, 'String', num2str(v0_cur, '%.2f'));
set(handles.v0_max, 'Value',  v0_max);
set(handles.v0_max, 'String', num2str(v0_max, '%.1f'));

set(handles.v0, 'Min',   v0_min);
set(handles.v0, 'Value', v0_cur);
set(handles.v0, 'Max',   v0_max);
majorstep = v0sld.stmax / (v0_max-v0_min);
minorstep = v0sld.stmin / (v0_max-v0_min);
set(handles.v0, 'SliderStep', [minorstep majorstep]);

%i0
set(handles.i0_min, 'Value',  i0_min);
set(handles.i0_min, 'String', num2str(i0_min, '%.1f'));
set(handles.i0_cur, 'Value',  i0_cur);
set(handles.i0_cur, 'String', num2str(i0_cur, '%.2f'));
set(handles.i0_max, 'Value',  i0_max);
set(handles.i0_max, 'String', num2str(i0_max, '%.1f'));

set(handles.i0, 'Min',   i0_min);
set(handles.i0, 'Value', i0_cur);
set(handles.i0, 'Max',   i0_max);
majorstep = i0sld.stmax / (i0_max-i0_min);
minorstep = i0sld.stmin / (i0_max-i0_min);
set(handles.i0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [V]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.Va_min, 'Value',  Va_min);
set(handles.Va_min, 'String', num2str(Va_min, '%.1f'));
set(handles.Va_cur, 'Value',  Va_cur);
set(handles.Va_cur, 'String', num2str(Va_cur, '%.2f'));
set(handles.Va_max, 'Value',  Va_max);
set(handles.Va_max, 'String', num2str(Va_max, '%.1f'));

set(handles.Va, 'Min',   Va_min);
set(handles.Va, 'Value', Va_cur);
set(handles.Va, 'Max',   Va_max);
majorstep = Vasld.stmax / (Va_max-Va_min);
minorstep = Vasld.stmin / (Va_max-Va_min);
set(handles.Va, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i0 parametri
% pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello induzione
L_min = get(handles.L_min, 'Value');
L_cur = get(handles.L_cur, 'Value');
L_max = get(handles.L_max, 'Value');

% pannello campo magnetico
B_min = get(handles.B_min, 'Value');
B_cur = get(handles.B_cur, 'Value');
B_max = get(handles.B_max, 'Value');

% pannello massa
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello coeff attrito
b_min = get(handles.b_min, 'Value');
b_cur = get(handles.b_cur, 'Value');
b_max = get(handles.b_max, 'Value');

% pannello lunghezza avvolgimento
l_min = get(handles.l_min, 'Value');
l_cur = get(handles.l_cur, 'Value');
l_max = get(handles.l_max, 'Value');

% pannello coeff elastico
k_min = get(handles.k_min, 'Value');
k_cur = get(handles.k_cur, 'Value');
k_max = get(handles.k_max, 'Value');

% pannello posizione
x0_min = get(handles.x0_min, 'Value');
x0_cur = get(handles.x0_cur, 'Value');
x0_max = get(handles.x0_max, 'Value');

% pannello velocit�
v0_min = get(handles.v0_min, 'Value');
v0_cur = get(handles.v0_cur, 'Value');
v0_max = get(handles.v0_max, 'Value');

% pannello corrente
i0_min = get(handles.i0_min, 'Value');
i0_cur = get(handles.i0_cur, 'Value');
i0_max = get(handles.i0_max, 'Value');


% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

Va_min = get(handles.Va_min, 'Value');
Va_cur = get(handles.Va_cur, 'Value');
Va_max = get(handles.Va_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'Va_min = %f;\n', Va_min);
fprintf(fid, 'Va_cur = %f;\n', Va_cur);
fprintf(fid, 'Va_max = %f;\n', Va_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'L_min = %f;\n', L_min);
fprintf(fid, 'L_cur = %f;\n', L_cur);
fprintf(fid, 'L_max = %f;\n', L_max);

fprintf(fid, 'B_min = %f;\n', B_min);
fprintf(fid, 'B_cur = %f;\n', B_cur);
fprintf(fid, 'B_max = %f;\n', B_max);

fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'b_min = %f;\n', b_min);
fprintf(fid, 'b_cur = %f;\n', b_cur);
fprintf(fid, 'b_max = %f;\n', b_max);

fprintf(fid, 'l_min = %f;\n', l_min);
fprintf(fid, 'l_cur = %f;\n', l_cur);
fprintf(fid, 'l_max = %f;\n', l_max);

fprintf(fid, 'k_min = %f;\n', k_min);
fprintf(fid, 'k_cur = %f;\n', k_cur);
fprintf(fid, 'k_max = %f;\n', k_max);

fprintf(fid, 'x0_min = %f;\n', x0_min);
fprintf(fid, 'x0_cur = %f;\n', x0_cur);
fprintf(fid, 'x0_max = %f;\n', x0_max);

fprintf(fid, 'v0_min = %f;\n', v0_min);
fprintf(fid, 'v0_cur = %f;\n', v0_cur);
fprintf(fid, 'v0_max = %f;\n', v0_max);

fprintf(fid, 'i0_min = %f;\n', i0_min);
fprintf(fid, 'i0_cur = %f;\n', i0_cur);
fprintf(fid, 'i0_max = %f;\n', i0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i0 nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');




% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i0 nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Rsld Lsld Bsld Msld bsld lsld ksld x0sld v0sld i0sld Vasld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [V]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider Va
set(handles.Va_min, 'Value', def(4));
set(handles.Va_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.Va_cur, 'Value', def(5));
set(handles.Va_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.Va_max, 'Value', def(6));
set(handles.Va_max, 'String', num2str(def(6), '%.1f'));

set(handles.Va, 'Min',   def(4)); 
set(handles.Va, 'Value', def(5));
set(handles.Va, 'Max',   def(6)); 
majorstep = Vasld.stmax / (def(6)-def(4));
minorstep = Vasld.stmin / (def(6)-def(4));
set(handles.Va, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.Va, 'Min') get(handles.Va, 'Value') get(handles.Va, 'Max')]);
evalin('base', ['input_params(''Ampiezza [V]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider L
set(handles.L_min, 'Value',  def(10));
set(handles.L_min, 'String', num2str(def(10), '%.1f'));
set(handles.L_cur, 'Value',  def(11));
set(handles.L_cur, 'String', num2str(def(11), '%.2f'));
set(handles.L_max, 'Value',  def(12));
set(handles.L_max, 'String', num2str(def(12), '%.1f'));

set(handles.L, 'Min',   def(10));
set(handles.L, 'Value', def(11));
set(handles.L, 'Max',   def(12));
majorstep = Lsld.stmax / (def(12)-def(10));
minorstep = Lsld.stmin / (def(12)-def(10));
set(handles.L, 'SliderStep', [minorstep majorstep]);

% slider B
set(handles.B_min, 'Value', def(13));
set(handles.B_min, 'String', num2str(def(13), '%.1f'));
set(handles.B_cur, 'Value', def(14));
set(handles.B_cur, 'String', num2str(def(14), '%.2f'));
set(handles.B_max, 'Value', def(15));
set(handles.B_max, 'String', num2str(def(15), '%.1f'));

set(handles.B, 'Min',   def(13)); 
set(handles.B, 'Value', def(14));
set(handles.B, 'Max',   def(15)); 
majorstep = Bsld.stmax / (def(15)-def(13));
minorstep = Bsld.stmin / (def(15)-def(13));
set(handles.B, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value', def(16));
set(handles.M_min, 'String', num2str(def(16), '%.1f'));
set(handles.M_cur, 'Value', def(17));
set(handles.M_cur, 'String', num2str(def(17), '%.2f'));
set(handles.M_max, 'Value', def(18));
set(handles.M_max, 'String', num2str(def(18), '%.1f'));

set(handles.M, 'Min',   def(16)); 
set(handles.M, 'Value', def(17));
set(handles.M, 'Max',   def(18)); 
majorstep = Msld.stmax / (def(18)-def(16));
minorstep = Msld.stmin / (def(18)-def(16));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider b
set(handles.b_min, 'Value', def(19));
set(handles.b_min, 'String', num2str(def(19), '%.1f'));
set(handles.b_cur, 'Value', def(20));
set(handles.b_cur, 'String', num2str(def(20), '%.2f'));
set(handles.b_max, 'Value', def(21));
set(handles.b_max, 'String', num2str(def(21), '%.1f'));

set(handles.b, 'Min',   def(19)); 
set(handles.b, 'Value', def(20));
set(handles.b, 'Max',   def(21)); 
majorstep = bsld.stmax / (def(21)-def(19));
minorstep = bsld.stmin / (def(21)-def(19));
set(handles.b, 'SliderStep', [minorstep majorstep]);

% slider l
set(handles.l_min, 'Value', def(22));
set(handles.l_min, 'String', num2str(def(22), '%.1f'));
set(handles.l_cur, 'Value', def(23));
set(handles.l_cur, 'String', num2str(def(23), '%.2f'));
set(handles.l_max, 'Value', def(24));
set(handles.l_max, 'String', num2str(def(24), '%.1f'));

set(handles.l, 'Min',   def(22)); 
set(handles.l, 'Value', def(23));
set(handles.l, 'Max',   def(24)); 
majorstep = lsld.stmax / (def(24)-def(22));
minorstep = lsld.stmin / (def(24)-def(22));
set(handles.l, 'SliderStep', [minorstep majorstep]);

% slider k
set(handles.k_min, 'Value', def(25));
set(handles.k_min, 'String', num2str(def(25), '%.1f'));
set(handles.k_cur, 'Value', def(26));
set(handles.k_cur, 'String', num2str(def(26), '%.2f'));
set(handles.k_max, 'Value', def(27));
set(handles.k_max, 'String', num2str(def(27), '%.1f'));

set(handles.k, 'Min',   def(25)); 
set(handles.k, 'Value', def(26));
set(handles.k, 'Max',   def(27)); 
majorstep = ksld.stmax / (def(27)-def(25));
minorstep = ksld.stmin / (def(27)-def(25));
set(handles.k, 'SliderStep', [minorstep majorstep]);

% slider x0
set(handles.x0_min, 'Value', def(28));
set(handles.x0_min, 'String', num2str(def(28), '%.1f'));
set(handles.x0_cur, 'Value', def(29));
set(handles.x0_cur, 'String', num2str(def(29), '%.2f'));
set(handles.x0_max, 'Value', def(30));
set(handles.x0_max, 'String', num2str(def(30), '%.1f'));

set(handles.x0, 'Min',   def(28)); 
set(handles.x0, 'Value', def(29));
set(handles.x0, 'Max',   def(30)); 
majorstep = x0sld.stmax / (def(30)-def(28));
minorstep = x0sld.stmin / (def(30)-def(28));
set(handles.x0, 'SliderStep', [minorstep majorstep]);

% slider v0
set(handles.v0_min, 'Value', def(31));
set(handles.v0_min, 'String', num2str(def(31), '%.1f'));
set(handles.v0_cur, 'Value', def(32));
set(handles.v0_cur, 'String', num2str(def(32), '%.2f'));
set(handles.v0_max, 'Value', def(33));
set(handles.v0_max, 'String', num2str(def(33), '%.1f'));

set(handles.v0, 'Min',   def(31)); 
set(handles.v0, 'Value', def(32));
set(handles.v0, 'Max',   def(33)); 
majorstep = v0sld.stmax / (def(33)-def(31));
minorstep = v0sld.stmin / (def(33)-def(31));
set(handles.v0, 'SliderStep', [minorstep majorstep]);

% slider i0
set(handles.i0_min, 'Value', def(34));
set(handles.i0_min, 'String', num2str(def(34), '%.1f'));
set(handles.i0_cur, 'Value', def(35));
set(handles.i0_cur, 'String', num2str(def(35), '%.2f'));
set(handles.i0_max, 'Value', def(36));
set(handles.i0_max, 'String', num2str(def(36), '%.1f'));

set(handles.i0, 'Min',   def(34)); 
set(handles.i0, 'Value', def(35));
set(handles.i0, 'Max',   def(36)); 
majorstep = i0sld.stmax / (def(36)-def(34));
minorstep = i0sld.stmin / (def(36)-def(34));
set(handles.i0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER k

% --- Executes on slider movement.
function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.k, handles.k_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function k_min_Callback(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ksld;
Slider_min_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_max_Callback(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ksld;
Slider_max_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_cur_Callback(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ksld;
Slider_cur_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER l
% --- Executes on slider movement.
function l_Callback(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.l, handles.l_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function l_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function l_min_Callback(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global lsld;
Slider_min_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function l_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function l_max_Callback(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global lsld;
Slider_max_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function l_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function l_cur_Callback(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global lsld;
Slider_cur_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function l_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER b
% --- Executes on slider movement.
function b_Callback(hObject, eventdata, handles)
% hObject    handle to b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.b, handles.b_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function b_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function b_min_Callback(hObject, eventdata, handles)
% hObject    handle to b_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bsld;
Slider_min_Callback(handles, handles.b, handles.b_min, handles.b_cur, handles.b_max, bsld.stmin, bsld.stmax, bsld.Llim, bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function b_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b_max_Callback(hObject, eventdata, handles)
% hObject    handle to b_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bsld;
Slider_max_Callback(handles, handles.b, handles.b_min, handles.b_cur, handles.b_max, bsld.stmin, bsld.stmax, bsld.Llim, bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function b_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b_cur_Callback(hObject, eventdata, handles)
% hObject    handle to b_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bsld;
Slider_cur_Callback(handles, handles.b, handles.b_min, handles.b_cur, handles.b_max, bsld.stmin, bsld.stmax, bsld.Llim, bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function b_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% SLIDER B
% --- Executes on slider movement.
function B_Callback(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.B, handles.B_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function B_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function B_min_Callback(hObject, eventdata, handles)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Bsld;
Slider_min_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function B_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function B_max_Callback(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Bsld;
Slider_max_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function B_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function B_cur_Callback(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Bsld;
Slider_cur_Callback(handles, handles.B, handles.B_min, handles.B_cur, handles.B_max, Bsld.stmin, Bsld.stmax, Bsld.Llim, Bsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function B_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to B_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER L
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.L, handles.L_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function L_min_Callback(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Lsld;
Slider_min_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_max_Callback(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Lsld;
Slider_max_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_cur_Callback(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Lsld;
Slider_cur_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
function Va_min_Callback(hObject, eventdata, handles)
% hObject    handle to Va_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Vasld;
Slider_min_Callback(handles, handles.Va, handles.Va_min, handles.Va_cur, handles.Va_max, Vasld.stmin, Vasld.stmax, Vasld.Llim, Vasld.Hlim);

minval = get(handles.Va_min, 'Value');
curval = get(handles.Va, 'Value');
maxval = get(handles.Va_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Va_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Va_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Va_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Va_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Vasld;
Slider_cur_Callback(handles, handles.Va, handles.Va_min, handles.Va_cur, handles.Va_max, Vasld.stmin, Vasld.stmax, Vasld.Llim, Vasld.Hlim);

minval = get(handles.Va_min, 'Value');
curval = get(handles.Va, 'Value');
maxval = get(handles.Va_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Va_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Va_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Va_max_Callback(hObject, eventdata, handles)
% hObject    handle to Va_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Vasld;
Slider_max_Callback(handles, handles.Va, handles.Va_min, handles.Va_cur, handles.Va_max, Vasld.stmin, Vasld.stmax, Vasld.Llim, Vasld.Hlim);

minval = get(handles.Va_min, 'Value');
curval = get(handles.Va, 'Value');
maxval = get(handles.Va_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Va_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Va_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Va_Callback(hObject, eventdata, handles)
% hObject    handle to Va (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Va, handles.Va_cur);

minval = get(handles.Va_min, 'Value');
curval = get(handles.Va, 'Value');
maxval = get(handles.Va_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Va_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Va (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [V]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Va_min, 'Value',  values(1));
set(handles.Va_min, 'String', num2str(values(1), '%.1f'));
set(handles.Va_cur, 'Value',  values(2));
set(handles.Va_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Va_max, 'Value',  values(3));
set(handles.Va_max, 'String', num2str(values(3), '%.1f'));
set(handles.Va, 'Min',   values(1));
set(handles.Va, 'Value', values(2));
set(handles.Va, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Va_min, 'Value',  values(1));
set(handles.Va_min, 'String', num2str(values(1), '%.1f'));
set(handles.Va_cur, 'Value',  values(2));
set(handles.Va_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Va_max, 'Value',  values(3));
set(handles.Va_max, 'String', num2str(values(3), '%.1f'));
set(handles.Va, 'Min',   values(1));
set(handles.Va, 'Max',   values(3));
set(handles.Va, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%%  SLIDER STATI INIZIALI

% --- Executes on slider movement.
function i0_Callback(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.i0, handles.i0_cur);


% --- Executes during object creation, after setting all properties.
function i0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function i0_min_Callback(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global isld;
Slider_min_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_max_Callback(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global isld;
Slider_max_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global isld;
Slider_cur_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function v0_Callback(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.v0, handles.v0_cur);


% --- Executes during object creation, after setting all properties.
function v0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function v0_min_Callback(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vsld;
Slider_min_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, vsld.stmin, vsld.stmax, vsld.Llim, vsld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v0_max_Callback(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vsld;
Slider_max_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, vsld.stmin, vsld.stmax, vsld.Llim, vsld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vsld;
Slider_cur_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, vsld.stmin, vsld.stmax, vsld.Llim, vsld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function x0_Callback(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.x0, handles.x0_cur);


% --- Executes during object creation, after setting all properties.
function x0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function x0_min_Callback(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global xsld;
Slider_min_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, xsld.stmin, xsld.stmax, xsld.Llim, xsld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_max_Callback(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global xsld;
Slider_max_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, xsld.stmin, xsld.stmax, xsld.Llim, xsld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global xsld;
Slider_cur_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, xsld.stmin, xsld.stmax, xsld.Llim, xsld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 281.003784179688, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 6, ...
    'uipanel', 24, ...
    'slider', 14, ...
    'edit', 41, ...
    'popupmenu', 5, ...
    'pushbutton', 8, ...
    'listbox', 2, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\21 - Speaker.3.17 - FMM\Main.fig', ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\21 - Speaker.3.17 - FMM\Main_export.m');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','SPEAKER ver. 3.17',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 24.75 173.571428571429 36.95],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[0.571428571428571 8.25 37.2857142857143 8.7],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.28571428571429 0.349999999999995 34.2857142857143 7.25],...
'Box','on',...
'CameraPosition',[376.5 207 4290.23775422228],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[9.96079517592166 1.85383688563169 7.27904262855814 1.26397969474887],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 752.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 413.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[374.933333333333 501.533333333333 1.02557181125758],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-123.266666666667 210.133333333333 1.02557181125758],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-48.0666666666667 -1331.46666666667 1.02557181125758],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel4';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[89.7142857142857 0.15 40 36.85],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel5';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Resistenza R [ohm]',...
'Clipping','on',...
'Position',[1.52681252681253 30.8224052594401 36.7327327327327 5.0308815272319],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R_Callback',hObject,eventdata,guidata(hObject)),...
'Max',1000,...
'Min',0.01,...
'Position',[1.16513157894737 2.30993517718237 33.7888157894737 1.26050561797753],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0010000100001 0.010000100001],...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R');

appdata = [];
appdata.lastValidTag = 'R_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Min',0.01,...
'Position',[1.42857142857143 0.529411764705882 9 1.35294117647059],...
'String','0.01',...
'Style','edit',...
'Value',0.01,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_min');

appdata = [];
appdata.lastValidTag = 'R_max';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.7142857142857 0.588235294117647 8.99999999999999 1.35294117647059],...
'String','1000.0',...
'Style','edit',...
'Value',1000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_max');

appdata = [];
appdata.lastValidTag = 'R_cur';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.5714285714286 0.529411764705882 9 1.35294117647059],...
'String','500.0',...
'Style','edit',...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_cur');

appdata = [];
appdata.lastValidTag = 'uipanel7';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Coefficiente di induzione L [mH]',...
'Clipping','on',...
'Position',[1.55092235092236 25.8009566350718 36.7327327327327 5.0308815272319],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'L';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('L_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.48855233270382 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L');

appdata = [];
appdata.lastValidTag = 'L_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.723526108927592 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_min');

appdata = [];
appdata.lastValidTag = 'L_max';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.673526108927592 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_max');

appdata = [];
appdata.lastValidTag = 'L_cur';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.673526108927592 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_cur');

appdata = [];
appdata.lastValidTag = 'uipanel16';

h18 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Campo magnetico B [tesla]',...
'Clipping','on',...
'Position',[1.45714285714286 20.8153846153846 36.8 5],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'B';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('B_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B');

appdata = [];
appdata.lastValidTag = 'B_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_min');

appdata = [];
appdata.lastValidTag = 'B_max';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_max');

appdata = [];
appdata.lastValidTag = 'B_cur';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('B_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('B_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','B_cur');

appdata = [];
appdata.lastValidTag = 'uipanel17';

h23 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Massa del cono M [kg]',...
'Clipping','on',...
'Position',[1.42857142857143 15.8 36.8571428571429 5],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'M';

h24 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('M_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M');

appdata = [];
appdata.lastValidTag = 'M_min';

h25 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_min');

appdata = [];
appdata.lastValidTag = 'M_max';

h26 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_max');

appdata = [];
appdata.lastValidTag = 'M_cur';

h27 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_cur');

appdata = [];
appdata.lastValidTag = 'uipanel18';

h28 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Coefficiente di attrito viscoso b',...
'Clipping','on',...
'Position',[1.57142857142857 10.8 36.8571428571429 5],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'b';

h29 = uicontrol(...
'Parent',h28,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('b_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('b_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','b');

appdata = [];
appdata.lastValidTag = 'b_min';

h30 = uicontrol(...
'Parent',h28,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('b_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('b_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','b_min');

appdata = [];
appdata.lastValidTag = 'b_max';

h31 = uicontrol(...
'Parent',h28,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('b_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('b_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','b_max');

appdata = [];
appdata.lastValidTag = 'b_cur';

h32 = uicontrol(...
'Parent',h28,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('b_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('b_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','b_cur');

appdata = [];
appdata.lastValidTag = 'uipanel19';

h33 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Lunghezza dell''avvolgimento l [m]',...
'Clipping','on',...
'Position',[1.57142857142857 5.8 36.8571428571429 5],...
'Tag','uipanel19',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'l';

h34 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('l_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l');

appdata = [];
appdata.lastValidTag = 'l_min';

h35 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_min');

appdata = [];
appdata.lastValidTag = 'l_max';

h36 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_max');

appdata = [];
appdata.lastValidTag = 'l_cur';

h37 = uicontrol(...
'Parent',h33,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_cur');

appdata = [];
appdata.lastValidTag = 'uipanel21';

h38 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Coefficiente elastico k',...
'Clipping','on',...
'Position',[1.71428571428571 0.75 36.8571428571429 5],...
'Tag','uipanel21',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'k';

h39 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('k_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k');

appdata = [];
appdata.lastValidTag = 'k_min';

h40 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_min');

appdata = [];
appdata.lastValidTag = 'k_max';

h41 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_max');

appdata = [];
appdata.lastValidTag = 'k_cur';

h42 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_cur');

appdata = [];
appdata.lastValidTag = 'Run';

h43 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[140.857142857143 2.4 21.5714285714286 2.05],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h44 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'ShadowColor',[0.501960784313725 0.501960784313725 0.501960784313725],...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[130.142857142857 5.1 43 3.25],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h45 = uicontrol(...
'Parent',h44,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.57142857142857 0.523529411764704 39.5714285714286 1.47058823529412],...
'String','Velocit� del cono',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h46 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[130.142857142857 25.9 43 11.1],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel8';

h47 = uipanel(...
'Parent',h46,...
'Units','characters',...
'Title','Tensione in ingresso Va [V]',...
'Clipping','on',...
'Position',[2.25 0.382658491280931 39.25 9.12697740112995],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Va_min';

h48 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Va_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.458018252933531 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Va_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Va_min');

appdata = [];
appdata.lastValidTag = 'Va_cur';

h49 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Va_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.430932203389843 8 1.4],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Va_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Va_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h50 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.25690500966255 5.86275597223179 35.9754528658498 1.77864473684211],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h51 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.25690500966255 3.66062439328441 35.9754528658498 2.03273684210527],...
'String','Ampiezza [V]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'Va_max';

h52 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Va_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[29.2857142857143 0.458018252933531 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Va_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Va_max');

appdata = [];
appdata.lastValidTag = 'Va';

h53 = uicontrol(...
'Parent',h47,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Va_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.25690500966255 2.22076913012652 35.9754528658498 1.4398552631579],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Va_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Va');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h54 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[53.4285714285714 5.75000000000001 23 2.05],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h55 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[40.8571428571429 8.30000000000009 48 6.95],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h56 = uicontrol(...
'Parent',h55,...
'Units','characters',...
'FontName','Courier',...
'HorizontalAlignment','left',...
'Position',[1 0.3 45.1428571428571 5.6],...
'String',blanks(0),...
'Style','text',...
'Value',1,...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel13';

h57 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[0.571428571428571 17.45 88.2857142857143 16.45],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h58 = axes(...
'Parent',h57,...
'Units','characters',...
'Position',[1.28571428571429 0.3 85.1428571428571 15.1],...
'Box','on',...
'CameraPosition',[869 424 9663.02943074431],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[13.1152421964991 2.87559598148166 9.5842154512878 1.96063362373749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 1737.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 847.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h59 = get(h58,'xlabel');

set(h59,...
'Parent',h58,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[867.542785234899 926.739093959731 1.05759614709859],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h60 = get(h58,'ylabel');

set(h60,...
'Parent',h58,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-114.619966442953 428.371644295302 1.05759614709859],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h61 = get(h58,'zlabel');

set(h61,...
'Parent',h58,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-44.6736577181206 -244.861577181208 1.05759614709859],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h62 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[130.142857142857 8.4 43 16.8],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h63 = uipanel(...
'Parent',h62,...
'Units','characters',...
'Title','Posizione del cono x0 [m]',...
'Clipping','on',...
'Position',[1.14285714285714 10.55 40.1428571428571 5],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'x0';

h64 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('x0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'TooltipString','Tensione iniziale del condensatore',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0');

appdata = [];
appdata.lastValidTag = 'x0_min';

h65 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_min');

appdata = [];
appdata.lastValidTag = 'x0_max';

h66 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_max');

appdata = [];
appdata.lastValidTag = 'x0_cur';

h67 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('x0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('x0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','x0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel22';

h68 = uipanel(...
'Parent',h62,...
'Units','characters',...
'Title','Velocit� movimento del cono v0 [m/s]',...
'Clipping','on',...
'Position',[1.28571428571429 5.40000000000001 40.1428571428571 5],...
'Tag','uipanel22',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'v0';

h69 = uicontrol(...
'Parent',h68,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('v0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'TooltipString','Tensione iniziale del condensatore',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0');

appdata = [];
appdata.lastValidTag = 'v0_min';

h70 = uicontrol(...
'Parent',h68,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_min');

appdata = [];
appdata.lastValidTag = 'v0_max';

h71 = uicontrol(...
'Parent',h68,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_max');

appdata = [];
appdata.lastValidTag = 'v0_cur';

h72 = uicontrol(...
'Parent',h68,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('v0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('v0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','v0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel23';

h73 = uipanel(...
'Parent',h62,...
'Units','characters',...
'Title','Corrente  i0 [A]',...
'Clipping','on',...
'Position',[1.28571428571429 0.250000000000004 40.1428571428571 5],...
'Tag','uipanel23',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'i0';

h74 = uicontrol(...
'Parent',h73,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('i0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'TooltipString','Tensione iniziale del condensatore',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0');

appdata = [];
appdata.lastValidTag = 'i0_min';

h75 = uicontrol(...
'Parent',h73,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_min');

appdata = [];
appdata.lastValidTag = 'i0_max';

h76 = uicontrol(...
'Parent',h73,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_max');

appdata = [];
appdata.lastValidTag = 'i0_cur';

h77 = uicontrol(...
'Parent',h73,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h78 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[0.571428571428571 33.9 88.2857142857143 3.15],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h79 = uicontrol(...
'Parent',h78,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.499999999999994 8.57142857142857 1.45],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h80 = uicontrol(...
'Parent',h78,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[73.1428571428571 0.499999999999994 9 1.45],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h81 = uicontrol(...
'Parent',h78,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.28571428571429 0.499999999999994 21 1.45],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h82 = uicontrol(...
'Parent',h78,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[51.8571428571429 0.599999999999994 20 1.3],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h83 = uicontrol(...
'Parent',h78,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.499999999999994 8 1.45],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


