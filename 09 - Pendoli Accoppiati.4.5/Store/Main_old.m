%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               P E N D O L I   A C C O P P I A T I                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alessandro Degli Agosti (2008)
% by Barbara Bertani (2015)
% by F. M. Marchese (2008-15)
%
% Tested with ver. MatLab R2013b

%%
function varargout = Main(varargin)
% Main M-file for Main.fig
%      Main, by itself, creates a new Main or raises the existing
%      singleton*.
%
%      H = Main returns the handle to a new Main or the handle to
%      the existing singleton*.
%
%      Main('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Main.M with the given input arguments.
%
%      Main('Property','Value',...) creates a new Main or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Main_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
%   IMPORTANTE: necessita l'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 17-Dec-2014 12:01:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Main (see VARARGIN)
clc;

% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);
% Trasformazione di parametroIngresso in cella
if ~iscell(get(handles.parametroIngresso, 'String'))
    set(handles.parametroIngresso, 'String', {get(handles.parametroIngresso, 'String')});
end

% Caricamento delle variabili nel Workspace
evalin('base', 'input_params = containers.Map();');
evalin('base', ['input_params(''Ampiezza [N]'') = ' mat2str([get(handles.F, 'Min') get(handles.F, 'Value') get(handles.F, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');
assignin('base', 'ampiezza', ampiezza(2));
assignin('base', 'frequenza', frequenza(2));
assignin('base', 'dutycycle', dutycycle(2));

% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%%
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actL,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function minL_Callback(hObject, eventdata, handles)
% hObject    handle to minL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.L,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of minL as text
%        str2double(get(hObject,'String')) returns contents of minL as a double


% --- Executes during object creation, after setting all properties.
function minL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actL_Callback(hObject, eventdata, handles)
% hObject    handle to actL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.L,'Value',str2double(get(hObject,'String')));

% Hints: get(hObject,'String') returns contents of actL as text
%        str2double(get(hObject,'String')) returns contents of actL as a double


% --- Executes during object creation, after setting all properties.
function actL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function maxL_Callback(hObject, eventdata, handles)
% hObject    handle to maxL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.L,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of maxL as text
%        str2double(get(hObject,'String')) returns contents of maxL as a double


% --- Executes during object creation, after setting all properties.
function maxL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actM,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function minM_Callback(hObject, eventdata, handles)
% hObject    handle to minM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.M,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of minM as text
%        str2double(get(hObject,'String')) returns contents of minM as a double


% --- Executes during object creation, after setting all properties.
function minM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actM_Callback(hObject, eventdata, handles)
% hObject    handle to actM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of actM as text
%        str2double(get(hObject,'String')) returns contents of actM as a double


% --- Executes during object creation, after setting all properties.
function actM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function maxM_Callback(hObject, eventdata, handles)
% hObject    handle to maxM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.M,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of maxM as text
%        str2double(get(hObject,'String')) returns contents of maxM as a double


% --- Executes during object creation, after setting all properties.
function maxM_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function G_Callback(hObject, eventdata, handles)
% hObject    handle to G (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actG,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function G_CreateFcn(hObject, eventdata, handles)
% hObject    handle to G (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function minG_Callback(hObject, eventdata, handles)
% hObject    handle to minG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.G,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of minG as text
%        str2double(get(hObject,'String')) returns contents of minG as a double


% --- Executes during object creation, after setting all properties.
function minG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actG_Callback(hObject, eventdata, handles)
% hObject    handle to actG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.G,'Value',str2double(get(hObject,'String')));

% Hints: get(hObject,'String') returns contents of actG as text
%        str2double(get(hObject,'String')) returns contents of actG as a double


% --- Executes during object creation, after setting all properties.
function actG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function maxG_Callback(hObject, eventdata, handles)
% hObject    handle to maxG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.G,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of maxG as text
%        str2double(get(hObject,'String')) returns contents of maxG as a double


% --- Executes during object creation, after setting all properties.
function maxG_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function F_Callback(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~iscell(get(handles.parametroIngresso, 'String'))
    set(handles.parametroIngresso, 'String', {get(handles.parametroIngresso, 'String')});
end
parametri = get(handles.parametroIngresso, 'String');
param_name = parametri{get(handles.parametroIngresso, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

values(2) = get(hObject, 'Value');
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
set(handles.actF,'String',num2str(get(hObject,'Value'), '%.1f'));
% Past instruction:
% evalin('base', ['input_param(' num2str(get(handles.parametroIngresso, 'Value')) ') = ' num2str(get(hObject, 'Value')) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function F_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function minF_Callback(hObject, eventdata, handles)
% hObject    handle to minF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.F,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of minF as text
%        str2double(get(hObject,'String')) returns contents of minF as a double


% --- Executes during object creation, after setting all properties.
function minF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actF_Callback(hObject, eventdata, handles)
% hObject    handle to actF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
parametri = get(handles.parametroIngresso, 'String');
param_name = parametri{get(handles.parametroIngresso, 'Value')};

if strcmp(param_name, 'Ampiezza [N]') == 0
    if str2double(get(hObject, 'String')) < 0
        set(hObject, 'String', '0');
    end
    if strcmp(param_name, 'Duty Cycle [%]') == 1
        if str2double(get(hObject, 'String')) > 100
            set(hObject, 'String', '100');
        end
    elseif strcmp(param_name, 'Frequenza�[Hz]') == 1
        if str2double(get(hObject, 'String')) > 1000
            set(hObject, 'String', '1000');
        end
    end
end
if str2double(get(hObject, 'String')) < get(handles.F, 'Min')
    set(handles.minF, 'String', get(hObject, 'String'));
    set(handles.F, 'Min', str2double(get(hObject, 'String')));
end
if str2double(get(hObject, 'String')) > get(handles.F, 'Max')
    set(handles.maxF, 'String', get(hObject, 'String'));
    set(handles.F, 'Max', str2double(get(hObject, 'String')));
end
set (handles.F,'Value',str2double(get(hObject,'String')));
values = [get(handles.F, 'Min') get(handles.F, 'Value'), get(handles.F, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of actF as text
%        str2double(get(hObject,'String')) returns contents of actF as a double


% --- Executes during object creation, after setting all properties.
function actF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function maxF_Callback(hObject, eventdata, handles)
% hObject    handle to maxF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.F,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of maxF as text
%        str2double(get(hObject,'String')) returns contents of maxF as a double


% --- Executes during object creation, after setting all properties.
function maxF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on selection change in tipoIngresso.
function tipoIngresso_Callback(hObject, eventdata, handles)
% hObject    handle to tipoIngresso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
id_ingresso = get(hObject, 'Value');

new_String = cell(1);

new_String{1} = 'Ampiezza [N]';

switch id_ingresso
    case {1, 2}
        set(handles.parametroIngresso, 'Value', 1);
        set(handles.parametroIngresso, 'Enable', 'off');
    case 3
        new_String{2} = 'Frequenza�[Hz]';
        if get(handles.parametroIngresso, 'Value') > 2
            set(handles.parametroIngresso, 'Value', 2);
        end
        set(handles.parametroIngresso, 'Enable', 'on');
    case 5
        new_String{2} = 'Frequenza [Hz]';
        new_String{3} = 'Duty Cycle [%]';
        if get(handles.parametroIngresso, 'Value') > 3
            set(handles.parametroIngresso, 'Value', 3);
        end
        set(handles.parametroIngresso, 'Enable', 'on');
    case {4, 6}
        new_String{2} = 'Frequenza [Hz]';
        if get(handles.parametroIngresso, 'Value') > 2
            set(handles.parametroIngresso, 'Value', 2);
        end
        set(handles.parametroIngresso, 'Enable', 'on');
end

set(handles.parametroIngresso, 'String', new_String);

parametri = get(handles.parametroIngresso, 'String');
param_name = parametri{get(handles.parametroIngresso, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));
set(handles.minF, 'String', num2str(values(1), '%.1f'));
set(handles.actF, 'String', num2str(values(2), '%.1f'));
set(handles.maxF, 'String', num2str(values(3), '%.1f'));


% --- Executes during object creation, after setting all properties.
function tipoIngresso_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tipoIngresso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on selection change in tipoUscita.
function tipoUscita_Callback(hObject, eventdata, handles)
% hObject    handle to tipoUscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns tipoUscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from tipoUscita


% --- Executes during object creation, after setting all properties.
function tipoUscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tipoUscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on button press in run.
function run_Callback(hObject, eventdata, handles)
% hObject    handle to esegui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)global uscita;
clc

global uscita;

% Chiudo il Modello simulink senza salvare
bdclose(uscita);

% Leggo la variabile di uscita del sistema (y)
val1 = get(handles.tipoUscita,'Value');
switch val1
    case 1
        uscita = 'Pendoli1';
    case 2
        uscita = 'Pendoli2';
    case 3
        uscita = 'Energia';
end;

% Carico i valori dei parametri dal Workspace
amp = evalin('base', 'input_params(''Ampiezza [N]'')');
if get(handles.tipoIngresso, 'Value') == 3
    freq = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    freq = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
duty = evalin('base', 'input_params(''Duty Cycle [%]'')');

ampiezza = amp(2);
frequenza = freq(2);
dutycycle = duty(2);

% Leggo i dati inseriti dall'utente
L = get(handles.L, 'value');
M = get(handles.M, 'value');
K = get(handles.K, 'value');

F = ampiezza;

theta10 = get(handles.theta1, 'value')*pi/180.0;
omega10 = get(handles.omega1, 'value')*pi/180.0;
theta20 = get(handles.theta2, 'value')*pi/180.0;
omega20 = get(handles.omega2, 'value')*pi/180.0;


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'L', L; 'M', M; 'theta10', theta10; 'theta20', theta20; 'K', K; 'F', F; 'omega10', omega10; 'omega20', omega20};
for i = 1 : size(vars,1)
    name  = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il Modello in Simulink
simulink('open');
simulink('close');

% Apre il sistema da simulare
open_system(uscita);

% verifica e pulisce il segnale in input al sistema
h = find_system(uscita,'Name','input');
if ( size(h)==[1,1])
    delete_line(uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(uscita);
    if numel(find(strcmp(system_blocks, [uscita '/step1']))) > 0
        delete_line(uscita, 'step1/1', 'input/1');
        delete_line(uscita, 'step2/1', 'input/2');
    end
    delete_block([uscita,'/input']);
    if numel(find(strcmp(system_blocks, [uscita '/step1']))) > 0
        delete_block([uscita,'/step1']);
        delete_block([uscita,'/step2']);
    end
end

% Legge qual � il nuovo segnale in input da simulare
val = get(handles.tipoIngresso,'Value');
switch val
    case 1
        add_block('Simulink/Sources/Step', [uscita,'/input'], 'Time', num2str(1/frequenza));
    case 2
        add_block('Simulink/Sources/Step', [uscita,'/step1'], 'Time', num2str(1/frequenza));
        add_block('Simulink/Sources/Step', [uscita,'/step2'], 'Time', num2str(1/frequenza+1e-3));
        add_block('Simulink/Math Operations/Sum', [uscita,'/input'], 'Inputs', '+-');
    case 3
        add_block('Simulink/Sources/Pulse Generator', [uscita,'/input'], 'Period', num2str(1/frequenza), 'PulseWidth', num2str(frequenza*0.1));
    case 4
        add_block('Simulink/Sources/Sine Wave', [uscita,'/input'], 'Frequency', num2str(2*pi*frequenza));
    case 5
        add_block('Simulink/Sources/Pulse Generator', [uscita,'/input'], 'Period', num2str(1/frequenza), 'PulseWidth', num2str(dutycycle));
    case 6
        add_block('Simulink/Sources/Repeating Sequence', [uscita,'/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico lo sfondo e la posizione del blocco inserito
set_param([uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(uscita);

% Avvia la simulazione
sim(uscita);


%%
% --- Executes on slider movement.
function K_Callback(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actK,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function minK_Callback(hObject, eventdata, handles)
% hObject    handle to minK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.K,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of minK as text
%        str2double(get(hObject,'String')) returns contents of minK as a double


% --- Executes during object creation, after setting all properties.
function minK_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actK_Callback(hObject, eventdata, handles)
% hObject    handle to actK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.K,'Value',str2double(get(hObject,'String')));

% Hints: get(hObject,'String') returns contents of actK as text
%        str2double(get(hObject,'String')) returns contents of actK as a double


% --- Executes during object creation, after setting all properties.
function actK_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function maxK_Callback(hObject, eventdata, handles)
% hObject    handle to maxK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.K,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of maxK as text
%        str2double(get(hObject,'String')) returns contents of maxK as a double


% --- Executes during object creation, after setting all properties.
function maxK_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function theta1_Callback(hObject, eventdata, handles)
% hObject    handle to theta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.acttheta1,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function theta1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Mintheta1_Callback(hObject, eventdata, handles)
% hObject    handle to Mintheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta1,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Mintheta1 as text
%        str2double(get(hObject,'String')) returns contents of Mintheta1 as a double


% --- Executes during object creation, after setting all properties.
function Mintheta1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mintheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function acttheta1_Callback(hObject, eventdata, handles)
% hObject    handle to acttheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta1,'Value',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of acttheta1 as text
%        str2double(get(hObject,'String')) returns contents of acttheta1 as a double


% --- Executes during object creation, after setting all properties.
function acttheta1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acttheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Maxtheta1_Callback(hObject, eventdata, handles)
% hObject    handle to Maxtheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta1,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Maxtheta1 as text
%        str2double(get(hObject,'String')) returns contents of Maxtheta1 as a double


% --- Executes during object creation, after setting all properties.
function Maxtheta1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Maxtheta1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function theta2_Callback(hObject, eventdata, handles)
% hObject    handle to theta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.acttheta2,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function theta2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Mintheta2_Callback(hObject, eventdata, handles)
% hObject    handle to Mintheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta2,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Mintheta2 as text
%        str2double(get(hObject,'String')) returns contents of Mintheta2 as a double


% --- Executes during object creation, after setting all properties.
function Mintheta2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mintheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function acttheta2_Callback(hObject, eventdata, handles)
% hObject    handle to acttheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta2,'Value',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of acttheta2 as text
%        str2double(get(hObject,'String')) returns contents of acttheta2 as a double


% --- Executes during object creation, after setting all properties.
function acttheta2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acttheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Maxtheta2_Callback(hObject, eventdata, handles)
% hObject    handle to Maxtheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.theta2,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Maxtheta2 as text
%        str2double(get(hObject,'String')) returns contents of Maxtheta2 as a double


% --- Executes during object creation, after setting all properties.
function Maxtheta2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Maxtheta2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('SchemaPendoli.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate Schema


%%
% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('ModelloPendoli.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate modello


%%
% --- Executes on slider movement.
function omega1_Callback(hObject, eventdata, handles)
% hObject    handle to omega1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actom,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function omega1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Minom_Callback(hObject, eventdata, handles)
% hObject    handle to Minom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega1,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Minom as text
%        str2double(get(hObject,'String')) returns contents of Minom as a double


% --- Executes during object creation, after setting all properties.
function Minom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Minom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actom_Callback(hObject, eventdata, handles)
% hObject    handle to actom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega1,'Value',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of actom as text
%        str2double(get(hObject,'String')) returns contents of actom as a double


% --- Executes during object creation, after setting all properties.
function actom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Maxom_Callback(hObject, eventdata, handles)
% hObject    handle to Maxom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega1,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Maxom as text
%        str2double(get(hObject,'String')) returns contents of Maxom as a double


% --- Executes during object creation, after setting all properties.
function Maxom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Maxom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function omega2_Callback(hObject, eventdata, handles)
% hObject    handle to omega2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.actom2,'String',num2str(get(hObject,'Value'), '%.1f'));
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function omega2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Minom2_Callback(hObject, eventdata, handles)
% hObject    handle to Minom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega2,'Min',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Minom2 as text
%        str2double(get(hObject,'String')) returns contents of Minom2 as a double


% --- Executes during object creation, after setting all properties.
function Minom2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Minom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function actom2_Callback(hObject, eventdata, handles)
% hObject    handle to actom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega2,'Value',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of actom2 as text
%        str2double(get(hObject,'String')) returns contents of actom2 as a double


% --- Executes during object creation, after setting all properties.
function actom2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to actom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Maxom2_Callback(hObject, eventdata, handles)
% hObject    handle to Maxom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set (handles.omega2,'Max',str2double(get(hObject,'String')));
% Hints: get(hObject,'String') returns contents of Maxom2 as text
%        str2double(get(hObject,'String')) returns contents of Maxom2 as a double


% --- Executes during object creation, after setting all properties.
function Maxom2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Maxom2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%chiudo il Modello simulink senza salvare
bdclose('all');
% Hint: delete(hObject) closes the figure
delete(hObject);




% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on button press in punto_eq.
function punto_eq_Callback(hObject, eventdata, handles)
% hObject    handle to punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% FUNZIONE SBAGLIATA IN GENERALE!!!!!!!!!!
% richiede soluzione equazioni trigonometriche
% per valutare i veri punti di equilibrio in qualunque situazione
% anche nelle zone di compportamento non lineare


set(handles.tipoIngresso, 'Value', 1);

% Leggo i dati inseriti dall'utente
L = get(handles.L, 'value');
M = get(handles.M, 'value');
K = get(handles.K, 'value');

F = get(handles.F, 'value');
g = 9.81;

u_eq = F;
mat_A = [0 0 1 0; 0 0 0 1; (-g/L-K/(4*M)) K/(4*M) 0 0; K/(4*M) (-g/L-K/(4*M)) 0 0];
mat_B = [0; 0; 1/(2*M*L); 0];

%Scriviamo l'equazione
%Esplicitiamo l'equazione 0 = mat_A*x_eq+mat_B*u_eq nell'incognita x_eq
x_eq = -inv(mat_A)*mat_B*u_eq;

autovals = eig(mat_A);
%disp(autovals);

x_eq = x_eq * 180.0/pi;
x_eq = round(x_eq*100)/100;
u_eq = round(u_eq*100)/100;

if abs(x_eq(1)) <= 4 && abs(x_eq(2)) <= 4
    if autovals(:,1) <= 0
        set(handles.punto_eq_text, 'String', {'Lo stato: ';...
            ['theta1 = ', num2str(x_eq(1)), '�', ', theta2 = ', num2str(x_eq(2)), '�'];...
            ['omega1 = ', num2str(x_eq(3)), '�/s', ', omega2 = ', num2str(x_eq(4)), '�/s'];...
            ' ';...
            'e'' stabile in corrispondenza all''ingresso: ';...
            ['F = ', num2str(u_eq(1)), ' N']});
    else
        set(handles.punto_eq_text, 'String', {'Lo stato: ';...
            ['theta1 = ', num2str(x_eq(1)), '�', ', theta2 = ', num2str(x_eq(2)), '�'];...
            ['omega1 = ', num2str(x_eq(3)), '�/s', ', omega2 = ', num2str(x_eq(4)), '�/s'];...
            ' ';...
            'e'' instabile in corrispondenza all''ingresso: ';...
            ['F = ', num2str(u_eq(1)), ' N']});
    end
else
  set(handles.punto_eq_text, 'String', {'Lo stato di equilibrio non � nell''intervallo di validit� del modello linearizzato.'});
end

% --- Executes on selection change in parametroIngresso.
function parametroIngresso_Callback(hObject, eventdata, handles)
% hObject    handle to parametroIngresso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
parametri = get(handles.parametroIngresso, 'String');
param_name = parametri{get(handles.parametroIngresso, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.F, 'Min', values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max', values(3));
set(handles.minF, 'String', num2str(values(1), '%.1f'));
set(handles.actF, 'String', num2str(values(2), '%.1f'));
set(handles.maxF, 'String', num2str(values(3), '%.1f'));


% --- Executes during object creation, after setting all properties.
function parametroIngresso_CreateFcn(hObject, eventdata, handles)
% hObject    handle to parametroIngresso (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
