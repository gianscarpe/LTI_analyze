%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               P E N D O L I   A C C O P P I A T I   4.5                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alessandro Degli Agosti (2008)
% by Barbara Bertani (2015)
% by F. M. Marchese (2008-17)
%
% Tested under MatLab R2013b
%


%% INIT
%
function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 27-Sep-2016 16:34:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        %  1 - IngressoTipo
      1        %  2 - IngressoPar
      1        %  3 - Uscita
      -10.0    %  4 - F_min
      1.0      %  5 - F, F_cur
      10.0     %  6 - F_max
      0.001     %  7 - L_min
      0.1      %  8 - L, L_cur
      10.0     %  9 - L_max
      0.0      % 10 - M_min
      1.0      % 11 - M, M_cur
      10.0     % 12 - M_max
      0.0      % 13 - k_min
      5000.0      % 14 - k, k_cur
      10000.0  % 15 - k_max
      -90.0    % 16 - theta10_min
      0.0      % 17 - theta10, theta10_cur
      +90.0    % 18 - theta10_max
      -45.0    % 19 - omega10_min
      0.0      % 20 - omega10, omega10_cur
      +45.0    % 21 - omega10_max      
      -90.0    % 22 - theta20_min
      0.0      % 23 - theta20, theta20_cur
      +90.0    % 24 - theta20_max      
      -45.0    % 25 - omega20_min
      0.0      % 26 - omega20, omega20_cur
      +45.0    % 27 - omega20_max        
      ];

% Par degli slider
global Fsld Lsld Msld ksld theta10sld omega10sld theta20sld omega20sld;

Fsld.stmin = 0.1;
Fsld.stmax = 1;
Fsld.Llim = -Inf;
Fsld.Hlim = +Inf;

Lsld.stmin = 0.1;
Lsld.stmax = 1;
Lsld.Llim = eps;
Lsld.Hlim = +Inf;

Msld.stmin = 0.1;
Msld.stmax = 1;
Msld.Llim = eps;
Msld.Hlim = +Inf;

ksld.stmin = 1;
ksld.stmax = 100;
ksld.Llim = eps;
ksld.Hlim = +Inf;

theta10sld.stmin = 1;
theta10sld.stmax = 10;
theta10sld.Llim = -180;
theta10sld.Hlim = +180;

omega10sld.stmin = 1;
omega10sld.stmax = 5;
omega10sld.Llim = -Inf;
omega10sld.Hlim = +Inf;

theta20sld.stmin = 1;
theta20sld.stmax = 10;
theta20sld.Llim = -180;
theta20sld.Hlim = +180;

omega20sld.stmin = 1;
omega20sld.stmax = 5;
omega20sld.Llim = -Inf;
omega20sld.Hlim = +Inf;

Load_Defaults(handles);


% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


evalin('base', 'input_params = containers.Map();');
evalin('base', ['input_params(''Ampiezza [N]'') = ' mat2str([get(handles.F, 'Min') get(handles.F, 'Value') get(handles.F, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.Dialog);



% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

global Uscita;

% Chiudo il Modello simulink senza salvare
bdclose(Uscita);

% Leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = 'Pendoli1';
    case 2
        Uscita = 'Pendoli2';
    case 3
        Uscita = 'Energia';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');


% Leggo i dati inseriti dall'utente
L = get(handles.L, 'value');
M = get(handles.M, 'value');
k = get(handles.k, 'value');
F = ampiezza(2);
g = 9.81;

theta10 = get(handles.theta10, 'value')*pi/180.0;
omega10 = get(handles.omega10, 'value')*pi/180.0;
theta20 = get(handles.theta20, 'value')*pi/180.0;
omega20 = get(handles.omega20, 'value')*pi/180.0;

% controllo sui dati nulli
if L == 0, L = eps; end
if M == 0, M = eps; end
if k < 0, k = 0; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% x_sym(1) <-> theta1 [rad]
% x_sym(2) <-> omega1 [rad/s]
% x_sym(3) <-> theta2 [rad]
% x_sym(4) <-> omega2 [rad/s]
x_sym = sym('x_sym', [4 1]);
dx_sym = [x_sym(2); 
          -(g/L+k/4/M) * sin(x_sym(1)) + k/4/M * sin(x_sym(3)) + F/(2*M*L);
          x_sym(4);
          +k/4/M * sin(x_sym(1)) - (g/L+k/4/M) * sin(x_sym(3))             ];
X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3), x_sym(4), 'IgnoreAnalyticConstraints', true);
% Calcolo dello jacobiano simbolico (linearizzazione)
F_mat = jacobian(dx_sym);
x = double([X.x_sym1(1); X.x_sym2(1); X.x_sym3(1); X.x_sym4(1)]);
F_mat = double(subs(F_mat, x_sym, x));

% Calcolo costanti di tempo
Tau = TimeConstantLTI(F_mat);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(0.05, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'L', L; 'M', M; 'g', g; 'theta10', theta10; 'theta20', theta20; 'k', k; 'F', F; 'omega10', omega10; 'omega20', omega20};
for i = 1 : size(vars, 1)
  name  = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end

% Apre il sistema da simulare
open_system(Uscita);


% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Legge qual � il nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;


% Modifico la durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end


% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end


% Aggiungo il collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.F_min, 'Value',  ampiezza(1));
set(handles.F_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.F_cur, 'Value',  ampiezza(2));
set(handles.F_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.F_max, 'Value',  ampiezza(3));
set(handles.F_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.F, 'Min',   ampiezza(1));
set(handles.F, 'Value', ampiezza(2));
set(handles.F, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
% Leggo i dati inseriti dall'utente
L = get(handles.L, 'value');
M = get(handles.M, 'value');
k = get(handles.k, 'value');
F = get(handles.F, 'value');
g = 9.81;
u = F * L / 2;

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if L <= 0, L = eps; end
if M <= 0, M = eps; end
if k < 0, k = 0; end

% Controllo dell'esistenza di un punto di equilibrio 
% ???

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)

% Impostazione dell'equazione dinamica non lineare
% x_sym(1) <-> theta1 [rad]
% x_sym(2) <-> omega1 [rad/s]
% x_sym(3) <-> theta2 [rad]
% x_sym(4) <-> omega2 [rad/s]
x_sym = sym('x_sym', [4 1]);

% dtheta1/dt = omega1
% I*domega1/dt = -M*g*L*sin(theta1) + k*L^2/4*(sin(theta2) - sin(theta1)) + F*L/2
% dtheta2/dt = omega2
% I*domega2/dt = -M*g*L*sin(theta2) - k*L^2/4*(sin(theta2) - sin(theta1))
% I = M*L^2
% dx_sym = [x_sym(2); 
%           (-M*g*L*sin(x_sym(1)) + k/4*L^2*(sin(x_sym(3))-sin(x_sym(1))) + u) / (M*L^2);
%           x_sym(4);
%           (-M*g*L*sin(x_sym(3)) - k/4*L^2*(sin(x_sym(3))-sin(x_sym(1)))) / (M*L^2)];
dx_sym = [x_sym(2); 
          -(g/L+k/4/M) * sin(x_sym(1)) +       k/4/M * sin(x_sym(3)) + F/(2*M*L);
          x_sym(4);
                +k/4/M * sin(x_sym(1)) - (g/L+k/4/M) * sin(x_sym(3))             ];
%%%% SOLVE senza il flag 'IgnoreAnalyticConstraints'=true NON FORNISCE LE SOLUZIONI CORRETTE DI QUESTE EQUAZIONI!!!
%%%% Allora e' meglio usare linearizzazione intorno a 0:
% dx_sym = [x_sym(2); 
%           -(g/L+k/4/M) * x_sym(1) +       k/4/M * x_sym(3) + F/(2*M*L);
%           x_sym(4);
%                 +k/4/M * x_sym(1) - (g/L+k/4/M) * x_sym(3)             ];
              
% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3), x_sym(4), 'IgnoreAnalyticConstraints', true);
% Solo il val principale (no periodicita')
% [x(1) x(2)] = solve(dx_sym == 0, x_sym(1), x_sym(2), 'PrincipalValue', true);
% x = double(x)';

if size(X.x_sym1) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['F = ', num2str(F, '%.1f'), ' N']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);


% Preparazione testo da visualizzare
set(handles.Punto_Eq_txt, 'String', '');
str = sprintf('In presenza dell''ingresso: F = %.1f N', F);

for j = 1 : size(X.x_sym1, 1)
  % Soluzione numerica
  x = double([X.x_sym1(j); X.x_sym2(j); X.x_sym3(j); X.x_sym4(j)]);

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intorno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X.x_sym1, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  theta1 = %.2f�    theta2 = %.2f�', x(1)*180.0/pi, x(3)*180.0/pi);
  str22 = sprintf('\n  omega1 = %.2f�/s  omega2 = %.2f�/s', x(2)*180.0/pi, x(4)*180.0/pi);

  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
  end        

  if j == size(X.x_sym1, 1), endstr = '.'; else endstr = ';'; end
  str = strcat(str, str1, str21, str22, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);



% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject,'String')) returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider k
% --- Executes on slider movement.
function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.k, handles.k_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function k_min_Callback(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ksld
Slider_min_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function k_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_cur_Callback(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ksld;
Slider_cur_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function k_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_max_Callback(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ksld;
Slider_max_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function k_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider L
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.L, handles.L_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function L_min_Callback(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Lsld
Slider_min_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function L_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_cur_Callback(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_cur_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, 0.1, 1, eps, +Inf);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_max_Callback(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Lsld;
Slider_max_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function L_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider F
% --- Executes on slider movement.
function F_Callback(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.F, handles.F_cur);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function F_min_Callback(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld
Slider_min_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function F_cur_Callback(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld;
Slider_cur_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function F_max_Callback(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Fsld;
Slider_max_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider IngressoTipo
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [N]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min',   values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max',   values(3));


% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.1f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min',   values(1));
set(handles.F, 'Max',   values(3));
set(handles.F, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider omega20
% --- Executes on slider movement.
function omega20_Callback(hObject, eventdata, handles)
% hObject    handle to omega20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.omega20, handles.omega20_cur);


% --- Executes during object creation, after setting all properties.
function omega20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function omega20_min_Callback(hObject, eventdata, handles)
% hObject    handle to omega20_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega20sld
Slider_min_Callback(handles, handles.omega20, handles.omega20_min, handles.omega20_cur, handles.omega20_max, omega20sld.stmin, omega20sld.stmax, omega20sld.Llim, omega20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega20_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega20_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function omega20_cur_Callback(hObject, eventdata, handles)
% hObject    handle to omega20_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega20sld;
Slider_cur_Callback(handles, handles.omega20, handles.omega20_min, handles.omega20_cur, handles.omega20_max, omega20sld.stmin, omega20sld.stmax, omega20sld.Llim, omega20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega20_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega20_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function omega20_max_Callback(hObject, eventdata, handles)
% hObject    handle to omega20_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega20sld;
Slider_max_Callback(handles, handles.omega20, handles.omega20_min, handles.omega20_cur, handles.omega20_max, omega20sld.stmin, omega20sld.stmax, omega20sld.Llim, omega20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega20_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega20_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider theta20
% --- Executes on slider movement.
function theta20_Callback(hObject, eventdata, handles)
% hObject    handle to theta20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.theta20, handles.theta20_cur);


% --- Executes during object creation, after setting all properties.
function theta20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function theta20_min_Callback(hObject, eventdata, handles)
% hObject    handle to theta20_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta20sld
Slider_min_Callback(handles, handles.theta20, handles.theta20_min, handles.theta20_cur, handles.theta20_max, theta20sld.stmin, theta20sld.stmax, theta20sld.Llim, theta20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta20_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta20_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta20_cur_Callback(hObject, eventdata, handles)
% hObject    handle to theta20_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta20sld;
Slider_cur_Callback(handles, handles.theta20, handles.theta20_min, handles.theta20_cur, handles.theta20_max, theta20sld.stmin, theta20sld.stmax, theta20sld.Llim, theta20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta20_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta20_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta20_max_Callback(hObject, eventdata, handles)
% hObject    handle to theta20_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta20sld;
Slider_max_Callback(handles, handles.theta20, handles.theta20_min, handles.theta20_cur, handles.theta20_max, theta20sld.stmin, theta20sld.stmax, theta20sld.Llim, theta20sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta20_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta20_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider omega10
% --- Executes on slider movement.
function omega10_Callback(hObject, eventdata, handles)
% hObject    handle to omega10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.omega10, handles.omega10_cur);


% --- Executes during object creation, after setting all properties.
function omega10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function omega10_min_Callback(hObject, eventdata, handles)
% hObject    handle to omega10_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega10sld
Slider_min_Callback(handles, handles.omega10, handles.omega10_min, handles.omega10_cur, handles.omega10_max, omega10sld.stmin, omega10sld.stmax, omega10sld.Llim, omega10sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega10_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega10_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function omega10_cur_Callback(hObject, eventdata, handles)
% hObject    handle to omega10_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega10sld;
Slider_cur_Callback(handles, handles.omega10, handles.omega10_min, handles.omega10_cur, handles.omega10_max, omega10sld.stmin, omega10sld.stmax, omega10sld.Llim, omega10sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega10_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega10_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function omega10_max_Callback(hObject, eventdata, handles)
% hObject    handle to omega10_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega10sld;
Slider_max_Callback(handles, handles.omega10, handles.omega10_min, handles.omega10_cur, handles.omega10_max, omega10sld.stmin, omega10sld.stmax, omega10sld.Llim, omega10sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega10_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega10_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider theta10
% --- Executes on slider movement.
function theta10_Callback(hObject, eventdata, handles)
% hObject    handle to theta10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.theta10, handles.theta10_cur);



% --- Executes during object creation, after setting all properties.
function theta10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function theta10_min_Callback(hObject, eventdata, handles)
% hObject    handle to theta10_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta10sld
Slider_min_Callback(handles, handles.theta10, handles.theta10_min, handles.theta10_cur, handles.theta10_max, theta10sld.stmin, theta10sld.stmax, theta10sld.Llim, theta10sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta10_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta10_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function theta10_cur_Callback(hObject, eventdata, handles)
% hObject    handle to theta10_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta10sld;
Slider_cur_Callback(handles, handles.theta10, handles.theta10_min, handles.theta10_cur, handles.theta10_max, theta10sld.stmin, theta10sld.stmax, theta10sld.Llim, theta10sld.Hlim);



% --- Executes during object creation, after setting all properties.
function theta10_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta10_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function theta10_max_Callback(hObject, eventdata, handles)
% hObject    handle to theta10_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta10sld;
Slider_max_Callback(handles, handles.theta10, handles.theta10_min, handles.theta10_cur, handles.theta10_max, theta10sld.stmin, theta10sld.stmax, theta10sld.Llim, theta10sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta10_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta10_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
axis equal;


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Fsld Lsld Msld ksld theta10sld omega10sld theta20sld omega20sld;
set(handles.L_min, 'Value',  L_min);
set(handles.L_min, 'String', num2str(L_min, '%.1f'));
set(handles.L_cur, 'Value',  L_cur);
set(handles.L_cur, 'String', num2str(L_cur, '%.2f'));
set(handles.L_max, 'Value',  L_max);
set(handles.L_max, 'String', num2str(L_max, '%.1f'));

set(handles.L, 'Min',   L_min);
set(handles.L, 'Value', L_cur);
set(handles.L, 'Max',   L_max);
majorstep = Lsld.stmax / (L_max-L_min);
minorstep = Lsld.stmin / (L_max-L_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

set(handles.k_min, 'Value',  k_min);
set(handles.k_min, 'String', num2str(k_min, '%.1f'));
set(handles.k_cur, 'Value',  k_cur);
set(handles.k_cur, 'String', num2str(k_cur, '%.2f'));
set(handles.k_max, 'Value',  k_max);
set(handles.k_max, 'String', num2str(k_max, '%.1f'));

set(handles.k, 'Min',   k_min);
set(handles.k, 'Value', k_cur);
set(handles.k, 'Max',   k_max);
majorstep = ksld.stmax / (k_max-k_min);
minorstep = ksld.stmin / (k_max-k_min);
set(handles.k, 'SliderStep', [minorstep majorstep]);

% stati
set(handles.theta10_min, 'Value',  theta10_min);
set(handles.theta10_min, 'String', num2str(theta10_min, '%.1f'));
set(handles.theta10_cur, 'Value',  theta10_cur);
set(handles.theta10_cur, 'String', num2str(theta10_cur, '%.2f'));
set(handles.theta10_max, 'Value',  theta10_max);
set(handles.theta10_max, 'String', num2str(theta10_max, '%.1f'));

set(handles.theta10, 'Min',   theta10_min);
set(handles.theta10, 'Value', theta10_cur);
set(handles.theta10, 'Max',   theta10_max);
majorstep = theta10sld.stmax / (theta10_max-theta10_min);
minorstep = theta10sld.stmin / (theta10_max-theta10_min);
set(handles.theta10, 'SliderStep', [minorstep majorstep]);

set(handles.omega10_min, 'Value',  omega10_min);
set(handles.omega10_min, 'String', num2str(omega10_min, '%.1f'));
set(handles.omega10_cur, 'Value',  omega10_cur);
set(handles.omega10_cur, 'String', num2str(omega10_cur, '%.2f'));
set(handles.omega10_max, 'Value',  omega10_max);
set(handles.omega10_max, 'String', num2str(omega10_max, '%.1f'));

set(handles.omega10, 'Min',   omega10_min);
set(handles.omega10, 'Value', omega10_cur);
set(handles.omega10, 'Max',   omega10_max);
majorstep = omega10sld.stmax / (omega10_max-omega10_min);
minorstep = omega10sld.stmin / (omega10_max-omega10_min);
set(handles.omega10, 'SliderStep', [minorstep majorstep]);

set(handles.theta20_min, 'Value',  theta20_min);
set(handles.theta20_min, 'String', num2str(theta20_min, '%.1f'));
set(handles.theta20_cur, 'Value',  theta20_cur);
set(handles.theta20_cur, 'String', num2str(theta20_cur, '%.2f'));
set(handles.theta20_max, 'Value',  theta20_max);
set(handles.theta20_max, 'String', num2str(theta20_max, '%.1f'));

set(handles.theta20, 'Min',   theta20_min);
set(handles.theta20, 'Value', theta20_cur);
set(handles.theta20, 'Max',   theta20_max);
majorstep = theta20sld.stmax / (theta20_max-theta20_min);
minorstep = theta20sld.stmin / (theta20_max-theta20_min);
set(handles.theta20, 'SliderStep', [minorstep majorstep]);

set(handles.omega20_min, 'Value',  omega20_min);
set(handles.omega20_min, 'String', num2str(omega20_min, '%.1f'));
set(handles.omega20_cur, 'Value',  omega20_cur);
set(handles.omega20_cur, 'String', num2str(omega20_cur, '%.2f'));
set(handles.omega20_max, 'Value',  omega20_max);
set(handles.omega20_max, 'String', num2str(omega20_max, '%.1f'));

set(handles.omega20, 'Min',   omega20_min);
set(handles.omega20, 'Value', omega20_cur);
set(handles.omega20, 'Max',   omega20_max);
majorstep = omega20sld.stmax / (omega20_max-omega20_min);
minorstep = omega20sld.stmin / (omega20_max-omega20_min);
set(handles.omega20, 'SliderStep', [minorstep majorstep]);

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.F_min, 'Value',  F_min);
set(handles.F_min, 'String', num2str(F_min, '%.1f'));
set(handles.F_cur, 'Value',  F_cur);
set(handles.F_cur, 'String', num2str(F_cur, '%.2f'));
set(handles.F_max, 'Value',  F_max);
set(handles.F_max, 'String', num2str(F_max, '%.1f'));

set(handles.F, 'Min',   F_min);
set(handles.F, 'Value', F_cur);
set(handles.F, 'Max',   F_max);
majorstep = Fsld.stmax / (F_max-F_min);
minorstep = Fsld.stmin / (F_max-F_min);
set(handles.F, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello lunghezza aste
L_min = get(handles.L_min, 'Value');
L_cur = get(handles.L_cur, 'Value');
L_max = get(handles.L_max, 'Value');

% pannello masse pendoli
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello costante elastica
k_min = get(handles.k_min, 'Value');
k_cur = get(handles.k_cur, 'Value');
k_max = get(handles.k_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

F_min = get(handles.F_min, 'Value');
F_cur = get(handles.F_cur, 'Value');
F_max = get(handles.F_max, 'Value');

% pannello stato iniziale
theta10_min = get(handles.theta10_min, 'Value');
theta10_cur = get(handles.theta10_cur, 'Value');
theta10_max = get(handles.theta10_max, 'Value');

omega10_min = get(handles.omega10_min, 'Value');
omega10_cur = get(handles.omega10_cur, 'Value');
omega10_max = get(handles.omega10_max, 'Value');

theta20_min = get(handles.theta20_min, 'Value');
theta20_cur = get(handles.theta20_cur, 'Value');
theta20_max = get(handles.theta20_max, 'Value');

omega20_min = get(handles.omega20_min, 'Value');
omega20_cur = get(handles.omega20_cur, 'Value');
omega20_max = get(handles.omega20_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'F_min = %f;\n', F_min);
fprintf(fid, 'F_cur = %f;\n', F_cur);
fprintf(fid, 'F_max = %f;\n', F_max);

fprintf(fid, 'L_min = %f;\n', L_min);
fprintf(fid, 'L_cur = %f;\n', L_cur);
fprintf(fid, 'L_max = %f;\n', L_max);

fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'k_min = %f;\n', k_min);
fprintf(fid, 'k_cur = %f;\n', k_cur);
fprintf(fid, 'k_max = %f;\n', k_max);

fprintf(fid, 'theta10_min = %f;\n', theta10_min);
fprintf(fid, 'theta10_cur = %f;\n', theta10_cur);
fprintf(fid, 'theta10_max = %f;\n', theta10_max);

fprintf(fid, 'omega10_min = %f;\n', omega10_min);
fprintf(fid, 'omega10_cur = %f;\n', omega10_cur);
fprintf(fid, 'omega10_max = %f;\n', omega10_max);

fprintf(fid, 'theta20_min = %f;\n', theta20_min);
fprintf(fid, 'theta20_cur = %f;\n', theta20_cur);
fprintf(fid, 'theta20_max = %f;\n', theta20_max);

fprintf(fid, 'omega20_min = %f;\n', omega20_min);
fprintf(fid, 'omega20_cur = %f;\n', omega20_cur);
fprintf(fid, 'omega20_max = %f;\n', omega20_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Fsld Lsld Msld ksld theta10sld omega10sld theta20sld omega20sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider F
set(handles.F_min, 'Value', def(4));
set(handles.F_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.F_cur, 'Value', def(5));
set(handles.F_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.F_max, 'Value', def(6));
set(handles.F_max, 'String', num2str(def(6), '%.1f'));

set(handles.F, 'Min',   def(4)); 
set(handles.F, 'Value', def(5));
set(handles.F, 'Max',   def(6)); 
majorstep = Fsld.stmax / (def(6)-def(4));
minorstep = Fsld.stmin / (def(6)-def(4));
set(handles.F, 'SliderStep', [minorstep majorstep]);

% slider L
set(handles.L_min, 'Value',  def(7));
set(handles.L_min, 'String', num2str(def(7), '%.1f'));
set(handles.L_cur, 'Value',  def(8));
set(handles.L_cur, 'String', num2str(def(8), '%.2f'));
set(handles.L_max, 'Value',  def(9));
set(handles.L_max, 'String', num2str(def(9), '%.1f'));

set(handles.L, 'Min',   def(7));
set(handles.L, 'Value', def(8));
set(handles.L, 'Max',   def(9));
majorstep = Lsld.stmax / (def(9)-def(7));
minorstep = Lsld.stmin / (def(9)-def(7));
set(handles.L, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value',  def(10));
set(handles.M_min, 'String', num2str(def(10), '%.1f'));
set(handles.M_cur, 'Value',  def(11));
set(handles.M_cur, 'String', num2str(def(11), '%.2f'));
set(handles.M_max, 'Value',  def(12));
set(handles.M_max, 'String', num2str(def(12), '%.1f'));

set(handles.M, 'Min',   def(10));
set(handles.M, 'Value', def(11));
set(handles.M, 'Max',   def(12));
majorstep = Msld.stmax / (def(12)-def(10));
minorstep = Msld.stmin / (def(12)-def(10));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider k
set(handles.k_min, 'Value',  def(13));
set(handles.k_min, 'String', num2str(def(13), '%.1f'));
set(handles.k_cur, 'Value',  def(14));
set(handles.k_cur, 'String', num2str(def(14), '%.2f'));
set(handles.k_max, 'Value',  def(15));
set(handles.k_max, 'String', num2str(def(15), '%.1f'));

set(handles.k, 'Min',   def(13));
set(handles.k, 'Value', def(14));
set(handles.k, 'Max',   def(15));
majorstep = ksld.stmax / (def(15)-def(13));
minorstep = ksld.stmin / (def(15)-def(13));
set(handles.k, 'SliderStep', [minorstep majorstep]);

% slider theta10
set(handles.theta10_min, 'Value', def(16));
set(handles.theta10_min, 'String', num2str(def(16), '%.1f'));
set(handles.theta10_cur, 'Value', def(17));
set(handles.theta10_cur, 'String', num2str(def(17), '%.2f'));
set(handles.theta10_max, 'Value', def(18));
set(handles.theta10_max, 'String', num2str(def(18), '%.1f'));

set(handles.theta10, 'Min',   def(16)); 
set(handles.theta10, 'Value', def(17));
set(handles.theta10, 'Max',   def(18)); 
majorstep = theta10sld.stmax / (def(18)-def(16));
minorstep = theta10sld.stmin / (def(18)-def(16));
set(handles.theta10, 'SliderStep', [minorstep majorstep]);

% slider omega10
set(handles.omega10_min, 'Value', def(19));
set(handles.omega10_min, 'String', num2str(def(19), '%.1f'));
set(handles.omega10_cur, 'Value', def(20));
set(handles.omega10_cur, 'String', num2str(def(20), '%.2f'));
set(handles.omega10_max, 'Value', def(21));
set(handles.omega10_max, 'String', num2str(def(21), '%.1f'));

set(handles.omega10, 'Min',   def(19)); 
set(handles.omega10, 'Value', def(20));
set(handles.omega10, 'Max',   def(21)); 
majorstep = omega10sld.stmax / (def(21)-def(19));
minorstep = omega10sld.stmin / (def(21)-def(19));
set(handles.omega10, 'SliderStep', [minorstep majorstep]);

% slider theta20
set(handles.theta20_min, 'Value', def(22));
set(handles.theta20_min, 'String', num2str(def(22), '%.1f'));
set(handles.theta20_cur, 'Value', def(23));
set(handles.theta20_cur, 'String', num2str(def(23), '%.2f'));
set(handles.theta20_max, 'Value', def(24));
set(handles.theta20_max, 'String', num2str(def(24), '%.1f'));

set(handles.theta20, 'Min',   def(22)); 
set(handles.theta20, 'Value', def(23));
set(handles.theta20, 'Max',   def(24)); 
majorstep = theta20sld.stmax / (def(24)-def(22));
minorstep = theta20sld.stmin / (def(24)-def(22));
set(handles.theta20, 'SliderStep', [minorstep majorstep]);

% slider omega20
set(handles.omega20_min, 'Value', def(25));
set(handles.omega20_min, 'String', num2str(def(25), '%.1f'));
set(handles.omega20_cur, 'Value', def(26));
set(handles.omega20_cur, 'String', num2str(def(26), '%.2f'));
set(handles.omega20_max, 'Value', def(27));
set(handles.omega20_max, 'String', num2str(def(27), '%.1f'));

set(handles.omega20, 'Min',   def(25)); 
set(handles.omega20, 'Value', def(26));
set(handles.omega20, 'Max',   def(27)); 
majorstep = omega20sld.stmax / (def(27)-def(25));
minorstep = omega20sld.stmin / (def(27)-def(25));
set(handles.omega20, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');
