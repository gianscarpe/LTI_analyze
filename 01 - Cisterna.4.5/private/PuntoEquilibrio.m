%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                P U N T O  D I  E Q U I L I B R I O                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function [x, stb] = PuntoEquilibrio(dx_sym, x_sym)
%
% dx_sym  vector of state equations (n x 1)
% x_sym   vector of state variables (n x 1)
%
% x     solutions vector (n x ?)
% stb   stability values (1 x ?, 1 for each solution)
%
% by F. M. Marchese (2016)
%
% Tested under MatLab R2013b
%


function [x, stb] = PuntoEquilibrio(dx_sym, x_sym)
  x   = NaN;
  stb = NaN;
  
  if nargin ~= 2 || isempty(dx_sym) || isempty(x_sym)
    fprintf('PuntoEquilibrio: wrong parameter(s) number!\n');
    fprintf('Use: PuntoEquilibrio(dx_sym, x_sym)\n');    
    return;
  end

  
  % Soluzioni equazioni diff. con derivate uguagliate a 0
  switch numel(x_sym)
    case 1
      X = solve(dx_sym == 0, x_sym(1));
    case 2
      X = solve(dx_sym == 0, x_sym(1), x_sym(2));
    case 3
      X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3));
  end

  % Calcolo dello jacobiano simbolico (linearizzazione)
  Jac_sym = jacobian(dx_sym);
  
  x = []; stb = [];
  for i = 1 : numel(X.x_sym1)
    % Vettore delle soluzioni
    x = [x double([X.x_sym1(i); X.x_sym2(i)])];
    
    % 1 Soluzione numerica
    x1 = x(:, i);
    
    % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
    % al punto di equilibrio), per calcolare gli autovalori 
    % e la stabilita' del sistema intornno al punto di equilibrio
    F = double(subs(Jac_sym, x_sym, x1));  
    
    stb = [stb StabilityLTI(F)];
  end
end




