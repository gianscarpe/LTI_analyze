%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           P E N D O l O    3.15                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Dalila Virgolino e Marco Zanotti (2015)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
%
function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property', 'Value', ...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK', hObject, ...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 22-Nov-2017 14:59:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1      % IngressoTipo
      1      % IngressoPar
      1      % Uscita
      -10    % mu_min
      1      % mu, mu_cur
      10     % mu_max
      0      % M_min
      1      % M, M_cur
      10     % M_max
      0      % l_min
      1      % l, l_cur
      10     % l_max
      0      % K_min
      1      % K, K_cur
      10     % K_max
      -90    % theta0_min
      0      % theta0, theta0_cur
      +90    % theta0_max
      -45    % omega0_min
      0      % omega0, omega0_cur
      45     % omega0_max
      ];

% Par degli slider
global musld Ksld lsld Msld theta0sld omega0sld;

musld.stmin = 0.1;
musld.stmax = 1;
musld.Llim = -Inf;
musld.Hlim = +Inf;

Ksld.stmin = 0.1;
Ksld.stmax = 1;
Ksld.Llim = eps;
Ksld.Hlim = +Inf;

lsld.stmin = 0.1;
lsld.stmax = 1;
lsld.Llim = eps;
lsld.Hlim = +Inf;

Msld.stmin = 0.1;
Msld.stmax = 1;
Msld.Llim = eps;
Msld.Hlim = +Inf;

theta0sld.stmin = 1;
theta0sld.stmax = 10;
theta0sld.Llim = -Inf;
theta0sld.Hlim = +Inf;

omega0sld.stmin = 1;
omega0sld.stmax = 5;
omega0sld.Llim = -Inf;
omega0sld.Hlim = +Inf;
  
evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);


% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Caricamento delle variabili nel Workspace



% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: configdelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



%% RUN
% --- Executes on button press in run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

% chiudo il modello simulink senza salvare
bdclose(Uscita);

% leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita, 'Value');
switch val1
  case 1
      Uscita = 'Posizione_angolare';
  case 2
      Uscita = 'Energia';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Nm]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
theta0 = get(handles.theta0, 'Value')*pi/180;
omega0 = get(handles.omega0, 'Value')*pi/180;
mu = ampiezza(2);
M = get(handles.M, 'Value');
l = get(handles.l, 'Value');
K = get(handles.K, 'Value');
g = 9.81;

% controllo sui dati nulli
if M == 0, M = eps; end
if l == 0, l = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% dTheta/dt = Omega
% dOmega/dt = -M*g*l/I*sin(Theta) - K/I*Omega + mu/I
% x_sym1 <-> Theta
% x_sym2 <-> Omega
x_sym = sym('x_sym', [2 1]);
dx_sym = [x_sym(2); -g*sin(x_sym(1))/l - K/(M*l*l)*x_sym(2) + mu/(M*l*l)];
X = solve(dx_sym == 0, x_sym(1), x_sym(2));
% Calcolo dello jacobiano simbolico (linearizzazione)
F = jacobian(dx_sym);
x = double([X.x_sym1(1); X.x_sym2(1)]);
F = double(subs(F, x_sym, x));

% Calcolo costanti di tempo nel punto di eq.
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(100, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'mu', mu; 'M', M; 'l', l; 'K', K; 'g', g; 'theta0', theta0; 'omega0', omega0};
for i = 1 : size(vars, 1)
  name = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end


% Apre il sistema da simulare
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end

% Legge qual e' il nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
    case 1  % step
        add_block('Simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(Tau/5));
    case 2  % impulso
        add_block('Simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(Tau/5));
        add_block('Simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(Tau/5+min(0.25*Tau, 0.01)));
        add_block('Simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
    case 3  % treno impulsi
        add_block('Simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
    case 4  % sinusoide
        add_block('Simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
    case 5  % onda quadra
        add_block('Simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
    case 6  % onda dente di sega
        add_block('Simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + Tau/5));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end


% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale;



%% PUNTO D'EQUILIBRIO
% --- Executes on button press in punto_eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Nm]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.mu_min, 'Value',  ampiezza(1));
set(handles.mu_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.mu_cur, 'Value',  ampiezza(2));
set(handles.mu_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.mu_max, 'Value',  ampiezza(3));
set(handles.mu_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.mu, 'Min',   ampiezza(1));
set(handles.mu, 'Value', ampiezza(2));
set(handles.mu, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
mu = get(handles.mu_cur, 'Value'); u = mu;
M = get(handles.M_cur, 'Value');
l = get(handles.l_cur, 'Value');
K = get(handles.K_cur, 'Value');
g = 9.81;

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if M == 0, M = eps; end
if l == 0, l = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% solo se (iff) -1 <= u/Mgl <= +1
if u < -M*g*l || u > +M*g*l
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['mu = ', num2str(u(1), '%.2f'), ' Nm']});
  return
end


% % Scrittura dell'equazione dinamica (vecchio metodo)
% % Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% % x_sym(1) <-> Theta
% % x_sym(2) <-> Omega
% x_sym = sym('x_sym', [2 1]);
% % dTheta/dt = Omega
% % dOmega/dt = -M*g*l*sin(Theta) - K*Omega + mu
% dx_sym = [x_sym(2); -M*g*l*sin(x_sym(1)) - K*x_sym(2) + u];
% % X = solve(dx_sym == 0, x_sym(1), x_sym(2));
% % X = double([X.x_sym1(1); X.x_sym2(1)]);
% [X, stb] = PuntoEquilibrio(dx_sym, x_sym);


% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
syms Theta Omega
dx_sym = [Omega; -M*g*l*sin(Theta) - K*Omega + u];
%dx_sym = [Omega; -M*g*l*Theta - K*Omega + u]; % eq. approssimata lineare
x_sym = ['Theta'; 'Omega'];
[X, stb] = PuntoEquilibrio2(dx_sym, x_sym);


% Visualizzazione del testo col risultato
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['mu = ', num2str(u(1), '%.2f'), ' Nm']});
  return
end

str = sprintf('In presenza dell''ingresso: mu = %.2f Nm', u(1));
[~, numsol] = size(X);
for i = 1 : numsol
  x = X(:, i);
  
  if numsol == 1, str1 = sprintf('\nlo stato:');
  else            str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  theta = %.2f rad   (%.1f�)',   x(1), x(1)*180.0/pi);
  str22 = sprintf('\n  omega = %.2f rad/s (%.1f�/s)', x(2), x(2)*180.0/pi);

  % Stabilita'
  switch stb(i)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
  end        

  if i == numsol, endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str22, str3, endstr);  
end

set(handles.Punto_Eq_txt, 'String', str);



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over punto_eq.
function Punto_Eq_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



%% FIGURE
% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Modello.jpg');
image(im);
axis off;


% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject, 'String')) returns Uscita contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER K
% --- Executes on slider movement.
function K_Callback(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.K, handles.K_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function K_min_Callback(hObject, eventdata, handles)
% hObject    handle to K_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_min_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function K_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function K_cur_Callback(hObject, eventdata, handles)
% hObject    handle to K_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_cur_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function K_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function K_max_Callback(hObject, eventdata, handles)
% hObject    handle to K_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_max_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function K_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER l
% --- Executes on slider movement.
function l_Callback(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.l, handles.l_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function l_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function l_min_Callback(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_min_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function l_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function l_cur_Callback(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_cur_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function l_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function l_max_Callback(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_max_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function l_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject, 'String')) returns IngressoTipo contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from IngressoTipo

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [Nm]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.mu_min, 'Value',  values(1));
set(handles.mu_min, 'String', num2str(values(1), '%.1f'));
set(handles.mu_cur, 'Value',  values(2));
set(handles.mu_cur, 'String', num2str(values(2), '%.1f'));
set(handles.mu_max, 'Value',  values(3));
set(handles.mu_max, 'String', num2str(values(3), '%.1f'));
set(handles.mu, 'Min',   values(1));
set(handles.mu, 'Value', values(2));
set(handles.mu, 'Max',   values(3));



% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject, 'String')) returns IngressoPar contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from IngressoPar

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.mu_min, 'Value',  values(1));
set(handles.mu_min, 'String', num2str(values(1), '%.1f'));
set(handles.mu_cur, 'Value',  values(2));
set(handles.mu_cur, 'String', num2str(values(2), '%.2f'));
set(handles.mu_max, 'Value',  values(3));
set(handles.mu_max, 'String', num2str(values(3), '%.1f'));
set(handles.mu, 'Min',   values(1));
set(handles.mu, 'Max',   values(3));
set(handles.mu, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on slider movement.
function mu_Callback(hObject, eventdata, handles)
% hObject    handle to mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.mu, handles.mu_cur);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function mu_min_Callback(hObject, eventdata, handles)
% hObject    handle to mu_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_min_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function mu_max_Callback(hObject, eventdata, handles)
% hObject    handle to mu_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_max_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function mu_cur_Callback(hObject, eventdata, handles)
% hObject    handle to mu_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_cur_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% STATO INIZIALE
% --- Executes on slider movement.
function omega0_Callback(hObject, eventdata, handles)
% hObject    handle to omega0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.omega0, handles.omega0_cur);


% --- Executes during object creation, after setting all properties.
function omega0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function omega0_min_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_min_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function omega0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_cur_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function omega0_max_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_max_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on slider movement.
function theta0_Callback(hObject, eventdata, handles)
% hObject    handle to theta0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.theta0, handles.theta0_cur);


% --- Executes during object creation, after setting all properties.
function theta0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function theta0_min_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_min_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function theta0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_cur_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function theta0_max_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_max_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Ksld lsld Msld musld theta0sld omega0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [Nm]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% slider mu
set(handles.mu_min, 'Value', def(4));
set(handles.mu_min, 'String', num2str(def(4), '%.1f'));
set(handles.mu_cur, 'Value', def(5));
set(handles.mu_cur, 'String', num2str(def(5), '%.2f'));
set(handles.mu_max, 'Value', def(6));
set(handles.mu_max, 'String', num2str(def(6), '%.1f'));

set(handles.mu, 'Min',   def(4)); 
set(handles.mu, 'Value', def(5));
set(handles.mu, 'Max',   def(6)); 
majorstep = musld.stmax / (def(6)-def(4));
minorstep = musld.stmin / (def(6)-def(4));
set(handles.mu, 'SliderStep', [minorstep majorstep]);

evalin('base', ['input_params(''Ampiezza [Nm]'') = ' mat2str([get(handles.mu, 'Min') get(handles.mu, 'Value') get(handles.mu, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% Uscita
set(handles.Uscita, 'Value', def(3));

% slider M
set(handles.M_min, 'Value',  def(7));
set(handles.M_min, 'String', num2str(def(7), '%.1f'));
set(handles.M_cur, 'Value',  def(8));
set(handles.M_cur, 'String', num2str(def(8), '%.2f'));
set(handles.M_max, 'Value',  def(9));
set(handles.M_max, 'String', num2str(def(9), '%.1f'));

set(handles.M, 'Min',   def(7));
set(handles.M, 'Value', def(8));
set(handles.M, 'Max',   def(9));
majorstep = Msld.stmax / (def(9)-def(7));
minorstep = Msld.stmin / (def(9)-def(7));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider l
set(handles.l_min, 'Value',  def(10));
set(handles.l_min, 'String', num2str(def(10), '%.1f'));
set(handles.l_cur, 'Value',  def(11));
set(handles.l_cur, 'String', num2str(def(11), '%.2f'));
set(handles.l_max, 'Value',  def(12));
set(handles.l_max, 'String', num2str(def(12), '%.1f'));

set(handles.l, 'Min',   def(10));
set(handles.l, 'Value', def(11));
set(handles.l, 'Max',   def(12));
majorstep = lsld.stmax / (def(12)-def(10));
minorstep = lsld.stmin / (def(12)-def(10));
set(handles.l, 'SliderStep', [minorstep majorstep]);

% slider K
set(handles.K_min, 'Value',  def(13));
set(handles.K_min, 'String', num2str(def(13), '%.1f'));
set(handles.K_cur, 'Value',  def(14));
set(handles.K_cur, 'String', num2str(def(14), '%.2f'));
set(handles.K_max, 'Value',  def(15));
set(handles.K_max, 'String', num2str(def(15), '%.1f'));

set(handles.K, 'Min',   def(13));
set(handles.K, 'Value', def(14));
set(handles.K, 'Max',   def(15));
majorstep = Ksld.stmax / (def(15)-def(13));
minorstep = Ksld.stmin / (def(15)-def(13));
set(handles.K, 'SliderStep', [minorstep majorstep]);

% slider theta0
set(handles.theta0_min, 'Value',  def(16));
set(handles.theta0_min, 'String', num2str(def(16), '%.1f'));
set(handles.theta0_cur, 'Value',  def(17));
set(handles.theta0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.theta0_max, 'Value',  def(18));
set(handles.theta0_max, 'String', num2str(def(18), '%.1f'));

set(handles.theta0, 'Min',   def(16));
set(handles.theta0, 'Value', def(17));
set(handles.theta0, 'Max',   def(18));
majorstep = theta0sld.stmax / (def(18)-def(16));
minorstep = theta0sld.stmin / (def(18)-def(16));
set(handles.theta0, 'SliderStep', [minorstep majorstep]);

% slider omega0
set(handles.omega0_min, 'Value',  def(19));
set(handles.omega0_min, 'String', num2str(def(19), '%.1f'));
set(handles.omega0_cur, 'Value',  def(20));
set(handles.omega0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.omega0_max, 'Value',  def(21));
set(handles.omega0_max, 'String', num2str(def(21), '%.1f'));

set(handles.omega0, 'Min',   def(19));
set(handles.omega0, 'Value', def(20));
set(handles.omega0, 'Max',   def(21));
majorstep = omega0sld.stmax / (def(21)-def(19));
minorstep = omega0sld.stmin / (def(21)-def(19));
set(handles.omega0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Ksld  lsld Msld musld theta0sld omega0sld;

set(handles.M_min, 'Value', M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value', M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value', M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

set(handles.l_min, 'Value', l_min);
set(handles.l_min, 'String', num2str(l_min, '%.1f'));
set(handles.l_cur, 'Value', l_cur);
set(handles.l_cur, 'String', num2str(l_cur, '%.2f'));
set(handles.l_max, 'Value', l_max);
set(handles.l_max, 'String', num2str(l_max, '%.1f'));

set(handles.l, 'Min',   l_min);
set(handles.l, 'Value', l_cur);
set(handles.l, 'Max',   l_max);
majorstep = lsld.stmax / (l_max-l_min);
minorstep = lsld.stmin / (l_max-l_min);
set(handles.l, 'SliderStep', [minorstep majorstep]);

set(handles.K_min, 'Value', K_min);
set(handles.K_min, 'String', num2str(K_min, '%.1f'));
set(handles.K_cur, 'Value', K_cur);
set(handles.K_cur, 'String', num2str(K_cur, '%.2f'));
set(handles.K_max, 'Value', K_max);
set(handles.K_max, 'String', num2str(K_max, '%.1f'));

set(handles.K, 'Min',   K_min);
set(handles.K, 'Value', K_cur);
set(handles.K, 'Max',   K_max);
majorstep = Ksld.stmax / (K_max-K_min);
minorstep = Ksld.stmin / (K_max-K_min);
set(handles.K, 'SliderStep', [minorstep majorstep]);

set(handles.theta0_min, 'Value', theta0_min);
set(handles.theta0_min, 'String', num2str(theta0_min, '%.1f'));
set(handles.theta0_cur, 'Value', theta0_cur);
set(handles.theta0_cur, 'String', num2str(theta0_cur, '%.2f'));
set(handles.theta0_max, 'Value', theta0_max);
set(handles.theta0_max, 'String', num2str(theta0_max, '%.1f'));

set(handles.theta0, 'Min',   theta0_min);
set(handles.theta0, 'Value', theta0_cur);
set(handles.theta0, 'Max',   theta0_max);
majorstep = theta0sld.stmax / (theta0_max-theta0_min);
minorstep = theta0sld.stmin / (theta0_max-theta0_min);
set(handles.theta0, 'SliderStep', [minorstep majorstep]);

set(handles.omega0_min, 'Value', omega0_min);
set(handles.omega0_min, 'String', num2str(omega0_min, '%.1f'));
set(handles.omega0_cur, 'Value', omega0_cur);
set(handles.omega0_cur, 'String', num2str(omega0_cur, '%.2f'));
set(handles.omega0_max, 'Value', omega0_max);
set(handles.omega0_max, 'String', num2str(omega0_max, '%.1f'));

set(handles.omega0, 'Min',   omega0_min);
set(handles.omega0, 'Value', omega0_cur);
set(handles.omega0, 'Max',   omega0_max);
majorstep = omega0sld.stmax / (omega0_max-omega0_min);
minorstep = omega0sld.stmin / (omega0_max-omega0_min);
set(handles.omega0, 'SliderStep', [minorstep majorstep]);

new_String = cell(1);
new_String{1} = 'Ampiezza [Nm]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      new_String{2} = 'Frequenza�[Hz]';
  case 5
      new_String{2} = 'Frequenza [Hz]';
      new_String{3} = 'Duty Cycle [%]';
  case {4, 6}
      new_String{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', IngressoTipo);
set(handles.IngressoPar, 'Value', IngressoPar);

set(handles.mu_min, 'Value',  mu_min);
set(handles.mu_min, 'String', num2str(mu_min, '%.1f'));
set(handles.mu_cur, 'Value',  mu_cur);
set(handles.mu_cur, 'String', num2str(mu_cur, '%.2f'));
set(handles.mu_max, 'Value',  mu_max);
set(handles.mu_max, 'String', num2str(mu_max, '%.1f'));
set(handles.mu, 'Min',   mu_min);
set(handles.mu, 'Value', mu_cur);
set(handles.mu, 'Max',   mu_max);
majorstep = musld.stmax / (mu_max-mu_min);
minorstep = musld.stmin / (mu_max-mu_min);
set(handles.mu, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if ~exist('Sistemi', 'dir')
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar  = get(handles.IngressoPar, 'Value');

mu_min = get(handles.mu_min, 'Value');
mu_cur = get(handles.mu_cur, 'Value');
mu_max = get(handles.mu_max, 'Value');

% pannello Uscita
Uscita = get(handles.Uscita, 'Value');

% pannello M
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello l
l_min = get(handles.l_min, 'Value');
l_cur = get(handles.l_cur, 'Value');
l_max = get(handles.l_max, 'Value');

% pannello K
K_min = get(handles.K_min, 'Value');
K_cur = get(handles.K_cur, 'Value');
K_max = get(handles.K_max, 'Value');

% pannello stato iniziale
theta0_min = get(handles.theta0_min, 'Value');
theta0_cur = get(handles.theta0_cur, 'Value');
theta0_max = get(handles.theta0_max, 'Value');

omega0_min = get(handles.omega0_min, 'Value');
omega0_cur = get(handles.omega0_cur, 'Value');
omega0_max = get(handles.omega0_max, 'Value');


% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'mu_min = %f;\n', mu_min);
fprintf(fid, 'mu_cur = %f;\n', mu_cur);
fprintf(fid, 'mu_max = %f;\n', mu_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %f;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'l_min = %f;\n', l_min);
fprintf(fid, 'l_cur = %f;\n', l_cur);
fprintf(fid, 'l_max = %f;\n', l_max);

fprintf(fid, 'K_min = %f;\n', K_min);
fprintf(fid, 'K_cur = %f;\n', K_cur);
fprintf(fid, 'K_max = %f;\n', K_max);

fprintf(fid, 'theta0_min = %f;\n', theta0_min);
fprintf(fid, 'theta0_cur = %f;\n', theta0_cur);
fprintf(fid, 'theta0_max = %f;\n', theta0_max);

fprintf(fid, 'omega0_min = %f;\n', omega0_min);
fprintf(fid, 'omega0_cur = %f;\n', omega0_cur);
fprintf(fid, 'omega0_max = %f;\n', omega0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject, 'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject, 'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action  
end


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 251.001831054688, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 4, ...
    'uipanel', 23, ...
    'popupmenu', 5, ...
    'slider', 9, ...
    'edit', 27, ...
    'pushbutton', 6, ...
    'listbox', 2, ...
    'text', 4), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\07 - Pendolo.3.15\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\07 - Pendolo.3.15\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','PENDOLO ver. 3.15',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 30.05 146 31.45],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[65.5714285714286 12.3 38.4285714285714 15.1],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel11';

h3 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Massa pendolo M [Kg]',...
'Clipping','on',...
'Position',[0.857142857142857 9.65 36 4.3],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'M';

h4 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('M_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.28571428571429 1.95 33 1],...
'String',blanks(0),...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M');

appdata = [];
appdata.lastValidTag = 'M_min';

h5 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.4 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_min');

appdata = [];
appdata.lastValidTag = 'M_cur';

h6 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.2857142857143 0.4 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_cur');

appdata = [];
appdata.lastValidTag = 'M_max';

h7 = uicontrol(...
'Parent',h3,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.2857142857143 0.4 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_max');

appdata = [];
appdata.lastValidTag = 'uipanel12';

h8 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Lunghezza Pendolo l [m]',...
'Clipping','on',...
'Position',[0.857142857142857 5.15 36 4.3],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'l';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('l_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',1,...
'Position',[1.28571428571429 2 33 1],...
'String',blanks(0),...
'Style','slider',...
'SliderStep',[0.0111111 0.111111],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l');

appdata = [];
appdata.lastValidTag = 'l_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.499999999999999 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_min');

appdata = [];
appdata.lastValidTag = 'l_cur';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.4285714285714 0.499999999999999 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_cur');

appdata = [];
appdata.lastValidTag = 'l_max';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('l_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.4285714285714 0.499999999999999 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('l_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','l_max');

appdata = [];
appdata.lastValidTag = 'uipanel13';

h13 = uipanel(...
'Parent',h2,...
'Units','characters',...
'Title','Coefficiente di attrito K [N�s�m/rad]',...
'Clipping','on',...
'Position',[1.14285714285714 0.5 36 4.4],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'K';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('K_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.14285714285714 2.15 33 1],...
'String',blanks(0),...
'Style','slider',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('K_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','K');

appdata = [];
appdata.lastValidTag = 'K_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('K_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1 0.6 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('K_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','K_min');

appdata = [];
appdata.lastValidTag = 'K_cur';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('K_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13 0.6 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('K_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','K_cur');

appdata = [];
appdata.lastValidTag = 'K_max';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('K_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25 0.6 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('K_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','K_max');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h18 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.42857142857143 0.35 46.1428571428571 10.65],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h19 = axes(...
'Parent',h18,...
'Units','characters',...
'Position',[1.14285714285714 0.4 43.4285714285714 9.15],...
'Box','on',...
'CameraPosition',[192 119.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[6.99843963032974 0.633732586665468 5.11424434524097 0.432090399999183],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 383.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 238.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h20 = get(h19,'xlabel');

set(h20,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[191.370065789474 274.265027322404 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h21 = get(h19,'ylabel');

set(h21,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-49.2648026315789 120.800546448088 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h22 = get(h19,'zlabel');

set(h22,...
'Parent',h19,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-25.3273026315789 -555.483606557377 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'Run';

h23 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[112.428571428571 0.7 25 1.75],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel15';

h24 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[105.857142857143 2.9 38.5714285714286 2.9],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h25 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2 0.4 35 1.3],...
'String',{  'Posizione Angolare'; 'Energia Totale' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel16';

h26 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[105.857142857143 18.2 38.5714285714286 9.2],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h27 = uipanel(...
'Parent',h26,...
'Units','characters',...
'Title','Coppia motrice mu [Nm]',...
'Clipping','on',...
'Position',[1.28571428571429 0.349999999999999 36 7.65],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h28 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1 5.1 33 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'mu';

h29 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('mu_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.1 33 1],...
'String',blanks(0),...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('mu_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','mu');

appdata = [];
appdata.lastValidTag = 'mu_min';

h30 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('mu_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.5 9 1],...
'String','-10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('mu_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','mu_min');

appdata = [];
appdata.lastValidTag = 'mu_max';

h31 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('mu_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.1428571428571 0.5 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('mu_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','mu_max');

appdata = [];
appdata.lastValidTag = 'mu_cur';

h32 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('mu_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.1428571428571 0.5 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('mu_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','mu_cur');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h33 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1 3.5 33 1.3],...
'String','Ampiezza [Nm]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h34 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[65.5714285714286 0.55 25 1.75],...
'String','Punto di Equilibrio',...
'ButtonDownFcn',@(hObject,eventdata)Main_export('Punto_Eq_ButtonDownFcn',hObject,eventdata,guidata(hObject)),...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel17';

h35 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[55.1428571428571 2.6 48.8571428571429 8.7],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h36 = uicontrol(...
'Parent',h35,...
'Units','characters',...
'FontName','Courier',...
'HorizontalAlignment','left',...
'Position',[1 0.299999999999997 46.5714285714286 7.3],...
'String',blanks(0),...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel18';

h37 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.28571428571429 11.3 63 19.95],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h38 = axes(...
'Parent',h37,...
'Units','characters',...
'Position',[1 0.499999999999997 60.4285714285714 18.4],...
'Box','on',...
'CameraPosition',[237.5 211 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[19.6857142857143 3.256 14.3857142857143 2.22],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 474.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 421.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h39 = get(h38,'xlabel');

set(h39,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[236.379432624114 452.960597826087 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h40 = get(h38,'ylabel');

set(h40,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-43.7624113475177 212.716032608696 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h41 = get(h38,'zlabel');

set(h41,...
'Parent',h38,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-20.2304964539007 -24.0964673913043 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel19';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[65.5714285714286 27.9 78.7142857142857 3.15],...
'Tag','uipanel19',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h43 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.45 8.57142857142857 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h44 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[67 0.45 9 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h45 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.28571428571429 0.45 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h46 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[45.7142857142857 0.6 20 1],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h47 = uicontrol(...
'Parent',h42,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.45 8 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel20';

h48 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'UserData',[],...
'Clipping','on',...
'Position',[105.857142857143 6.6 38.5714285714286 11.1],...
'Tag','uipanel20',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel21';

h49 = uipanel(...
'Parent',h48,...
'Units','characters',...
'Title','Posizione iniziale theta0 [�]',...
'UserData',[],...
'Clipping','on',...
'Position',[1.14285714285714 5.5 36 4.45],...
'Tag','uipanel21',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'theta0';

h50 = uicontrol(...
'Parent',h49,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('theta0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',1.5707963267949,...
'Min',-1.5707963267949,...
'Position',[1.28571428571429 2.05 33 1],...
'String',blanks(0),...
'Style','slider',...
'SliderStep',[0.00318309 0.0318309],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('theta0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','theta0');

appdata = [];
appdata.lastValidTag = 'theta0_min';

h51 = uicontrol(...
'Parent',h49,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('theta0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.5 9 1],...
'String','-90.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('theta0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','theta0_min');

appdata = [];
appdata.lastValidTag = 'theta0_cur';

h52 = uicontrol(...
'Parent',h49,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('theta0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.2857142857143 0.5 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('theta0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','theta0_cur');

appdata = [];
appdata.lastValidTag = 'theta0_max';

h53 = uicontrol(...
'Parent',h49,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('theta0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.2857142857143 0.5 9 1],...
'String','90.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('theta0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','theta0_max');

appdata = [];
appdata.lastValidTag = 'uipanel22';

h54 = uipanel(...
'Parent',h48,...
'Units','characters',...
'Title','Velocit� iniziale omega0 [�/s]',...
'UserData',[],...
'Clipping','on',...
'Position',[1.14285714285714 0.45 36 4.55],...
'Tag','uipanel22',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'omega0';

h55 = uicontrol(...
'Parent',h54,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('omega0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.28571428571429 2.05 33 1],...
'String',blanks(0),...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('omega0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','omega0');

appdata = [];
appdata.lastValidTag = 'omega0_min';

h56 = uicontrol(...
'Parent',h54,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('omega0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.549999999999999 9 1],...
'String','-45.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('omega0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','omega0_min');

appdata = [];
appdata.lastValidTag = 'omega0_cur';

h57 = uicontrol(...
'Parent',h54,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('omega0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.2857142857143 0.549999999999999 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('omega0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','omega0_cur');

appdata = [];
appdata.lastValidTag = 'omega0_max';

h58 = uicontrol(...
'Parent',h54,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('omega0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.2857142857143 0.549999999999999 9 1],...
'String','45.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('omega0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','omega0_max');


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


