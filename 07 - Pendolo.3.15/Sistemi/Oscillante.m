% Ingressi
IngressoTipo = 1.000000;
IngressoPar = 1.000000;
mu_min = -10.000000;
mu_cur = 1.000000;
mu_max = 10.000000;

% Uscita
Uscita = 1.000000;

% Parametri
M_min = 0.000000;
M_cur = 1.000000;
M_max = 10.000000;
l_min = 0.000000;
l_cur = 1.000000;
l_max = 10.000000;
K_min = 0.000000;
K_cur = 1.000000;
K_max = 10.000000;
theta0_min = -90.000000;
theta0_cur = 0.000000;
theta0_max = 90.000000;
omega0_min = -45.000000;
omega0_cur = 0.000000;
omega0_max = 45.000000;
