%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           P E N D O l O    3.15                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Dalila Virgolino e Marco Zanotti (2015)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%% INIT
%
function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property', 'Value', ...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK', hObject, ...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 22-Sep-2016 15:12:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1      % IngressoTipo
      1      % IngressoPar
      1      % Uscita
      -10    % mu_min
      1      % mu, mu_cur
      10     % mu_max
      0      % M_min
      1      % M, M_cur
      10     % M_max
      0      % l_min
      1      % l, l_cur
      10     % l_max
      0      % K_min
      1      % K, K_cur
      10     % K_max
      -90    % theta0_min
      0      % theta0, theta0_cur
      +90    % theta0_max
      -45    % omega0_min
      0      % omega0, omega0_cur
      45     % omega0_max
      ];

% Par degli slider
global musld Ksld lsld Msld theta0sld omega0sld;

musld.stmin = 0.1;
musld.stmax = 1;
musld.Llim = -Inf;
musld.Hlim = +Inf;

Ksld.stmin = 0.1;
Ksld.stmax = 1;
Ksld.Llim = eps;
Ksld.Hlim = +Inf;

lsld.stmin = 0.1;
lsld.stmax = 1;
lsld.Llim = eps;
lsld.Hlim = +Inf;

Msld.stmin = 0.1;
Msld.stmax = 1;
Msld.Llim = eps;
Msld.Hlim = +Inf;

theta0sld.stmin = 1;
theta0sld.stmax = 10;
theta0sld.Llim = -Inf;
theta0sld.Hlim = +Inf;

omega0sld.stmin = 1;
omega0sld.stmax = 5;
omega0sld.Llim = -Inf;
omega0sld.Hlim = +Inf;
  
evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);


% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% Caricamento delle variabili nel Workspace



% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: configdelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



%% RUN
% --- Executes on button press in run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

% chiudo il modello simulink senza salvare
bdclose(Uscita);

% leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita, 'Value');
switch val1
  case 1
      Uscita = 'Posizione_angolare';
  case 2
      Uscita = 'Energia';
end;

% Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Nm]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
theta0 = get(handles.theta0, 'Value')*pi/180;
omega0 = get(handles.omega0, 'Value')*pi/180;
mu = ampiezza(2);
M = get(handles.M, 'Value');
l = get(handles.l, 'Value');
K = get(handles.K, 'Value');
g = 9.81;

% controllo sui dati nulli
if M == 0, M = eps; end
if l == 0, l = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% dTheta/dt = Omega
% dOmega/dt = -M*g*l/I*sin(Theta) - K/I*Omega + mu/I
% x_sym1 <-> Theta
% x_sym2 <-> Omega
x_sym = sym('x_sym', [2 1]);
dx_sym = [x_sym(2); -g*sin(x_sym(1))/l - K/(M*l*l)*x_sym(2) + mu/(M*l*l)];
X = solve(dx_sym == 0, x_sym(1), x_sym(2));
% Calcolo dello jacobiano simbolico (linearizzazione)
F = jacobian(dx_sym, x_sym);
x = double([X.x_sym1(1); X.x_sym2(1)]);
F = double(subs(F, x_sym, x));

% Calcolo costanti di tempo nel punto di eq.
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(100, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'mu', mu; 'M', M; 'l', l; 'K', K; 'g', g; 'theta0', theta0; 'omega0', omega0};
for i = 1 : size(vars, 1)
  name = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end


% Apre il sistema da simulare
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end

% Legge qual e' il nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
    case 1  % step
        add_block('Simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(Tau/5));
    case 2  % impulso
        add_block('Simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(Tau/5));
        add_block('Simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(Tau/5+min(0.25*Tau, 0.01)));
        add_block('Simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
    case 3  % treno impulsi
        add_block('Simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
    case 4  % sinusoide
        add_block('Simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
    case 5  % onda quadra
        add_block('Simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
    case 6  % onda dente di sega
        add_block('Simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + Tau/5));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end


% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale;



%% PUNTO D'EQUILIBRIO
% --- Executes on button press in punto_eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Nm]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.mu_min, 'Value',  ampiezza(1));
set(handles.mu_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.mu_cur, 'Value',  ampiezza(2));
set(handles.mu_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.mu_max, 'Value',  ampiezza(3));
set(handles.mu_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.mu, 'Min',   ampiezza(1));
set(handles.mu, 'Value', ampiezza(2));
set(handles.mu, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
mu = get(handles.mu_cur, 'Value'); u = mu;
M = get(handles.M_cur, 'Value');
l = get(handles.l_cur, 'Value');
K = get(handles.K_cur, 'Value');
g = 9.81;

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if M == 0, M = eps; end
if l == 0, l = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% solo se (iff) -1 <= u/Mgl <= +1
if u < -M*g*l || u > +M*g*l
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['mu = ', num2str(u(1), '%.2f'), ' Nm']});
  return
end


% % Scrittura dell'equazione dinamica (vecchio metodo)
% % Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% % x_sym(1) <-> Theta
% % x_sym(2) <-> Omega
% x_sym = sym('x_sym', [2 1]);
% % dTheta/dt = Omega
% % dOmega/dt = -M*g*l*sin(Theta) - K*Omega + mu
% dx_sym = [x_sym(2); -M*g*l*sin(x_sym(1)) - K*x_sym(2) + u];
% % X = solve(dx_sym == 0, x_sym(1), x_sym(2));
% % X = double([X.x_sym1(1); X.x_sym2(1)]);
% [X, stb] = PuntoEquilibrio(dx_sym, x_sym);


% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
syms Theta Omega
dx_sym = [Omega; -M*g*l*sin(Theta) - K*Omega + u];
%dx_sym = [Omega; -M*g*l*Theta - K*Omega + u]; % eq. approssimata lineare
x_sym = ['Theta'; 'Omega'];
[X, stb] = PuntoEquilibrio2(dx_sym, x_sym);


% Visualizzazione del testo col risultato
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['mu = ', num2str(u(1), '%.2f'), ' Nm']});
  return
end

str = sprintf('In presenza dell''ingresso: mu = %.2f Nm', u(1));
[~, numsol] = size(X);
for i = 1 : numsol
  x = X(:, i);
  
  if numsol == 1, str1 = sprintf('\nlo stato:');
  else            str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  theta = %.2f rad   (%.1f�)',   x(1), x(1)*180.0/pi);
  str22 = sprintf('\n  omega = %.2f rad/s (%.1f�/s)', x(2), x(2)*180.0/pi);

  % Stabilita'
  switch stb(i)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
  end        

  if i == numsol, endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str22, str3, endstr);  
end

set(handles.Punto_Eq_txt, 'String', str);



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over punto_eq.
function Punto_Eq_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to punto_eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



%% FIGURE
% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Modello.jpg');
image(im);
axis off;
axis equal;


% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject, 'String')) returns Uscita contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER K
% --- Executes on slider movement.
function K_Callback(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.K, handles.K_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function K_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function K_min_Callback(hObject, eventdata, handles)
% hObject    handle to K_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_min_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function K_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function K_cur_Callback(hObject, eventdata, handles)
% hObject    handle to K_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_cur_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function K_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function K_max_Callback(hObject, eventdata, handles)
% hObject    handle to K_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Ksld;
Slider_max_Callback(handles, handles.K, handles.K_min, handles.K_cur, handles.K_max, Ksld.stmin, Ksld.stmax, Ksld.Llim, Ksld.Hlim);


% --- Executes during object creation, after setting all properties.
function K_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER l
% --- Executes on slider movement.
function l_Callback(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.l, handles.l_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function l_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function l_min_Callback(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_min_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function l_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function l_cur_Callback(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_cur_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function l_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function l_max_Callback(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global lsld;
Slider_max_Callback(handles, handles.l, handles.l_min, handles.l_cur, handles.l_max, lsld.stmin, lsld.stmax, lsld.Llim, lsld.Hlim);


% --- Executes during object creation, after setting all properties.
function l_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to l_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);


% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


%% INGRESSI
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject, 'String')) returns IngressoTipo contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from IngressoTipo

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [Nm]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.mu_min, 'Value',  values(1));
set(handles.mu_min, 'String', num2str(values(1), '%.1f'));
set(handles.mu_cur, 'Value',  values(2));
set(handles.mu_cur, 'String', num2str(values(2), '%.1f'));
set(handles.mu_max, 'Value',  values(3));
set(handles.mu_max, 'String', num2str(values(3), '%.1f'));
set(handles.mu, 'Min',   values(1));
set(handles.mu, 'Value', values(2));
set(handles.mu, 'Max',   values(3));



% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject, 'String')) returns IngressoPar contents as cell array
%        contents{get(hObject, 'Value')} returns selected item from IngressoPar

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.mu_min, 'Value',  values(1));
set(handles.mu_min, 'String', num2str(values(1), '%.1f'));
set(handles.mu_cur, 'Value',  values(2));
set(handles.mu_cur, 'String', num2str(values(2), '%.2f'));
set(handles.mu_max, 'Value',  values(3));
set(handles.mu_max, 'String', num2str(values(3), '%.1f'));
set(handles.mu, 'Min',   values(1));
set(handles.mu, 'Max',   values(3));
set(handles.mu, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on slider movement.
function mu_Callback(hObject, eventdata, handles)
% hObject    handle to mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.mu, handles.mu_cur);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function mu_min_Callback(hObject, eventdata, handles)
% hObject    handle to mu_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_min_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function mu_max_Callback(hObject, eventdata, handles)
% hObject    handle to mu_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_max_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function mu_cur_Callback(hObject, eventdata, handles)
% hObject    handle to mu_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global musld;
Slider_cur_Callback(handles, handles.mu, handles.mu_min, handles.mu_cur, handles.mu_max, musld.stmin, musld.stmax, musld.Llim, musld.Hlim);

minval = get(handles.mu_min, 'Value');
curval = get(handles.mu, 'Value');
maxval = get(handles.mu_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function mu_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mu_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% STATO INIZIALE
% --- Executes on slider movement.
function omega0_Callback(hObject, eventdata, handles)
% hObject    handle to omega0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.omega0, handles.omega0_cur);


% --- Executes during object creation, after setting all properties.
function omega0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function omega0_min_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_min_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function omega0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_cur_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function omega0_max_Callback(hObject, eventdata, handles)
% hObject    handle to omega0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global omega0sld;
Slider_max_Callback(handles, handles.omega0, handles.omega0_min, handles.omega0_cur, handles.omega0_max, omega0sld.stmin, omega0sld.stmax, omega0sld.Llim, omega0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function omega0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to omega0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on slider movement.
function theta0_Callback(hObject, eventdata, handles)
% hObject    handle to theta0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.theta0, handles.theta0_cur);


% --- Executes during object creation, after setting all properties.
function theta0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', [.9 .9 .9]);
end


function theta0_min_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_min_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function theta0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_cur_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function theta0_max_Callback(hObject, eventdata, handles)
% hObject    handle to theta0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global theta0sld;
Slider_max_Callback(handles, handles.theta0, handles.theta0_min, handles.theta0_cur, handles.theta0_max, theta0sld.stmin, theta0sld.stmax, theta0sld.Llim, theta0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function theta0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to theta0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end



%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Ksld lsld Msld musld theta0sld omega0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [Nm]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% slider mu
set(handles.mu_min, 'Value', def(4));
set(handles.mu_min, 'String', num2str(def(4), '%.1f'));
set(handles.mu_cur, 'Value', def(5));
set(handles.mu_cur, 'String', num2str(def(5), '%.2f'));
set(handles.mu_max, 'Value', def(6));
set(handles.mu_max, 'String', num2str(def(6), '%.1f'));

set(handles.mu, 'Min',   def(4)); 
set(handles.mu, 'Value', def(5));
set(handles.mu, 'Max',   def(6)); 
majorstep = musld.stmax / (def(6)-def(4));
minorstep = musld.stmin / (def(6)-def(4));
set(handles.mu, 'SliderStep', [minorstep majorstep]);

evalin('base', ['input_params(''Ampiezza [Nm]'') = ' mat2str([get(handles.mu, 'Min') get(handles.mu, 'Value') get(handles.mu, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');


% Uscita
set(handles.Uscita, 'Value', def(3));

% slider M
set(handles.M_min, 'Value',  def(7));
set(handles.M_min, 'String', num2str(def(7), '%.1f'));
set(handles.M_cur, 'Value',  def(8));
set(handles.M_cur, 'String', num2str(def(8), '%.2f'));
set(handles.M_max, 'Value',  def(9));
set(handles.M_max, 'String', num2str(def(9), '%.1f'));

set(handles.M, 'Min',   def(7));
set(handles.M, 'Value', def(8));
set(handles.M, 'Max',   def(9));
majorstep = Msld.stmax / (def(9)-def(7));
minorstep = Msld.stmin / (def(9)-def(7));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider l
set(handles.l_min, 'Value',  def(10));
set(handles.l_min, 'String', num2str(def(10), '%.1f'));
set(handles.l_cur, 'Value',  def(11));
set(handles.l_cur, 'String', num2str(def(11), '%.2f'));
set(handles.l_max, 'Value',  def(12));
set(handles.l_max, 'String', num2str(def(12), '%.1f'));

set(handles.l, 'Min',   def(10));
set(handles.l, 'Value', def(11));
set(handles.l, 'Max',   def(12));
majorstep = lsld.stmax / (def(12)-def(10));
minorstep = lsld.stmin / (def(12)-def(10));
set(handles.l, 'SliderStep', [minorstep majorstep]);

% slider K
set(handles.K_min, 'Value',  def(13));
set(handles.K_min, 'String', num2str(def(13), '%.1f'));
set(handles.K_cur, 'Value',  def(14));
set(handles.K_cur, 'String', num2str(def(14), '%.2f'));
set(handles.K_max, 'Value',  def(15));
set(handles.K_max, 'String', num2str(def(15), '%.1f'));

set(handles.K, 'Min',   def(13));
set(handles.K, 'Value', def(14));
set(handles.K, 'Max',   def(15));
majorstep = Ksld.stmax / (def(15)-def(13));
minorstep = Ksld.stmin / (def(15)-def(13));
set(handles.K, 'SliderStep', [minorstep majorstep]);

% slider theta0
set(handles.theta0_min, 'Value',  def(16));
set(handles.theta0_min, 'String', num2str(def(16), '%.1f'));
set(handles.theta0_cur, 'Value',  def(17));
set(handles.theta0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.theta0_max, 'Value',  def(18));
set(handles.theta0_max, 'String', num2str(def(18), '%.1f'));

set(handles.theta0, 'Min',   def(16));
set(handles.theta0, 'Value', def(17));
set(handles.theta0, 'Max',   def(18));
majorstep = theta0sld.stmax / (def(18)-def(16));
minorstep = theta0sld.stmin / (def(18)-def(16));
set(handles.theta0, 'SliderStep', [minorstep majorstep]);

% slider omega0
set(handles.omega0_min, 'Value',  def(19));
set(handles.omega0_min, 'String', num2str(def(19), '%.1f'));
set(handles.omega0_cur, 'Value',  def(20));
set(handles.omega0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.omega0_max, 'Value',  def(21));
set(handles.omega0_max, 'String', num2str(def(21), '%.1f'));

set(handles.omega0, 'Min',   def(19));
set(handles.omega0, 'Value', def(20));
set(handles.omega0, 'Max',   def(21));
majorstep = omega0sld.stmax / (def(21)-def(19));
minorstep = omega0sld.stmin / (def(21)-def(19));
set(handles.omega0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Ksld  lsld Msld musld theta0sld omega0sld;

set(handles.M_min, 'Value', M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value', M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value', M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

set(handles.l_min, 'Value', l_min);
set(handles.l_min, 'String', num2str(l_min, '%.1f'));
set(handles.l_cur, 'Value', l_cur);
set(handles.l_cur, 'String', num2str(l_cur, '%.2f'));
set(handles.l_max, 'Value', l_max);
set(handles.l_max, 'String', num2str(l_max, '%.1f'));

set(handles.l, 'Min',   l_min);
set(handles.l, 'Value', l_cur);
set(handles.l, 'Max',   l_max);
majorstep = lsld.stmax / (l_max-l_min);
minorstep = lsld.stmin / (l_max-l_min);
set(handles.l, 'SliderStep', [minorstep majorstep]);

set(handles.K_min, 'Value', K_min);
set(handles.K_min, 'String', num2str(K_min, '%.1f'));
set(handles.K_cur, 'Value', K_cur);
set(handles.K_cur, 'String', num2str(K_cur, '%.2f'));
set(handles.K_max, 'Value', K_max);
set(handles.K_max, 'String', num2str(K_max, '%.1f'));

set(handles.K, 'Min',   K_min);
set(handles.K, 'Value', K_cur);
set(handles.K, 'Max',   K_max);
majorstep = Ksld.stmax / (K_max-K_min);
minorstep = Ksld.stmin / (K_max-K_min);
set(handles.K, 'SliderStep', [minorstep majorstep]);

set(handles.theta0_min, 'Value', theta0_min);
set(handles.theta0_min, 'String', num2str(theta0_min, '%.1f'));
set(handles.theta0_cur, 'Value', theta0_cur);
set(handles.theta0_cur, 'String', num2str(theta0_cur, '%.2f'));
set(handles.theta0_max, 'Value', theta0_max);
set(handles.theta0_max, 'String', num2str(theta0_max, '%.1f'));

set(handles.theta0, 'Min',   theta0_min);
set(handles.theta0, 'Value', theta0_cur);
set(handles.theta0, 'Max',   theta0_max);
majorstep = theta0sld.stmax / (theta0_max-theta0_min);
minorstep = theta0sld.stmin / (theta0_max-theta0_min);
set(handles.theta0, 'SliderStep', [minorstep majorstep]);

set(handles.omega0_min, 'Value', omega0_min);
set(handles.omega0_min, 'String', num2str(omega0_min, '%.1f'));
set(handles.omega0_cur, 'Value', omega0_cur);
set(handles.omega0_cur, 'String', num2str(omega0_cur, '%.2f'));
set(handles.omega0_max, 'Value', omega0_max);
set(handles.omega0_max, 'String', num2str(omega0_max, '%.1f'));

set(handles.omega0, 'Min',   omega0_min);
set(handles.omega0, 'Value', omega0_cur);
set(handles.omega0, 'Max',   omega0_max);
majorstep = omega0sld.stmax / (omega0_max-omega0_min);
minorstep = omega0sld.stmin / (omega0_max-omega0_min);
set(handles.omega0, 'SliderStep', [minorstep majorstep]);

new_String = cell(1);
new_String{1} = 'Ampiezza [Nm]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      new_String{2} = 'Frequenza�[Hz]';
  case 5
      new_String{2} = 'Frequenza [Hz]';
      new_String{3} = 'Duty Cycle [%]';
  case {4, 6}
      new_String{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', IngressoTipo);
set(handles.IngressoPar, 'Value', IngressoPar);

set(handles.mu_min, 'Value',  mu_min);
set(handles.mu_min, 'String', num2str(mu_min, '%.1f'));
set(handles.mu_cur, 'Value',  mu_cur);
set(handles.mu_cur, 'String', num2str(mu_cur, '%.2f'));
set(handles.mu_max, 'Value',  mu_max);
set(handles.mu_max, 'String', num2str(mu_max, '%.1f'));
set(handles.mu, 'Min',   mu_min);
set(handles.mu, 'Value', mu_cur);
set(handles.mu, 'Max',   mu_max);
majorstep = musld.stmax / (mu_max-mu_min);
minorstep = musld.stmin / (mu_max-mu_min);
set(handles.mu, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if ~exist('Sistemi', 'dir')
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar  = get(handles.IngressoPar, 'Value');

mu_min = get(handles.mu_min, 'Value');
mu_cur = get(handles.mu_cur, 'Value');
mu_max = get(handles.mu_max, 'Value');

% pannello Uscita
Uscita = get(handles.Uscita, 'Value');

% pannello M
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello l
l_min = get(handles.l_min, 'Value');
l_cur = get(handles.l_cur, 'Value');
l_max = get(handles.l_max, 'Value');

% pannello K
K_min = get(handles.K_min, 'Value');
K_cur = get(handles.K_cur, 'Value');
K_max = get(handles.K_max, 'Value');

% pannello stato iniziale
theta0_min = get(handles.theta0_min, 'Value');
theta0_cur = get(handles.theta0_cur, 'Value');
theta0_max = get(handles.theta0_max, 'Value');

omega0_min = get(handles.omega0_min, 'Value');
omega0_cur = get(handles.omega0_cur, 'Value');
omega0_max = get(handles.omega0_max, 'Value');


% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'mu_min = %f;\n', mu_min);
fprintf(fid, 'mu_cur = %f;\n', mu_cur);
fprintf(fid, 'mu_max = %f;\n', mu_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %f;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'l_min = %f;\n', l_min);
fprintf(fid, 'l_cur = %f;\n', l_cur);
fprintf(fid, 'l_max = %f;\n', l_max);

fprintf(fid, 'K_min = %f;\n', K_min);
fprintf(fid, 'K_cur = %f;\n', K_cur);
fprintf(fid, 'K_max = %f;\n', K_max);

fprintf(fid, 'theta0_min = %f;\n', theta0_min);
fprintf(fid, 'theta0_cur = %f;\n', theta0_cur);
fprintf(fid, 'theta0_max = %f;\n', theta0_max);

fprintf(fid, 'omega0_min = %f;\n', omega0_min);
fprintf(fid, 'omega0_cur = %f;\n', omega0_cur);
fprintf(fid, 'omega0_max = %f;\n', omega0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject, 'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject, 'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
    set(hObject, 'BackgroundColor', 'white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action  
end
