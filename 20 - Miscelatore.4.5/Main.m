%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       M I S C E L A T O R E    4.5                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 30-Sep-2016 00:52:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      0.0      % qf_min        4
      0.2      % qf, qf_cur    5
      1.0      % qf_max        6
      0.0      % qc_min        7
      0.1      % qc, qc_cur    8
      1.0      % qc_max        9
      0.0      % M_min         10
      5.0      % M, M_cur      11
      10.0     % M_max         12
      0.0      % Tf_min        13
      10.0     % Tf, Tf_cur    14
      100.0    % Tf_max        15
      0.0      % Tc_min        16
      40.0     % Tc, Tc_cur    17
      100.0    % Tc_max        18
      0.00     % Te0_min       19
      30.0     % Te0, Te0_cur  20
      100.0    % Te0_max       21
     
      ];

% Par degli slider
global Msld Tfsld Tcsld qfsld qcsld Te0sld;

Msld.stmin = 0.1;
Msld.stmax = 1;
Msld.Llim = eps;
Msld.Hlim = +Inf;

Tfsld.stmin = 0.1;
Tfsld.stmax = 1;
Tfsld.Llim = eps;
Tfsld.Hlim = +Inf;

Tcsld.stmin = 0.1;
Tcsld.stmax = 1;
Tcsld.Llim = eps;
Tcsld.Hlim = +Inf;

qfsld.stmin = 0.01;
qfsld.stmax = 0.01;
qfsld.Llim = eps;
qfsld.Hlim = +Inf;

qcsld.stmin = 0.01;
qcsld.stmax = 0.01;
qcsld.Llim = eps;
qcsld.Hlim = +Inf;

Te0sld.stmin = 0.1;
Te0sld.stmax = 1;
Te0sld.Llim = eps;
Te0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles); 
% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Chiusura dello schema simulink (if any) senza salvare
clc

global Uscita;

bdclose(Uscita);

Uscita = 'Temperatura_Serbatoio';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Kg/s]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
qf  = get(handles.qf,  'Value');
qc  = get(handles.qc,  'Value');
M   = get(handles.M,   'Value');
Tf  = get(handles.Tf,  'Value');
Tc  = get(handles.Tc,  'Value');
Te0 = get(handles.Te0, 'Value');

% Controllo sui dati nulli per evitare singolarita'
if M <= 0, M = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [-(qf+qc)/M];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(100, Tau);  % sogliatura per evitare sim troppo lunghe


vars = {'qf', qf; 'qc', qc; 'Te0', Te0; 'Tf', Tf; 'Tc', Tc; 'M', M};
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end

% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end

% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Kg/s]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.qf_min, 'Value',  ampiezza(1));
set(handles.qf_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.qf_cur, 'Value',  ampiezza(2));
set(handles.qf_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.qf_max, 'Value',  ampiezza(3));
set(handles.qf_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.qf, 'Min',   ampiezza(1));
set(handles.qf, 'Value', ampiezza(2));
set(handles.qf, 'Max',   ampiezza(3));


% Lettura dei dati inseriti dall'utente
qf = get(handles.qf, 'Value'); u(1) = qf;
qc = get(handles.qc, 'Value'); u(2) = qc;
M  = get(handles.M,  'Value');
Tf  = get(handles.Tf,  'Value');
Tc  = get(handles.Tc,  'Value');
Te0  = get(handles.Te0,  'Value');

% Controllo sui dati nulli per evitare singolarita'
if M <= 0, M = eps; end


% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)

% Impostazione dell'equazione dinamica non lineare
% x_sym1 <-> Te0
x_sym = sym('x_sym', [1 1]);
% dTe/dt  = - (qf+qc)/M * x_sym(1) + Tf/M*qf + Tc/M*qc
dx_sym = [ -(u(1)+u(2)) / M * x_sym(1) + Tf/M*u(1) + Tc/M*u(2)];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1));

if size(X) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['qf = ', num2str(u(1), '%.2f'), ' Kg/s']; ...
    ['qc = ', num2str(u(2), '%.2f'), ' Kg/s'] });
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: \n  qf = %.2f Kg/s \n  qc = %.2f Kg/s', u(1), u(2));
for k = 1 : size(X)
  % Soluzione numerica
  x = double(X(k));

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intornno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  Te  = %.2f �C', x(1));

  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
      str3 = '';
  end        

  if k == size(X, 1), endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);



%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Msld Tfsld Tcsld qfsld qcsld Te0sld;

% Aggiornamento degli slider
%M
set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

%Tf
set(handles.Tf_min, 'Value',  Tf_min);
set(handles.Tf_min, 'String', num2str(Tf_min, '%.1f'));
set(handles.Tf_cur, 'Value',  Tf_cur);
set(handles.Tf_cur, 'String', num2str(Tf_cur, '%.2f'));
set(handles.Tf_max, 'Value',  Tf_max);
set(handles.Tf_max, 'String', num2str(Tf_max, '%.1f'));

set(handles.Tf, 'Min',   Tf_min);
set(handles.Tf, 'Value', Tf_cur);
set(handles.Tf, 'Max',   Tf_max);
majorstep = Tfsld.stmax / (Tf_max-Tf_min);
minorstep = Tfsld.stmin / (Tf_max-Tf_min);
set(handles.Tf, 'SliderStep', [minorstep majorstep]);

%Tc
set(handles.Tc_min, 'Value',  Tc_min);
set(handles.Tc_min, 'String', num2str(Tc_min, '%.1f'));
set(handles.Tc_cur, 'Value',  Tc_cur);
set(handles.Tc_cur, 'String', num2str(Tc_cur, '%.2f'));
set(handles.Tc_max, 'Value',  Tc_max);
set(handles.Tc_max, 'String', num2str(Tc_max, '%.1f'));

set(handles.Tc, 'Min',   Tc_min);
set(handles.Tc, 'Value', Tc_cur);
set(handles.Tc, 'Max',   Tc_max);
majorstep = Tcsld.stmax / (Tc_max-Tc_min);
minorstep = Tcsld.stmin / (Tc_max-Tc_min);
set(handles.Tc, 'SliderStep', [minorstep majorstep]);

%Te0
set(handles.Te0_min, 'Value',  Te0_min);
set(handles.Te0_min, 'String', num2str(Te0_min, '%.1f'));
set(handles.Te0_cur, 'Value',  Te0_cur);
set(handles.Te0_cur, 'String', num2str(Te0_cur, '%.2f'));
set(handles.Te0_max, 'Value',  Te0_max);
set(handles.Te0_max, 'String', num2str(Te0_max, '%.1f'));

set(handles.Te0, 'Min',   Te0_min);
set(handles.Te0, 'Value', Te0_cur);
set(handles.Te0, 'Max',   Te0_max);
majorstep = Te0sld.stmax / (Te0_max-Te0_min);
minorstep = Te0sld.stmin / (Te0_max-Te0_min);
set(handles.Te0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [Kg/s]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

%qf
set(handles.qf_min, 'Value',  qf_min);
set(handles.qf_min, 'String', num2str(qf_min, '%.1f'));
set(handles.qf_cur, 'Value',  qf_cur);
set(handles.qf_cur, 'String', num2str(qf_cur, '%.2f'));
set(handles.qf_max, 'Value',  qf_max);
set(handles.qf_max, 'String', num2str(qf_max, '%.1f'));

set(handles.qf, 'Min',   qf_min);
set(handles.qf, 'Value', qf_cur);
set(handles.qf, 'Max',   qf_max);
majorstep = qfsld.stmax / (qf_max-qf_min);
minorstep = qfsld.stmin / (qf_max-qf_min);
set(handles.qf, 'SliderStep', [minorstep majorstep]);

%qc
set(handles.qc_min, 'Value',  qc_min);
set(handles.qc_min, 'String', num2str(qc_min, '%.1f'));
set(handles.qc_cur, 'Value',  qc_cur);
set(handles.qc_cur, 'String', num2str(qc_cur, '%.2f'));
set(handles.qc_max, 'Value',  qc_max);
set(handles.qc_max, 'String', num2str(qc_max, '%.1f'));

set(handles.qc, 'Min',   qc_min);
set(handles.qc, 'Value', qc_cur);
set(handles.qc, 'Max',   qc_max);
majorstep = qcsld.stmax / (qc_max-qc_min);
minorstep = qcsld.stmin / (qc_max-qc_min);
set(handles.qc, 'SliderStep', [minorstep majorstep]);



set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello massa
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello temperatura acqua fredda
Tf_min = get(handles.Tf_min, 'Value');
Tf_cur = get(handles.Tf_cur, 'Value');
Tf_max = get(handles.Tf_max, 'Value');

% pannello temperatura acqua calda
Tc_min = get(handles.Tc_min, 'Value');
Tc_cur = get(handles.Tc_cur, 'Value');
Tc_max = get(handles.Tc_max, 'Value');

% pannello temperatura vasca
Te0_min = get(handles.Te0_min, 'Value');
Te0_cur = get(handles.Te0_cur, 'Value');
Te0_max = get(handles.Te0_max, 'Value');


% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

qf_min = get(handles.qf_min, 'Value');
qf_cur = get(handles.qf_cur, 'Value');
qf_max = get(handles.qf_max, 'Value');

qc_min = get(handles.qc_min, 'Value');
qc_cur = get(handles.qc_cur, 'Value');
qc_max = get(handles.qc_max, 'Value');
% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'qf_min = %f;\n', qf_min);
fprintf(fid, 'qf_cur = %f;\n', qf_cur);
fprintf(fid, 'qf_max = %f;\n', qf_max);

fprintf(fid, 'qc_min = %f;\n', qc_min);
fprintf(fid, 'qc_cur = %f;\n', qc_cur);
fprintf(fid, 'qc_max = %f;\n', qc_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'Tf_min = %f;\n', Tf_min);
fprintf(fid, 'Tf_cur = %f;\n', Tf_cur);
fprintf(fid, 'Tf_max = %f;\n', Tf_max);

fprintf(fid, 'Tc_min = %f;\n', Tc_min);
fprintf(fid, 'Tc_cur = %f;\n', Tc_cur);
fprintf(fid, 'Tc_max = %f;\n', Tc_max);

fprintf(fid, 'Te0_min = %f;\n', Te0_min);
fprintf(fid, 'Te0_cur = %f;\n', Te0_cur);
fprintf(fid, 'Te0_max = %f;\n', Te0_max);


fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end

function Load_Defaults(handles)

global def;
global Msld Tfsld Tcsld qfsld qcsld Te0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [Kg/s]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider qf
set(handles.qf_min, 'Value', def(4));
set(handles.qf_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.qf_cur, 'Value', def(5));
set(handles.qf_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.qf_max, 'Value', def(6));
set(handles.qf_max, 'String', num2str(def(6), '%.1f'));

set(handles.qf, 'Min',   def(4)); 
set(handles.qf, 'Value', def(5));
set(handles.qf, 'Max',   def(6)); 
majorstep = qfsld.stmax / (def(6)-def(4));
minorstep = qfsld.stmin / (def(6)-def(4));
set(handles.qf, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.qf, 'Min') get(handles.qf, 'Value') get(handles.qf, 'Max')]);
evalin('base', ['input_params(''Ampiezza [Kg/s]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider qc
set(handles.qc_min, 'Value',  def(7));
set(handles.qc_min, 'String', num2str(def(7), '%.1f'));
set(handles.qc_cur, 'Value',  def(8));
set(handles.qc_cur, 'String', num2str(def(8), '%.2f'));
set(handles.qc_max, 'Value',  def(9));
set(handles.qc_max, 'String', num2str(def(9), '%.1f'));

set(handles.qc, 'Min',   def(7));
set(handles.qc, 'Value', def(8));
set(handles.qc, 'Max',   def(9));
majorstep = qcsld.stmax / (def(9)-def(7));
minorstep = qcsld.stmin / (def(9)-def(7));
set(handles.qc, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value',  def(10));
set(handles.M_min, 'String', num2str(def(10), '%.1f'));
set(handles.M_cur, 'Value',  def(11));
set(handles.M_cur, 'String', num2str(def(11), '%.2f'));
set(handles.M_max, 'Value',  def(12));
set(handles.M_max, 'String', num2str(def(12), '%.1f'));

set(handles.M, 'Min',   def(10));
set(handles.M, 'Value', def(11));
set(handles.M, 'Max',   def(12));
majorstep = Msld.stmax / (def(12)-def(10));
minorstep = Msld.stmin / (def(12)-def(10));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider Tf
set(handles.Tf_min, 'Value', def(13));
set(handles.Tf_min, 'String', num2str(def(13), '%.1f'));
set(handles.Tf_cur, 'Value', def(14));
set(handles.Tf_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Tf_max, 'Value', def(15));
set(handles.Tf, 'String', num2str(def(15), '%.1f'));

set(handles.Tf, 'Min',   def(13)); 
set(handles.Tf, 'Value', def(14));
set(handles.Tf, 'Max',   def(15)); 
majorstep = Tfsld.stmax / (def(15)-def(13));
minorstep = Tfsld.stmin / (def(15)-def(13));
set(handles.Tf, 'SliderStep', [minorstep majorstep]);

% slider Tc
set(handles.Tc_min, 'Value', def(16));
set(handles.Tc_min, 'String', num2str(def(16), '%.1f'));
set(handles.Tc_cur, 'Value', def(17));
set(handles.Tc_cur, 'String', num2str(def(17), '%.2f'));
set(handles.Tc_max, 'Value', def(18));
set(handles.Tc_max, 'String', num2str(def(18), '%.1f'));

set(handles.Tc, 'Min',   def(16)); 
set(handles.Tc, 'Value', def(17));
set(handles.Tc, 'Max',   def(18)); 
majorstep = Tcsld.stmax / (def(18)-def(16));
minorstep = Tcsld.stmin / (def(18)-def(16));
set(handles.Tc, 'SliderStep', [minorstep majorstep]);


% slider Te0
set(handles.Te0_min, 'Value', def(19));
set(handles.Te0_min, 'String', num2str(def(19), '%.1f'));
set(handles.Te0_cur, 'Value', def(20));
set(handles.Te0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.Te0_max, 'Value', def(21));
set(handles.Te0_max, 'String', num2str(def(21), '%.1f'));

set(handles.Te0, 'Min',   def(19)); 
set(handles.Te0, 'Value', def(20));
set(handles.Te0, 'Max',   def(21)); 
majorstep = Te0sld.stmax / (def(21)-def(19));
minorstep = Te0sld.stmin / (def(21)-def(19));
set(handles.Te0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Tc
% --- Executes on slider movement.
function Tc_Callback(hObject, eventdata, handles)
% hObject    handle to Tc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Tc, handles.Tc_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Tc_min_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_min_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tc_max_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_max_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tc_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_cur_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Tf
function Tf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Tfsld;
Slider_cur_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Tf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tf_max_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tfsld;
Slider_max_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Tf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tf_min_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tfsld;
Slider_min_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Tf_Callback(hObject, eventdata, handles)
% hObject    handle to Tf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Tf, handles.Tf_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% INGRESSI

% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [Kg/s]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.qf_min, 'Value',  values(1));
set(handles.qf_min, 'String', num2str(values(1), '%.1f'));
set(handles.qf_cur, 'Value',  values(2));
set(handles.qf_cur, 'String', num2str(values(2), '%.2f'));
set(handles.qf_max, 'Value',  values(3));
set(handles.qf_max, 'String', num2str(values(3), '%.1f'));
set(handles.qf, 'Min',   values(1));
set(handles.qf, 'Value', values(2));
set(handles.qf, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.qf_min, 'Value',  values(1));
set(handles.qf_min, 'String', num2str(values(1), '%.1f'));
set(handles.qf_cur, 'Value',  values(2));
set(handles.qf_cur, 'String', num2str(values(2), '%.2f'));
set(handles.qf_max, 'Value',  values(3));
set(handles.qf_max, 'String', num2str(values(3), '%.1f'));
set(handles.qf, 'Min',   values(1));
set(handles.qf, 'Max',   values(3));
set(handles.qf, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Slider qc

function qf_max_Callback(hObject, eventdata, handles)
% hObject    handle to qf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_max_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function qf_min_Callback(hObject, eventdata, handles)
% hObject    handle to qf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_min_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to qf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_cur_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on slider movement.
function qf_Callback(hObject, eventdata, handles)
% hObject    handle to qf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.qf, handles.qf_cur);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Slider qc
function qc_cur_Callback(hObject, eventdata, handles)
% hObject    handle to qc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_cur_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qc_max_Callback(hObject, eventdata, handles)
% hObject    handle to qc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_max_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qc_min_Callback(hObject, eventdata, handles)
% hObject    handle to qc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_min_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function qc_Callback(hObject, eventdata, handles)
% hObject    handle to qc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.qc, handles.qc_cur);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%%  SLIDER STATO INIZIALE

% --- Executes on slider movement.
function Te0_Callback(hObject, eventdata, handles)
% hObject    handle to Te0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Te0, handles.Te0_cur);

% --- Executes during object creation, after setting all properties.
function Te0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Te0_min_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_min_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Te0_max_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_max_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Te0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_cur_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
