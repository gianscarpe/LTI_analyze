%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       M I S C E L A T O R E    3.18                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 28-Nov-2017 14:01:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      0.0      % qf_min        4
      0.2      % qf, qf_cur    5
      1.0      % qf_max        6
      0.0      % qc_min        7
      0.1      % qc, qc_cur    8
      1.0      % qc_max        9
      0.0      % M_min         10
      5.0      % M, M_cur      11
      10.0     % M_max         12
      0.0      % Tf_min        13
      10.0     % Tf, Tf_cur    14
      100.0    % Tf_max        15
      0.0      % Tc_min        16
      40.0     % Tc, Tc_cur    17
      100.0    % Tc_max        18
      0.00     % Te0_min       19
      30.0     % Te0, Te0_cur  20
      100.0    % Te0_max       21
     
      ];

% Par degli slider
global Msld Tfsld Tcsld qfsld qcsld Te0sld;

Msld.stmin = 0.1;
Msld.stmax = 1;
Msld.Llim = eps;
Msld.Hlim = +Inf;

Tfsld.stmin = 0.1;
Tfsld.stmax = 1;
Tfsld.Llim = eps;
Tfsld.Hlim = +Inf;

Tcsld.stmin = 0.1;
Tcsld.stmax = 1;
Tcsld.Llim = eps;
Tcsld.Hlim = +Inf;

qfsld.stmin = 0.01;
qfsld.stmax = 0.01;
qfsld.Llim = eps;
qfsld.Hlim = +Inf;

qcsld.stmin = 0.01;
qcsld.stmax = 0.01;
qcsld.Llim = eps;
qcsld.Hlim = +Inf;

Te0sld.stmin = 0.1;
Te0sld.stmax = 1;
Te0sld.Llim = eps;
Te0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles); 
% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Chiusura dello schema simulink (if any) senza salvare
clc

global Uscita;

bdclose(Uscita);

Uscita = 'Temperatura_Serbatoio';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Kg/s]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
qf  = get(handles.qf,  'Value');
qc  = get(handles.qc,  'Value');
M   = get(handles.M,   'Value');
Tf  = get(handles.Tf,  'Value');
Tc  = get(handles.Tc,  'Value');
Te0 = get(handles.Te0, 'Value');

% Controllo sui dati nulli per evitare singolarita'
if M <= 0, M = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


%syms Te_(t) M_ Tf_ Tc_    qf_ qc_
%Te_(t) = dsolve(diff(Te_,t) == -(qf_+qc_) / M_ * Te_ + Tf_/M_*qf_ + Tc_/M_*qc_, Te_(0) == 0)
  
% Costante di tempo
Tau = M/(qf+qc);

vars = {'qf', qf; 'qc', qc; 'Te0', Te0; 'Tf', Tf; 'Tc', Tc; 'M', M};
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end

% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end

% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Kg/s]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.qf_min, 'Value',  ampiezza(1));
set(handles.qf_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.qf_cur, 'Value',  ampiezza(2));
set(handles.qf_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.qf_max, 'Value',  ampiezza(3));
set(handles.qf_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.qf, 'Min',   ampiezza(1));
set(handles.qf, 'Value', ampiezza(2));
set(handles.qf, 'Max',   ampiezza(3));


% Lettura dei dati inseriti dall'utente
qf = get(handles.qf, 'Value'); u(1) = qf;
qc = get(handles.qc, 'Value'); u(2) = qc;
M  = get(handles.M,  'Value');
Tf  = get(handles.Tf,  'Value');
Tc  = get(handles.Tc,  'Value');
Te0  = get(handles.Te0,  'Value');

% Controllo sui dati nulli per evitare singolarita'
if M <= 0, M = eps; end


% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)

% Impostazione dell'equazione dinamica non lineare
% x_sym1 <-> Te0
x_sym = sym('x_sym', [1 1]);
% dTe/dt  = - (qf+qc)/M * x_sym(1) + Tf/M*qf + Tc/M*qc
dx_sym = [ -(u(1)+u(2)) / M * x_sym(1) + Tf/M*u(1) + Tc/M*u(2)];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1));

if size(X) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['qf = ', num2str(u(1), '%.2f'), ' Kg/s']; ...
    ['qc = ', num2str(u(2), '%.2f'), ' Kg/s'] });
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: \n  qf = %.2f Kg/s \n  qc = %.2f Kg/s', u(1), u(2));
for k = 1 : size(X)
  % Soluzione numerica
  x = double(X(k));

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intornno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  Te  = %.2f �C', x(1));

  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
      str3 = '';
  end        

  if k == size(X, 1), endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);



%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Msld Tfsld Tcsld qfsld qcsld Te0sld;

% Aggiornamento degli slider
%M
set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

%Tf
set(handles.Tf_min, 'Value',  Tf_min);
set(handles.Tf_min, 'String', num2str(Tf_min, '%.1f'));
set(handles.Tf_cur, 'Value',  Tf_cur);
set(handles.Tf_cur, 'String', num2str(Tf_cur, '%.2f'));
set(handles.Tf_max, 'Value',  Tf_max);
set(handles.Tf_max, 'String', num2str(Tf_max, '%.1f'));

set(handles.Tf, 'Min',   Tf_min);
set(handles.Tf, 'Value', Tf_cur);
set(handles.Tf, 'Max',   Tf_max);
majorstep = Tfsld.stmax / (Tf_max-Tf_min);
minorstep = Tfsld.stmin / (Tf_max-Tf_min);
set(handles.Tf, 'SliderStep', [minorstep majorstep]);

%Tc
set(handles.Tc_min, 'Value',  Tc_min);
set(handles.Tc_min, 'String', num2str(Tc_min, '%.1f'));
set(handles.Tc_cur, 'Value',  Tc_cur);
set(handles.Tc_cur, 'String', num2str(Tc_cur, '%.2f'));
set(handles.Tc_max, 'Value',  Tc_max);
set(handles.Tc_max, 'String', num2str(Tc_max, '%.1f'));

set(handles.Tc, 'Min',   Tc_min);
set(handles.Tc, 'Value', Tc_cur);
set(handles.Tc, 'Max',   Tc_max);
majorstep = Tcsld.stmax / (Tc_max-Tc_min);
minorstep = Tcsld.stmin / (Tc_max-Tc_min);
set(handles.Tc, 'SliderStep', [minorstep majorstep]);

%Te0
set(handles.Te0_min, 'Value',  Te0_min);
set(handles.Te0_min, 'String', num2str(Te0_min, '%.1f'));
set(handles.Te0_cur, 'Value',  Te0_cur);
set(handles.Te0_cur, 'String', num2str(Te0_cur, '%.2f'));
set(handles.Te0_max, 'Value',  Te0_max);
set(handles.Te0_max, 'String', num2str(Te0_max, '%.1f'));

set(handles.Te0, 'Min',   Te0_min);
set(handles.Te0, 'Value', Te0_cur);
set(handles.Te0, 'Max',   Te0_max);
majorstep = Te0sld.stmax / (Te0_max-Te0_min);
minorstep = Te0sld.stmin / (Te0_max-Te0_min);
set(handles.Te0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [Kg/s]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

%qf
set(handles.qf_min, 'Value',  qf_min);
set(handles.qf_min, 'String', num2str(qf_min, '%.1f'));
set(handles.qf_cur, 'Value',  qf_cur);
set(handles.qf_cur, 'String', num2str(qf_cur, '%.2f'));
set(handles.qf_max, 'Value',  qf_max);
set(handles.qf_max, 'String', num2str(qf_max, '%.1f'));

set(handles.qf, 'Min',   qf_min);
set(handles.qf, 'Value', qf_cur);
set(handles.qf, 'Max',   qf_max);
majorstep = qfsld.stmax / (qf_max-qf_min);
minorstep = qfsld.stmin / (qf_max-qf_min);
set(handles.qf, 'SliderStep', [minorstep majorstep]);

%qc
set(handles.qc_min, 'Value',  qc_min);
set(handles.qc_min, 'String', num2str(qc_min, '%.1f'));
set(handles.qc_cur, 'Value',  qc_cur);
set(handles.qc_cur, 'String', num2str(qc_cur, '%.2f'));
set(handles.qc_max, 'Value',  qc_max);
set(handles.qc_max, 'String', num2str(qc_max, '%.1f'));

set(handles.qc, 'Min',   qc_min);
set(handles.qc, 'Value', qc_cur);
set(handles.qc, 'Max',   qc_max);
majorstep = qcsld.stmax / (qc_max-qc_min);
minorstep = qcsld.stmin / (qc_max-qc_min);
set(handles.qc, 'SliderStep', [minorstep majorstep]);



set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello massa
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello temperatura acqua fredda
Tf_min = get(handles.Tf_min, 'Value');
Tf_cur = get(handles.Tf_cur, 'Value');
Tf_max = get(handles.Tf_max, 'Value');

% pannello temperatura acqua calda
Tc_min = get(handles.Tc_min, 'Value');
Tc_cur = get(handles.Tc_cur, 'Value');
Tc_max = get(handles.Tc_max, 'Value');

% pannello temperatura vasca
Te0_min = get(handles.Te0_min, 'Value');
Te0_cur = get(handles.Te0_cur, 'Value');
Te0_max = get(handles.Te0_max, 'Value');


% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

qf_min = get(handles.qf_min, 'Value');
qf_cur = get(handles.qf_cur, 'Value');
qf_max = get(handles.qf_max, 'Value');

qc_min = get(handles.qc_min, 'Value');
qc_cur = get(handles.qc_cur, 'Value');
qc_max = get(handles.qc_max, 'Value');
% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'qf_min = %f;\n', qf_min);
fprintf(fid, 'qf_cur = %f;\n', qf_cur);
fprintf(fid, 'qf_max = %f;\n', qf_max);

fprintf(fid, 'qc_min = %f;\n', qc_min);
fprintf(fid, 'qc_cur = %f;\n', qc_cur);
fprintf(fid, 'qc_max = %f;\n', qc_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'Tf_min = %f;\n', Tf_min);
fprintf(fid, 'Tf_cur = %f;\n', Tf_cur);
fprintf(fid, 'Tf_max = %f;\n', Tf_max);

fprintf(fid, 'Tc_min = %f;\n', Tc_min);
fprintf(fid, 'Tc_cur = %f;\n', Tc_cur);
fprintf(fid, 'Tc_max = %f;\n', Tc_max);

fprintf(fid, 'Te0_min = %f;\n', Te0_min);
fprintf(fid, 'Te0_cur = %f;\n', Te0_cur);
fprintf(fid, 'Te0_max = %f;\n', Te0_max);


fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');

% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end

function Load_Defaults(handles)

global def;
global Msld Tfsld Tcsld qfsld qcsld Te0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [Kg/s]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider qf
set(handles.qf_min, 'Value', def(4));
set(handles.qf_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.qf_cur, 'Value', def(5));
set(handles.qf_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.qf_max, 'Value', def(6));
set(handles.qf_max, 'String', num2str(def(6), '%.1f'));

set(handles.qf, 'Min',   def(4)); 
set(handles.qf, 'Value', def(5));
set(handles.qf, 'Max',   def(6)); 
majorstep = qfsld.stmax / (def(6)-def(4));
minorstep = qfsld.stmin / (def(6)-def(4));
set(handles.qf, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.qf, 'Min') get(handles.qf, 'Value') get(handles.qf, 'Max')]);
evalin('base', ['input_params(''Ampiezza [Kg/s]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider qc
set(handles.qc_min, 'Value',  def(7));
set(handles.qc_min, 'String', num2str(def(7), '%.1f'));
set(handles.qc_cur, 'Value',  def(8));
set(handles.qc_cur, 'String', num2str(def(8), '%.2f'));
set(handles.qc_max, 'Value',  def(9));
set(handles.qc_max, 'String', num2str(def(9), '%.1f'));

set(handles.qc, 'Min',   def(7));
set(handles.qc, 'Value', def(8));
set(handles.qc, 'Max',   def(9));
majorstep = qcsld.stmax / (def(9)-def(7));
minorstep = qcsld.stmin / (def(9)-def(7));
set(handles.qc, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value',  def(10));
set(handles.M_min, 'String', num2str(def(10), '%.1f'));
set(handles.M_cur, 'Value',  def(11));
set(handles.M_cur, 'String', num2str(def(11), '%.2f'));
set(handles.M_max, 'Value',  def(12));
set(handles.M_max, 'String', num2str(def(12), '%.1f'));

set(handles.M, 'Min',   def(10));
set(handles.M, 'Value', def(11));
set(handles.M, 'Max',   def(12));
majorstep = Msld.stmax / (def(12)-def(10));
minorstep = Msld.stmin / (def(12)-def(10));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider Tf
set(handles.Tf_min, 'Value', def(13));
set(handles.Tf_min, 'String', num2str(def(13), '%.1f'));
set(handles.Tf_cur, 'Value', def(14));
set(handles.Tf_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Tf_max, 'Value', def(15));
set(handles.Tf, 'String', num2str(def(15), '%.1f'));

set(handles.Tf, 'Min',   def(13)); 
set(handles.Tf, 'Value', def(14));
set(handles.Tf, 'Max',   def(15)); 
majorstep = Tfsld.stmax / (def(15)-def(13));
minorstep = Tfsld.stmin / (def(15)-def(13));
set(handles.Tf, 'SliderStep', [minorstep majorstep]);

% slider Tc
set(handles.Tc_min, 'Value', def(16));
set(handles.Tc_min, 'String', num2str(def(16), '%.1f'));
set(handles.Tc_cur, 'Value', def(17));
set(handles.Tc_cur, 'String', num2str(def(17), '%.2f'));
set(handles.Tc_max, 'Value', def(18));
set(handles.Tc_max, 'String', num2str(def(18), '%.1f'));

set(handles.Tc, 'Min',   def(16)); 
set(handles.Tc, 'Value', def(17));
set(handles.Tc, 'Max',   def(18)); 
majorstep = Tcsld.stmax / (def(18)-def(16));
minorstep = Tcsld.stmin / (def(18)-def(16));
set(handles.Tc, 'SliderStep', [minorstep majorstep]);


% slider Te0
set(handles.Te0_min, 'Value', def(19));
set(handles.Te0_min, 'String', num2str(def(19), '%.1f'));
set(handles.Te0_cur, 'Value', def(20));
set(handles.Te0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.Te0_max, 'Value', def(21));
set(handles.Te0_max, 'String', num2str(def(21), '%.1f'));

set(handles.Te0, 'Min',   def(19)); 
set(handles.Te0, 'Value', def(20));
set(handles.Te0, 'Max',   def(21)); 
majorstep = Te0sld.stmax / (def(21)-def(19));
minorstep = Te0sld.stmin / (def(21)-def(19));
set(handles.Te0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');


%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Tc
% --- Executes on slider movement.
function Tc_Callback(hObject, eventdata, handles)
% hObject    handle to Tc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Tc, handles.Tc_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Tc_min_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_min_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tc_max_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_max_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tc_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Tc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tcsld;
Slider_cur_Callback(handles, handles.Tc, handles.Tc_min, handles.Tc_cur, handles.Tc_max, Tcsld.stmin, Tcsld.stmax, Tcsld.Llim, Tcsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tc_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Tf
function Tf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Tfsld;
Slider_cur_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Tf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tf_max_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tfsld;
Slider_max_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Tf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Tf_min_Callback(hObject, eventdata, handles)
% hObject    handle to Tf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tfsld;
Slider_min_Callback(handles, handles.Tf, handles.Tf_min, handles.Tf_cur, handles.Tf_max, Tfsld.stmin, Tfsld.stmax, Tfsld.Llim, Tfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Tf_Callback(hObject, eventdata, handles)
% hObject    handle to Tf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Tf, handles.Tf_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Tf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Tf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% INGRESSI

% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [Kg/s]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.qf_min, 'Value',  values(1));
set(handles.qf_min, 'String', num2str(values(1), '%.1f'));
set(handles.qf_cur, 'Value',  values(2));
set(handles.qf_cur, 'String', num2str(values(2), '%.2f'));
set(handles.qf_max, 'Value',  values(3));
set(handles.qf_max, 'String', num2str(values(3), '%.1f'));
set(handles.qf, 'Min',   values(1));
set(handles.qf, 'Value', values(2));
set(handles.qf, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.qf_min, 'Value',  values(1));
set(handles.qf_min, 'String', num2str(values(1), '%.1f'));
set(handles.qf_cur, 'Value',  values(2));
set(handles.qf_cur, 'String', num2str(values(2), '%.2f'));
set(handles.qf_max, 'Value',  values(3));
set(handles.qf_max, 'String', num2str(values(3), '%.1f'));
set(handles.qf, 'Min',   values(1));
set(handles.qf, 'Max',   values(3));
set(handles.qf, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Slider qc

function qf_max_Callback(hObject, eventdata, handles)
% hObject    handle to qf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_max_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function qf_min_Callback(hObject, eventdata, handles)
% hObject    handle to qf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_min_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to qf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qfsld;
Slider_cur_Callback(handles, handles.qf, handles.qf_min, handles.qf_cur, handles.qf_max, qfsld.stmin, qfsld.stmax, qfsld.Llim, qfsld.Hlim);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on slider movement.
function qf_Callback(hObject, eventdata, handles)
% hObject    handle to qf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.qf, handles.qf_cur);

minval = get(handles.qf_min, 'Value');
curval = get(handles.qf, 'Value');
maxval = get(handles.qf_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Slider qc
function qc_cur_Callback(hObject, eventdata, handles)
% hObject    handle to qc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_cur_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qc_max_Callback(hObject, eventdata, handles)
% hObject    handle to qc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_max_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function qc_min_Callback(hObject, eventdata, handles)
% hObject    handle to qc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global qcsld;
Slider_min_Callback(handles, handles.qc, handles.qc_min, handles.qc_cur, handles.qc_max, qcsld.stmin, qcsld.stmax, qcsld.Llim, qcsld.Hlim);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function qc_Callback(hObject, eventdata, handles)
% hObject    handle to qc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.qc, handles.qc_cur);
% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function qc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



%%  SLIDER STATO INIZIALE

% --- Executes on slider movement.
function Te0_Callback(hObject, eventdata, handles)
% hObject    handle to Te0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Te0, handles.Te0_cur);

% --- Executes during object creation, after setting all properties.
function Te0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Te0_min_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_min_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Te0_max_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_max_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Te0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Te0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Tesld;
Slider_cur_Callback(handles, handles.Te0, handles.Te0_min, handles.Te0_cur, handles.Te0_max, Tesld.stmin, Tesld.stmax, Tesld.Llim, Tesld.Hlim);


% --- Executes during object creation, after setting all properties.
function Te0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Te0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 252.00732421875, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 6, ...
    'uipanel', 23, ...
    'slider', 14, ...
    'edit', 41, ...
    'popupmenu', 9, ...
    'pushbutton', 8, ...
    'listbox', 3, ...
    'text', 3), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\20 - Miscelatore.3.18 - FMM\Main.fig', ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\20 - Miscelatore.3.18 - FMM\Main_export.m');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','MISCELATORE ver. 3.18',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 31.2 168.857142857143 30.6],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.14285714285714 7.6 47.8571428571429 7.25],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.14285714285714 0.4 45.4285714285714 5.75],...
'Box','on',...
'CameraPosition',[511.5 183.5 5428.30112015907],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[9.96079517592166 1.85383688563169 7.27904262855814 1.26397969474887],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 1022.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 366.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[509.893081761006 456.676100628931 1.03235519792997],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-126.446540880503 186.713836477988 1.03235519792997],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-58.9559748427672 -1073.11006289308 1.03235519792997],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel4';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[80.1428571428571 9.8 42.4285714285714 16.7],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel7';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Massa fluido nel serbatoio [kg]',...
'Clipping','on',...
'Position',[0.571428571428571 10.6076923076923 40 5],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'M';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('M_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.14285714285714 2.35 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M');

appdata = [];
appdata.lastValidTag = 'M_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.6 8 1.1],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_min');

appdata = [];
appdata.lastValidTag = 'M_max';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.6 8 1.1],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_max');

appdata = [];
appdata.lastValidTag = 'M_cur';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.6 8 1.1],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_cur');

appdata = [];
appdata.lastValidTag = 'uipanel17';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Temperatura acqua fredda Tf [�C]',...
'Clipping','on',...
'Position',[0.6 5.62307692307693 40 5],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Tf_cur';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tf_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.6 8 1.1],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tf_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tf_cur');

appdata = [];
appdata.lastValidTag = 'Tf_max';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tf_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.6 8 1.1],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tf_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tf_max');

appdata = [];
appdata.lastValidTag = 'Tf_min';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tf_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.6 8 1.1],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tf_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tf_min');

appdata = [];
appdata.lastValidTag = 'Tf';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Tf_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.14285714285714 2.35 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tf_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tf');

appdata = [];
appdata.lastValidTag = 'uipanel18';

h18 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Temperatura acqua calda Tc [�C]',...
'Clipping','on',...
'Position',[0.6 0.546153846153849 40 5],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Tc';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Tc_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.14285714285714 2.35 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tc_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tc');

appdata = [];
appdata.lastValidTag = 'Tc_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tc_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.6 8 1.1],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tc_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tc_min');

appdata = [];
appdata.lastValidTag = 'Tc_max';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tc_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.6 8 1.1],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tc_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tc_max');

appdata = [];
appdata.lastValidTag = 'Tc_cur';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Tc_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.6 8 1.1],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Tc_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Tc_cur');

appdata = [];
appdata.lastValidTag = 'Run';

h23 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[7 3.3 21.5714285714286 1.75],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h24 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'ShadowColor',[0.501960784313725 0.501960784313725 0.501960784313725],...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[123.714285714286 0.45 43 3.25],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h25 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.6 0.557692307692308 39.6 1.46153846153846],...
'String','Temperatura nel serbatoio',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h26 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[123.714285714286 10.2 43 16.3],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel8';

h27 = uipanel(...
'Parent',h26,...
'Units','characters',...
'Title','Portata acqua fredda qf [kg/s]',...
'Clipping','on',...
'Position',[1.8 5.3 40 9.69230769230769],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'qf_min';

h28 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qf_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.585104302477217 8 1.1],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qf_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qf_min');

appdata = [];
appdata.lastValidTag = 'qf_cur';

h29 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qf_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.4 0.557637983485441 8 1.1],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qf_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qf_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h30 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.25690500966255 6.29753432946778 37 1.77864473684211],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h31 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.25690500966255 4.09540275052041 37 2.03273684210527],...
'String','Ampiezza [V]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'qf_max';

h32 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qf_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[29.2857142857143 0.585104302477217 8 1.1],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qf_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qf_max');

appdata = [];
appdata.lastValidTag = 'qf';

h33 = uicontrol(...
'Parent',h27,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('qf_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.2 2.45384615384616 37 1.46153846153846],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qf_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qf');

appdata = [];
appdata.lastValidTag = 'uipanel22';

h34 = uipanel(...
'Parent',h26,...
'Units','characters',...
'Title','Portata acqua calda qc [kg/s]',...
'Clipping','on',...
'Position',[1.85714285714286 0.45 40 4.6],...
'Tag','uipanel22',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'qc_cur';

h35 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qc_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.15 8 1.1],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qc_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qc_cur');

appdata = [];
appdata.lastValidTag = 'qc_max';

h36 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qc_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.15 8 1.1],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qc_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qc_max');

appdata = [];
appdata.lastValidTag = 'qc_min';

h37 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('qc_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.2 8 1.1],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qc_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qc_min');

appdata = [];
appdata.lastValidTag = 'qc';

h38 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('qc_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 1.76502622377623 37 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('qc_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','qc');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h39 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[87.5714285714286 0.9 27.1428571428571 2],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h40 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[79.7142857142857 3.15 42.8571428571429 6.3],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h41 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'FontName','Courier',...
'HorizontalAlignment','left',...
'Position',[0.857142857142857 0.249999999999999 40.5714285714286 4.85],...
'String',blanks(0),...
'Style','text',...
'Value',1,...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel13';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.14285714285714 15.2 77.5714285714286 14.8],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h43 = axes(...
'Parent',h42,...
'Units','characters',...
'Position',[1.28571428571429 0.469230769230759 75 13.3],...
'Box','on',...
'CameraPosition',[810 409 9067.81906353802],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[13.1152421964991 2.87559598148166 9.5842154512878 1.96063362373749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 1619.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 817.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h44 = get(h43,'xlabel');

set(h44,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[806.91619047619 903.951428571428 1.05404841663767],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h45 = get(h43,'ylabel');

set(h45,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-121.310476190476 413.625714285714 1.05404841663767],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h46 = get(h43,'zlabel');

set(h46,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-59.6342857142857 -92.1190476190476 1.05404841663767],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h47 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[123.714285714286 3.65 43 6.6],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h48 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Temperatura serbatoio Te0 [�C]',...
'Clipping','on',...
'Position',[1.17142857142857 0.349999999999999 40 5],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Te0';

h49 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Te0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'TooltipString','Tensione iniziale del condensatore',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Te0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Te0');

appdata = [];
appdata.lastValidTag = 'Te0_min';

h50 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Te0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.1],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Te0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Te0_min');

appdata = [];
appdata.lastValidTag = 'Te0_max';

h51 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Te0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.1],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Te0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Te0_max');

appdata = [];
appdata.lastValidTag = 'Te0_cur';

h52 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Te0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Te0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Te0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h53 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[80.1428571428571 26.8 86.5714285714286 3.15],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h54 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.499999999999993 8.57142857142857 1.45],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h55 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[73.1428571428571 0.499999999999993 9 1.45],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h56 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.28571428571429 0.499999999999993 21 1.45],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h57 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[51.8571428571429 0.599999999999994 20 1.3],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h58 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.499999999999993 8 1.45],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


