% Ingressi
IngressoTipo = 1;
IngressoPar = 1;
qf_min = 0.000000;
qf_cur = 1.000000;
qf_max = 1.000000;
qc_min = 0.000000;
qc_cur = 1.000000;
qc_max = 1.000000;

% Uscita
Uscita = 1;

% Parametri
M_min = 0.000000;
M_cur = 10.000000;
M_max = 10.000000;
Tf_min = 0.000000;
Tf_cur = 100.000000;
Tf_max = 100.000000;
Tc_min = 0.000000;
Tc_cur = 100.000000;
Tc_max = 100.000000;
Te0_min = 0.000000;
Te0_cur = 100.000000;
Te0_max = 100.000000;
