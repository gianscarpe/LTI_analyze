%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           P O L M O N E  4.5                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alberto Margari (2011)
% by Barbara Bertani (2015)
% by Mattia Lorenzetti (2016)
% by F. M. Marchese (2011-17)
%
% Tested with ver. MatLab R2013b
%


%% INIT
function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 22-Sep-2016 17:39:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1         % IngressoTipo
      1         % IngressoPar
      1         % Uscita
      0         % Pi_min
      300000    % Pi, Pi_cur
      500000    % Pi_max
      1         % A_min
      2         % A, A_cur
      10        % A_max
      0         % H_min
      5         % H, H_cur
      10        % H_max
      100       % R1_min
      5000      % R1, R1_cur
      100000    % R1_max
      100       % R2_min
      1000      % R2, R2_cur
      200000    % R2_max
      1000      % c_min
      50000     % c, c_cur
      400000    % c_max
      0.6       % rho_min
      1         % rho, rho_cur
      16        % rho_max
      0         % h0_min
      3         % h0, h0_cur
      10        % h0_max
      5         % rho_par
      ];

% Par degli slider
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;

Pisld.stmin = 1000;
Pisld.stmax = 10000;
Pisld.Llim =  eps;
Pisld.Hlim = +Inf;

Hsld.stmin = 0.1;
Hsld.stmax = 1;
Hsld.Llim =  eps;
Hsld.Hlim = +Inf;

Asld.stmin = 0.1;
Asld.stmax = 1;
Asld.Llim =  eps;
Asld.Hlim = +Inf;

R1sld.stmin = 1000;
R1sld.stmax = 5000;
R1sld.Llim =  eps;
R1sld.Hlim = +Inf;

R2sld.stmin = 1000;
R2sld.stmax = 5000;
R2sld.Llim =  eps;
R2sld.Hlim = +Inf;

csld.stmin = 1000;
csld.stmax = 10000;
csld.Llim =  eps;
csld.Hlim = +Inf;

rhosld.stmin = 0.1;
rhosld.stmax = 1;
rhosld.Llim =  eps;
rhosld.Hlim = +Inf;

h0sld.stmin = 0.1;
h0sld.stmax = 1;
h0sld.Llim =  eps;
h0sld.Hlim = +Inf;
    
evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);

% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


evalin('base', ['input_params(''Ampiezza [Pa]'') = ' mat2str([get(handles.Pi, 'Min') get(handles.Pi, 'Value') get(handles.Pi, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

global Uscita;

% Chiusura dello schema simulink (if any) senza salvare
bdclose(Uscita);

% Leggo la variabile di uscita del sistema (y)
Uscita = 'Polmone';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Pa]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
h0  = get(handles.h0, 'value');
Pi  = ampiezza(2);
Pe  = 101325;
A   = get(handles.A, 'value');
R1  = get(handles.R1, 'value');
R2  = get(handles.R2, 'value');
H   = get(handles.H, 'value');
c   = get(handles.c, 'value');
rho = get(handles.rho, 'value'); rho = rho * 1000;
g   = 9.81;
 
% Controllo sui dati nulli per evitare singolarita' (1/0)
if R1 == 0, R1 = eps; end 
if R2 == 0, R2 = eps; end 
if A == 0, return; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% dh/dt = ...
% x_sym1 <-> h
x_sym = sym('x_sym', [1 1]);
dx_sym = [-rho*g/(A*R2)*x_sym(1) - c*(1/R1+1/R2)/(A*A)/(H-x_sym(1)) + Pi/(A*R1) + Pe/(A*R2)];
X = solve(dx_sym == 0, x_sym(1), 'PrincipalValue', true);
% Calcolo dello jacobiano simbolico (linearizzazione)
F = jacobian(dx_sym);
x = double([X(1)]);
F = double(subs(F, x_sym, x));

Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe
Tau = 0.03

% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilita'�
vars = {'h0', h0; 'H', H; 'Pi', Pi; 'A', A; 'R1', R1; 'rho', rho; 'c', c; 'R2', R2};
for i = 1:size(vars, 1)
  name  = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end

% Creazione della finestra simulink (browser libreria) 
% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end


% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Pa]'')');

set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.Pi_min, 'Value',  ampiezza(1));
set(handles.Pi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.Pi_cur, 'Value',  ampiezza(2));
set(handles.Pi_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.Pi_max, 'Value',  ampiezza(3));
set(handles.Pi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.Pi, 'Min',   ampiezza(1));
set(handles.Pi, 'Value', ampiezza(2));
set(handles.Pi, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
Pi = get(handles.Pi, 'Value'); u = Pi;
h0  = get(handles.h0, 'Value');
Pe  = 101325;
A   = get(handles.A, 'Value');
H   = get(handles.H, 'Value');
R1  = get(handles.R1, 'Value');
R2  = get(handles.R2, 'Value');
c   = get(handles.c, 'Value');
rho = get(handles.rho, 'Value'); rho = rho * 1000;
g   = 9.81;

% controllo sui dati nulli
if R1 == 0, R1 = eps; end 
if R2 == 0, R2 = eps; end 
if A == 0, return; end


% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica

% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% x_sym1 <-> h
x_sym = sym('x_sym', [1 1]);
% dh/dt = ...
dx_sym = [-rho*g/R2*x_sym(1)-(1/R1+1/R2)*c/A/(H-x_sym(1)) + u/R1 + Pe/R2];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1));
% Solo il val principale (no periodicita')
% [x(1) x(2)] = solve(dx_sym == 0, x_sym(1), x_sym(2), 'PrincipalValue', true);
% x = double(x)';

if size(X) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['Pi = ', num2str(u(1), '%.1f'), ' Pa']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: Pi = %.1f Pa', u(1));
for k = 1 : size(X)
  % Soluzione numerica
  x = double(X(k));

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intorno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X) == 1, str1 = sprintf('\nlo stato:');
  else             str1 = sprintf('\n- lo stato'); end
  str21 = sprintf('\n  h = %.1f m', x(1));
  
  if x(1) < 0 || x(1) > H
    str3 = sprintf('\n non e'' ammissibile');
  else
    % Stabilita'
    switch StabilityLTI(F)
      case -1.2
        str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
      case -1.1
        str3 = sprintf('\n e'' asintoticamente stabile');
      case  0.1
        str3 = sprintf('\n e'' semplicemente stabile');
      case  0.2
        str3 = sprintf('\n e'' semplicemente stabile (osc.)');
      case +1.1
        str3 = sprintf('\n e'' debolmente instabile');
      case +1.2
        str3 = sprintf('\n e'' debolmente instabile (osc.)');
      case +2.1
        str3 = sprintf('\n e'' fortemente instabile');
      case +2.2
        str3 = sprintf('\n e'' fortemente instabile (osc.)');
      otherwise
        str1 = sprintf('\nper lo stato:');
        str3 = sprintf('\n la stabilit� NON e'' determinabile');
    end       
  end

  endstr = '.';
  str = strcat(str, str1, str21, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);



% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      Save_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);
% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;
set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

set(handles.H_min, 'Value',  H_min);
set(handles.H_min, 'String', num2str(H_min, '%.1f'));
set(handles.H_cur, 'Value',  H_cur);
set(handles.H_cur, 'String', num2str(H_cur, '%.2f'));
set(handles.H_max, 'Value',  H_max);
set(handles.H_max, 'String', num2str(H_max, '%.1f'));

set(handles.H, 'Min',   H_min);
set(handles.H, 'Value', H_cur);
set(handles.H, 'Max',   H_max);
majorstep = Hsld.stmax / (H_max-H_min);
minorstep = Hsld.stmin / (H_max-H_min);
set(handles.H, 'SliderStep', [minorstep majorstep]);

set(handles.R1_min, 'Value',  R1_min);
set(handles.R1_min, 'String', num2str(R1_min, '%.1f'));
set(handles.R1_cur, 'Value',  R1_cur);
set(handles.R1_cur, 'String', num2str(R1_cur, '%.2f'));
set(handles.R1_max, 'Value',  R1_max);
set(handles.R1_max, 'String', num2str(R1_max, '%.1f'));

set(handles.R1, 'Min',   R1_min);
set(handles.R1, 'Value', R1_cur);
set(handles.R1, 'Max',   R1_max);
majorstep = R1sld.stmax / (R1_max-R1_min);
minorstep = R1sld.stmin / (R1_max-R1_min);
set(handles.R1, 'SliderStep', [minorstep majorstep]);

set(handles.R2_min, 'Value',  R2_min);
set(handles.R2_min, 'String', num2str(R2_min, '%.1f'));
set(handles.R2_cur, 'Value',  R2_cur);
set(handles.R2_cur, 'String', num2str(R2_cur, '%.2f'));
set(handles.R2_max, 'Value',  R2_max);
set(handles.R2_max, 'String', num2str(R2_max, '%.1f'));

set(handles.R2, 'Min',   R2_min);
set(handles.R2, 'Value', R2_cur);
set(handles.R2, 'Max',   R2_max);
majorstep = R2sld.stmax / (R2_max-R2_min);
minorstep = R2sld.stmin / (R2_max-R2_min);
set(handles.R2, 'SliderStep', [minorstep majorstep]);

set(handles.c_min, 'Value',  c_min);
set(handles.c_min, 'String', num2str(c_min, '%.1f'));
set(handles.c_cur, 'Value',  c_cur);
set(handles.c_cur, 'String', num2str(c_cur, '%.2f'));
set(handles.c_max, 'Value',  c_max);
set(handles.c_max, 'String', num2str(c_max, '%.1f'));

set(handles.c, 'Min',   c_min);
set(handles.c, 'Value', c_cur);
set(handles.c, 'Max',   c_max);
majorstep = csld.stmax / (c_max-c_min);
minorstep = csld.stmin / (c_max-c_min);
set(handles.c, 'SliderStep', [minorstep majorstep]);

set(handles.rho_par, 'Value',  rho_par);
set(handles.rho_min, 'Value',  rho_min);
set(handles.rho_min, 'String', num2str(rho_min, '%.1f'));
set(handles.rho_cur, 'Value',  rho_cur);
set(handles.rho_cur, 'String', num2str(rho_cur, '%.2f'));
set(handles.rho_max, 'Value',  rho_max);
set(handles.rho_max, 'String', num2str(rho_max, '%.1f'));

set(handles.rho, 'Min',   rho_min);
set(handles.rho, 'Value', rho_cur);
set(handles.rho, 'Max',   rho_max);
majorstep = rhosld.stmax / (rho_max-rho_min);
minorstep = rhosld.stmin / (rho_max-rho_min);
set(handles.rho, 'SliderStep', [minorstep majorstep]);

set(handles.h0_min, 'Value',  h0_min);
set(handles.h0_min, 'String', num2str(h0_min, '%.1f'));
set(handles.h0_cur, 'Value',  h0_cur);
set(handles.h0_cur, 'String', num2str(h0_cur, '%.2f'));
set(handles.h0_max, 'Value',  h0_max);
set(handles.h0_max, 'String', num2str(h0_max, '%.1f'));

set(handles.h0, 'Min',   h0_min);
set(handles.h0, 'Value', h0_cur);
set(handles.h0, 'Max',   h0_max);
majorstep = h0sld.stmax / (h0_max-h0_min);
minorstep = h0sld.stmin / (h0_max-h0_min);
set(handles.h0, 'SliderStep', [minorstep majorstep]);

% ingressi
Ingressoparstr = cell(1);
Ingressoparstr{1} = 'Ampiezza [Pa]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Ingressoparstr{2} = 'Frequenza�[Hz]';
  case 5
      Ingressoparstr{2} = 'Frequenza [Hz]';
      Ingressoparstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      Ingressoparstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', Ingressoparstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.Pi_min, 'Value',  Pi_min);
set(handles.Pi_min, 'String', num2str(Pi_min, '%.1f'));
set(handles.Pi_cur, 'Value',  Pi_cur);
set(handles.Pi_cur, 'String', num2str(Pi_cur, '%.2f'));
set(handles.Pi_max, 'Value',  Pi_max);
set(handles.Pi_max, 'String', num2str(Pi_max, '%.1f'));

set(handles.Pi, 'Min',   Pi_min);
set(handles.Pi, 'Value', Pi_cur);
set(handles.Pi, 'Max',   Pi_max);
majorstep = Pisld.stmax / (Pi_max-Pi_min);
minorstep = Pisld.stmin / (Pi_max-Pi_min);
set(handles.Pi, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);




% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello AREA
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello altezza
H_min = get(handles.H_min, 'Value');
H_cur = get(handles.H_cur, 'Value');
H_max = get(handles.H_max, 'Value');

% pannello resistenza valvola 1
R1_min = get(handles.R1_min, 'Value');
R1_cur = get(handles.R1_cur, 'Value');
R1_max = get(handles.R1_max, 'Value');

% pannello resistenza valvola 2
R2_min = get(handles.R2_min, 'Value');
R2_cur = get(handles.R2_cur, 'Value');
R2_max = get(handles.R2_max, 'Value');

% pannello costante boyle
c_min = get(handles.c_min, 'Value');
c_cur = get(handles.c_cur, 'Value');
c_max = get(handles.c_max, 'Value');

% pannello peso specifico
rho_par = get(handles.rho_par, 'Value');
rho_min = get(handles.rho_min, 'Value');
rho_cur = get(handles.rho_cur, 'Value');
rho_max = get(handles.rho_max, 'Value');


% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

Pi_min = get(handles.Pi_min, 'Value');
Pi_cur = get(handles.Pi_cur, 'Value');
Pi_max = get(handles.Pi_max, 'Value');

% pannello stato iniziale
h0_min = get(handles.h0_min, 'Value');
h0_cur = get(handles.h0_cur, 'Value');
h0_max = get(handles.h0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoPar);
fprintf(fid, 'Uscita = %d;\n', Uscita);

fprintf(fid, 'Pi_min = %f;\n', Pi_min);
fprintf(fid, 'Pi_cur = %f;\n', Pi_cur);
fprintf(fid, 'Pi_max = %f;\n', Pi_max);

fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'H_min = %f;\n', H_min);
fprintf(fid, 'H_cur = %f;\n', H_cur);
fprintf(fid, 'H_max = %f;\n', H_max);

fprintf(fid, 'R1_min = %f;\n', R1_min);
fprintf(fid, 'R1_cur = %f;\n', R1_cur);
fprintf(fid, 'R1_max = %f;\n', R1_max);

fprintf(fid, 'R2_min = %f;\n', R2_min);
fprintf(fid, 'R2_cur = %f;\n', R2_cur);
fprintf(fid, 'R2_max = %f;\n', R2_max);

fprintf(fid, 'c_min = %f;\n', c_min);
fprintf(fid, 'c_cur = %f;\n', c_cur);
fprintf(fid, 'c_max = %f;\n', c_max);

fprintf(fid, 'rho_par = %d;\n', rho_par);

fprintf(fid, 'rho_min = %f;\n', rho_min);
fprintf(fid, 'rho_cur = %f;\n', rho_cur);
fprintf(fid, 'rho_max = %f;\n', rho_max);

fprintf(fid, 'h0_min = %f;\n', h0_min);
fprintf(fid, 'h0_cur = %f;\n', h0_cur);
fprintf(fid, 'h0_max = %f;\n', h0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;


% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject,'String')) returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;


%% Slider H
function H_cur_Callback(hObject, eventdata, handles)
% hObject    handle to H_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_cur_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_max_Callback(hObject, eventdata, handles)
% hObject    handle to H_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_max_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_min_Callback(hObject, eventdata, handles)
% hObject    handle to H_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_min_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function H_Callback(hObject, eventdata, handles)
% hObject    handle to H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.H, handles.H_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Slider c
% --- Executes on slider movement.
function c_Callback(hObject, eventdata, handles)
% hObject    handle to c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.c, handles.c_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function c_min_Callback(hObject, eventdata, handles)
% hObject    handle to c_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_min_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function c_max_Callback(hObject, eventdata, handles)
% hObject    handle to c_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_max_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function c_cur_Callback(hObject, eventdata, handles)
% hObject    handle to c_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_cur_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Silder R2
function R2_max_Callback(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_max_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R2_min_Callback(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_min_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function R2_Callback(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R2, handles.R2_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R2_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_cur_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% slider rho
% --- Executes on slider movement.
function rho_Callback(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.rho, handles.rho_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function rho_min_Callback(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_min_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rho_max_Callback(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_max_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rho_cur_Callback(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_cur_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in rho_par.
function rho_par_Callback(hObject, eventdata, handles)
% hObject    handle to rho_par (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val = get(hObject,'Value');
switch val
    case 1
    set(handles.rho_cur,'String','0.670');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 2
    set(handles.rho_cur,'String','0.760');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 3
    set(handles.rho_cur,'String','0.920');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 4
    set(handles.rho_cur,'String','0.960');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 5
    set(handles.rho_cur,'String','1.000');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 6
    set(handles.rho_cur,'String','13.595');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 7
    set(handles.rho_cur,'Enable','on');
    set(handles.rho_min,'Enable','on');
    set(handles.rho_max,'Enable','on');
    set(handles.rho,'Enable','on');
end

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_par_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_par (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider R2
% --- Executes on slider movement.
function R1_Callback(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R1, handles.R1_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R1_min_Callback(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_min_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_max_Callback(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_max_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_cur_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% slider A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.A, handles.A_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% INGRESSI
% --- Executes on slider movement.
function Pi_Callback(hObject, eventdata, handles)
% hObject    handle to Pi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Pi, handles.Pi_cur);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function Pi_min_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_min_Callback(handles, handles.H, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Pi_max_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_max_Callback(handles, handles.H, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Pi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_cur_Callback(handles, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [Pa]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    new_String{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    new_String{2} = 'Frequenza [Hz]'; 
    new_String{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    new_String{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.Pi_min, 'Value',  values(1));
set(handles.Pi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Pi_cur, 'Value',  values(2));
set(handles.Pi_cur, 'String', num2str(values(2), '%.1f'));
set(handles.Pi_max, 'Value',  values(3));
set(handles.Pi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Pi, 'Min',   values(1));
set(handles.Pi, 'Value', values(2));
set(handles.Pi, 'Max',   values(3));



% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.Pi_min, 'Value',  values(1));
set(handles.Pi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Pi_cur, 'Value',  values(2));
set(handles.Pi_cur, 'String', num2str(values(2), '%.1f'));
set(handles.Pi_max, 'Value',  values(3));
set(handles.Pi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Pi, 'Min',   values(1));
set(handles.Pi, 'Max',   values(3));
set(handles.Pi, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%  SLIDER STATO INIZIALE
% --- Executes on slider movement.
function h0_Callback(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h0, handles.h0_cur);


% --- Executes during object creation, after setting all properties.
function h0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function h0_min_Callback(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_min_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h0_max_Callback(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_max_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_cur_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;

% ingressi
Ingressoparstr = cell(1);
Ingressoparstr{1} = 'Ampiezza [Pa]';
Ingressoparval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    Ingressoparstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    Ingressoparstr{2} = 'Frequenza [Hz]'; 
    Ingressoparstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    Ingressoparstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', Ingressoparstr);
set(handles.IngressoPar, 'Value', def(2));
set(handles.IngressoTipo, 'Value', def(1));


% uscita
set(handles.Uscita, 'Value', def(3));

% slider Pi
set(handles.Pi_min, 'Value', def(4));
set(handles.Pi_min, 'String', num2str(def(4), '%.1f')); 
set(handles.Pi_cur, 'Value', def(5));
set(handles.Pi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.Pi_max, 'Value', def(6));
set(handles.Pi_max, 'String', num2str(def(6), '%.1f'));

set(handles.Pi, 'Min',   def(4)); 
set(handles.Pi, 'Value', def(5));
set(handles.Pi, 'Max',   def(6)); 
majorstep = Pisld.stmax / (def(6)-def(4));
minorstep = Pisld.stmin / (def(6)-def(4));
set(handles.Pi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.Pi, 'Min') get(handles.Pi, 'Value') get(handles.Pi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [Pa]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider A
set(handles.A_min, 'Value',  def(7));
set(handles.A_min, 'String', num2str(def(7), '%.1f'));
set(handles.A_cur, 'Value',  def(8));
set(handles.A_cur, 'String', num2str(def(8), '%.2f'));
set(handles.A_max, 'Value',  def(9));
set(handles.A_max, 'String', num2str(def(9), '%.1f'));

set(handles.A, 'Min',   def(7));
set(handles.A, 'Value', def(8));
set(handles.A, 'Max',   def(9));
majorstep = Asld.stmax / (def(9)-def(7));
minorstep = Asld.stmin / (def(9)-def(7));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider H
set(handles.H_min, 'Value',  def(10));
set(handles.H_min, 'String', num2str(def(10), '%.1f'));
set(handles.H_cur, 'Value',  def(11));
set(handles.H_cur, 'String', num2str(def(11), '%.2f'));
set(handles.H_max, 'Value',  def(12));
set(handles.H_max, 'String', num2str(def(12), '%.1f'));

set(handles.H, 'Min',   def(10));
set(handles.H, 'Value', def(11));
set(handles.H, 'Max',   def(12));
majorstep = Hsld.stmax / (def(12)-def(10));
minorstep = Hsld.stmin / (def(12)-def(10));
set(handles.H, 'SliderStep', [minorstep majorstep]);

% slider R1
set(handles.R1_min, 'Value',  def(13));
set(handles.R1_min, 'String', num2str(def(13), '%.1f'));
set(handles.R1_cur, 'Value',  def(14));
set(handles.R1_cur, 'String', num2str(def(14), '%.2f'));
set(handles.R1_max, 'Value',  def(15));
set(handles.R1_max, 'String', num2str(def(15), '%.1f'));

set(handles.R1, 'Min',   def(13));
set(handles.R1, 'Value', def(14));
set(handles.R1, 'Max',   def(15));
majorstep = R1sld.stmax / (def(15)-def(13));
minorstep = R1sld.stmin / (def(15)-def(13));
set(handles.R1, 'SliderStep', [minorstep majorstep]);

% slider R2
set(handles.R2_min, 'Value',  def(16));
set(handles.R2_min, 'String', num2str(def(16), '%.1f'));
set(handles.R2_cur, 'Value',  def(17));
set(handles.R2_cur, 'String', num2str(def(17), '%.2f'));
set(handles.R2_max, 'Value',  def(18));
set(handles.R2_max, 'String', num2str(def(18), '%.1f'));

set(handles.R2, 'Min',   def(16));
set(handles.R2, 'Value', def(17));
set(handles.R2, 'Max',   def(18));
majorstep = R2sld.stmax / (def(18)-def(16));
minorstep = R2sld.stmin / (def(18)-def(16));
set(handles.R2, 'SliderStep', [minorstep majorstep]);

% slider c
set(handles.c_min, 'Value',  def(19));
set(handles.c_min, 'String', num2str(def(19), '%.1f'));
set(handles.c_cur, 'Value',  def(20));
set(handles.c_cur, 'String', num2str(def(20), '%.2f'));
set(handles.c_max, 'Value',  def(21));
set(handles.c_max, 'String', num2str(def(21), '%.1f'));

set(handles.c, 'Min',   def(19));
set(handles.c, 'Value', def(20));
set(handles.c, 'Max',   def(21));
majorstep = csld.stmax / (def(21)-def(19));
minorstep = csld.stmin / (def(21)-def(19));
set(handles.c, 'SliderStep', [minorstep majorstep]);

% slider rho
set(handles.rho_min, 'Value',  def(22));
set(handles.rho_min, 'String', num2str(def(22), '%.1f'));
set(handles.rho_cur, 'Value',  def(23));
set(handles.rho_cur, 'String', num2str(def(23), '%.2f'));
set(handles.rho_max, 'Value',  def(24));
set(handles.rho_max, 'String', num2str(def(24), '%.1f'));

set(handles.rho, 'Min',   def(22));
set(handles.rho, 'Value', def(23));
set(handles.rho, 'Max',   def(24));
majorstep = rhosld.stmax / (def(24)-def(22));
minorstep = rhosld.stmin / (def(24)-def(22));
set(handles.rho, 'SliderStep', [minorstep majorstep]);

% slider h0
set(handles.h0_min, 'Value', def(25));
set(handles.h0_min, 'String', num2str(def(25), '%.1f'));
set(handles.h0_cur, 'Value', def(26));
set(handles.h0_cur, 'String', num2str(def(26), '%.2f'));
set(handles.h0_max, 'Value', def(27));
set(handles.h0_max, 'String', num2str(def(27), '%.1f'));

set(handles.h0, 'Min',   def(25)); 
set(handles.h0, 'Value', def(26));
set(handles.h0, 'Max',   def(27)); 
majorstep = h0sld.stmax / (def(27)-def(25));
minorstep = h0sld.stmin / (def(27)-def(25));
set(handles.h0, 'SliderStep', [minorstep majorstep]);

set(handles.rho_par, 'Value', def(28));

set(handles.ConfigSaveName, 'String', 'nomefile');
