%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                           P O L M O N E  4.1                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Alberto Margari (2011)
% by Barbara Bertani (2015)
% by Mattia Lorenzetti (2016)
% by F. M. Marchese (2011-16)
%
% Tested with ver. MatLab R2013b
%


%% INIT
function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to
%      the existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN_EXPORT.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 01-Dec-2016 16:23:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1         % IngressoTipo
      1         % IngressoPar
      1         % Uscita
      0         % Pi_min
      300000    % Pi, Pi_cur
      500000    % Pi_max
      1         % A_min
      2         % A, A_cur
      10        % A_max
      0         % H_min
      5         % H, H_cur
      10        % H_max
      100       % R1_min
      5000      % R1, R1_cur
      100000    % R1_max
      100       % R2_min
      1000      % R2, R2_cur
      200000    % R2_max
      1000      % c_min
      50000     % c, c_cur
      400000    % c_max
      0.6       % rho_min
      1         % rho, rho_cur
      16        % rho_max
      0         % h0_min
      3         % h0, h0_cur
      10        % h0_max
      5         % rho_par
      ];

% Par degli slider
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;

Pisld.stmin = 1000;
Pisld.stmax = 10000;
Pisld.Llim =  eps;
Pisld.Hlim = +Inf;

Hsld.stmin = 0.1;
Hsld.stmax = 1;
Hsld.Llim =  eps;
Hsld.Hlim = +Inf;

Asld.stmin = 0.1;
Asld.stmax = 1;
Asld.Llim =  eps;
Asld.Hlim = +Inf;

R1sld.stmin = 1000;
R1sld.stmax = 5000;
R1sld.Llim =  eps;
R1sld.Hlim = +Inf;

R2sld.stmin = 1000;
R2sld.stmax = 5000;
R2sld.Llim =  eps;
R2sld.Hlim = +Inf;

csld.stmin = 1000;
csld.stmax = 10000;
csld.Llim =  eps;
csld.Hlim = +Inf;

rhosld.stmin = 0.1;
rhosld.stmax = 1;
rhosld.Llim =  eps;
rhosld.Hlim = +Inf;

h0sld.stmin = 0.1;
h0sld.stmax = 1;
h0sld.Llim =  eps;
h0sld.Hlim = +Inf;
    
evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);

% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


evalin('base', ['input_params(''Ampiezza [Pa]'') = ' mat2str([get(handles.Pi, 'Min') get(handles.Pi, 'Value') get(handles.Pi, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');


% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc;

global Uscita;

% Chiusura dello schema simulink (if any) senza salvare
bdclose(Uscita);

% Leggo la variabile di uscita del sistema (y)
Uscita = 'Polmone';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [Pa]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
h0  = get(handles.h0, 'value');
Pi  = ampiezza(2);
Pe  = 101325;
A   = get(handles.A, 'value');
R1  = get(handles.R1, 'value');
R2  = get(handles.R2, 'value');
H   = get(handles.H, 'value');
c   = get(handles.c, 'value');
rho = get(handles.rho, 'value'); rho = rho * 1000;
g   = 9.81;
 
% Controllo sui dati nulli per evitare singolarita' (1/0)
if R1 == 0, R1 = eps; end 
if R2 == 0, R2 = eps; end 
if A == 0, return; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% dh/dt = ...
% x_sym1 <-> h
x_sym = sym('x_sym', [1 1]);
dx_sym = [-rho*g/(A*R2)*x_sym(1) - c*(1/R1+1/R2)/(A*A)/(H-x_sym(1)) + Pi/(A*R1) + Pe/(A*R2)];
X = solve(dx_sym == 0, x_sym(1), 'PrincipalValue', true);
% Calcolo dello jacobiano simbolico (linearizzazione)
F = jacobian(dx_sym);
x = double([X(1)]);
F = double(subs(F, x_sym, x));

Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe
Tau = 0.03

% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilita'�
vars = {'h0', h0; 'H', H; 'Pi', Pi; 'A', A; 'R1', R1; 'rho', rho; 'c', c; 'R2', R2};
for i = 1:size(vars, 1)
  name  = vars(i, 1);
  value = vars(i, 2);
  assignin('base', name{1}, value{1}); 
end

% Creazione della finestra simulink (browser libreria) 
% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  delete_line(Uscita, 'input/1', 'Gain/1');
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_line(Uscita, 'step2/1', 'input/2');
  end
  delete_block([Uscita, '/input']);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_block([Uscita, '/step1']);
    delete_block([Uscita, '/step2']);
  end
end

% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
if val ~= 2
    set_param([Uscita, '/input'], 'Position', '[40, 90, 105, 120]');
else
    set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
    set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
    set_param([Uscita, '/step1'], 'Position', '[20, 45, 85, 75]');
    set_param([Uscita, '/step2'], 'Position', '[20, 135, 85, 165]');
    set_param([Uscita, '/input'], 'Position', '[95, 95, 115, 115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [Pa]'')');

set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.Pi_min, 'Value',  ampiezza(1));
set(handles.Pi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.Pi_cur, 'Value',  ampiezza(2));
set(handles.Pi_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.Pi_max, 'Value',  ampiezza(3));
set(handles.Pi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.Pi, 'Min',   ampiezza(1));
set(handles.Pi, 'Value', ampiezza(2));
set(handles.Pi, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
Pi = get(handles.Pi, 'Value'); u = Pi;
h0  = get(handles.h0, 'Value');
Pe  = 101325;
A   = get(handles.A, 'Value');
H   = get(handles.H, 'Value');
R1  = get(handles.R1, 'Value');
R2  = get(handles.R2, 'Value');
c   = get(handles.c, 'Value');
rho = get(handles.rho, 'Value'); rho = rho * 1000;
g   = 9.81;

% controllo sui dati nulli
if R1 == 0, R1 = eps; end 
if R2 == 0, R2 = eps; end 
if A == 0, return; end


% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica

% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% x_sym1 <-> h
x_sym = sym('x_sym', [1 1]);
% dh/dt = ...
dx_sym = [-rho*g/R2*x_sym(1)-(1/R1+1/R2)*c/A/(H-x_sym(1)) + u/R1 + Pe/R2];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1));
% Solo il val principale (no periodicita')
% [x(1) x(2)] = solve(dx_sym == 0, x_sym(1), x_sym(2), 'PrincipalValue', true);
% x = double(x)';

if size(X) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['Pi = ', num2str(u(1), '%.1f'), ' Pa']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: Pi = %.1f Pa', u(1));
for k = 1 : size(X)
  % Soluzione numerica
  x = double(X(k));

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intorno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X) == 1, str1 = sprintf('\nlo stato:');
  else             str1 = sprintf('\n- lo stato'); end
  str21 = sprintf('\n  h = %.1f m', x(1));
  
  if x(1) < 0 || x(1) > H
    str3 = sprintf('\n non e'' ammissibile');
  else
    % Stabilita'
    switch StabilityLTI(F)
      case -1.2
        str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
      case -1.1
        str3 = sprintf('\n e'' asintoticamente stabile');
      case  0.1
        str3 = sprintf('\n e'' semplicemente stabile');
      case  0.2
        str3 = sprintf('\n e'' semplicemente stabile (osc.)');
      case +1.1
        str3 = sprintf('\n e'' debolmente instabile');
      case +1.2
        str3 = sprintf('\n e'' debolmente instabile (osc.)');
      case +2.1
        str3 = sprintf('\n e'' fortemente instabile');
      case +2.2
        str3 = sprintf('\n e'' fortemente instabile (osc.)');
      otherwise
        str1 = sprintf('\nper lo stato:');
        str3 = sprintf('\n la stabilit� NON e'' determinabile');
    end       
  end

  endstr = '.';
  str = strcat(str, str1, str21, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);



% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Recupera il nome della config
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      Save_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);
% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;
set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

set(handles.H_min, 'Value',  H_min);
set(handles.H_min, 'String', num2str(H_min, '%.1f'));
set(handles.H_cur, 'Value',  H_cur);
set(handles.H_cur, 'String', num2str(H_cur, '%.2f'));
set(handles.H_max, 'Value',  H_max);
set(handles.H_max, 'String', num2str(H_max, '%.1f'));

set(handles.H, 'Min',   H_min);
set(handles.H, 'Value', H_cur);
set(handles.H, 'Max',   H_max);
majorstep = Hsld.stmax / (H_max-H_min);
minorstep = Hsld.stmin / (H_max-H_min);
set(handles.H, 'SliderStep', [minorstep majorstep]);

set(handles.R1_min, 'Value',  R1_min);
set(handles.R1_min, 'String', num2str(R1_min, '%.1f'));
set(handles.R1_cur, 'Value',  R1_cur);
set(handles.R1_cur, 'String', num2str(R1_cur, '%.2f'));
set(handles.R1_max, 'Value',  R1_max);
set(handles.R1_max, 'String', num2str(R1_max, '%.1f'));

set(handles.R1, 'Min',   R1_min);
set(handles.R1, 'Value', R1_cur);
set(handles.R1, 'Max',   R1_max);
majorstep = R1sld.stmax / (R1_max-R1_min);
minorstep = R1sld.stmin / (R1_max-R1_min);
set(handles.R1, 'SliderStep', [minorstep majorstep]);

set(handles.R2_min, 'Value',  R2_min);
set(handles.R2_min, 'String', num2str(R2_min, '%.1f'));
set(handles.R2_cur, 'Value',  R2_cur);
set(handles.R2_cur, 'String', num2str(R2_cur, '%.2f'));
set(handles.R2_max, 'Value',  R2_max);
set(handles.R2_max, 'String', num2str(R2_max, '%.1f'));

set(handles.R2, 'Min',   R2_min);
set(handles.R2, 'Value', R2_cur);
set(handles.R2, 'Max',   R2_max);
majorstep = R2sld.stmax / (R2_max-R2_min);
minorstep = R2sld.stmin / (R2_max-R2_min);
set(handles.R2, 'SliderStep', [minorstep majorstep]);

set(handles.c_min, 'Value',  c_min);
set(handles.c_min, 'String', num2str(c_min, '%.1f'));
set(handles.c_cur, 'Value',  c_cur);
set(handles.c_cur, 'String', num2str(c_cur, '%.2f'));
set(handles.c_max, 'Value',  c_max);
set(handles.c_max, 'String', num2str(c_max, '%.1f'));

set(handles.c, 'Min',   c_min);
set(handles.c, 'Value', c_cur);
set(handles.c, 'Max',   c_max);
majorstep = csld.stmax / (c_max-c_min);
minorstep = csld.stmin / (c_max-c_min);
set(handles.c, 'SliderStep', [minorstep majorstep]);

set(handles.rho_par, 'Value',  rho_par);
set(handles.rho_min, 'Value',  rho_min);
set(handles.rho_min, 'String', num2str(rho_min, '%.1f'));
set(handles.rho_cur, 'Value',  rho_cur);
set(handles.rho_cur, 'String', num2str(rho_cur, '%.2f'));
set(handles.rho_max, 'Value',  rho_max);
set(handles.rho_max, 'String', num2str(rho_max, '%.1f'));

set(handles.rho, 'Min',   rho_min);
set(handles.rho, 'Value', rho_cur);
set(handles.rho, 'Max',   rho_max);
majorstep = rhosld.stmax / (rho_max-rho_min);
minorstep = rhosld.stmin / (rho_max-rho_min);
set(handles.rho, 'SliderStep', [minorstep majorstep]);

set(handles.h0_min, 'Value',  h0_min);
set(handles.h0_min, 'String', num2str(h0_min, '%.1f'));
set(handles.h0_cur, 'Value',  h0_cur);
set(handles.h0_cur, 'String', num2str(h0_cur, '%.2f'));
set(handles.h0_max, 'Value',  h0_max);
set(handles.h0_max, 'String', num2str(h0_max, '%.1f'));

set(handles.h0, 'Min',   h0_min);
set(handles.h0, 'Value', h0_cur);
set(handles.h0, 'Max',   h0_max);
majorstep = h0sld.stmax / (h0_max-h0_min);
minorstep = h0sld.stmin / (h0_max-h0_min);
set(handles.h0, 'SliderStep', [minorstep majorstep]);

% ingressi
Ingressoparstr = cell(1);
Ingressoparstr{1} = 'Ampiezza [Pa]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Ingressoparstr{2} = 'Frequenza�[Hz]';
  case 5
      Ingressoparstr{2} = 'Frequenza [Hz]';
      Ingressoparstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      Ingressoparstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', Ingressoparstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.Pi_min, 'Value',  Pi_min);
set(handles.Pi_min, 'String', num2str(Pi_min, '%.1f'));
set(handles.Pi_cur, 'Value',  Pi_cur);
set(handles.Pi_cur, 'String', num2str(Pi_cur, '%.2f'));
set(handles.Pi_max, 'Value',  Pi_max);
set(handles.Pi_max, 'String', num2str(Pi_max, '%.1f'));

set(handles.Pi, 'Min',   Pi_min);
set(handles.Pi, 'Value', Pi_cur);
set(handles.Pi, 'Max',   Pi_max);
majorstep = Pisld.stmax / (Pi_max-Pi_min);
minorstep = Pisld.stmin / (Pi_max-Pi_min);
set(handles.Pi, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);




% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello AREA
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello altezza
H_min = get(handles.H_min, 'Value');
H_cur = get(handles.H_cur, 'Value');
H_max = get(handles.H_max, 'Value');

% pannello resistenza valvola 1
R1_min = get(handles.R1_min, 'Value');
R1_cur = get(handles.R1_cur, 'Value');
R1_max = get(handles.R1_max, 'Value');

% pannello resistenza valvola 2
R2_min = get(handles.R2_min, 'Value');
R2_cur = get(handles.R2_cur, 'Value');
R2_max = get(handles.R2_max, 'Value');

% pannello costante boyle
c_min = get(handles.c_min, 'Value');
c_cur = get(handles.c_cur, 'Value');
c_max = get(handles.c_max, 'Value');

% pannello peso specifico
rho_par = get(handles.rho_par, 'Value');
rho_min = get(handles.rho_min, 'Value');
rho_cur = get(handles.rho_cur, 'Value');
rho_max = get(handles.rho_max, 'Value');


% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

Pi_min = get(handles.Pi_min, 'Value');
Pi_cur = get(handles.Pi_cur, 'Value');
Pi_max = get(handles.Pi_max, 'Value');

% pannello stato iniziale
h0_min = get(handles.h0_min, 'Value');
h0_cur = get(handles.h0_cur, 'Value');
h0_max = get(handles.h0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoPar);
fprintf(fid, 'Uscita = %d;\n', Uscita);

fprintf(fid, 'Pi_min = %f;\n', Pi_min);
fprintf(fid, 'Pi_cur = %f;\n', Pi_cur);
fprintf(fid, 'Pi_max = %f;\n', Pi_max);

fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'H_min = %f;\n', H_min);
fprintf(fid, 'H_cur = %f;\n', H_cur);
fprintf(fid, 'H_max = %f;\n', H_max);

fprintf(fid, 'R1_min = %f;\n', R1_min);
fprintf(fid, 'R1_cur = %f;\n', R1_cur);
fprintf(fid, 'R1_max = %f;\n', R1_max);

fprintf(fid, 'R2_min = %f;\n', R2_min);
fprintf(fid, 'R2_cur = %f;\n', R2_cur);
fprintf(fid, 'R2_max = %f;\n', R2_max);

fprintf(fid, 'c_min = %f;\n', c_min);
fprintf(fid, 'c_cur = %f;\n', c_cur);
fprintf(fid, 'c_max = %f;\n', c_max);

fprintf(fid, 'rho_par = %d;\n', rho_par);

fprintf(fid, 'rho_min = %f;\n', rho_min);
fprintf(fid, 'rho_cur = %f;\n', rho_cur);
fprintf(fid, 'rho_max = %f;\n', rho_max);

fprintf(fid, 'h0_min = %f;\n', h0_min);
fprintf(fid, 'h0_cur = %f;\n', h0_cur);
fprintf(fid, 'h0_max = %f;\n', h0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;


% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);

% Hints: contents = cellstr(get(hObject,'String')) returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;


%% Slider H
function H_cur_Callback(hObject, eventdata, handles)
% hObject    handle to H_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_cur_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_max_Callback(hObject, eventdata, handles)
% hObject    handle to H_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_max_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function H_min_Callback(hObject, eventdata, handles)
% hObject    handle to H_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Hsld;
Slider_min_Callback(handles, handles.H, handles.H_min, handles.H_cur, handles.H_max, Hsld.stmin, Hsld.stmax, Hsld.Llim, Hsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function H_Callback(hObject, eventdata, handles)
% hObject    handle to H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.H, handles.H_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function H_CreateFcn(hObject, eventdata, handles)
% hObject    handle to H (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%% Slider c
% --- Executes on slider movement.
function c_Callback(hObject, eventdata, handles)
% hObject    handle to c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.c, handles.c_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function c_min_Callback(hObject, eventdata, handles)
% hObject    handle to c_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_min_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function c_max_Callback(hObject, eventdata, handles)
% hObject    handle to c_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_max_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function c_cur_Callback(hObject, eventdata, handles)
% hObject    handle to c_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global csld;
Slider_cur_Callback(handles, handles.c, handles.c_min, handles.c_cur, handles.c_max, csld.stmin, csld.stmax, csld.Llim, csld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function c_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to c_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Silder R2
function R2_max_Callback(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_max_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R2_min_Callback(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_min_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function R2_Callback(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R2, handles.R2_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R2_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R2sld;
Slider_cur_Callback(handles, handles.R2, handles.R2_min, handles.R2_cur, handles.R2_max, R2sld.stmin, R2sld.stmax, R2sld.Llim, R2sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R2_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% slider rho
% --- Executes on slider movement.
function rho_Callback(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.rho, handles.rho_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function rho_min_Callback(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_min_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rho_max_Callback(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_max_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rho_cur_Callback(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global rhosld;
Slider_cur_Callback(handles, handles.rho, handles.rho_min, handles.rho_cur, handles.rho_max, rhosld.stmin, rhosld.stmax, rhosld.Llim, rhosld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in rho_par.
function rho_par_Callback(hObject, eventdata, handles)
% hObject    handle to rho_par (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

val = get(hObject,'Value');
switch val
    case 1
    set(handles.rho_cur,'String','0.670');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 2
    set(handles.rho_cur,'String','0.760');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 3
    set(handles.rho_cur,'String','0.920');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 4
    set(handles.rho_cur,'String','0.960');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 5
    set(handles.rho_cur,'String','1.000');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 6
    set(handles.rho_cur,'String','13.595');
    set(handles.rho,'Value',str2double(get(handles.rho_cur,'String')));
    set(handles.rho_cur,'Enable','off');
    set(handles.rho_min,'Enable','off');
    set(handles.rho_max,'Enable','off');
    set(handles.rho,'Enable','off');
case 7
    set(handles.rho_cur,'Enable','on');
    set(handles.rho_min,'Enable','on');
    set(handles.rho_max,'Enable','on');
    set(handles.rho,'Enable','on');
end

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function rho_par_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_par (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% Slider R2
% --- Executes on slider movement.
function R1_Callback(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.R1, handles.R1_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R1_min_Callback(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_min_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_max_Callback(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_max_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function R1_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global R1sld;
Slider_cur_Callback(handles, handles.R1, handles.R1_min, handles.R1_cur, handles.R1_max, R1sld.stmin, R1sld.stmax, R1sld.Llim, R1sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R1_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R1_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% slider A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.A, handles.A_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% INGRESSI
% --- Executes on slider movement.
function Pi_Callback(hObject, eventdata, handles)
% hObject    handle to Pi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Pi, handles.Pi_cur);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function Pi_min_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_min_Callback(handles, handles.H, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Pi_max_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_max_Callback(handles, handles.H, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Pi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Pi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Pisld;
Slider_cur_Callback(handles, handles.Pi, handles.Pi_min, handles.Pi_cur, handles.Pi_max, Pisld.stmin, Pisld.stmax, Pisld.Llim, Pisld.Hlim);

minval = get(handles.Pi_min, 'Value');
curval = get(handles.Pi, 'Value');
maxval = get(handles.Pi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Pi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Pi_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [Pa]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    new_String{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    new_String{2} = 'Frequenza [Hz]'; 
    new_String{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    new_String{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.Pi_min, 'Value',  values(1));
set(handles.Pi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Pi_cur, 'Value',  values(2));
set(handles.Pi_cur, 'String', num2str(values(2), '%.1f'));
set(handles.Pi_max, 'Value',  values(3));
set(handles.Pi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Pi, 'Min',   values(1));
set(handles.Pi, 'Value', values(2));
set(handles.Pi, 'Max',   values(3));



% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.Pi_min, 'Value',  values(1));
set(handles.Pi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Pi_cur, 'Value',  values(2));
set(handles.Pi_cur, 'String', num2str(values(2), '%.1f'));
set(handles.Pi_max, 'Value',  values(3));
set(handles.Pi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Pi, 'Min',   values(1));
set(handles.Pi, 'Max',   values(3));
set(handles.Pi, 'Value', values(2));


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%  SLIDER STATO INIZIALE
% --- Executes on slider movement.
function h0_Callback(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.h0, handles.h0_cur);


% --- Executes during object creation, after setting all properties.
function h0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function h0_min_Callback(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_min_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h0_max_Callback(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_max_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function h0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h0sld;
Slider_cur_Callback(handles, handles.h0, handles.h0_min, handles.h0_cur, handles.h0_max, h0sld.stmin, h0sld.stmax, h0sld.Llim, h0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function h0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to h0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Pisld Asld Hsld R1sld R2sld csld rhosld h0sld;

% ingressi
Ingressoparstr = cell(1);
Ingressoparstr{1} = 'Ampiezza [Pa]';
Ingressoparval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    Ingressoparstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    Ingressoparstr{2} = 'Frequenza [Hz]'; 
    Ingressoparstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    Ingressoparstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', Ingressoparstr);
set(handles.IngressoPar, 'Value', def(2));
set(handles.IngressoTipo, 'Value', def(1));


% uscita
set(handles.Uscita, 'Value', def(3));

% slider Pi
set(handles.Pi_min, 'Value', def(4));
set(handles.Pi_min, 'String', num2str(def(4), '%.1f')); 
set(handles.Pi_cur, 'Value', def(5));
set(handles.Pi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.Pi_max, 'Value', def(6));
set(handles.Pi_max, 'String', num2str(def(6), '%.1f'));

set(handles.Pi, 'Min',   def(4)); 
set(handles.Pi, 'Value', def(5));
set(handles.Pi, 'Max',   def(6)); 
majorstep = Pisld.stmax / (def(6)-def(4));
minorstep = Pisld.stmin / (def(6)-def(4));
set(handles.Pi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.Pi, 'Min') get(handles.Pi, 'Value') get(handles.Pi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [Pa]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider A
set(handles.A_min, 'Value',  def(7));
set(handles.A_min, 'String', num2str(def(7), '%.1f'));
set(handles.A_cur, 'Value',  def(8));
set(handles.A_cur, 'String', num2str(def(8), '%.2f'));
set(handles.A_max, 'Value',  def(9));
set(handles.A_max, 'String', num2str(def(9), '%.1f'));

set(handles.A, 'Min',   def(7));
set(handles.A, 'Value', def(8));
set(handles.A, 'Max',   def(9));
majorstep = Asld.stmax / (def(9)-def(7));
minorstep = Asld.stmin / (def(9)-def(7));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider H
set(handles.H_min, 'Value',  def(10));
set(handles.H_min, 'String', num2str(def(10), '%.1f'));
set(handles.H_cur, 'Value',  def(11));
set(handles.H_cur, 'String', num2str(def(11), '%.2f'));
set(handles.H_max, 'Value',  def(12));
set(handles.H_max, 'String', num2str(def(12), '%.1f'));

set(handles.H, 'Min',   def(10));
set(handles.H, 'Value', def(11));
set(handles.H, 'Max',   def(12));
majorstep = Hsld.stmax / (def(12)-def(10));
minorstep = Hsld.stmin / (def(12)-def(10));
set(handles.H, 'SliderStep', [minorstep majorstep]);

% slider R1
set(handles.R1_min, 'Value',  def(13));
set(handles.R1_min, 'String', num2str(def(13), '%.1f'));
set(handles.R1_cur, 'Value',  def(14));
set(handles.R1_cur, 'String', num2str(def(14), '%.2f'));
set(handles.R1_max, 'Value',  def(15));
set(handles.R1_max, 'String', num2str(def(15), '%.1f'));

set(handles.R1, 'Min',   def(13));
set(handles.R1, 'Value', def(14));
set(handles.R1, 'Max',   def(15));
majorstep = R1sld.stmax / (def(15)-def(13));
minorstep = R1sld.stmin / (def(15)-def(13));
set(handles.R1, 'SliderStep', [minorstep majorstep]);

% slider R2
set(handles.R2_min, 'Value',  def(16));
set(handles.R2_min, 'String', num2str(def(16), '%.1f'));
set(handles.R2_cur, 'Value',  def(17));
set(handles.R2_cur, 'String', num2str(def(17), '%.2f'));
set(handles.R2_max, 'Value',  def(18));
set(handles.R2_max, 'String', num2str(def(18), '%.1f'));

set(handles.R2, 'Min',   def(16));
set(handles.R2, 'Value', def(17));
set(handles.R2, 'Max',   def(18));
majorstep = R2sld.stmax / (def(18)-def(16));
minorstep = R2sld.stmin / (def(18)-def(16));
set(handles.R2, 'SliderStep', [minorstep majorstep]);

% slider c
set(handles.c_min, 'Value',  def(19));
set(handles.c_min, 'String', num2str(def(19), '%.1f'));
set(handles.c_cur, 'Value',  def(20));
set(handles.c_cur, 'String', num2str(def(20), '%.2f'));
set(handles.c_max, 'Value',  def(21));
set(handles.c_max, 'String', num2str(def(21), '%.1f'));

set(handles.c, 'Min',   def(19));
set(handles.c, 'Value', def(20));
set(handles.c, 'Max',   def(21));
majorstep = csld.stmax / (def(21)-def(19));
minorstep = csld.stmin / (def(21)-def(19));
set(handles.c, 'SliderStep', [minorstep majorstep]);

% slider rho
set(handles.rho_min, 'Value',  def(22));
set(handles.rho_min, 'String', num2str(def(22), '%.1f'));
set(handles.rho_cur, 'Value',  def(23));
set(handles.rho_cur, 'String', num2str(def(23), '%.2f'));
set(handles.rho_max, 'Value',  def(24));
set(handles.rho_max, 'String', num2str(def(24), '%.1f'));

set(handles.rho, 'Min',   def(22));
set(handles.rho, 'Value', def(23));
set(handles.rho, 'Max',   def(24));
majorstep = rhosld.stmax / (def(24)-def(22));
minorstep = rhosld.stmin / (def(24)-def(22));
set(handles.rho, 'SliderStep', [minorstep majorstep]);

% slider h0
set(handles.h0_min, 'Value', def(25));
set(handles.h0_min, 'String', num2str(def(25), '%.1f'));
set(handles.h0_cur, 'Value', def(26));
set(handles.h0_cur, 'String', num2str(def(26), '%.2f'));
set(handles.h0_max, 'Value', def(27));
set(handles.h0_max, 'String', num2str(def(27), '%.1f'));

set(handles.h0, 'Min',   def(25)); 
set(handles.h0, 'Value', def(26));
set(handles.h0, 'Max',   def(27)); 
majorstep = h0sld.stmax / (def(27)-def(25));
minorstep = h0sld.stmin / (def(27)-def(25));
set(handles.h0, 'SliderStep', [minorstep majorstep]);

set(handles.rho_par, 'Value', def(28));

set(handles.ConfigSaveName, 'String', 'nomefile');


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', [], ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 5, ...
    'uipanel', 19, ...
    'slider', 11, ...
    'edit', 32, ...
    'popupmenu', 8, ...
    'pushbutton', 6, ...
    'text', 5), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\13 - Polmone.4.1\Main.fig', ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\13 - Polmone.4.1\Main_export.m');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','POLMONE',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 23.25 145.857142857143 38.35],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[0.571428571428571 0.35 30.1428571428571 16.25],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1 0.65 26.7142857142857 14.4],...
'Box','on',...
'CameraPosition',[75.5 120 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[7.37790752351097 1.77230270674485 5.39154780564263 1.20838820914422],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 150.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 239.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[74.6978609625669 262.321180555556 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-31.1844919786096 121.244791666667 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-10.3288770053476 -377.501736111111 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel5';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[61.4285714285714 4.3 39.1428571428571 30.1],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel3';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Area base A [m^2]',...
'Clipping','on',...
'Position',[1 24.4846153846154 36.8 4.30769230769231],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'A';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('A_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',1,...
'Position',[1.14285714285714 1.9 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0111111 0.111111],...
'Value',2,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A');

appdata = [];
appdata.lastValidTag = 'A_max';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.1428571428571 0.45 10 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_max');

appdata = [];
appdata.lastValidTag = 'A_cur';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.1428571428571 0.45 10 1],...
'String','2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_cur');

appdata = [];
appdata.lastValidTag = 'A_min';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('A_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.45 10 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('A_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','A_min');

appdata = [];
appdata.lastValidTag = 'uipanel6';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Resistenza valvola R1 [Pa�s/m^3]',...
'Clipping','on',...
'Position',[1.2 15.4846153846154 36.4 4.38461538461539],...
'Tag','uipanel6',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R1';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R1_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100000,...
'Min',1000,...
'Position',[1 2 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.00505050505050505 0.0505050505050505],...
'Value',50000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1');

appdata = [];
appdata.lastValidTag = 'R1_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1 0.5 10 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_min');

appdata = [];
appdata.lastValidTag = 'R1_max';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25 0.5 10 1],...
'String','100000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_max');

appdata = [];
appdata.lastValidTag = 'R1_cur';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R1_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13 0.497214854111405 10 1],...
'String','50000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R1_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R1_cur');

appdata = [];
appdata.lastValidTag = 'uipanel9';

h18 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Peso specifico fluido rho [Kg/dm^3]',...
'Clipping','on',...
'Position',[1.8 0.561538461538437 35.8 6.23076923076923],...
'Tag','uipanel9',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'rho';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('rho_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Max',16,...
'Min',0.6,...
'Position',[0.571428571428571 2.15 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0077 0.06457],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho');

appdata = [];
appdata.lastValidTag = 'rho_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_min_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'HorizontalAlignment','right',...
'Position',[0.571428571428571 0.6 10 1],...
'String','0.600',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_min');

appdata = [];
appdata.lastValidTag = 'rho_max';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_max_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'HorizontalAlignment','right',...
'Position',[24.5714285714286 0.6 10 1],...
'String','16.000',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_max');

appdata = [];
appdata.lastValidTag = 'rho_cur';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_cur_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'HorizontalAlignment','right',...
'Position',[12.5714285714286 0.6 10 1],...
'String','1.000',...
'Style','edit',...
'Value',1000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','rho_cur');

appdata = [];
appdata.lastValidTag = 'rho_par';

h23 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('rho_par_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Position',[0.571428571428571 3.75 34 1.3],...
'String',{  'Etere di petrolio a 0�C - 0.67'; 'Benzina - 0.76'; 'Olio d''oliva - 0.92'; 'Vino - 0.96'; 'Acqua a 4�C - 1.0'; 'Mercurio a 0�C - 13.595'; 'Personalizza...' },...
'Style','popupmenu',...
'Value',5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('rho_par_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','rho_par');

appdata = [];
appdata.lastValidTag = 'uipanel13';

h24 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Resistenza valvola R2 [Pa�s/m^3]',...
'Clipping','on',...
'Position',[1.2 11.1 36.4 4.38461538461539],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R2_max';

h25 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25 0.55 10 1],...
'String','200000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_max');

appdata = [];
appdata.lastValidTag = 'R2_min';

h26 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1 0.55 10 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_min');

appdata = [];
appdata.lastValidTag = 'R2';

h27 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R2_Callback',hObject,eventdata,guidata(hObject)),...
'Max',200000,...
'Min',1000,...
'Position',[1 2 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0050251256281407 0.050251256281407],...
'Value',100000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2');

appdata = [];
appdata.lastValidTag = 'R2_cur';

h28 = uicontrol(...
'Parent',h24,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R2_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13 0.55 10 1],...
'String','100000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R2_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R2_cur');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h29 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Costante Legge di Boyle c  [Pa�s]',...
'UserData',[],...
'Clipping','on',...
'Position',[1.8 6.71538461538459 35.8 4.38461538461539],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'c';

h30 = uicontrol(...
'Parent',h29,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('c_Callback',hObject,eventdata,guidata(hObject)),...
'Max',400000,...
'Min',1000,...
'Position',[0.714285714285714 1.95 34 1],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0025062656641604 0.025062656641604],...
'Value',400000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('c_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','c');

appdata = [];
appdata.lastValidTag = 'c_min';

h31 = uicontrol(...
'Parent',h29,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('c_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[0.714285714285714 0.45 10 1],...
'String','1000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('c_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','c_min');

appdata = [];
appdata.lastValidTag = 'c_max';

h32 = uicontrol(...
'Parent',h29,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('c_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[24.7142857142857 0.45 10 1],...
'String','400000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('c_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','c_max');

appdata = [];
appdata.lastValidTag = 'c_cur';

h33 = uicontrol(...
'Parent',h29,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('c_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[12.7142857142857 0.45 10 1],...
'String','400000.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('c_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','c_cur');

appdata = [];
appdata.lastValidTag = 'uipanel15';

h34 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Altezza del polmone H [m]',...
'Clipping','on',...
'Position',[1.2 19.9461538461538 36.4 4.38461538461539],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'H_cur';

h35 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('H_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13 0.45 10 1],...
'String','2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('H_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','H_cur');

appdata = [];
appdata.lastValidTag = 'H_max';

h36 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('H_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25 0.45 10 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('H_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','H_max');

appdata = [];
appdata.lastValidTag = 'H_min';

h37 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('H_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1 0.45 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('H_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','H_min');

appdata = [];
appdata.lastValidTag = 'H';

h38 = uicontrol(...
'Parent',h34,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('H_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1 2 34 1],...
'String',blanks(0),...
'Style','slider',...
'Value',2,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('H_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','H');

appdata = [];
appdata.lastValidTag = 'Run';

h39 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[68.5714285714286 1.55 25.5714285714286 1.65],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel11';

h40 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[101.428571428571 14.65 42 3.3],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h41 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.85714285714286 0.649999999999999 37 1.3],...
'String','Altezza',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel12';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[101.428571428571 24.75 42 9.65],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h43 = uipanel(...
'Parent',h42,...
'Units','characters',...
'Title','Pressione ingresso Pi [Pa]',...
'Clipping','on',...
'Position',[1.14285714285714 0.399999999999999 38.5714285714286 7.95],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Pi';

h44 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Pi_Callback',hObject,eventdata,guidata(hObject)),...
'Max',200000,...
'Position',[1.28571428571429 2 36 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',105000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Pi_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Pi');

appdata = [];
appdata.lastValidTag = 'Pi_min';

h45 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Pi_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.45 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Pi_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Pi_min');

appdata = [];
appdata.lastValidTag = 'Pi_max';

h46 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Pi_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[27.2857142857143 0.45 10 1],...
'String','200000',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Pi_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Pi_max');

appdata = [];
appdata.lastValidTag = 'Pi_cur';

h47 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Pi_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.2857142857143 0.45 10 1],...
'String','105000',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Pi_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Pi_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h48 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Position',[1.14285714285714 5.4 36 1.3],...
'String',{  'Pressione costante (step)'; 'Getto (impulso)'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h49 = uicontrol(...
'Parent',h43,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.14285714285714 3.65 36 1.3],...
'String','Ampiezza [Pa]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'uipanel16';

h50 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di equilibrio',...
'Clipping','on',...
'Position',[101.428571428571 6.25 42 7.9],...
'Tag','uipanel16',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h51 = uicontrol(...
'Parent',h50,...
'Units','characters',...
'HorizontalAlignment','left',...
'Position',[1 0.5 39 5.9],...
'Style','text',...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h52 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[104.857142857143 4.35 35.5714285714286 1.45],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel17';

h53 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1 16.65 59.7142857142857 21.55],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h54 = axes(...
'Parent',h53,...
'Units','characters',...
'Position',[1.14285714285714 0.761538461538453 56.4285714285714 19.55],...
'Box','on',...
'CameraPosition',[209.5 216 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[4.93810346677971 0.625714975299001 3.60861407187748 0.426623846794773],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 418.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 431.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h55 = get(h54,'xlabel');

set(h55,...
'Parent',h54,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[208.441772151899 461.813299232736 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h56 = get(h54,'ylabel');

set(h56,...
'Parent',h54,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-41.3 217.102301790281 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h57 = get(h54,'zlabel');

set(h57,...
'Parent',h54,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-18.0189873417722 -26.5063938618926 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel18';

h58 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[61.4285714285714 35.1 82 3.1],...
'Tag','uipanel18',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h59 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.5 8.57142857142857 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h60 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[70.7142857142857 0.5 9 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h61 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.28571428571429 0.5 21 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h62 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[49.5714285714286 0.6 20 1.1],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h63 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.5 8 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel4';

h64 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[101.428571428571 18.45 42 5.85],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h65 = uipanel(...
'Parent',h64,...
'Units','characters',...
'Title','Altezza liquido h0 [m]',...
'Clipping','on',...
'Position',[1.14285714285714 0.449999999999999 39.4285714285714 4.1],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'h0';

h66 = uicontrol(...
'Parent',h65,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('h0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.14285714285714 1.9 36.5714285714286 1],...
'String',{  'Slider' },...
'Style','slider',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0');

appdata = [];
appdata.lastValidTag = 'h0_min';

h67 = uicontrol(...
'Parent',h65,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.14285714285714 0.5 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0_min');

appdata = [];
appdata.lastValidTag = 'h0_max';

h68 = uicontrol(...
'Parent',h65,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_max_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'HorizontalAlignment','right',...
'Position',[27.7142857142857 0.5 10 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','h0_max');

appdata = [];
appdata.lastValidTag = 'h0_cur';

h69 = uicontrol(...
'Parent',h65,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('h0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14.4285714285714 0.5 10 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('h0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','h0_cur');


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


