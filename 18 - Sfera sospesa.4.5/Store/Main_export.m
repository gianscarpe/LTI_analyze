%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   S F E R A   S O S P E S A    3.17                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main_export(varargin)
%MAIN_EXPORT M-file for Main_export.fig
%      MAIN_EXPORT, by itself, creates a new MAIN_EXPORT or raises the existing
%      singleton*.
%
%      H = MAIN_EXPORT returns the handle to a new MAIN_EXPORT or the handle to the
%      existing singleton*.
%
%      MAIN_EXPORT('Property','Value',...) creates a new MAIN_EXPORT using the given
%      property value pairs. Unrecognized properties are passed via
%      varargin to Main_export_OpeningFcn.  This calling syntax produces a warning
%      when there is an existing singleton*.
%
%      MAIN_EXPORT('CALLBACK') and MAIN_EXPORT('CALLBACK',hObject,...) call the local
%      function named CALLBACK in MAIN_EXPORT.M with the given input arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main_export

% Last Modified by GUIDE v2.5 23-Nov-2017 14:19:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main_export is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn. hObject    handle to
% figure eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) varargin
% unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      -10      % Vi_min        4
      3        % Vi, Vi_cur    5
      10       % Vi_max        6
      0.00     % R_min         7
      10       % R, R_cur      8
      50       % R_max         9
      0        % L_min         10
      250      % L, L_cur      11
      1000     % L_max         12
      0.00     % M_min         13
      0.01     % M, M_cur      14
      0.1      % M_max         15
      0.00     % k_min         16
      0.5      % k, k_cur      17
      2        % k_max         18
      0.00     % y0_min        19
      0.01     % y0, y0_cur    20
      0.1      % y0_max        21
      0.00     % ve0_min       22
      0.00     % ve0, ve0_cur  23
      1.00     % ve0_max       24
      0.00     % i0_min        25
      0.1      % i0, i0_cur    26
      10       % i0_max        27
      ];

% Par degli slider
global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;


Rsld.stmin = 1;
Rsld.stmax = 10;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Lsld.stmin = 10;
Lsld.stmax = 100;
Lsld.Llim = -Inf;
Lsld.Hlim = +Inf;

Msld.stmin = 0.005;
Msld.stmax = 0.01;
Msld.Llim = eps;
Msld.Hlim = +Inf;

ksld.stmin = 0.01;
ksld.stmax = 0.1;
ksld.Llim = eps;
ksld.Hlim = +Inf;

y0sld.stmin = 0.01;
y0sld.stmax = 0.01;
y0sld.Llim = -Inf;
y0sld.Hlim = +Inf;

ve0sld.stmin = 0.01;
ve0sld.stmax = 0.1;
ve0sld.Llim = -Inf;
ve0sld.Hlim = +Inf;

i0sld.stmin = 0.1;
i0sld.stmax = 1;
i0sld.Llim = -Inf;
i0sld.Hlim = +Inf;

Visld.stmin = 0.1;
Visld.stmax = 1;
Visld.Llim = -Inf;
Visld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
% Choose default command line output for Main_export
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% FIGURE

% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;

% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;

% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT); hObject
% handle to figure eventdata  reserved - to be defined in a future version
% of MATLAB handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

clc

% Chiusura dello modello simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)
global Uscita;

% Chiusura dello modello simulink (if any) senza salvare
bdclose(Uscita);

Uscita = 'Posizione_Sfera';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
Vi = get(handles.Vi, 'Value'); u = Vi;
R  = get(handles.R,  'Value');
L  = get(handles.L,  'Value'); L = L/1000;
M  = get(handles.M,  'Value');
k  = get(handles.k,  'Value');
y0  = get(handles.y0,  'Value');
ve0 = get(handles.ve0,  'Value');
i0  = get(handles.i0,  'Value');
g = 9.81;

% Controllo sui dati nulli per evitare singolarita' (1/0)
if R == 0, R = eps; end
if M <= 0, M = eps; end
if k <= 0, k = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end

% Costante di tempo
syms i_(t) ve_(t) y_(t) R_ L_ M_ k_ u_;
eqn1 = diff(y_)  == ve_;
eqn2 = diff(i_)  == u_/L_ - (R_/L_)*i_;
eqn3 = diff(ve_) == -(k_/M_)*(i_*i_)/y_ + g;

[y_Sol, i_Sol, ve_Sol] = dsolve(eqn1, eqn2, eqn3, y_(0) == y0, i_(0) == i0, ve_(0) == ve0);

Tau = 0.1;  % <- da correggere

vars = {'Vi', Vi; 'y0', y0; 've0', ve0; 'i0', i0; 'R', R; 'L', L; 'M', M; 'k', k; 'g', 9.81 };
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end

% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.Vi_min, 'Value',  ampiezza(1));
set(handles.Vi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.Vi_cur, 'Value',  ampiezza(2));
set(handles.Vi_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.Vi_max, 'Value',  ampiezza(3));
set(handles.Vi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.Vi, 'Min',   ampiezza(1));
set(handles.Vi, 'Value', ampiezza(2));
set(handles.Vi, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
Vi = get(handles.Vi_cur, 'Value'); u = Vi;
M = get(handles.M_cur, 'Value');
L = get(handles.L_cur, 'Value'); L = L/1000;
k = get(handles.k_cur, 'Value');
R = get(handles.R_cur, 'Value');
g = 9.81;


% Controllo dati....................................................
if M == 0, M = eps; end
if R <= 0,  R = eps; end


% Scrittura dell'equazione dinamica Risoluzione dell'equazione 0 = f(x, u)
% nell'incognita x (u noto)

% Impostazione dell'equazione dinamica non lineare x_sym1 <-> y0 x_sym2 <->
% ve0 x_sym3 <-> i0
x_sym = sym('x_sym', [3 1]);
% dy/dt  = ve0 dve/dt = -k/M * (i0^2) / y0 + g di/dt  = Vi/L - R/L*i0
dx_sym = [x_sym(2); -k/M * (x_sym(3)^2) / x_sym(1) + g; u/L - R/L*x_sym(3)];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3));

if size(X.x_sym1) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['Vi = ', num2str(u(1), '%.2f'), ' V']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: Vi = %.2f V', u(1));
for k = 1 : size(X.x_sym1)
  % Soluzione numerica
  x = double([X.x_sym1(k); X.x_sym2(k); X.x_sym3(k)]);

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno
  % al punto di equilibrio), per calcolare gli autovalori e la
  % stabilita'�del sistema intornno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X.x_sym1, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  y  = %.2f m   ', x(1));
  str22 = sprintf('\n  ve = %.2f m/s ', x(2));
  str23 = sprintf('\n  i  = %.2f A   ', x(3));

  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
      str3 = '';
  end        

  if k == size(X.x_sym1, 1), endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str22, str23, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config che l'utente voleva
% caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;

% Aggiornamento degli slider
%R
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

%L
set(handles.L_min, 'Value',  L_min);
set(handles.L_min, 'String', num2str(L_min, '%.1f'));
set(handles.L_cur, 'Value',  L_cur);
set(handles.L_cur, 'String', num2str(L_cur, '%.2f'));
set(handles.L_max, 'Value',  L_max);
set(handles.L_max, 'String', num2str(L_max, '%.1f'));

set(handles.L, 'Min',   L_min);
set(handles.L, 'Value', L_cur);
set(handles.L, 'Max',   L_max);
majorstep = Lsld.stmax / (L_max-L_min);
minorstep = Lsld.stmin / (L_max-L_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

%M
set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

%k
set(handles.k_min, 'Value',  k_min);
set(handles.k_min, 'String', num2str(k_min, '%.1f'));
set(handles.k_cur, 'Value',  k_cur);
set(handles.k_cur, 'String', num2str(k_cur, '%.2f'));
set(handles.k_max, 'Value',  k_max);
set(handles.k_max, 'String', num2str(k_max, '%.1f'));

set(handles.k, 'Min',   k_min);
set(handles.k, 'Value', k_cur);
set(handles.k, 'Max',   k_max);
majorstep = ksld.stmax / (k_max-k_min);
minorstep = ksld.stmin / (k_max-k_min);
set(handles.k, 'SliderStep', [minorstep majorstep]);

%y0
set(handles.y0_min, 'Value',  y0_min);
set(handles.y0_min, 'String', num2str(y0_min, '%.1f'));
set(handles.y0_cur, 'Value',  y0_cur);
set(handles.y0_cur, 'String', num2str(y0_cur, '%.2f'));
set(handles.y0_max, 'Value',  y0_max);
set(handles.y0_max, 'String', num2str(y0_max, '%.1f'));

set(handles.y0, 'Min',   y0_min);
set(handles.y0, 'Value', y0_cur);
set(handles.y0, 'Max',   y0_max);
majorstep = y0sld.stmax / (y0_max-y0_min);
minorstep = y0sld.stmin / (y0_max-y0_min);
set(handles.y0, 'SliderStep', [minorstep majorstep]);

%ve0
set(handles.ve0_min, 'Value',  ve0_min);
set(handles.ve0_min, 'String', num2str(ve0_min, '%.1f'));
set(handles.ve0_cur, 'Value',  ve0_cur);
set(handles.ve0_cur, 'String', num2str(ve0_cur, '%.2f'));
set(handles.ve0_max, 'Value',  ve0_max);
set(handles.ve0_max, 'String', num2str(ve0_max, '%.1f'));

set(handles.ve0, 'Min',   ve0_min);
set(handles.ve0, 'Value', ve0_cur);
set(handles.ve0, 'Max',   ve0_max);
majorstep = ve0sld.stmax / (ve0_max-ve0_min);
minorstep = ve0sld.stmin / (ve0_max-ve0_min);
set(handles.ve0, 'SliderStep', [minorstep majorstep]);

%i0
set(handles.i0_min, 'Value',  i0_min);
set(handles.i0_min, 'String', num2str(i0_min, '%.1f'));
set(handles.i0_cur, 'Value',  i0_cur);
set(handles.i0_cur, 'String', num2str(i0_cur, '%.2f'));
set(handles.i0_max, 'Value',  i0_max);
set(handles.i0_max, 'String', num2str(i0_max, '%.1f'));

set(handles.i0, 'Min',   i0_min);
set(handles.i0, 'Value', i0_cur);
set(handles.i0, 'Max',   i0_max);
majorstep = i0sld.stmax / (i0_max-i0_min);
minorstep = i0sld.stmin / (i0_max-i0_min);
set(handles.i0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [V]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.Vi_min, 'Value',  Vi_min);
set(handles.Vi_min, 'String', num2str(Vi_min, '%.1f'));
set(handles.Vi_cur, 'Value',  Vi_cur);
set(handles.Vi_cur, 'String', num2str(Vi_cur, '%.2f'));
set(handles.Vi_max, 'Value',  Vi_max);
set(handles.Vi_max, 'String', num2str(Vi_max, '%.1f'));

set(handles.Vi, 'Min',   Vi_min);
set(handles.Vi, 'Value', Vi_cur);
set(handles.Vi, 'Max',   Vi_max);
majorstep = Visld.stmax / (Vi_max-Vi_min);
minorstep = Visld.stmin / (Vi_max-Vi_min);
set(handles.Vi, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i0 parametri pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello induzione
L_min = get(handles.L_min, 'Value');
L_cur = get(handles.L_cur, 'Value');
L_max = get(handles.L_max, 'Value');

% pannello massa
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello coeff
k_min = get(handles.k_min, 'Value');
k_cur = get(handles.k_cur, 'Value');
k_max = get(handles.k_max, 'Value');

% pannello posizione
y0_min = get(handles.y0_min, 'Value');
y0_cur = get(handles.y0_cur, 'Value');
y0_max = get(handles.y0_max, 'Value');

% pannello velocit�
ve0_min = get(handles.ve0_min, 'Value');
ve0_cur = get(handles.ve0_cur, 'Value');
ve0_max = get(handles.ve0_max, 'Value');

% pannello corrente
i0_min = get(handles.i0_min, 'Value');
i0_cur = get(handles.i0_cur, 'Value');
i0_max = get(handles.i0_max, 'Value');


% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

Vi_min = get(handles.Vi_min, 'Value');
Vi_cur = get(handles.Vi_cur, 'Value');
Vi_max = get(handles.Vi_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'Vi_min = %f;\n', Vi_min);
fprintf(fid, 'Vi_cur = %f;\n', Vi_cur);
fprintf(fid, 'Vi_max = %f;\n', Vi_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'L_min = %f;\n', L_min);
fprintf(fid, 'L_cur = %f;\n', L_cur);
fprintf(fid, 'L_max = %f;\n', L_max);

fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'k_min = %f;\n', k_min);
fprintf(fid, 'k_cur = %f;\n', k_cur);
fprintf(fid, 'k_max = %f;\n', k_max);

fprintf(fid, 'y0_min = %f;\n', y0_min);
fprintf(fid, 'y0_cur = %f;\n', y0_cur);
fprintf(fid, 'y0_max = %f;\n', y0_max);

fprintf(fid, 've0_min = %f;\n', ve0_min);
fprintf(fid, 've0_cur = %f;\n', ve0_cur);
fprintf(fid, 've0_max = %f;\n', ve0_max);

fprintf(fid, 'i0_min = %f;\n', i0_min);
fprintf(fid, 'i0_cur = %f;\n', i0_cur);
fprintf(fid, 'i0_max = %f;\n', i0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli Aggiunge tutti i0 nomi dei
% modelli trovati nella directory corrente in una struttura temporanea
% (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    structure with
% handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    structure with
% handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of
%        ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i0 nomi delle config trovati nella directory corrente
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [V]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider Va
set(handles.Vi_min, 'Value', def(4));
set(handles.Vi_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.Vi_cur, 'Value', def(5));
set(handles.Vi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.Vi_max, 'Value', def(6));
set(handles.Vi_max, 'String', num2str(def(6), '%.1f'));

set(handles.Vi, 'Min',   def(4)); 
set(handles.Vi, 'Value', def(5));
set(handles.Vi, 'Max',   def(6)); 
majorstep = Visld.stmax / (def(6)-def(4));
minorstep = Visld.stmin / (def(6)-def(4));
set(handles.Vi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.Vi, 'Min') get(handles.Vi, 'Value') get(handles.Vi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [V]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider L
set(handles.L_min, 'Value',  def(10));
set(handles.L_min, 'String', num2str(def(10), '%.1f'));
set(handles.L_cur, 'Value',  def(11));
set(handles.L_cur, 'String', num2str(def(11), '%.2f'));
set(handles.L_max, 'Value',  def(12));
set(handles.L_max, 'String', num2str(def(12), '%.1f'));

set(handles.L, 'Min',   def(10));
set(handles.L, 'Value', def(11));
set(handles.L, 'Max',   def(12));
majorstep = Lsld.stmax / (def(12)-def(10));
minorstep = Lsld.stmin / (def(12)-def(10));
set(handles.L, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value', def(13));
set(handles.M_min, 'String', num2str(def(13), '%.1f'));
set(handles.M_cur, 'Value', def(14));
set(handles.M_cur, 'String', num2str(def(14), '%.2f'));
set(handles.M_max, 'Value', def(15));
set(handles.M_max, 'String', num2str(def(15), '%.1f'));

set(handles.M, 'Min',   def(13)); 
set(handles.M, 'Value', def(14));
set(handles.M, 'Max',   def(15)); 
majorstep = Msld.stmax / (def(15)-def(13));
minorstep = Msld.stmin / (def(15)-def(13));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider k
set(handles.k_min, 'Value', def(16));
set(handles.k_min, 'String', num2str(def(16), '%.1f'));
set(handles.k_cur, 'Value', def(17));
set(handles.k_cur, 'String', num2str(def(17), '%.2f'));
set(handles.k_max, 'Value', def(18));
set(handles.k_max, 'String', num2str(def(18), '%.1f'));

set(handles.k, 'Min',   def(16)); 
set(handles.k, 'Value', def(17));
set(handles.k, 'Max',   def(18)); 
majorstep = ksld.stmax / (def(18)-def(16));
minorstep = ksld.stmin / (def(18)-def(16));
set(handles.k, 'SliderStep', [minorstep majorstep]);

% slider y0
set(handles.y0_min, 'Value', def(19));
set(handles.y0_min, 'String', num2str(def(19), '%.1f'));
set(handles.y0_cur, 'Value', def(20));
set(handles.y0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.y0_max, 'Value', def(21));
set(handles.y0_max, 'String', num2str(def(21), '%.1f'));

set(handles.y0, 'Min',   def(19)); 
set(handles.y0, 'Value', def(20));
set(handles.y0, 'Max',   def(21)); 
majorstep = y0sld.stmax / (def(21)-def(19));
minorstep = y0sld.stmin / (def(21)-def(19));
set(handles.y0, 'SliderStep', [minorstep majorstep]);

% slider ve0
set(handles.ve0_min, 'Value', def(22));
set(handles.ve0_min, 'String', num2str(def(22), '%.1f'));
set(handles.ve0_cur, 'Value', def(23));
set(handles.ve0_cur, 'String', num2str(def(23), '%.2f'));
set(handles.ve0_max, 'Value', def(24));
set(handles.ve0_max, 'String', num2str(def(24), '%.1f'));

set(handles.ve0, 'Min',   def(22)); 
set(handles.ve0, 'Value', def(23));
set(handles.ve0, 'Max',   def(24)); 
majorstep = ve0sld.stmax / (def(24)-def(22));
minorstep = ve0sld.stmin / (def(24)-def(22));
set(handles.ve0, 'SliderStep', [minorstep majorstep]);

% slider i0
set(handles.i0_min, 'Value', def(25));
set(handles.i0_min, 'String', num2str(def(25), '%.1f'));
set(handles.i0_cur, 'Value', def(26));
set(handles.i0_cur, 'String', num2str(def(26), '%.2f'));
set(handles.i0_max, 'Value', def(27));
set(handles.i0_max, 'String', num2str(def(27), '%.1f'));

set(handles.i0, 'Min',   def(25)); 
set(handles.i0, 'Value', def(26));
set(handles.i0, 'Max',   def(27)); 
majorstep = i0sld.stmax / (def(27)-def(25));
minorstep = i0sld.stmin / (def(27)-def(25));
set(handles.i0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER k
% --- Executes on slider movement.
function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.k, handles.k_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function k_min_Callback(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_min_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_max_Callback(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_max_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_cur_Callback(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_cur_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER L
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.L, handles.L_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function L_min_Callback(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_min_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_max_Callback(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_max_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_cur_Callback(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_cur_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% INGRESSI
function Vi_min_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_min_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Vi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Vi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_cur_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Vi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Vi_max_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_max_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Vi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Vi_Callback(hObject, eventdata, handles)
% hObject    handle to Vi (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.Vi, handles.Vi_cur);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Vi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [V]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Vi_min, 'Value',  values(1));
set(handles.Vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Vi_cur, 'Value',  values(2));
set(handles.Vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Vi_max, 'Value',  values(3));
set(handles.Vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Vi, 'Min',   values(1));
set(handles.Vi, 'Value', values(2));
set(handles.Vi, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA) Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Vi_min, 'Value',  values(1));
set(handles.Vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Vi_cur, 'Value',  values(2));
set(handles.Vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Vi_max, 'Value',  values(3));
set(handles.Vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Vi, 'Min',   values(1));
set(handles.Vi, 'Max',   values(3));
set(handles.Vi, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%%  SLIDER STATI INIZIALI

% --- Executes on slider movement.
function i0_Callback(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.i0, handles.i0_cur);


% --- Executes during object creation, after setting all properties.
function i0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function i0_min_Callback(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_min_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_max_Callback(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_max_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_cur_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function ve0_Callback(hObject, eventdata, handles)
% hObject    handle to ve0 (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.ve0, handles.ve0_cur);

% --- Executes during object creation, after setting all properties.
function ve0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0 (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ve0_min_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_min_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ve0_max_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_max_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ve0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_cur_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_min_Callback(hObject, eventdata, handles)
% hObject    handle to y0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_min_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_max_Callback(hObject, eventdata, handles)
% hObject    handle to y0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_max_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to y0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_cur_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function y0_Callback(hObject, eventdata, handles)
% hObject    handle to y0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.y0, handles.y0_cur);

% --- Executes during object creation, after setting all properties.
function y0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', 263.008178710938, ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 5, ...
    'uipanel', 24, ...
    'slider', 14, ...
    'edit', 41, ...
    'popupmenu', 5, ...
    'pushbutton', 8, ...
    'listbox', 2, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\18 - Sfera sospesa.3.17 - FMM\Main.fig', ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi Mutti 2016\18 - Sfera sospesa.3.17 - FMM\Main_export.m');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','SFERA SOSPESA ver. 3.17',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.285714285714 31.35 168.571428571429 30.25],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1 1.3 33.4285714285714 9.7],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.42857142857143 0.4 30.8571428571428 8.15],...
'Box','on',...
'CameraPosition',[364.5 260.5 4473.71193327569],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[9.96079517592166 1.85383688563169 7.27904262855814 1.26397969474887],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 728.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 520.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[362.814814814815 614.388888888889 1.02666540265],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-132.62962962963 263.87037037037 1.02666540265],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-65.2222222222222 -1377.5 1.02666540265],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel4';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[83.8571428571428 6.78076923076924 40 23.3],...
'Tag','uipanel4',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel5';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Resistenza R [ohm]',...
'Clipping','on',...
'Position',[1.52681252681253 16.8724052594401 36.7327327327327 5.0308815272319],...
'Tag','uipanel5',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'R';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('R_Callback',hObject,eventdata,guidata(hObject)),...
'Max',1000,...
'Min',0.01,...
'Position',[1.16513157894737 2.30993517718237 33.7888157894737 1.26050561797753],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0010000100001 0.010000100001],...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R');

appdata = [];
appdata.lastValidTag = 'R_min';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Min',0.01,...
'Position',[1.42857142857143 0.529411764705882 9 1.35294117647059],...
'String','0.01',...
'Style','edit',...
'Value',0.01,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_min');

appdata = [];
appdata.lastValidTag = 'R_max';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[25.7142857142857 0.588235294117647 8.99999999999999 1.35294117647059],...
'String','1000.0',...
'Style','edit',...
'Value',1000,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_max');

appdata = [];
appdata.lastValidTag = 'R_cur';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('R_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[13.5714285714286 0.529411764705882 9 1.35294117647059],...
'String','500.0',...
'Style','edit',...
'Value',500,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('R_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','R_cur');

appdata = [];
appdata.lastValidTag = 'uipanel7';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Coefficiente di induzione L [mH]',...
'Clipping','on',...
'Position',[1.55092235092236 11.5009566350718 36.7327327327327 5.0308815272319],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'L';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('L_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.48855233270382 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L');

appdata = [];
appdata.lastValidTag = 'L_min';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.723526108927592 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_min');

appdata = [];
appdata.lastValidTag = 'L_max';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.673526108927592 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_max');

appdata = [];
appdata.lastValidTag = 'L_cur';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('L_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.673526108927592 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('L_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','L_cur');

appdata = [];
appdata.lastValidTag = 'uipanel17';

h18 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Massa della sfera M [kg]',...
'Clipping','on',...
'Position',[1.57142857142857 6.25000000000002 36.8571428571429 5],...
'Tag','uipanel17',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'M';

h19 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('M_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M');

appdata = [];
appdata.lastValidTag = 'M_min';

h20 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_min');

appdata = [];
appdata.lastValidTag = 'M_max';

h21 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_max');

appdata = [];
appdata.lastValidTag = 'M_cur';

h22 = uicontrol(...
'Parent',h18,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('M_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('M_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','M_cur');

appdata = [];
appdata.lastValidTag = 'uipanel21';

h23 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Coefficiente k [J/A]',...
'Clipping','on',...
'Position',[1.57142857142857 0.800000000000002 36.8571428571429 5],...
'Tag','uipanel21',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'k';

h24 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('k_Callback',hObject,eventdata,guidata(hObject)),...
'Max',100,...
'Min',1,...
'Position',[1.16513157894737 2.33414469654433 33.7888157894737 1.29056818181818],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.0101010101010101 0.101010101010101],...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k');

appdata = [];
appdata.lastValidTag = 'k_min';

h25 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.42857142857143 0.569118472768102 8 1.35],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_min');

appdata = [];
appdata.lastValidTag = 'k_max';

h26 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[26.7142857142857 0.519118472768102 8 1.35],...
'String','100.0',...
'Style','edit',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_max');

appdata = [];
appdata.lastValidTag = 'k_cur';

h27 = uicontrol(...
'Parent',h23,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('k_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[14 0.519118472768102 8 1.35],...
'String','50.0',...
'Style','edit',...
'Value',50,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('k_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','k_cur');

appdata = [];
appdata.lastValidTag = 'Run';

h28 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[93 0.7 21.5714285714286 1.75],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h29 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'ShadowColor',[0.501960784313725 0.501960784313725 0.501960784313725],...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[83.8571428571428 2.8 39.8571428571429 3.25],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h30 = uicontrol(...
'Parent',h29,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.57142857142857 0.5 36.1428571428571 1.45],...
'String','Distanza sfera - elettromagnete',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h31 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[123.857142857143 19.0384615384616 43.4 11.0769230769231],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel8';

h32 = uipanel(...
'Parent',h31,...
'Units','characters',...
'Title','Tensione in ingresso Vi [V]',...
'Clipping','on',...
'Position',[1.65 0.359581568204013 39.25 9.12697740112995],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Vi_min';

h33 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Vi_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.434995654063478 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Vi_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Vi_min');

appdata = [];
appdata.lastValidTag = 'Vi_cur';

h34 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Vi_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.40790960451979 8 1.4],...
'String','1.0',...
'Style','edit',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Vi_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Vi_cur');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h35 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.25690500966255 5.83973337336173 35.9754528658498 1.77864473684211],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h36 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.25690500966255 3.63760179441436 35.9754528658498 2.03273684210527],...
'String','Ampiezza [V]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'Vi_max';

h37 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Vi_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[29.2857142857143 0.434995654063478 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Vi_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Vi_max');

appdata = [];
appdata.lastValidTag = 'Vi';

h38 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Vi_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.25690500966255 2.19774653125646 35.9754528658498 1.4398552631579],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Vi_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Vi');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h39 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[47 1.3 24.5714285714286 1.75],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h40 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[35.7142857142857 3.4 47.2857142857143 7.55],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h41 = uicontrol(...
'Parent',h40,...
'Units','characters',...
'FontName','Courier',...
'HorizontalAlignment','left',...
'Position',[1 0.25 45.1428571428571 6.2],...
'String',blanks(0),...
'Style','text',...
'Value',1,...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel13';

h42 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[0.999999999999999 11.45 81.7142857142857 15.5],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h43 = axes(...
'Parent',h42,...
'Units','characters',...
'Position',[1.28571428571428 0.45 79 14],...
'Box','on',...
'CameraPosition',[753 379 8423.79359573795],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'DataAspectRatio',get(0,'defaultaxesDataAspectRatio'),...
'DataAspectRatioMode','manual',...
'Layer','top',...
'LooseInset',[13.1152421964991 2.87559598148166 9.5842154512878 1.96063362373749],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 1505.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 757.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h44 = get(h43,'xlabel');

set(h44,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[750.278481012658 832.132911392405 1.05020972548664],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h45 = get(h43,'ylabel');

set(h45,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-107 383.082278481013 1.05020972548664],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h46 = get(h43,'zlabel');

set(h46,...
'Parent',h43,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-49.8481012658226 -229.259493670886 1.05020972548664],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel3';

h47 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[123.857142857143 2.8 43.4285714285714 16.3],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h48 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Distanza sfera - elettromagnete y0 [m]',...
'Clipping','on',...
'Position',[0.942857142857143 10.1692307692308 40.1428571428571 5],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'y0';

h49 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('y0_Callback',hObject,eventdata,guidata(hObject)),...
'CData',[],...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('y0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'UserData',[],...
'Tag','y0');

appdata = [];
appdata.lastValidTag = 'y0_min';

h50 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('y0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('y0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','y0_min');

appdata = [];
appdata.lastValidTag = 'y0_max';

h51 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('y0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('y0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','y0_max');

appdata = [];
appdata.lastValidTag = 'y0_cur';

h52 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('y0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('y0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','y0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel22';

h53 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Velocit� della sfera ve0 [m/s]',...
'Clipping','on',...
'Position',[1 5.23076923076923 40.2 5],...
'Tag','uipanel22',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 've0';

h54 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('ve0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ve0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ve0');

appdata = [];
appdata.lastValidTag = 've0_min';

h55 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ve0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ve0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ve0_min');

appdata = [];
appdata.lastValidTag = 've0_max';

h56 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ve0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ve0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ve0_max');

appdata = [];
appdata.lastValidTag = 've0_cur';

h57 = uicontrol(...
'Parent',h53,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ve0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ve0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ve0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel23';

h58 = uipanel(...
'Parent',h47,...
'Units','characters',...
'Title','Corrente  i0 [A]',...
'Clipping','on',...
'Position',[1.08571428571429 0.346153846153844 40.1428571428571 5],...
'Tag','uipanel23',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'i0';

h59 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('i0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',-10,...
'Position',[1.14285714285714 2.3 37 1.3],...
'String',{  'Slider' },...
'Style','slider',...
'SliderStep',[0.005 0.05],...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0');

appdata = [];
appdata.lastValidTag = 'i0_min';

h60 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.28571428571429 0.5 8 1.35],...
'String','-10.0',...
'Style','edit',...
'Value',-10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_min');

appdata = [];
appdata.lastValidTag = 'i0_max';

h61 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.1428571428571 0.55 8 1.35],...
'String','10.0',...
'Style','edit',...
'Value',10,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_max');

appdata = [];
appdata.lastValidTag = 'i0_cur';

h62 = uicontrol(...
'Parent',h58,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('i0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[15.7142857142857 0.5 8 1.35],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('i0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','i0_cur');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h63 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'Clipping','on',...
'Position',[0.999999999999999 26.95 81.8571428571428 3.15],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h64 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[24.7142857142857 0.499999999999995 8.57142857142857 1.45],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h65 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[68.5714285714286 0.499999999999995 9 1.45],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h66 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2.28571428571429 0.499999999999995 21 1.45],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h67 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[47.2857142857143 0.599999999999996 20 1.3],...
'String','nomefile',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h68 = uicontrol(...
'Parent',h63,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'FontSize',10,...
'Position',[34.5714285714286 0.499999999999995 8 1.45],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


