%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   S F E R A   S O S P E S A    4.5                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main(varargin)
%MAIN M-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to the
%      existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the given
%      property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a warning
%      when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 29-Sep-2016 21:48:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn. hObject    handle to
% figure eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) varargin
% unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      -10      % Vi_min        4
      3        % Vi, Vi_cur    5
      10       % Vi_max        6
      0.00     % R_min         7
      10       % R, R_cur      8
      50       % R_max         9
      0        % L_min         10
      250      % L, L_cur      11
      1000     % L_max         12
      0.00     % M_min         13
      0.01     % M, M_cur      14
      0.1      % M_max         15
      0.00     % k_min         16
      0.5      % k, k_cur      17
      2        % k_max         18
      0.00     % y0_min        19
      0.01     % y0, y0_cur    20
      0.1      % y0_max        21
      0.00     % ve0_min       22
      0.00     % ve0, ve0_cur  23
      1.00     % ve0_max       24
      0.00     % i0_min        25
      0.1      % i0, i0_cur    26
      10       % i0_max        27
      ];

% Par degli slider
global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;


Rsld.stmin = 1;
Rsld.stmax = 10;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Lsld.stmin = 10;
Lsld.stmax = 100;
Lsld.Llim = -Inf;
Lsld.Hlim = +Inf;

Msld.stmin = 0.005;
Msld.stmax = 0.01;
Msld.Llim = eps;
Msld.Hlim = +Inf;

ksld.stmin = 0.01;
ksld.stmax = 0.1;
ksld.Llim = eps;
ksld.Hlim = +Inf;

y0sld.stmin = 0.01;
y0sld.stmax = 0.01;
y0sld.Llim = -Inf;
y0sld.Hlim = +Inf;

ve0sld.stmin = 0.01;
ve0sld.stmax = 0.1;
ve0sld.Llim = -Inf;
ve0sld.Hlim = +Inf;

i0sld.stmin = 0.1;
i0sld.stmax = 1;
i0sld.Llim = -Inf;
i0sld.Hlim = +Inf;

Visld.stmin = 0.1;
Visld.stmax = 1;
Visld.Llim = -Inf;
Visld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% FIGURE

% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT); hObject
% handle to figure eventdata  reserved - to be defined in a future version
% of MATLAB handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

clc

% Chiusura dello modello simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)
global Uscita;

% Chiusura dello modello simulink (if any) senza salvare
bdclose(Uscita);

Uscita = 'Posizione_Sfera';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Lettura dei dati inseriti dall'utente
Vi = get(handles.Vi, 'Value'); u = Vi;
R  = get(handles.R,  'Value');
L  = get(handles.L,  'Value'); L = L/1000;
M  = get(handles.M,  'Value');
k  = get(handles.k,  'Value');
y0  = get(handles.y0,  'Value');
ve0 = get(handles.ve0,  'Value');
i0  = get(handles.i0,  'Value');
g = 9.81;

% Controllo sui dati nulli per evitare singolarita' (1/0)
if R == 0, R = eps; end
if M <= 0, M = eps; end
if k <= 0, k = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end

% Calcolo delle matrici F, G, H linearizzate nell'intorno del P.to di Eq.
%
% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x, u) nell'incognita x (u noto)
% Impostazione dell'equazione dinamica non lineare
% x_sym(1) <-> y
% x_sym(2) <-> ve
% x_sym(3) <-> i
x_sym = sym('x_sym', [3 1]);
dx_sym = [x_sym(2); -k/M*x_sym(3)*x_sym(3)/x_sym(1) + g; u/L - R/L*x_sym(3);];
X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3));
% Calcolo dello jacobiano simbolico (linearizzazione)
F = jacobian(dx_sym);
x = double([X.x_sym1(1); X.x_sym2(1); X.x_sym3(1)]);
F = double(subs(F, x_sym, x));

% Calcolo costanti di tempo nel punto di eq.
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(10, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'Vi', Vi; 'y0', y0; 've0', ve0; 'i0', i0; 'R', R; 'L', L; 'M', M; 'k', k; 'g', 9.81 };
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end

% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [V]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.Vi_min, 'Value',  ampiezza(1));
set(handles.Vi_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.Vi_cur, 'Value',  ampiezza(2));
set(handles.Vi_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.Vi_max, 'Value',  ampiezza(3));
set(handles.Vi_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.Vi, 'Min',   ampiezza(1));
set(handles.Vi, 'Value', ampiezza(2));
set(handles.Vi, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
Vi = get(handles.Vi_cur, 'Value'); u = Vi;
M = get(handles.M_cur, 'Value');
L = get(handles.L_cur, 'Value'); L = L/1000;
k = get(handles.k_cur, 'Value');
R = get(handles.R_cur, 'Value');
g = 9.81;


% Controllo dati....................................................
if M == 0, M = eps; end
if R <= 0,  R = eps; end


% Scrittura dell'equazione dinamica Risoluzione dell'equazione 0 = f(x, u)
% nell'incognita x (u noto)

% Impostazione dell'equazione dinamica non lineare x_sym1 <-> y0 x_sym2 <->
% ve0 x_sym3 <-> i0
x_sym = sym('x_sym', [3 1]);
% dy/dt  = ve0 dve/dt = -k/M * (i0^2) / y0 + g di/dt  = Vi/L - R/L*i0
dx_sym = [x_sym(2); -k/M * (x_sym(3)^2) / x_sym(1) + g; u/L - R/L*x_sym(3)];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1), x_sym(2), x_sym(3));

if size(X.x_sym1) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['Vi = ', num2str(u(1), '%.2f'), ' V']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym);

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: Vi = %.2f V', u(1));
for k = 1 : size(X.x_sym1)
  % Soluzione numerica
  x = double([X.x_sym1(k); X.x_sym2(k); X.x_sym3(k)]);

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno
  % al punto di equilibrio), per calcolare gli autovalori e la
  % stabilita'�del sistema intornno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X.x_sym1, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  y  = %.2f m   ', x(1));
  str22 = sprintf('\n  ve = %.2f m/s ', x(2));
  str23 = sprintf('\n  i  = %.2f A   ', x(3));

  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
      str3 = '';
  end        

  if k == size(X.x_sym1, 1), endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str22, str23, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config che l'utente voleva
% caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;

% Aggiornamento degli slider
%R
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

%L
set(handles.L_min, 'Value',  L_min);
set(handles.L_min, 'String', num2str(L_min, '%.1f'));
set(handles.L_cur, 'Value',  L_cur);
set(handles.L_cur, 'String', num2str(L_cur, '%.2f'));
set(handles.L_max, 'Value',  L_max);
set(handles.L_max, 'String', num2str(L_max, '%.1f'));

set(handles.L, 'Min',   L_min);
set(handles.L, 'Value', L_cur);
set(handles.L, 'Max',   L_max);
majorstep = Lsld.stmax / (L_max-L_min);
minorstep = Lsld.stmin / (L_max-L_min);
set(handles.L, 'SliderStep', [minorstep majorstep]);

%M
set(handles.M_min, 'Value',  M_min);
set(handles.M_min, 'String', num2str(M_min, '%.1f'));
set(handles.M_cur, 'Value',  M_cur);
set(handles.M_cur, 'String', num2str(M_cur, '%.2f'));
set(handles.M_max, 'Value',  M_max);
set(handles.M_max, 'String', num2str(M_max, '%.1f'));

set(handles.M, 'Min',   M_min);
set(handles.M, 'Value', M_cur);
set(handles.M, 'Max',   M_max);
majorstep = Msld.stmax / (M_max-M_min);
minorstep = Msld.stmin / (M_max-M_min);
set(handles.M, 'SliderStep', [minorstep majorstep]);

%k
set(handles.k_min, 'Value',  k_min);
set(handles.k_min, 'String', num2str(k_min, '%.1f'));
set(handles.k_cur, 'Value',  k_cur);
set(handles.k_cur, 'String', num2str(k_cur, '%.2f'));
set(handles.k_max, 'Value',  k_max);
set(handles.k_max, 'String', num2str(k_max, '%.1f'));

set(handles.k, 'Min',   k_min);
set(handles.k, 'Value', k_cur);
set(handles.k, 'Max',   k_max);
majorstep = ksld.stmax / (k_max-k_min);
minorstep = ksld.stmin / (k_max-k_min);
set(handles.k, 'SliderStep', [minorstep majorstep]);

%y0
set(handles.y0_min, 'Value',  y0_min);
set(handles.y0_min, 'String', num2str(y0_min, '%.1f'));
set(handles.y0_cur, 'Value',  y0_cur);
set(handles.y0_cur, 'String', num2str(y0_cur, '%.2f'));
set(handles.y0_max, 'Value',  y0_max);
set(handles.y0_max, 'String', num2str(y0_max, '%.1f'));

set(handles.y0, 'Min',   y0_min);
set(handles.y0, 'Value', y0_cur);
set(handles.y0, 'Max',   y0_max);
majorstep = y0sld.stmax / (y0_max-y0_min);
minorstep = y0sld.stmin / (y0_max-y0_min);
set(handles.y0, 'SliderStep', [minorstep majorstep]);

%ve0
set(handles.ve0_min, 'Value',  ve0_min);
set(handles.ve0_min, 'String', num2str(ve0_min, '%.1f'));
set(handles.ve0_cur, 'Value',  ve0_cur);
set(handles.ve0_cur, 'String', num2str(ve0_cur, '%.2f'));
set(handles.ve0_max, 'Value',  ve0_max);
set(handles.ve0_max, 'String', num2str(ve0_max, '%.1f'));

set(handles.ve0, 'Min',   ve0_min);
set(handles.ve0, 'Value', ve0_cur);
set(handles.ve0, 'Max',   ve0_max);
majorstep = ve0sld.stmax / (ve0_max-ve0_min);
minorstep = ve0sld.stmin / (ve0_max-ve0_min);
set(handles.ve0, 'SliderStep', [minorstep majorstep]);

%i0
set(handles.i0_min, 'Value',  i0_min);
set(handles.i0_min, 'String', num2str(i0_min, '%.1f'));
set(handles.i0_cur, 'Value',  i0_cur);
set(handles.i0_cur, 'String', num2str(i0_cur, '%.2f'));
set(handles.i0_max, 'Value',  i0_max);
set(handles.i0_max, 'String', num2str(i0_max, '%.1f'));

set(handles.i0, 'Min',   i0_min);
set(handles.i0, 'Value', i0_cur);
set(handles.i0, 'Max',   i0_max);
majorstep = i0sld.stmax / (i0_max-i0_min);
minorstep = i0sld.stmin / (i0_max-i0_min);
set(handles.i0, 'SliderStep', [minorstep majorstep]);

% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [V]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.Vi_min, 'Value',  Vi_min);
set(handles.Vi_min, 'String', num2str(Vi_min, '%.1f'));
set(handles.Vi_cur, 'Value',  Vi_cur);
set(handles.Vi_cur, 'String', num2str(Vi_cur, '%.2f'));
set(handles.Vi_max, 'Value',  Vi_max);
set(handles.Vi_max, 'String', num2str(Vi_max, '%.1f'));

set(handles.Vi, 'Min',   Vi_min);
set(handles.Vi, 'Value', Vi_cur);
set(handles.Vi, 'Max',   Vi_max);
majorstep = Visld.stmax / (Vi_max-Vi_min);
minorstep = Visld.stmin / (Vi_max-Vi_min);
set(handles.Vi, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i0 parametri pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello induzione
L_min = get(handles.L_min, 'Value');
L_cur = get(handles.L_cur, 'Value');
L_max = get(handles.L_max, 'Value');

% pannello massa
M_min = get(handles.M_min, 'Value');
M_cur = get(handles.M_cur, 'Value');
M_max = get(handles.M_max, 'Value');

% pannello coeff
k_min = get(handles.k_min, 'Value');
k_cur = get(handles.k_cur, 'Value');
k_max = get(handles.k_max, 'Value');

% pannello posizione
y0_min = get(handles.y0_min, 'Value');
y0_cur = get(handles.y0_cur, 'Value');
y0_max = get(handles.y0_max, 'Value');

% pannello velocit�
ve0_min = get(handles.ve0_min, 'Value');
ve0_cur = get(handles.ve0_cur, 'Value');
ve0_max = get(handles.ve0_max, 'Value');

% pannello corrente
i0_min = get(handles.i0_min, 'Value');
i0_cur = get(handles.i0_cur, 'Value');
i0_max = get(handles.i0_max, 'Value');


% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

Vi_min = get(handles.Vi_min, 'Value');
Vi_cur = get(handles.Vi_cur, 'Value');
Vi_max = get(handles.Vi_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'Vi_min = %f;\n', Vi_min);
fprintf(fid, 'Vi_cur = %f;\n', Vi_cur);
fprintf(fid, 'Vi_max = %f;\n', Vi_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'L_min = %f;\n', L_min);
fprintf(fid, 'L_cur = %f;\n', L_cur);
fprintf(fid, 'L_max = %f;\n', L_max);

fprintf(fid, 'M_min = %f;\n', M_min);
fprintf(fid, 'M_cur = %f;\n', M_cur);
fprintf(fid, 'M_max = %f;\n', M_max);

fprintf(fid, 'k_min = %f;\n', k_min);
fprintf(fid, 'k_cur = %f;\n', k_cur);
fprintf(fid, 'k_max = %f;\n', k_max);

fprintf(fid, 'y0_min = %f;\n', y0_min);
fprintf(fid, 'y0_cur = %f;\n', y0_cur);
fprintf(fid, 'y0_max = %f;\n', y0_max);

fprintf(fid, 've0_min = %f;\n', ve0_min);
fprintf(fid, 've0_cur = %f;\n', ve0_cur);
fprintf(fid, 've0_max = %f;\n', ve0_max);

fprintf(fid, 'i0_min = %f;\n', i0_min);
fprintf(fid, 'i0_cur = %f;\n', i0_cur);
fprintf(fid, 'i0_max = %f;\n', i0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli Aggiunge tutti i0 nomi dei
% modelli trovati nella directory corrente in una struttura temporanea
% (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    structure with
% handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    structure with
% handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of
%        ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO) eventdata  reserved - to
% be defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i0 nomi delle config trovati nella directory corrente
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Rsld Lsld Msld ksld y0sld ve0sld i0sld Visld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [V]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider Va
set(handles.Vi_min, 'Value', def(4));
set(handles.Vi_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.Vi_cur, 'Value', def(5));
set(handles.Vi_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.Vi_max, 'Value', def(6));
set(handles.Vi_max, 'String', num2str(def(6), '%.1f'));

set(handles.Vi, 'Min',   def(4)); 
set(handles.Vi, 'Value', def(5));
set(handles.Vi, 'Max',   def(6)); 
majorstep = Visld.stmax / (def(6)-def(4));
minorstep = Visld.stmin / (def(6)-def(4));
set(handles.Vi, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.Vi, 'Min') get(handles.Vi, 'Value') get(handles.Vi, 'Max')]);
evalin('base', ['input_params(''Ampiezza [V]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider L
set(handles.L_min, 'Value',  def(10));
set(handles.L_min, 'String', num2str(def(10), '%.1f'));
set(handles.L_cur, 'Value',  def(11));
set(handles.L_cur, 'String', num2str(def(11), '%.2f'));
set(handles.L_max, 'Value',  def(12));
set(handles.L_max, 'String', num2str(def(12), '%.1f'));

set(handles.L, 'Min',   def(10));
set(handles.L, 'Value', def(11));
set(handles.L, 'Max',   def(12));
majorstep = Lsld.stmax / (def(12)-def(10));
minorstep = Lsld.stmin / (def(12)-def(10));
set(handles.L, 'SliderStep', [minorstep majorstep]);

% slider M
set(handles.M_min, 'Value', def(13));
set(handles.M_min, 'String', num2str(def(13), '%.1f'));
set(handles.M_cur, 'Value', def(14));
set(handles.M_cur, 'String', num2str(def(14), '%.2f'));
set(handles.M_max, 'Value', def(15));
set(handles.M_max, 'String', num2str(def(15), '%.1f'));

set(handles.M, 'Min',   def(13)); 
set(handles.M, 'Value', def(14));
set(handles.M, 'Max',   def(15)); 
majorstep = Msld.stmax / (def(15)-def(13));
minorstep = Msld.stmin / (def(15)-def(13));
set(handles.M, 'SliderStep', [minorstep majorstep]);

% slider k
set(handles.k_min, 'Value', def(16));
set(handles.k_min, 'String', num2str(def(16), '%.1f'));
set(handles.k_cur, 'Value', def(17));
set(handles.k_cur, 'String', num2str(def(17), '%.2f'));
set(handles.k_max, 'Value', def(18));
set(handles.k_max, 'String', num2str(def(18), '%.1f'));

set(handles.k, 'Min',   def(16)); 
set(handles.k, 'Value', def(17));
set(handles.k, 'Max',   def(18)); 
majorstep = ksld.stmax / (def(18)-def(16));
minorstep = ksld.stmin / (def(18)-def(16));
set(handles.k, 'SliderStep', [minorstep majorstep]);

% slider y0
set(handles.y0_min, 'Value', def(19));
set(handles.y0_min, 'String', num2str(def(19), '%.1f'));
set(handles.y0_cur, 'Value', def(20));
set(handles.y0_cur, 'String', num2str(def(20), '%.2f'));
set(handles.y0_max, 'Value', def(21));
set(handles.y0_max, 'String', num2str(def(21), '%.1f'));

set(handles.y0, 'Min',   def(19)); 
set(handles.y0, 'Value', def(20));
set(handles.y0, 'Max',   def(21)); 
majorstep = y0sld.stmax / (def(21)-def(19));
minorstep = y0sld.stmin / (def(21)-def(19));
set(handles.y0, 'SliderStep', [minorstep majorstep]);

% slider ve0
set(handles.ve0_min, 'Value', def(22));
set(handles.ve0_min, 'String', num2str(def(22), '%.1f'));
set(handles.ve0_cur, 'Value', def(23));
set(handles.ve0_cur, 'String', num2str(def(23), '%.2f'));
set(handles.ve0_max, 'Value', def(24));
set(handles.ve0_max, 'String', num2str(def(24), '%.1f'));

set(handles.ve0, 'Min',   def(22)); 
set(handles.ve0, 'Value', def(23));
set(handles.ve0, 'Max',   def(24)); 
majorstep = ve0sld.stmax / (def(24)-def(22));
minorstep = ve0sld.stmin / (def(24)-def(22));
set(handles.ve0, 'SliderStep', [minorstep majorstep]);

% slider i0
set(handles.i0_min, 'Value', def(25));
set(handles.i0_min, 'String', num2str(def(25), '%.1f'));
set(handles.i0_cur, 'Value', def(26));
set(handles.i0_cur, 'String', num2str(def(26), '%.2f'));
set(handles.i0_max, 'Value', def(27));
set(handles.i0_max, 'String', num2str(def(27), '%.1f'));

set(handles.i0, 'Min',   def(25)); 
set(handles.i0, 'Value', def(26));
set(handles.i0, 'Max',   def(27)); 
majorstep = i0sld.stmax / (def(27)-def(25));
minorstep = i0sld.stmin / (def(27)-def(25));
set(handles.i0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER k
% --- Executes on slider movement.
function k_Callback(hObject, eventdata, handles)
% hObject    handle to k (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.k, handles.k_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function k_min_Callback(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_min_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_max_Callback(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_max_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function k_cur_Callback(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global ksld;
Slider_cur_Callback(handles, handles.k, handles.k_min, handles.k_cur, handles.k_max, ksld.stmin, ksld.stmax, ksld.Llim, ksld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function k_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to k_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER M
% --- Executes on slider movement.
function M_Callback(hObject, eventdata, handles)
% hObject    handle to M (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.M, handles.M_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function M_min_Callback(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_min_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_max_Callback(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_max_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function M_cur_Callback(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Msld;
Slider_cur_Callback(handles, handles.M, handles.M_min, handles.M_cur, handles.M_max, Msld.stmin, Msld.stmax, Msld.Llim, Msld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function M_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to M_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER L
% --- Executes on slider movement.
function L_Callback(hObject, eventdata, handles)
% hObject    handle to L (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.L, handles.L_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function L_min_Callback(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_min_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function L_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_max_Callback(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_max_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L_cur_Callback(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Lsld;
Slider_cur_Callback(handles, handles.L, handles.L_min, handles.L_cur, handles.L_max, Lsld.stmin, Lsld.stmax, Lsld.Llim, Lsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function L_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to R (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_min (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_max (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R_cur (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% INGRESSI
function Vi_min_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_min_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Vi_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Vi_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_cur_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Vi_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Vi_max_Callback(hObject, eventdata, handles)
% hObject    handle to Vi_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global Visld;
Slider_max_Callback(handles, handles.Vi, handles.Vi_min, handles.Vi_cur, handles.Vi_max, Visld.stmin, Visld.stmax, Visld.Llim, Visld.Hlim);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function Vi_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Vi_Callback(hObject, eventdata, handles)
% hObject    handle to Vi (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.Vi, handles.Vi_cur);

minval = get(handles.Vi_min, 'Value');
curval = get(handles.Vi, 'Value');
maxval = get(handles.Vi_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Vi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Vi (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [V]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Vi_min, 'Value',  values(1));
set(handles.Vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Vi_cur, 'Value',  values(2));
set(handles.Vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Vi_max, 'Value',  values(3));
set(handles.Vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Vi, 'Min',   values(1));
set(handles.Vi, 'Value', values(2));
set(handles.Vi, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA) Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.Vi_min, 'Value',  values(1));
set(handles.Vi_min, 'String', num2str(values(1), '%.1f'));
set(handles.Vi_cur, 'Value',  values(2));
set(handles.Vi_cur, 'String', num2str(values(2), '%.2f'));
set(handles.Vi_max, 'Value',  values(3));
set(handles.Vi_max, 'String', num2str(values(3), '%.1f'));
set(handles.Vi, 'Min',   values(1));
set(handles.Vi, 'Max',   values(3));
set(handles.Vi, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%%  SLIDER STATI INIZIALI

% --- Executes on slider movement.
function i0_Callback(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.i0, handles.i0_cur);


% --- Executes during object creation, after setting all properties.
function i0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function i0_min_Callback(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_min_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_max_Callback(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_max_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function i0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global isld;
Slider_cur_Callback(handles, handles.i0, handles.i0_min, handles.i0_cur, handles.i0_max, isld.stmin, isld.stmax, isld.Llim, isld.Hlim);


% --- Executes during object creation, after setting all properties.
function i0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to i0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function ve0_Callback(hObject, eventdata, handles)
% hObject    handle to ve0 (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.ve0, handles.ve0_cur);

% --- Executes during object creation, after setting all properties.
function ve0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0 (see GCBO) eventdata  reserved - to be defined
% in a future version of MATLAB handles    empty - handles not created
% until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ve0_min_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_min_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ve0_max_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_max_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ve0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to ve0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global vesld;
Slider_cur_Callback(handles, handles.ve0, handles.ve0_min, handles.ve0_cur, handles.ve0_max, vesld.stmin, vesld.stmax, vesld.Llim, vesld.Hlim);


% --- Executes during object creation, after setting all properties.
function ve0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ve0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_min_Callback(hObject, eventdata, handles)
% hObject    handle to y0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_min_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_min (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_max_Callback(hObject, eventdata, handles)
% hObject    handle to y0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_max_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_max (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function y0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to y0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    structure with handles
% and user data (see GUIDATA)

global ysld;
Slider_cur_Callback(handles, handles.y0, handles.y0_min, handles.y0_cur, handles.y0_max, ysld.stmin, ysld.stmax, ysld.Llim, ysld.Hlim);


% --- Executes during object creation, after setting all properties.
function y0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0_cur (see GCBO) eventdata  reserved - to be
% defined in a future version of MATLAB handles    empty - handles not
% created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function y0_Callback(hObject, eventdata, handles)
% hObject    handle to y0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    structure with handles and user
% data (see GUIDATA)

Slider_sld_Callback(handles, handles.y0, handles.y0_cur);

% --- Executes during object creation, after setting all properties.
function y0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to y0 (see GCBO) eventdata  reserved - to be defined in
% a future version of MATLAB handles    empty - handles not created until
% after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

