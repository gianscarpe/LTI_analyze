%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 P I S T O N E  I D R A U L I C O    3.17                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Marco Mutti (2016)
% by F. M. Marchese (2016-17)
%
% Tested under MatLab R2013b
%


function varargout = Main(varargin)
%MAIN Mp-file for Main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('Property','Value',...) creates a new MAIN using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to Main_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      MAIN('CALLBACK') and MAIN('CALLBACK',hObject,...) call the
%      local function named CALLBACK in MAIN.Mp with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Main

% Last Modified by GUIDE v2.5 10-Oct-2016 18:23:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Main is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

clc

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1        % IngressoTipo  1
      1        % IngressoPar   2
      1        % Uscita        3
      0        % F_min         4
      3        % F, F_cur      5
      10       % F_max         6
      0.00     % R_min         7
      1000     % R, R_cur      8
      5000     % R_max         9
      0        % A_min         10
      0.01     % A, A_cur      11
      0.1      % A_max         12
      0.00     % Mp_min        13
      1        % Mp, Mp_cur    14
      10       % Mp_max        15
      0.00     % Ml0_min       16
      0.5      % Ml0, Ml0_cur  17
      10       % Ml0_max       18
      0.00     % d_min         19
      1000     % d, d_cur      20
      5000     % d_max         21
      0.00     % x0_min        22
      0.00     % x0, x0_cur    23
      0.1      % x0_max        24
      0.00     % v0_min        25
      0.00     % v0, v0_cur    26
      0.2      % v0_max        27
      ];

% Par degli slider
global Rsld Asld Mpsld Ml0sld dsld Fsld x0sld v0sld;


Rsld.stmin = 1;
Rsld.stmax = 100;
Rsld.Llim = eps;
Rsld.Hlim = +Inf;

Asld.stmin = 0.01;
Asld.stmax = 0.01;
Asld.Llim = eps;
Asld.Hlim = +Inf;

Mpsld.stmin = 0.01;
Mpsld.stmax = 0.1;
Mpsld.Llim = eps;
Mpsld.Hlim = +Inf;

Ml0sld.stmin = 0.01;
Ml0sld.stmax = 0.1;
Ml0sld.Llim = eps;
Ml0sld.Hlim = +Inf;

dsld.stmin = 1;
dsld.stmax = 100;
dsld.Llim = eps;
dsld.Hlim = +Inf;

Fsld.stmin = 0.01;
Fsld.stmax = 1;
Fsld.Llim = eps;
Fsld.Hlim = +Inf;

x0sld.stmin = 0.01;
x0sld.stmax = 0.01;
x0sld.Llim = eps;
x0sld.Hlim = +Inf;

v0sld.stmin = 0.01;
v0sld.stmax = 0.01;
v0sld.Llim = eps;
v0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
% Choose default command line output for Main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');


% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%% FIGURE

% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% ConfigLoad jpg
im = imread('Schema.jpg');
image(im);
axis off;
axis equal;
axis image;


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% ConfigLoad jpg
im2 = imread('Modello.jpg');
image(im2);
axis off;
axis equal;
axis image;


% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc

% Chiusura dello schema simulink senza salvare
bdclose('all');

% Hint: ConfigDelete(hObject) closes the figure
delete(hObject);


%% RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Uscita;

% Chiusura dello schema simulink (if any) senza salvare
bdclose(Uscita);

Uscita = 'Portata_uscente';

% Lettura dei valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');


% Lettura dei dati inseriti dall'utente
F   = get(handles.F, 'Value');
R   = get(handles.R,  'Value');
A   = get(handles.A,  'Value'); 
Mp  = get(handles.Mp,  'Value');
Ml0 = get(handles.Ml0,  'Value');
d   = get(handles.d,  'Value');
x0  = get(handles.x0,  'Value');
v0  = get(handles.v0,  'Value');


% Controllo sui dati nulli per evitare singolarita' (1/0)
if Ml0 <= 0, Ml0 = eps; end
if Mp <= 0, Mp = eps; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3
  if frequenza(2) == 1000, frequenza(2) = 999.999; end
end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end

% Costante di tempo
Tau = 0.1;   %%!! rifare

vars = {'F', F; 'x0', x0; 'v0', v0; 'd', d; 'R', R; 'A', A; 'Mp', Mp; 'Ml0', Ml0};
for index = 1:size(vars, 1)
  name  = vars(index, 1);
  value = vars(index, 2);
  assignin('base', name{1}, value{1}); 
end

% Apertura del sistema da simulare
open_system(Uscita);

% Impostazione del segnale in input al sistema
h = find_system(Uscita, 'Name', 'input');
if size(h) == [1, 1]
  system_blocks = find_system(Uscita);
  if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
    delete_line(Uscita, 'step1/1', 'input/1');
    delete_block([Uscita, '/step1']);
  end
  if numel(find(strcmp(system_blocks, [Uscita '/step2']))) > 0
    delete_line(Uscita, 'step2/1', 'input/2');
    delete_block([Uscita, '/step2']);  
  end
  if numel(find(strcmp(system_blocks, [Uscita '/input']))) > 0
    delete_line(Uscita, 'input/1', 'Gain/1');
    delete_block([Uscita, '/input']);
  end
end


% Lettura del nuovo segnale in input da simulare
val = get(handles.IngressoTipo, 'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(.5*Tau+1e-3));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifica della durata della simulazione
switch val
  case {1, 2}
      set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
  case {3, 4, 5, 6}
      set_param(Uscita, 'StopTime', num2str(6/frequenza(2)));
end

% Modifica dello sfondo e della posizione del blocco inserito
set_param([Uscita, '/input'], 'BackgroundColor', '[0, 206, 206]');
GainPos = get_param([Uscita, '/Gain'], 'Position');
avgGainh = (GainPos(2)+GainPos(4))/2;
if val ~= 2
  set_param([Uscita, '/input'], 'Position', ['[0,' num2str(avgGainh-15) ', 65,' num2str(avgGainh+15) ']']); % '[40, 90, 105, 120]');
else
  set_param([Uscita, '/step1'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step2'], 'BackgroundColor', '[0, 206, 206]');
  set_param([Uscita, '/step1'], 'Position', ['[0,' num2str(avgGainh-30-15) ', 65 ,' num2str(avgGainh-30+15) ']']);    % '[20, 45, 85, 75]');
  set_param([Uscita, '/step2'], 'Position', ['[0,' num2str(avgGainh+30-15) ', 65 ,' num2str(avgGainh+30+15)  ']']);  % '[20, 135, 85, 165]');
  set_param([Uscita, '/input'], 'Position', ['[75,' num2str(avgGainh-10) ', 95,' num2str(avgGainh+10) ']']); % '[95, 95, 115, 115]');
  add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
  add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end

% Nuovo collegamento con il blocco successivo (Gain)
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salvataggio del sistema
save_system(Uscita);

% Avvio della simulazione
sim(Uscita);

ViewerAutoscale();


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [N]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');
set(handles.F_min, 'Value',  ampiezza(1));
set(handles.F_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.F_cur, 'Value',  ampiezza(2));
set(handles.F_cur, 'String', num2str(ampiezza(2), '%.1f'));
set(handles.F_max, 'Value',  ampiezza(3));
set(handles.F_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.F, 'Min',   ampiezza(1));
set(handles.F, 'Value', ampiezza(2));
set(handles.F, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
F   = get(handles.F_cur, 'Value'); u = F;
R   = get(handles.R_cur,  'Value');
A   = get(handles.A_cur,  'Value'); 
Mp  = get(handles.Mp_cur,  'Value');
Ml0 = get(handles.Ml0_cur,  'Value');
d   = get(handles.d_cur,  'Value');
x0  = get(handles.x0_cur,  'Value');
v0  = get(handles.v0_cur,  'Value');

% Controllo sui dati nulli per evitare singolarita' (1/0)
if Ml0 <= 0, Ml0 = eps; end
if Mp <= 0, Mp = eps; end

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = f(x0, u) nell'incognita x0 (u noto)

% Impostazione dell'equazione dinamica non lineare
% x_sym1 <-> x
% x_sym2 <-> v
x_sym = sym('x_sym', [2 1]);
% dx/dt  = v
% dv/dt = (F-d*A^2*v*R+d*A*v^2)/(Mp+Ml0)

dx_sym = [x_sym(2); (u - d*A^2*x_sym(2)*R+d*A*x_sym(2)^2)/(Mp+Ml0)];

% Risoluzione simbolica in forma chiusa (caso non lineare)
X = solve(dx_sym == 0, x_sym(1), x_sym(2));

 
if size(X) == 0
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['F = ', num2str(u(1), '%.2f'), ' N']});
  return
end

% Calcolo dello jacobiano simbolico (linearizzazione)
Jac_sym = jacobian(dx_sym, [x_sym(1),x_sym(2)])

% Preparazione testo da visualizzare
str = sprintf('In presenza dell''ingresso: F = %.2f N', u(1));
for k = 1 : size(X.x_sym1)
  %Eliminazione del parametro z in caso di infinite soluzioni 
  if F == 0
    temp = 0;
  else
    temp = X.x_sym1(k);
  end
  % Soluzione numerica
  x = double([temp ; X.x_sym2(k)]);

  % Calcolo dello jacobiano numerico (matrice F - linearizzazione intorno 
  % al punto di equilibrio), per calcolare gli autovalori 
  % e la stabilita'�del sistema intornno al punto di equilibrio
  F = double(subs(Jac_sym, x_sym, x));

  if size(X.x_sym1, 1) == 1, str1 = sprintf('\nlo stato:');
  else                       str1 = sprintf('\n- lo stato'); end
  
  str21 = sprintf('\n  x  = %.2f m  ', x(1));
  str22 = sprintf('\n  v = %.2f m/s ', x(2));
  
  % Stabilita'
  switch StabilityLTI(F)
    case -1.2
      str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
    case -1.1
      str3 = sprintf('\n e'' asintoticamente stabile');
    case  0.1
      str3 = sprintf('\n e'' semplicemente stabile');
    case  0.2
      str3 = sprintf('\n e'' semplicemente stabile (osc.)');
    case +1.1
      str3 = sprintf('\n e'' debolmente instabile');
    case +1.2
      str3 = sprintf('\n e'' debolmente instabile (osc.)');
    case +2.1
      str3 = sprintf('\n e'' fortemente instabile');
    case +2.2
      str3 = sprintf('\n e'' fortemente instabile (osc.)');
    otherwise
      str3 = '';
  end        

  if k == size(X.x_sym1, 1), endstr = '.'; else endstr = ';'; end
  
  str = strcat(str, str1, str21, str22, str3, endstr);
end

set(handles.Punto_Eq_txt, 'String', str);


%% GESTIONE CONFIGURAZIONI
% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che a'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end


% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

global Rsld Asld Mpsld Ml0sld dsld Fsld x0sld v0sld;

% Aggiornamento degli slider
%A
set(handles.A_min, 'Value',  A_min);
set(handles.A_min, 'String', num2str(A_min, '%.1f'));
set(handles.A_cur, 'Value',  A_cur);
set(handles.A_cur, 'String', num2str(A_cur, '%.2f'));
set(handles.A_max, 'Value',  A_max);
set(handles.A_max, 'String', num2str(A_max, '%.1f'));

set(handles.A, 'Min',   A_min);
set(handles.A, 'Value', A_cur);
set(handles.A, 'Max',   A_max);
majorstep = Asld.stmax / (A_max-A_min);
minorstep = Asld.stmin / (A_max-A_min);
set(handles.A, 'SliderStep', [minorstep majorstep]);

%R
set(handles.R_min, 'Value',  R_min);
set(handles.R_min, 'String', num2str(R_min, '%.1f'));
set(handles.R_cur, 'Value',  R_cur);
set(handles.R_cur, 'String', num2str(R_cur, '%.2f'));
set(handles.R_max, 'Value',  R_max);
set(handles.R_max, 'String', num2str(R_max, '%.1f'));

set(handles.R, 'Min',   R_min);
set(handles.R, 'Value', R_cur);
set(handles.R, 'Max',   R_max);
majorstep = Rsld.stmax / (R_max-R_min);
minorstep = Rsld.stmin / (R_max-R_min);
set(handles.R, 'SliderStep', [minorstep majorstep]);

%Mp
set(handles.Mp_min, 'Value',  Mp_min);
set(handles.Mp_min, 'String', num2str(Mp_min, '%.1f'));
set(handles.Mp_cur, 'Value',  Mp_cur);
set(handles.Mp_cur, 'String', num2str(Mp_cur, '%.2f'));
set(handles.Mp_max, 'Value',  Mp_max);
set(handles.Mp_max, 'String', num2str(Mp_max, '%.1f'));

set(handles.Mp, 'Min',   Mp_min);
set(handles.Mp, 'Value', Mp_cur);
set(handles.Mp, 'Max',   Mp_max);
majorstep = Mpsld.stmax / (Mp_max-Mp_min);
minorstep = Mpsld.stmin / (Mp_max-Mp_min);
set(handles.Mp, 'SliderStep', [minorstep majorstep]);

%Ml0
set(handles.Ml0_min, 'Value',  Ml0_min);
set(handles.Ml0_min, 'String', num2str(Ml0_min, '%.1f'));
set(handles.Ml0_cur, 'Value',  Ml0_cur);
set(handles.Ml0_cur, 'String', num2str(Ml0_cur, '%.2f'));
set(handles.Ml0_max, 'Value',  Ml0_max);
set(handles.Ml0_max, 'String', num2str(Ml0_max, '%.1f'));

set(handles.Ml0, 'Min',   Ml0_min);
set(handles.Ml0, 'Value', Ml0_cur);
set(handles.Ml0, 'Max',   Ml0_max);
majorstep = Ml0sld.stmax / (Ml0_max-Ml0_min);
minorstep = Ml0sld.stmin / (Ml0_max-Ml0_min);
set(handles.Ml0, 'SliderStep', [minorstep majorstep]);

%d
set(handles.d_min, 'Value',  d_min);
set(handles.d_min, 'String', num2str(d_min, '%.1f'));
set(handles.d_cur, 'Value',  d_cur);
set(handles.d_cur, 'String', num2str(d_cur, '%.2f'));
set(handles.d_max, 'Value',  d_max);
set(handles.d_max, 'String', num2str(d_max, '%.1f'));

set(handles.d, 'Min',   d_min);
set(handles.d, 'Value', d_cur);
set(handles.d, 'Max',   d_max);
majorstep = dsld.stmax / (d_max-d_min);
minorstep = dsld.stmin / (d_max-d_min);
set(handles.d, 'SliderStep', [minorstep majorstep]);

%x0
set(handles.x0_min, 'Value',  x0_min);
set(handles.x0_min, 'String', num2str(x0_min, '%.1f'));
set(handles.x0_cur, 'Value',  x0_cur);
set(handles.x0_cur, 'String', num2str(x0_cur, '%.2f'));
set(handles.x0_max, 'Value',  x0_max);
set(handles.x0_max, 'String', num2str(x0_max, '%.1f'));

set(handles.x0, 'Min',   x0_min);
set(handles.x0, 'Value', x0_cur);
set(handles.x0, 'Max',   x0_max);
majorstep = x0sld.stmax / (x0_max-x0_min);
minorstep = x0sld.stmin / (x0_max-x0_min);
set(handles.x0, 'SliderStep', [minorstep majorstep]);

%v0
set(handles.v0_min, 'Value',  v0_min);
set(handles.v0_min, 'String', num2str(v0_min, '%.1f'));
set(handles.v0_cur, 'Value',  v0_cur);
set(handles.v0_cur, 'String', num2str(v0_cur, '%.2f'));
set(handles.v0_max, 'Value',  v0_max);
set(handles.v0_max, 'String', num2str(v0_max, '%.1f'));

set(handles.v0, 'Min',   v0_min);
set(handles.v0, 'Value', v0_cur);
set(handles.v0, 'Max',   v0_max);
majorstep = v0sld.stmax / (v0_max-v0_min);
minorstep = v0sld.stmin / (v0_max-v0_min);
set(handles.v0, 'SliderStep', [minorstep majorstep]);


% Ingressi
Parnamelist = cell(1);
Parnamelist{1} = 'Ampiezza [N]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      Parnamelist{2} = 'Frequenza�[Hz]';
  case 5
      Parnamelist{2} = 'Frequenza [Hz]';
      Parnamelist{3} = 'Duty Cycle [%]';
  case {4, 6}
      Parnamelist{2} = 'Frequenza [Hz]';
end


set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'String', Parnamelist);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.F_min, 'Value',  F_min);
set(handles.F_min, 'String', num2str(F_min, '%.1f'));
set(handles.F_cur, 'Value',  F_cur);
set(handles.F_cur, 'String', num2str(F_cur, '%.2f'));
set(handles.F_max, 'Value',  F_max);
set(handles.F_max, 'String', num2str(F_max, '%.1f'));

set(handles.F, 'Min',   F_min);
set(handles.F, 'Value', F_cur);
set(handles.F, 'Max',   F_max);
majorstep = Fsld.stmax / (F_max-F_min);
minorstep = Fsld.stmin / (F_max-F_min);
set(handles.F, 'SliderStep', [minorstep majorstep]);


set(handles.Uscita, 'Value', Uscita);

% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;

% Salvataggio di tutti i parametri
% pannello resistenza
R_min = get(handles.R_min, 'Value');
R_cur = get(handles.R_cur, 'Value');
R_max = get(handles.R_max, 'Value');

% pannello area
A_min = get(handles.A_min, 'Value');
A_cur = get(handles.A_cur, 'Value');
A_max = get(handles.A_max, 'Value');

% pannello massa del pistone
Mp_min = get(handles.Mp_min, 'Value');
Mp_cur = get(handles.Mp_cur, 'Value');
Mp_max = get(handles.Mp_max, 'Value');

% pannello massa iniziale del fluido
Ml0_min = get(handles.Ml0_min, 'Value');
Ml0_cur = get(handles.Ml0_cur, 'Value');
Ml0_max = get(handles.Ml0_max, 'Value');

% pannello densit�
d_min = get(handles.d_min, 'Value');
d_cur = get(handles.d_cur, 'Value');
d_max = get(handles.d_max, 'Value');


% pannello posizione
x0_min = get(handles.x0_min, 'Value');
x0_cur = get(handles.x0_cur, 'Value');
x0_max = get(handles.x0_max, 'Value');

% pannello velocit�
v0_min = get(handles.v0_min, 'Value');
v0_cur = get(handles.v0_cur, 'Value');
v0_max = get(handles.v0_max, 'Value');



% pannello uscita
Uscita = get(handles.Uscita, 'Value');

% pannello ingressi
IngressoTipo   = get(handles.IngressoTipo, 'Value');
IngressoParval = get(handles.IngressoPar, 'Value');
IngressoParstr = get(handles.IngressoPar, 'String');

F_min = get(handles.F_min, 'Value');
F_cur = get(handles.F_cur, 'Value');
F_max = get(handles.F_max, 'Value');

% Salvataggio file
fid = fopen(namem, 'w');

% Salvataggio ingressi su file
fprintf(fid, '%% Ingressi\n');
fprintf(fid, 'IngressoTipo = %d;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %d;\n', IngressoParval);
fprintf(fid, 'F_min = %f;\n', F_min);
fprintf(fid, 'F_cur = %f;\n', F_cur);
fprintf(fid, 'F_max = %f;\n', F_max);

% Salvataggio uscita su file
fprintf(fid, '\n%% Uscita\n');
fprintf(fid, 'Uscita = %d;\n', Uscita);

% Salvataggio parametri su file
fprintf(fid, '\n%% Parametri\n');
fprintf(fid, 'R_min = %f;\n', R_min);
fprintf(fid, 'R_cur = %f;\n', R_cur);
fprintf(fid, 'R_max = %f;\n', R_max);

fprintf(fid, 'A_min = %f;\n', A_min);
fprintf(fid, 'A_cur = %f;\n', A_cur);
fprintf(fid, 'A_max = %f;\n', A_max);

fprintf(fid, 'Mp_min = %f;\n', Mp_min);
fprintf(fid, 'Mp_cur = %f;\n', Mp_cur);
fprintf(fid, 'Mp_max = %f;\n', Mp_max);

fprintf(fid, 'Ml0_min = %f;\n', Ml0_min);
fprintf(fid, 'Ml0_cur = %f;\n', Ml0_cur);
fprintf(fid, 'Ml0_max = %f;\n', Ml0_max);

fprintf(fid, 'd_min = %f;\n', d_min);
fprintf(fid, 'd_cur = %f;\n', d_cur);
fprintf(fid, 'd_max = %f;\n', d_max);

fprintf(fid, 'x0_min = %f;\n', x0_min);
fprintf(fid, 'x0_cur = %f;\n', x0_cur);
fprintf(fid, 'x0_max = %f;\n', x0_max);

fprintf(fid, 'v0_min = %f;\n', v0_min);
fprintf(fid, 'v0_cur = %f;\n', v0_cur);
fprintf(fid, 'v0_max = %f;\n', v0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');


% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone a'estensione .mp)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');



% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end

% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone a'estensione .mp)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end


%% VALORI DI DEFAULT
% 
function Load_Defaults(handles)

global def;
global Rsld Asld Mpsld Ml0sld dsld Fsld x0sld v0sld;

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [N]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% uscita
set(handles.Uscita, 'Value', def(3));

% slider F
set(handles.F_min, 'Value', def(4));
set(handles.F_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.F_cur, 'Value', def(5));
set(handles.F_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.F_max, 'Value', def(6));
set(handles.F_max, 'String', num2str(def(6), '%.1f'));

set(handles.F, 'Min',   def(4)); 
set(handles.F, 'Value', def(5));
set(handles.F, 'Max',   def(6)); 
majorstep = Fsld.stmax / (def(6)-def(4));
minorstep = Fsld.stmin / (def(6)-def(4));
set(handles.F, 'SliderStep', [minorstep majorstep]);

vect = mat2str([get(handles.F, 'Min') get(handles.F, 'Value') get(handles.F, 'Max')]);
evalin('base', ['input_params(''Ampiezza [N]'') = ' vect ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];');   % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider R
set(handles.R_min, 'Value',  def(7));
set(handles.R_min, 'String', num2str(def(7), '%.1f'));
set(handles.R_cur, 'Value',  def(8));
set(handles.R_cur, 'String', num2str(def(8), '%.2f'));
set(handles.R_max, 'Value',  def(9));
set(handles.R_max, 'String', num2str(def(9), '%.1f'));

set(handles.R, 'Min',   def(7));
set(handles.R, 'Value', def(8));
set(handles.R, 'Max',   def(9));
majorstep = Rsld.stmax / (def(9)-def(7));
minorstep = Rsld.stmin / (def(9)-def(7));
set(handles.R, 'SliderStep', [minorstep majorstep]);

% slider A
set(handles.A_min, 'Value',  def(10));
set(handles.A_min, 'String', num2str(def(10), '%.1f'));
set(handles.A_cur, 'Value',  def(11));
set(handles.A_cur, 'String', num2str(def(11), '%.2f'));
set(handles.A_max, 'Value',  def(12));
set(handles.A_max, 'String', num2str(def(12), '%.1f'));

set(handles.A, 'Min',   def(10));
set(handles.A, 'Value', def(11));
set(handles.A, 'Max',   def(12));
majorstep = Asld.stmax / (def(12)-def(10));
minorstep = Asld.stmin / (def(12)-def(10));
set(handles.A, 'SliderStep', [minorstep majorstep]);

% slider Mp
set(handles.Mp_min, 'Value', def(13));
set(handles.Mp_min, 'String', num2str(def(13), '%.1f'));
set(handles.Mp_cur, 'Value', def(14));
set(handles.Mp_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Mp_max, 'Value', def(15));
set(handles.Mp_max, 'String', num2str(def(15), '%.1f'));

set(handles.Mp, 'Min',   def(13)); 
set(handles.Mp, 'Value', def(14));
set(handles.Mp, 'Max',   def(15)); 
majorstep = Mpsld.stmax / (def(15)-def(13));
minorstep = Mpsld.stmin / (def(15)-def(13));
set(handles.Mp, 'SliderStep', [minorstep majorstep]);

% slider Ml0
set(handles.Ml0_min, 'Value', def(16));
set(handles.Ml0_min, 'String', num2str(def(16), '%.1f'));
set(handles.Ml0_cur, 'Value', def(17));
set(handles.Ml0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.Ml0_max, 'Value', def(18));
set(handles.Ml0_max, 'String', num2str(def(18), '%.1f'));

set(handles.Ml0, 'Min',   def(16)); 
set(handles.Ml0, 'Value', def(17));
set(handles.Ml0, 'Max',   def(18)); 
majorstep = Ml0sld.stmax / (def(18)-def(16));
minorstep = Ml0sld.stmin / (def(18)-def(16));
set(handles.Ml0, 'SliderStep', [minorstep majorstep]);

% slider d
set(handles.d_min, 'Value', def(19));
set(handles.d_min, 'String', num2str(def(19), '%.1f'));
set(handles.d_cur, 'Value', def(20));
set(handles.d_cur, 'String', num2str(def(20), '%.2f'));
set(handles.d_max, 'Value', def(21));
set(handles.d_max, 'String', num2str(def(21), '%.1f'));

set(handles.d, 'Min',   def(19)); 
set(handles.d, 'Value', def(20));
set(handles.d, 'Max',   def(21)); 
majorstep = dsld.stmax / (def(21)-def(19));
minorstep = dsld.stmin / (def(21)-def(19));
set(handles.d, 'SliderStep', [minorstep majorstep]);

% slider x0
set(handles.x0_min, 'Value', def(22));
set(handles.x0_min, 'String', num2str(def(22), '%.1f'));
set(handles.x0_cur, 'Value', def(23));
set(handles.x0_cur, 'String', num2str(def(23), '%.2f'));
set(handles.x0_max, 'Value', def(24));
set(handles.x0_max, 'String', num2str(def(24), '%.1f'));

set(handles.x0, 'Min',   def(22)); 
set(handles.x0, 'Value', def(23));
set(handles.x0, 'Max',   def(24)); 
majorstep = x0sld.stmax / (def(24)-def(22));
minorstep = x0sld.stmin / (def(24)-def(22));
set(handles.x0, 'SliderStep', [minorstep majorstep]);

% slider v0
set(handles.v0_min, 'Value', def(25));
set(handles.v0_min, 'String', num2str(def(25), '%.1f'));
set(handles.v0_cur, 'Value', def(26));
set(handles.v0_cur, 'String', num2str(def(26), '%.2f'));
set(handles.v0_max, 'Value', def(27));
set(handles.v0_max, 'String', num2str(def(27), '%.1f'));

set(handles.v0, 'Min',   def(25)); 
set(handles.v0, 'Value', def(26));
set(handles.v0, 'Max',   def(27)); 
majorstep = v0sld.stmax / (def(27)-def(25));
minorstep = v0sld.stmin / (def(27)-def(25));
set(handles.v0, 'SliderStep', [minorstep majorstep]);


set(handles.ConfigSaveName, 'String', 'nomefile');



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica della config
global modified;
modified = true;
CfgList  = get(handles.ConfigLoadName, 'String');
CfgIndex = get(handles.ConfigLoadName, 'Value');
CfgName  = [CfgList{CfgIndex}, '_'];
set(handles.ConfigSaveName, 'String', CfgName);


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Ml0
% --- Executes on slider movement.
function Ml0_Callback(hObject, eventdata, handles)
% hObject    handle to Ml0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Ml0, handles.Ml0_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Ml0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ml0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function Ml0_min_Callback(hObject, eventdata, handles)
% hObject    handle to Ml0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Ml0sld;
Slider_min_Callback(handles, handles.Ml0, handles.Ml0_min, handles.Ml0_cur, handles.Ml0_max, Ml0sld.stmin, Ml0sld.stmax, Ml0sld.Llim, Ml0sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Ml0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ml0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Ml0_max_Callback(hObject, eventdata, handles)
% hObject    handle to Ml0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Ml0sld;
Slider_max_Callback(handles, handles.Ml0, handles.Ml0_min, handles.Ml0_cur, handles.Ml0_max, Ml0sld.stmin, Ml0sld.stmax, Ml0sld.Llim, Ml0sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Ml0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ml0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Ml0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Ml0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Ml0sld;
Slider_cur_Callback(handles, handles.Ml0, handles.Ml0_min, handles.Ml0_cur, handles.Ml0_max, Ml0sld.stmin, Ml0sld.stmax, Ml0sld.Llim, Ml0sld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Ml0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Ml0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Mp
% --- Executes on slider movement.
function Mp_Callback(hObject, eventdata, handles)
% hObject    handle to Mp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.Mp, handles.Mp_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Mp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function Mp_min_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Mpsld;
Slider_min_Callback(handles, handles.Mp, handles.Mp_min, handles.Mp_cur, handles.Mp_max, Mpsld.stmin, Mpsld.stmax, Mpsld.Llim, Mpsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Mp_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Mp_max_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Mpsld;
Slider_max_Callback(handles, handles.Mp, handles.Mp_min, handles.Mp_cur, handles.Mp_max, Mpsld.stmin, Mpsld.stmax, Mpsld.Llim, Mpsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Mp_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Mp_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Mpsld;
Slider_cur_Callback(handles, handles.Mp, handles.Mp_min, handles.Mp_cur, handles.Mp_max, Mpsld.stmin, Mpsld.stmax, Mpsld.Llim, Mpsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function Mp_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER A
% --- Executes on slider movement.
function A_Callback(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.A, handles.A_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function A_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function A_min_Callback(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Asld;
Slider_min_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_max_Callback(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Asld;
Slider_max_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function A_cur_Callback(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Asld;
Slider_cur_Callback(handles, handles.A, handles.A_min, handles.A_cur, handles.A_max, Asld.stmin, Asld.stmax, Asld.Llim, Asld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function A_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to A_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER d
% --- Executes on slider movement.
function d_Callback(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.d, handles.d_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function d_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function d_min_Callback(hObject, eventdata, handles)
% hObject    handle to d_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global dsld;
Slider_min_Callback(handles, handles.d, handles.d_min, handles.d_cur, handles.d_max, dsld.stmin, dsld.stmax, dsld.Llim, dsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function d_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function d_max_Callback(hObject, eventdata, handles)
% hObject    handle to d_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global dsld;
Slider_max_Callback(handles, handles.d, handles.d_min, handles.d_cur, handles.d_max, dsld.stmin, dsld.stmax, dsld.Llim, dsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function d_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function d_cur_Callback(hObject, eventdata, handles)
% hObject    handle to d_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global dsld;
Slider_cur_Callback(handles, handles.d, handles.d_min, handles.d_cur, handles.d_max, dsld.stmin, dsld.stmax, dsld.Llim, dsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function d_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to d_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



%% SLIDER R
% --- Executes on slider movement.
function R_Callback(hObject, eventdata, handles)
% hObject    handle to Mp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.R, handles.R_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function R_min_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_min_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_max_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_max_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Mp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Rsld;
Slider_cur_Callback(handles, handles.R, handles.R_min, handles.R_cur, handles.R_max, Rsld.stmin, Rsld.stmax, Rsld.Llim, Rsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function R_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Mp_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSI
function F_min_Callback(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Fsld;
Slider_min_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function F_cur_Callback(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Fsld;
Slider_cur_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function F_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function F_max_Callback(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Fsld;
Slider_max_Callback(handles, handles.F, handles.F_min, handles.F_cur, handles.F_max, Fsld.stmin, Fsld.stmax, Fsld.Llim, Fsld.Hlim);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');


% --- Executes during object creation, after setting all properties.
function F_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function F_Callback(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.F, handles.F_cur);

minval = get(handles.F_min, 'Value');
curval = get(handles.F, 'Value');
maxval = get(handles.F_max, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(1) = minval;
values(2) = curval;
values(3) = maxval;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');

% --- Executes during object creation, after setting all properties.
function F_CreateFcn(hObject, eventdata, handles)
% hObject    handle to F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

IngressoTipo = get(hObject, 'Value');

set(handles.IngressoPar, 'Enable', 'on');
ParnameList = cell(1);
ParnameList{1} = 'Ampiezza [N]';
switch IngressoTipo
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    ParnameList{2} = 'Frequenza�[Hz]';
  case 5
    ParnameList{2} = 'Frequenza [Hz]'; 
    ParnameList{3} = 'Duty Cycle [%]'; 
  case {4, 6}
    ParnameList{2} = 'Frequenza [Hz]'; 
end

set(handles.IngressoPar, 'String', ParnameList);
set(handles.IngressoPar, 'Value', 1);
Parname = ParnameList{1};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.2f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min',   values(1));
set(handles.F, 'Value', values(2));
set(handles.F, 'Max',   values(3));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

ParnameList = get(handles.IngressoPar, 'String');
Parnameval = get(handles.IngressoPar, 'Value');
Parname = ParnameList{Parnameval};
values = evalin('base', ['input_params(''' Parname ''')']);

set(handles.F_min, 'Value',  values(1));
set(handles.F_min, 'String', num2str(values(1), '%.1f'));
set(handles.F_cur, 'Value',  values(2));
set(handles.F_cur, 'String', num2str(values(2), '%.2f'));
set(handles.F_max, 'Value',  values(3));
set(handles.F_max, 'String', num2str(values(3), '%.1f'));
set(handles.F, 'Min',   values(1));
set(handles.F, 'Max',   values(3));
set(handles.F, 'Value', values(2));

% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%%  SLIDER STATI INIZIALI


% --- Executes on slider movement.
function v0_Callback(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.v0, handles.v0_cur);

% --- Executes during object creation, after setting all properties.
function v0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function v0_min_Callback(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global v0sld;
Slider_min_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v0_max_Callback(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global v0sld;
Slider_max_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function v0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global v0sld;
Slider_cur_Callback(handles, handles.v0, handles.v0_min, handles.v0_cur, handles.v0_max, v0sld.stmin, v0sld.stmax, v0sld.Llim, v0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function v0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to v0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_min_Callback(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global x0sld;
Slider_min_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_max_Callback(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global x0sld;
Slider_max_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function x0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global x0sld;
Slider_cur_Callback(handles, handles.x0, handles.x0_min, handles.x0_cur, handles.x0_max, x0sld.stmin, x0sld.stmax, x0sld.Llim, x0sld.Hlim);


% --- Executes during object creation, after setting all properties.
function x0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function x0_Callback(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Slider_sld_Callback(handles, handles.x0, handles.x0_cur);

% --- Executes during object creation, after setting all properties.
function x0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to x0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




