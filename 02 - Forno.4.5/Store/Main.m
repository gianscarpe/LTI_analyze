%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                F O R N O   S E N Z A   P A R E T E   4.1                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Giorgio Codara (2016)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%%
function varargout = Main(varargin)
% MAIN M-file for Main.fig
%      FORNO, by itself, creates a new FORNO or raises the existing
%      singleton*.
%
%      H = FORNO returns the handle to a new FORNO or the handle to
%      the existing singleton*.
%
%      FORNO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FORNO.M with the given input arguments.
%
%      FORNO('Property','Value',...) creates a new FORNO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Forno_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Forno_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
%   IMPORTANTE: necessita l'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Forno

% Last Modified by GUIDE v2.5 30-Nov-2016 11:50:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Forno is made visible.
function Main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Forno (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1     % IngressoTipo
      1     % IngressoPar
      1     % Uscita
      0     % q_min
      100   % q, q_cur
      300   % q_max
      0     % thetae_min
      20    % thetae, thetae_cur
      35    % thetae_max
      1     % Cf_min
      2     % Cf, Cf_cur
      10    % Cf_max
      0     % Kie_min
      5     % Kie, Kie_cur
      10    % Kie_max
      0     % thetai0_min
      20    % thetai0, thetai0_cur
      300   % thetai0_max
      ];

% Par degli slider
global qsld thetaesld Cfsld Kiesld thetai0sld;

qsld.stmin = 1;
qsld.stmax = 10;
qsld.Llim = eps;
qsld.Hlim = +Inf;

thetaesld.stmin = 1;
thetaesld.stmax = 10;
thetaesld.Llim = eps;
thetaesld.Hlim = +Inf;

Cfsld.stmin = 0.1;
Cfsld.stmax = 1;
Cfsld.Llim = eps;
Cfsld.Hlim = +Inf;

Kiesld.stmin = 0.1;
Kiesld.stmax = 1;
Kiesld.Llim = eps;
Kiesld.Hlim = +Inf;

thetai0sld.stmin = 1;
thetai0sld.stmax = 10;
thetai0sld.Llim = eps;
thetai0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
  
% Choose default command line output for Forno
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main wait for user response (see UIRESUME)
% uiwait(handles.Dialog);

% --- Outputs from this function are returned to the command line.
function varargout = Main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to  populate Schema


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate modello


%%  RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

% Chiudo il Modello simulink senza salvare
bdclose(Uscita);

% Leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = ['Theta_i'];
end;

%Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
thetai0 = get(handles.thetai0, 'Value');
q = ampiezza(2);
thetae = get(handles.thetae, 'Value');
Cf = get(handles.Cf, 'Value');
Kie = get(handles.Kie, 'Value');

%controllo sui dati nulli
if Cf == 0, Cf = eps; end
if Kie == 0, Kie = 0.001; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [- Kie/Cf];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'thetai0', thetai0; 'q', q; 'thetae', thetae; 'Cf', Cf; 'Kie', Kie};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il Modello in Simulink
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita,'Name','input');
if ( size(h)==[1,1])
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita,'/step1']);
        delete_block([Uscita,'/step2']);
    end
end


% Legge qual'� il nuovo segnale in input da simulare
val = get(handles.IngressoTipo,'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

%Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([Uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.q_min, 'Value',  ampiezza(1));
set(handles.q_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.q_cur, 'Value',  ampiezza(2));
set(handles.q_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.q_max, 'Value',  ampiezza(3));
set(handles.q_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.q, 'Min',   ampiezza(1));
set(handles.q, 'Value', ampiezza(2));
set(handles.q, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
thetae = get(handles.thetae, 'Value'); 
q  = get(handles.q,  'Value');
Cf  = get(handles.Cf,  'Value');
Kie  = get(handles.Kie,  'Value');

u = [q; thetae];

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if Cf <= 0, Cf = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)

% Preparazione matrici 
F = [- Kie/Cf];
G = [1/Cf, Kie/Cf];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['q = ', num2str(u(1), '%.2f'), ' W , thetae = ', num2str(u(2), '%.2f'), ' �C']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza degli ingressi: q = %.2f W , thetae = %.2f �C', u(1), u(2));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  thetai = %.2f �C', x(1));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);



%% SLIDER Cf
% --- Executes on slider movement.
function Cf_Callback(hObject, eventdata, handles)
% hObject    handle to Cf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Cf, handles.Cf_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Cf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Cf_min_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_min_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cf_min as text
%        str2double(get(hObject,'String')) returns contents of Cf_min as a double


% --- Executes during object creation, after setting all properties.
function Cf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Cf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_cur_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Cf_cur as text
%        str2double(get(hObject,'String')) returns contents of Cf_cur as a double


% --- Executes during object creation, after setting all properties.
function Cf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Cf_max_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_max_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cf_max as text
%        str2double(get(hObject,'String')) returns contents of Cf_max as a double


% --- Executes during object creation, after setting all properties.
function Cf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Kie
% --- Executes on slider movement.
function Kie_Callback(hObject, eventdata, handles)
% hObject    handle to Kie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Kie, handles.Kie_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Kie_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Kie_min_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_min_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);

% Hints: get(hObject,'String') returns contents of Kie_min as text
%        str2double(get(hObject,'String')) returns contents of Kie_min as a double


% --- Executes during object creation, after setting all properties.
function Kie_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Kie_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_cur_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Kie_cur as text
%        str2double(get(hObject,'String')) returns contents of Kie_cur as a double


% --- Executes during object creation, after setting all properties.
function Kie_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Kie_max_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_max_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);
% Hints: get(hObject,'String') returns contents of Kie_max as text
%        str2double(get(hObject,'String')) returns contents of Kie_max as a double


% --- Executes during object creation, after setting all properties.
function Kie_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSO TIPO
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns IngressoTipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoTipo

global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);


id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [W]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
    case {1, 2}
        set(handles.IngressoPar, 'Value', 1);
        set(handles.IngressoPar, 'Enable', 'off');
    case 3
        new_String{2} = 'Frequenza�[Hz]';
        set(handles.IngressoPar, 'Value', 2);
    case 5
        new_String{2} = 'Frequenza [Hz]';
        new_String{3} = 'Duty Cycle [%]';
        set(handles.IngressoPar, 'Value', 3);
    case {4, 6}
        new_String{2} = 'Frequenza [Hz]';
        set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q, 'Min', values(1));
set(handles.q, 'Value', values(2));
set(handles.q, 'Max', values(3));
set(handles.q_min, 'Value', values(1));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_cur, 'Value', values(2));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_max, 'Value', values(3));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q, 'Min', values(1));
set(handles.q, 'Value', values(2));
set(handles.q, 'Max', values(3));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_min, 'Value', values(1));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_cur, 'Value', values(2));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));
set(handles.q_max, 'Value', values(3));
% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%
% --- Executes on slider movement.
function q_Callback(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.q, handles.q_cur);

val = get(handles.q, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function q_min_Callback(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_min_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_min as text
%        str2double(get(hObject,'String')) returns contents of q_min as a double


% --- Executes during object creation, after setting all properties.
function q_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function q_cur_Callback(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_cur_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_cur as text
%        str2double(get(hObject,'String')) returns contents of q_cur as a double


% --- Executes during object creation, after setting all properties.
function q_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function q_max_Callback(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_max_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_max as text
%        str2double(get(hObject,'String')) returns contents of q_max as a double


% --- Executes during object creation, after setting all properties.
function q_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function thetae_Callback(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetae, handles.thetae_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetae_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function thetae_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_min_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_min as text
%        str2double(get(hObject,'String')) returns contents of thetae_min as a double


% --- Executes during object creation, after setting all properties.
function thetae_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetae_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_cur_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);

% Hints: get(hObject,'String') returns contents of thetae_cur as text
%        str2double(get(hObject,'String')) returns contents of thetae_cur as a double


% --- Executes during object creation, after setting all properties.
function thetae_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetae_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_max_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_max as text
%        str2double(get(hObject,'String')) returns contents of thetae_max as a double


% --- Executes during object creation, after setting all properties.
function thetae_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function thetai0_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetai0, handles.thetai0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetai0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function thetai0_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_min_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_min as text
%        str2double(get(hObject,'String')) returns contents of thetai0_min as a double


% --- Executes during object creation, after setting all properties.
function thetai0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetai0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_cur_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_cur as text
%        str2double(get(hObject,'String')) returns contents of thetai0_cur as a double


% --- Executes during object creation, after setting all properties.
function thetai0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetai0_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_max_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_max as text
%        str2double(get(hObject,'String')) returns contents of thetai0_max as a double


% --- Executes during object creation, after setting all properties.
function thetai0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% VALORI DI DEFAULT
%
function Load_Defaults(handles)

global def;
global qsld thetaesld Cfsld Kiesld thetai0sld;

%ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider q
set(handles.q_min, 'Value', def(4));
set(handles.q_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.q_cur, 'Value', def(5));
set(handles.q_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.q_max, 'Value', def(6));
set(handles.q_max, 'String', num2str(def(6), '%.1f'));

set(handles.q, 'Min',   def(4)); 
set(handles.q, 'Value', def(5));
set(handles.q, 'Max',   def(6)); 
majorstep = qsld.stmax / (def(6)-def(4));
minorstep = qsld.stmin / (def(6)-def(4));
set(handles.q, 'SliderStep', [minorstep majorstep]);

% Caricamento delle variabili nel Workspace
evalin('base', ['input_params(''Ampiezza [W]'') = ' mat2str([get(handles.q, 'Min') get(handles.q, 'Value') get(handles.q, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider thetae
set(handles.thetae_min, 'Value', def(7));
set(handles.thetae_min, 'String', num2str(def(7),  '%.1f')); 
set(handles.thetae_cur, 'Value', def(8));
set(handles.thetae_cur, 'String', num2str(def(8), '%.2f')); 
set(handles.thetae_max, 'Value', def(9));
set(handles.thetae_max, 'String', num2str(def(9), '%.1f'));

set(handles.thetae, 'Min',   def(7)); 
set(handles.thetae, 'Value', def(8));
set(handles.thetae, 'Max',   def(9)); 
majorstep = thetaesld.stmax / (def(9)-def(7));
minorstep = thetaesld.stmin / (def(9)-def(7));
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

% slider Cf
set(handles.Cf_min, 'Value',  def(10));
set(handles.Cf_min, 'String', num2str(def(10), '%.3f'));
set(handles.Cf_cur, 'Value',  def(11));
set(handles.Cf_cur, 'String', num2str(def(11), '%.2f'));
set(handles.Cf_max, 'Value',  def(12));
set(handles.Cf_max, 'String', num2str(def(12), '%.1f'));

set(handles.Cf, 'Min',   def(10));
set(handles.Cf, 'Value', def(11));
set(handles.Cf, 'Max',   def(12));
majorstep = Cfsld.stmax / (def(12)-def(10));
minorstep = Cfsld.stmin / (def(12)-def(10));
set(handles.Cf, 'SliderStep', [minorstep majorstep]);

% slider Kie
set(handles.Kie_min, 'Value',  def(13));
set(handles.Kie_min, 'String', num2str(def(13), '%.1f'));
set(handles.Kie_cur, 'Value',  def(14));
set(handles.Kie_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Kie_max, 'Value',  def(15));
set(handles.Kie_max, 'String', num2str(def(15), '%.1f'));

set(handles.Kie, 'Min',   def(13));
set(handles.Kie, 'Value', def(14));
set(handles.Kie, 'Max',   def(15));
majorstep = Kiesld.stmax / (def(15)-def(13));
minorstep = Kiesld.stmin / (def(15)-def(13));
set(handles.Kie, 'SliderStep', [minorstep majorstep]);

% slider thetai0
set(handles.thetai0_min, 'Value', def(16));
set(handles.thetai0_min, 'String', num2str(def(16), '%.1f'));
set(handles.thetai0_cur, 'Value', def(17));
set(handles.thetai0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.thetai0_max, 'Value', def(18));
set(handles.thetai0_max, 'String', num2str(def(18), '%.1f'));

set(handles.thetai0, 'Min',   def(16)); 
set(handles.thetai0, 'Value', def(17));
set(handles.thetai0, 'Max',   def(18)); 
majorstep = thetai0sld.stmax / (def(18)-def(16));
minorstep = thetai0sld.stmin / (def(18)-def(16));
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiudo il Modello simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



%% GESTIONE CONFIGURAZIONI
% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end
% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
set(handles.Cf_min, 'Value',  Cf_min);
set(handles.Cf_min, 'String', num2str(Cf_min, '%.3f'));
set(handles.Cf_cur, 'Value',  Cf_cur);
set(handles.Cf_cur, 'String', num2str(Cf_cur, '%.2f'));
set(handles.Cf_max, 'Value',  Cf_max);
set(handles.Cf_max, 'String', num2str(Cf_max, '%.1f'));

set(handles.Cf, 'Min',   Cf_min);
set(handles.Cf, 'Value', Cf_cur);
set(handles.Cf, 'Max',   Cf_max);
majorstep =  Cfsld.stmax / (Cf_max-Cf_min);
minorstep =  Cfsld.stmin / (Cf_max-Cf_min);
set(handles.Cf, 'SliderStep', [minorstep majorstep]);

set(handles.Kie_min, 'Value',  Kie_min);
set(handles.Kie_min, 'String', num2str(Kie_min, '%.1f'));
set(handles.Kie_cur, 'Value',  Kie_cur);
set(handles.Kie_cur, 'String', num2str(Kie_cur, '%.2f'));
set(handles.Kie_max, 'Value',  Kie_max);
set(handles.Kie_max, 'String', num2str(Kie_max, '%.1f'));

set(handles.Kie, 'Min',   Kie_min);
set(handles.Kie, 'Value', Kie_cur);
set(handles.Kie, 'Max',   Kie_max);
majorstep =  Kiesld.stmax / (Kie_max-Kie_min);
minorstep =  Kiesld.stmin / (Kie_max-Kie_min);
set(handles.Kie, 'SliderStep', [minorstep majorstep]);

set(handles.thetai0_min, 'Value',  thetai0_min);
set(handles.thetai0_min, 'String', num2str(thetai0_min, '%.1f'));
set(handles.thetai0_cur, 'Value',  thetai0_cur);
set(handles.thetai0_cur, 'String', num2str(thetai0_cur, '%.2f'));
set(handles.thetai0_max, 'Value',  thetai0_max);
set(handles.thetai0_max, 'String', num2str(thetai0_max, '%.1f'));

set(handles.thetai0, 'Min',   thetai0_min);
set(handles.thetai0, 'Value', thetai0_cur);
set(handles.thetai0, 'Max',   thetai0_max);
majorstep = thetai0sld.stmax / (thetai0_max-thetai0_min);
minorstep = thetai0sld.stmin / (thetai0_max-thetai0_min);
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.thetae_min, 'Value',  thetae_min);
set(handles.thetae_min, 'String', num2str(thetae_min, '%.1f'));
set(handles.thetae_cur, 'Value',  thetae_cur);
set(handles.thetae_cur, 'String', num2str(thetae_cur, '%.2f'));
set(handles.thetae_max, 'Value',  thetae_max);
set(handles.thetae_max, 'String', num2str(thetae_max, '%.1f'));

set(handles.thetae, 'Min',   thetae_min);
set(handles.thetae, 'Value', thetae_cur);
set(handles.thetae, 'Max',   thetae_max);
majorstep = thetaesld.stmax / (thetae_max-thetae_min);
minorstep = thetaesld.stmin / (thetae_max-thetae_min);
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

set(handles.q_min, 'Value',  q_min);
set(handles.q_min, 'String', num2str(q_min, '%.1f'));
set(handles.q_cur, 'Value',  q_cur);
set(handles.q_cur, 'String', num2str(q_cur, '%.2f'));
set(handles.q_max, 'Value',  q_max);
set(handles.q_max, 'String', num2str(q_max, '%.1f'));

set(handles.q, 'Min',   q_min);
set(handles.q, 'Value', q_cur);
set(handles.q, 'Max',   q_max);
majorstep = qsld.stmax / (q_max-q_min);
minorstep = qsld.stmin / (q_max-q_min);
set(handles.q, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello capacit� termica forno
Cf_min = get(handles.Cf_min, 'Value');
Cf_cur = get(handles.Cf_cur, 'Value');
Cf_max = get(handles.Cf_max, 'Value');

% pannello conducibilit� termica
Kie_min = get(handles.Kie_min, 'Value');
Kie_cur = get(handles.Kie_cur, 'Value');
Kie_max = get(handles.Kie_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

q_min = get(handles.q_min, 'Value');
q_cur = get(handles.q_cur, 'Value');
q_max = get(handles.q_max, 'Value');

thetae_min = get(handles.thetae_min, 'Value');
thetae_cur = get(handles.thetae_cur, 'Value');
thetae_max = get(handles.thetae_max, 'Value');

% pannello stato iniziale
thetai0_min = get(handles.thetai0_min, 'Value');
thetai0_cur = get(handles.thetai0_cur, 'Value');
thetai0_max = get(handles.thetai0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'Cf_min = %f;\n', Cf_min);
fprintf(fid, 'Cf_cur = %f;\n', Cf_cur);
fprintf(fid, 'Cf_max = %f;\n', Cf_max);

fprintf(fid, 'Kie_min = %f;\n', Kie_min);
fprintf(fid, 'Kie_cur = %f;\n', Kie_cur);
fprintf(fid, 'Kie_max = %f;\n', Kie_max);

fprintf(fid, 'q_min = %f;\n', q_min);
fprintf(fid, 'q_cur = %f;\n', q_cur);
fprintf(fid, 'q_max = %f;\n', q_max);

fprintf(fid, 'thetae_min = %f;\n', thetae_min);
fprintf(fid, 'thetae_cur = %f;\n', thetae_cur);
fprintf(fid, 'thetae_max = %f;\n', thetae_max);

fprintf(fid, 'thetai0_min = %f;\n', thetai0_min);
fprintf(fid, 'thetai0_cur = %f;\n', thetai0_cur);
fprintf(fid, 'thetai0_max = %f;\n', thetai0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');
