%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                F O R N O   S E N Z A   P A R E T E   4.1                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% by Laura Masciadri (2008)
% by Dany Thach (2014)
% by Giorgio Codara (2016)
% by F. M. Marchese (2008-16)
%
% Tested under MatLab R2013b
%


%%
function varargout = Main_export(varargin)
% MAIN_EXPORT M-file for Main_export.fig
%      FORNO, by itself, creates a new FORNO or raises the existing
%      singleton*.
%
%      H = FORNO returns the handle to a new FORNO or the handle to
%      the existing singleton*.
%
%      FORNO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FORNO.M with the given input arguments.
%
%      FORNO('Property','Value',...) creates a new FORNO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Forno_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Forno_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to Run (singleton)".
%
%   IMPORTANTE: necessita l'installazione del Image Processing Toolbox
%   per la funzione imshow()

%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Forno

% Last Modified by GUIDE v2.5 01-Dec-2016 15:28:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Main_export_OpeningFcn, ...
                   'gui_OutputFcn',  @Main_export_OutputFcn, ...
                   'gui_LayoutFcn',  @Main_export_LayoutFcn, ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Forno is made visible.
function Main_export_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Forno (see VARARGIN)

% Pulizia cmd wnd
clc

% Pulizia workspace
evalin('base', 'clear');

global Sistemi;
Sistemi = 'Sistemi/*.m';

% Impostazioni di default
global def;
def = [
      1     % IngressoTipo
      1     % IngressoPar
      1     % Uscita
      0     % q_min
      100   % q, q_cur
      300   % q_max
      0     % thetae_min
      20    % thetae, thetae_cur
      35    % thetae_max
      1     % Cf_min
      2     % Cf, Cf_cur
      10    % Cf_max
      0     % Kie_min
      5     % Kie, Kie_cur
      10    % Kie_max
      0     % thetai0_min
      20    % thetai0, thetai0_cur
      300   % thetai0_max
      ];

% Par degli slider
global qsld thetaesld Cfsld Kiesld thetai0sld;

qsld.stmin = 1;
qsld.stmax = 10;
qsld.Llim = eps;
qsld.Hlim = +Inf;

thetaesld.stmin = 1;
thetaesld.stmax = 10;
thetaesld.Llim = eps;
thetaesld.Hlim = +Inf;

Cfsld.stmin = 0.1;
Cfsld.stmax = 1;
Cfsld.Llim = eps;
Cfsld.Hlim = +Inf;

Kiesld.stmin = 0.1;
Kiesld.stmax = 1;
Kiesld.Llim = eps;
Kiesld.Hlim = +Inf;

thetai0sld.stmin = 1;
thetai0sld.stmax = 10;
thetai0sld.Llim = eps;
thetai0sld.Hlim = +Inf;

evalin('base', 'input_params = containers.Map();');

Load_Defaults(handles);
  
% Choose default command line output for Forno
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% Preparazione menu elenco dei modelli trovati nella directory corrente
model_list = {};
file_list = dir(Sistemi);
for i = 1:size(file_list, 1)
    model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);
set(handles.ConfigLoadName, 'Value',  1);
set(handles.ConfigDelete, 'Enable', 'off');

% UIWAIT makes Main_export wait for user response (see UIRESUME)
% uiwait(handles.Dialog);

% --- Outputs from this function are returned to the command line.
function varargout = Main_export_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% FIGURE
% --- Executes during object creation, after setting all properties.
function Schema_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Schema (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Schema.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to  populate Schema


% --- Executes during object creation, after setting all properties.
function Modello_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modello (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
im = imread('Modello.jpg');
image(im);
axis off;
% Hint: place code in OpeningFcn to populate modello


%%  RUN
% --- Executes on button press in Run.
function Run_Callback(hObject, eventdata, handles)
% hObject    handle to Run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

global Uscita;

% Chiudo il Modello simulink senza salvare
bdclose(Uscita);

% Leggo la variabile di Uscita del sistema (y)
val1 = get(handles.Uscita,'Value');
switch val1
    case 1
        Uscita = ['Theta_i'];
end;

%Carico i valori dei parametri dal Workspace
ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');
if get(handles.IngressoTipo, 'Value') == 3
    frequenza = evalin('base', 'input_params(''Frequenza�[Hz]'')');
else
    frequenza = evalin('base', 'input_params(''Frequenza [Hz]'')');
end
dutycycle = evalin('base', 'input_params(''Duty Cycle [%]'')');

% Leggo i dati inseriti dall'utente
thetai0 = get(handles.thetai0, 'Value');
q = ampiezza(2);
thetae = get(handles.thetae, 'Value');
Cf = get(handles.Cf, 'Value');
Kie = get(handles.Kie, 'Value');

%controllo sui dati nulli
if Cf == 0, Cf = eps; end
if Kie == 0, Kie = 0.001; end

if frequenza(2) == 0, frequenza(2) = eps; end
if get(handles.IngressoTipo, 'Value') == 3, if frequenza(2) == 1000, frequenza(2) = 999.999; end, end
if dutycycle(2) < 0.0001, dutycycle(2) = 0.0001; end
if dutycycle(2) == 100, dutycycle(2) = 100-0.0001; end


% Calcolo costanti di tempo
F = [- Kie/Cf];
Tau = TimeConstantLTI(F);
% valore di default in caso di fallimento calcolo
if isnan(Tau), Tau = 0.1; end
Tau = min(1, Tau);  % sogliatura per evitare sim troppo lunghe


% Esporta tutte le variabili nel Workspace per permettere a Simulink
% di averne visibilit�
vars = {'thetai0', thetai0; 'q', q; 'thetae', thetae; 'Cf', Cf; 'Kie', Kie};
for i=1:size(vars,1)
    name = vars(i,1);
    value = vars(i,2);
    assignin('base', name{1}, value{1}); 
end

% Crea il Modello in Simulink
open_system(Uscita);

% Verifica e pulisce il segnale in input al sistema
h = find_system(Uscita,'Name','input');
if ( size(h)==[1,1])
    delete_line(Uscita, 'input/1', 'Gain/1');
    system_blocks = find_system(Uscita);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_line(Uscita, 'step1/1', 'input/1');
        delete_line(Uscita, 'step2/1', 'input/2');
    end
    delete_block([Uscita,'/input']);
    if numel(find(strcmp(system_blocks, [Uscita '/step1']))) > 0
        delete_block([Uscita,'/step1']);
        delete_block([Uscita,'/step2']);
    end
end


% Legge qual'� il nuovo segnale in input da simulare
val = get(handles.IngressoTipo,'Value');
switch val
  case 1  % step
      add_block('simulink/Sources/Step', [Uscita, '/input'], 'Time', num2str(0.5*Tau));
  case 2  % impulso
      add_block('simulink/Sources/Step', [Uscita, '/step1'], 'Time', num2str(0.5*Tau));
      add_block('simulink/Sources/Step', [Uscita, '/step2'], 'Time', num2str(0.5*Tau+min(0.25*Tau, 0.01)));
      add_block('simulink/Math Operations/Sum', [Uscita, '/input'], 'Inputs', '+-');
  case 3  % treno impulsi
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(frequenza(2)*0.1));
  case 4  % sinusoide
      add_block('simulink/Sources/Sine Wave', [Uscita, '/input'], 'Frequency', num2str(2*pi*frequenza(2)));
  case 5  % onda quadra
      add_block('simulink/Sources/Pulse Generator', [Uscita, '/input'], 'Period', num2str(1/frequenza(2)), 'PulseWidth', num2str(dutycycle(2)));
  case 6  % onda dente di sega
      add_block('simulink/Sources/Repeating Sequence', [Uscita, '/input'], 'rep_seq_t', ['[0 ' num2str(1/frequenza(2)) ']'], 'rep_seq_y', '[0 1]');
end;

% Modifico la durata della simulazione
switch val
    case {1, 2}
        set_param(Uscita, 'StopTime', num2str(5*Tau + 0.5*Tau));
    case {3, 4, 5, 6}
        set_param(Uscita, 'StopTime', num2str(5/frequenza(2)));
end

%Modifico lo sfondo e la posizione del blocco inserito
set_param([Uscita,'/input'], 'BackgroundColor','[0,206,206]');
if val~=2
    set_param([Uscita,'/input'], 'Position', '[40,90,105,120]');
else
    set_param([Uscita,'/step1'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step2'], 'BackgroundColor','[0,206,206]');
    set_param([Uscita,'/step1'], 'Position', '[20,45,85,75]');
    set_param([Uscita,'/step2'], 'Position', '[20,135,85,165]');
    set_param([Uscita,'/input'], 'Position', '[95,95,115,115]');
    add_line(Uscita, 'step1/1', 'input/1' , 'autorouting', 'on');
    add_line(Uscita, 'step2/1', 'input/2' , 'autorouting', 'on');
end
add_line(Uscita, 'input/1', 'Gain/1' , 'autorouting', 'on');

% Salva il sistema
save_system(Uscita);

% Avvia la simulazione
sim(Uscita);

ViewerAutoscale();



%% USCITA
% --- Executes on selection change in Uscita.
function Uscita_Callback(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns Uscita contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Uscita


% --- Executes during object creation, after setting all properties.
function Uscita_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Uscita (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% PUNTO D'EQUILIBRIO
% --- Executes on button press in Punto_Eq.
function Punto_Eq_Callback(hObject, eventdata, handles)
% hObject    handle to Punto_Eq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Riimpostazione menu ingressi
ampiezza = evalin('base', 'input_params(''Ampiezza [W]'')');
set(handles.IngressoTipo, 'Value', 1);
set(handles.IngressoPar, 'Value', 1);
set(handles.IngressoPar, 'Enable', 'off');

set(handles.q_min, 'Value',  ampiezza(1));
set(handles.q_min, 'String', num2str(ampiezza(1), '%.1f'));
set(handles.q_cur, 'Value',  ampiezza(2));
set(handles.q_cur, 'String', num2str(ampiezza(2), '%.2f'));
set(handles.q_max, 'Value',  ampiezza(3));
set(handles.q_max, 'String', num2str(ampiezza(3), '%.1f'));
set(handles.q, 'Min',   ampiezza(1));
set(handles.q, 'Value', ampiezza(2));
set(handles.q, 'Max',   ampiezza(3));

% Lettura dei dati inseriti dall'utente
thetae = get(handles.thetae, 'Value'); 
q  = get(handles.q,  'Value');
Cf  = get(handles.Cf,  'Value');
Kie  = get(handles.Kie,  'Value');

u = [q; thetae];

% Controllo sui dati (realta' fisica, singolarita' delle equazioni)
if Cf <= 0, Cf = eps; end

% Controllo dell'esistenza di un punto di equilibrio 
% none (esiste sempre nei sistemi lineari se esiste inv(F), 
% ossia se F e' non singolare, altrimenti ci sono infinite soluzioni)

% Scrittura dell'equazione dinamica
% Risoluzione dell'equazione 0 = F*x + G*u nell'incognita x (u noto)

% Preparazione matrici 
F = [- Kie/Cf];
G = [1/Cf, Kie/Cf];

[x, stb] = PuntoEquilibrioLTI2(F, G, u);
if isnan(stb)
  set(handles.Punto_Eq_txt, 'String', ...
    {'Non esiste uno stato di equilibrio con l''ingresso: ';...
    ['q = ', num2str(u(1), '%.2f'), ' W , thetae = ', num2str(u(2), '%.2f'), ' �C']});
  return
end


% Preparazione testo da visualizzare
str = sprintf('In presenza degli ingressi: q = %.2f W , thetae = %.2f �C', u(1), u(2));
str1 = sprintf('\nlo stato:');
str21 = sprintf('\n  thetai = %.2f �C', x(1));

% Stabilita'
switch stb
  case -1.2
    str3 = sprintf('\n e'' asintoticamente stabile (osc.)');
  case -1.1
    str3 = sprintf('\n e'' asintoticamente stabile');
  case  0.1
    str3 = sprintf('\n e'' semplicemente stabile');
  case  0.2
    str3 = sprintf('\n e'' semplicemente stabile (osc.)');
  case +1.1
    str3 = sprintf('\n e'' debolmente instabile');
  case +1.2
    str3 = sprintf('\n e'' debolmente instabile (osc.)');
  case +2.1
    str3 = sprintf('\n e'' fortemente instabile');
  case +2.2
    str3 = sprintf('\n e'' fortemente instabile (osc.)');
  otherwise
    str1 = sprintf('\nper lo stato:');
    str3 = sprintf('\n la stabilit� NON e'' determinabile');
end

endstr = '.';
str = strcat(str, str1, str21, str3, endstr);
set(handles.Punto_Eq_txt, 'String', str);



%% SLIDER Cf
% --- Executes on slider movement.
function Cf_Callback(hObject, eventdata, handles)
% hObject    handle to Cf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Cf, handles.Cf_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Cf_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Cf_min_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_min_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cf_min as text
%        str2double(get(hObject,'String')) returns contents of Cf_min as a double


% --- Executes during object creation, after setting all properties.
function Cf_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Cf_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_cur_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Cf_cur as text
%        str2double(get(hObject,'String')) returns contents of Cf_cur as a double


% --- Executes during object creation, after setting all properties.
function Cf_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Cf_max_Callback(hObject, eventdata, handles)
% hObject    handle to Cf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Cfsld;
Slider_max_Callback(handles, handles.Cf, handles.Cf_min, handles.Cf_cur, handles.Cf_max, Cfsld.stmin, Cfsld.stmax, Cfsld.Llim, Cfsld.Hlim);
% Hints: get(hObject,'String') returns contents of Cf_max as text
%        str2double(get(hObject,'String')) returns contents of Cf_max as a double


% --- Executes during object creation, after setting all properties.
function Cf_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Cf_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER Kie
% --- Executes on slider movement.
function Kie_Callback(hObject, eventdata, handles)
% hObject    handle to Kie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.Kie, handles.Kie_cur);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Kie_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function Kie_min_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_min_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);

% Hints: get(hObject,'String') returns contents of Kie_min as text
%        str2double(get(hObject,'String')) returns contents of Kie_min as a double


% --- Executes during object creation, after setting all properties.
function Kie_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Kie_cur_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_cur_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);

% Reset testo punto di equilibrio
set(handles.Punto_Eq_txt, 'String', '');
% Hints: get(hObject,'String') returns contents of Kie_cur as text
%        str2double(get(hObject,'String')) returns contents of Kie_cur as a double


% --- Executes during object creation, after setting all properties.
function Kie_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function Kie_max_Callback(hObject, eventdata, handles)
% hObject    handle to Kie_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Kiesld;
Slider_max_Callback(handles, handles.Kie, handles.Kie_min, handles.Kie_cur, handles.Kie_max, Kiesld.stmin, Kiesld.stmax, Kiesld.Llim, Kiesld.Hlim);
% Hints: get(hObject,'String') returns contents of Kie_max as text
%        str2double(get(hObject,'String')) returns contents of Kie_max as a double


% --- Executes during object creation, after setting all properties.
function Kie_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Kie_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% INGRESSO TIPO
% --- Executes on selection change in IngressoTipo.
function IngressoTipo_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns IngressoTipo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoTipo

global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);


id_ingresso = get(hObject, 'Value');

new_String = cell(1);
new_String{1} = 'Ampiezza [W]';
par = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch id_ingresso
    case {1, 2}
        set(handles.IngressoPar, 'Value', 1);
        set(handles.IngressoPar, 'Enable', 'off');
    case 3
        new_String{2} = 'Frequenza�[Hz]';
        set(handles.IngressoPar, 'Value', 2);
    case 5
        new_String{2} = 'Frequenza [Hz]';
        new_String{3} = 'Duty Cycle [%]';
        set(handles.IngressoPar, 'Value', 3);
    case {4, 6}
        new_String{2} = 'Frequenza [Hz]';
        set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', new_String);
set(handles.IngressoTipo, 'Value', id_ingresso);
set(handles.IngressoPar, 'Value', 1);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q, 'Min', values(1));
set(handles.q, 'Value', values(2));
set(handles.q, 'Max', values(3));
set(handles.q_min, 'Value', values(1));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_cur, 'Value', values(2));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_max, 'Value', values(3));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));

% --- Executes during object creation, after setting all properties.
function IngressoTipo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoTipo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in IngressoPar.
function IngressoPar_Callback(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Segnala la modifica
global modified;
modified = true;
list = get(handles.ConfigLoadName, 'String');
index = get(handles.ConfigLoadName, 'Value');
name = [list{index}, '_'];
set(handles.ConfigSaveName, 'String', name);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);

set(handles.q, 'Min', values(1));
set(handles.q, 'Value', values(2));
set(handles.q, 'Max', values(3));
set(handles.q_min, 'String', num2str(values(1), '%.1f'));
set(handles.q_min, 'Value', values(1));
set(handles.q_cur, 'String', num2str(values(2), '%.1f'));
set(handles.q_cur, 'Value', values(2));
set(handles.q_max, 'String', num2str(values(3), '%.1f'));
set(handles.q_max, 'Value', values(3));
% Hints: contents = cellstr(get(hObject,'String')) returns IngressoPar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from IngressoPar


% --- Executes during object creation, after setting all properties.
function IngressoPar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IngressoPar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%
% --- Executes on slider movement.
function q_Callback(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.q, handles.q_cur);

val = get(handles.q, 'Value');
parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = evalin('base', ['input_params(''' param_name ''')']);
values(2) = val;
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function q_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function q_min_Callback(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_min_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_min as text
%        str2double(get(hObject,'String')) returns contents of q_min as a double


% --- Executes during object creation, after setting all properties.
function q_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function q_cur_Callback(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_cur_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_cur as text
%        str2double(get(hObject,'String')) returns contents of q_cur as a double


% --- Executes during object creation, after setting all properties.
function q_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function q_max_Callback(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global qsld;
Slider_max_Callback(handles, handles.q, handles.q_min, handles.q_cur, handles.q_max, qsld.stmin, qsld.stmax, qsld.Llim, qsld.Hlim);

parametri = get(handles.IngressoPar, 'String');
param_name = parametri{get(handles.IngressoPar, 'Value')};
values = [get(handles.q, 'Min') get(handles.q, 'Value'), get(handles.q, 'Max')];
evalin('base', ['input_params(''' param_name ''') = ' mat2str(values) ';']);
% Hints: get(hObject,'String') returns contents of q_max as text
%        str2double(get(hObject,'String')) returns contents of q_max as a double


% --- Executes during object creation, after setting all properties.
function q_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
% --- Executes on slider movement.
function thetae_Callback(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetae, handles.thetae_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetae_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function thetae_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_min_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_min as text
%        str2double(get(hObject,'String')) returns contents of thetae_min as a double


% --- Executes during object creation, after setting all properties.
function thetae_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetae_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_cur_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);

% Hints: get(hObject,'String') returns contents of thetae_cur as text
%        str2double(get(hObject,'String')) returns contents of thetae_cur as a double


% --- Executes during object creation, after setting all properties.
function thetae_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetae_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetaesld;
Slider_max_Callback(handles, handles.thetae, handles.thetae_min, handles.thetae_cur, handles.thetae_max, thetaesld.stmin, thetaesld.stmax, thetaesld.Llim, thetaesld.Hlim);
% Hints: get(hObject,'String') returns contents of thetae_max as text
%        str2double(get(hObject,'String')) returns contents of thetae_max as a double


% --- Executes during object creation, after setting all properties.
function thetae_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetae_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%% SLIDER STATO INIZIALE
% --- Executes on slider movement.
function thetai0_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Slider_sld_Callback(handles, handles.thetai0, handles.thetai0_cur);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function thetai0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%%
function thetai0_min_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_min_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_min as text
%        str2double(get(hObject,'String')) returns contents of thetai0_min as a double


% --- Executes during object creation, after setting all properties.
function thetai0_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetai0_cur_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_cur_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_cur as text
%        str2double(get(hObject,'String')) returns contents of thetai0_cur as a double


% --- Executes during object creation, after setting all properties.
function thetai0_cur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_cur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%
function thetai0_max_Callback(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thetai0sld;
Slider_max_Callback(handles, handles.thetai0, handles.thetai0_min, handles.thetai0_cur, handles.thetai0_max, thetai0sld.stmin, thetai0sld.stmax, thetai0sld.Llim, thetai0sld.Hlim);
% Hints: get(hObject,'String') returns contents of thetai0_max as text
%        str2double(get(hObject,'String')) returns contents of thetai0_max as a double


% --- Executes during object creation, after setting all properties.
function thetai0_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thetai0_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% VALORI DI DEFAULT
%
function Load_Defaults(handles)

global def;
global qsld thetaesld Cfsld Kiesld thetai0sld;

%ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
IngressoParval = get(handles.IngressoPar, 'Value');
set(handles.IngressoPar, 'Enable', 'on');

switch def(1)
  case {1, 2}
    set(handles.IngressoPar, 'Value', 1);
    set(handles.IngressoPar, 'Enable', 'off');
  case 3
    IngressoParstr{2} = 'Frequenza�[Hz]';
    set(handles.IngressoPar, 'Value', 2);
  case 5
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    IngressoParstr{3} = 'Duty Cycle [%]'; 
    set(handles.IngressoPar, 'Value', 3);
  case {4, 6}
    IngressoParstr{2} = 'Frequenza [Hz]'; 
    set(handles.IngressoPar, 'Value', 2);
end

set(handles.IngressoPar, 'String', IngressoParstr);
set(handles.IngressoTipo, 'Value', def(1));
set(handles.IngressoPar, 'Value', def(2));

% Uscita
set(handles.Uscita, 'Value', def(3));

% slider q
set(handles.q_min, 'Value', def(4));
set(handles.q_min, 'String', num2str(def(4),  '%.1f')); 
set(handles.q_cur, 'Value', def(5));
set(handles.q_cur, 'String', num2str(def(5), '%.2f')); 
set(handles.q_max, 'Value', def(6));
set(handles.q_max, 'String', num2str(def(6), '%.1f'));

set(handles.q, 'Min',   def(4)); 
set(handles.q, 'Value', def(5));
set(handles.q, 'Max',   def(6)); 
majorstep = qsld.stmax / (def(6)-def(4));
minorstep = qsld.stmin / (def(6)-def(4));
set(handles.q, 'SliderStep', [minorstep majorstep]);

% Caricamento delle variabili nel Workspace
evalin('base', ['input_params(''Ampiezza [W]'') = ' mat2str([get(handles.q, 'Min') get(handles.q, 'Value') get(handles.q, 'Max')]) ';']);
evalin('base', 'input_params(''Frequenza [Hz]'') = [0 100 10000];'); % Frequenza dei segnali periodici tranne il treno di impulsi
evalin('base', 'input_params(''Frequenza�[Hz]'') = [0 100 500];'); % Frequenza del treno di impulsi
evalin('base', 'input_params(''Duty Cycle [%]'') = [0 50 100];');

% slider thetae
set(handles.thetae_min, 'Value', def(7));
set(handles.thetae_min, 'String', num2str(def(7),  '%.1f')); 
set(handles.thetae_cur, 'Value', def(8));
set(handles.thetae_cur, 'String', num2str(def(8), '%.2f')); 
set(handles.thetae_max, 'Value', def(9));
set(handles.thetae_max, 'String', num2str(def(9), '%.1f'));

set(handles.thetae, 'Min',   def(7)); 
set(handles.thetae, 'Value', def(8));
set(handles.thetae, 'Max',   def(9)); 
majorstep = thetaesld.stmax / (def(9)-def(7));
minorstep = thetaesld.stmin / (def(9)-def(7));
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

% slider Cf
set(handles.Cf_min, 'Value',  def(10));
set(handles.Cf_min, 'String', num2str(def(10), '%.3f'));
set(handles.Cf_cur, 'Value',  def(11));
set(handles.Cf_cur, 'String', num2str(def(11), '%.2f'));
set(handles.Cf_max, 'Value',  def(12));
set(handles.Cf_max, 'String', num2str(def(12), '%.1f'));

set(handles.Cf, 'Min',   def(10));
set(handles.Cf, 'Value', def(11));
set(handles.Cf, 'Max',   def(12));
majorstep = Cfsld.stmax / (def(12)-def(10));
minorstep = Cfsld.stmin / (def(12)-def(10));
set(handles.Cf, 'SliderStep', [minorstep majorstep]);

% slider Kie
set(handles.Kie_min, 'Value',  def(13));
set(handles.Kie_min, 'String', num2str(def(13), '%.1f'));
set(handles.Kie_cur, 'Value',  def(14));
set(handles.Kie_cur, 'String', num2str(def(14), '%.2f'));
set(handles.Kie_max, 'Value',  def(15));
set(handles.Kie_max, 'String', num2str(def(15), '%.1f'));

set(handles.Kie, 'Min',   def(13));
set(handles.Kie, 'Value', def(14));
set(handles.Kie, 'Max',   def(15));
majorstep = Kiesld.stmax / (def(15)-def(13));
minorstep = Kiesld.stmin / (def(15)-def(13));
set(handles.Kie, 'SliderStep', [minorstep majorstep]);

% slider thetai0
set(handles.thetai0_min, 'Value', def(16));
set(handles.thetai0_min, 'String', num2str(def(16), '%.1f'));
set(handles.thetai0_cur, 'Value', def(17));
set(handles.thetai0_cur, 'String', num2str(def(17), '%.2f'));
set(handles.thetai0_max, 'Value', def(18));
set(handles.thetai0_max, 'String', num2str(def(18), '%.1f'));

set(handles.thetai0, 'Min',   def(16)); 
set(handles.thetai0, 'Value', def(17));
set(handles.thetai0, 'Max',   def(18)); 
majorstep = thetai0sld.stmax / (def(18)-def(16));
minorstep = thetai0sld.stmin / (def(18)-def(16));
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

set(handles.ConfigSaveName, 'String', 'nomefile');



% --- Executes when user attempts to close Dialog.
function Dialog_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Dialog (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc

% Chiudo il Modello simulink senza salvare
bdclose('all');

% Hint: delete(hObject) closes the figure
delete(hObject);

% Pulizia workspace
evalin('base', 'clear');



%% GESTIONE CONFIGURAZIONI
% --- Executes on selection change in ConfigLoadName.
function ConfigLoadName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.ConfigLoadName, 'Value') == 1
    set(handles.ConfigDelete, 'Enable', 'off');
else
    set(handles.ConfigDelete, 'Enable', 'on');
end
% Hints: contents = cellstr(get(hObject,'String')) returns ConfigLoadName contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ConfigLoadName


% --- Executes during object creation, after setting all properties.
function ConfigLoadName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigLoadName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigLoad.
function ConfigLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = string_list{val};

% Controllo se la configurazione corrente sia stata modificata
global modified;
if modified == true
  choice = questdlg('Do you want to save this configuration?', 'Config Saving', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      % ConfigSave della configurazione corrente
      ConfigSave_Callback(hObject, [], handles);
    case 'No'
      % no action
  end
end
modified = false;
fclose('all');

% Ricerca nella lista della posizione della config
% che l'utente voleva caricare prima del salvataggio
string_list = get(handles.ConfigLoadName, 'String');
for i = 1 : size(string_list, 1)
  if strcmp(string_list{i}, name) == 1
    val = i;
    % Selezione nel menu della configurazione da caricare
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

% Se tale configurazione e' 'Default', si chiama la funzione Load_Defaults
if val == 1
  Load_Defaults(handles);
  return;
end


if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
  return;
end
cd('Sistemi');

set(handles.ConfigSaveName, 'String', name);

% Lettura dei dati e creazione delle variabili
text = fileread([name, '.m']);
eval(text);

fclose('all');

cd('..');

% Aggiornamento degli slider
set(handles.Cf_min, 'Value',  Cf_min);
set(handles.Cf_min, 'String', num2str(Cf_min, '%.3f'));
set(handles.Cf_cur, 'Value',  Cf_cur);
set(handles.Cf_cur, 'String', num2str(Cf_cur, '%.2f'));
set(handles.Cf_max, 'Value',  Cf_max);
set(handles.Cf_max, 'String', num2str(Cf_max, '%.1f'));

set(handles.Cf, 'Min',   Cf_min);
set(handles.Cf, 'Value', Cf_cur);
set(handles.Cf, 'Max',   Cf_max);
majorstep =  Cfsld.stmax / (Cf_max-Cf_min);
minorstep =  Cfsld.stmin / (Cf_max-Cf_min);
set(handles.Cf, 'SliderStep', [minorstep majorstep]);

set(handles.Kie_min, 'Value',  Kie_min);
set(handles.Kie_min, 'String', num2str(Kie_min, '%.1f'));
set(handles.Kie_cur, 'Value',  Kie_cur);
set(handles.Kie_cur, 'String', num2str(Kie_cur, '%.2f'));
set(handles.Kie_max, 'Value',  Kie_max);
set(handles.Kie_max, 'String', num2str(Kie_max, '%.1f'));

set(handles.Kie, 'Min',   Kie_min);
set(handles.Kie, 'Value', Kie_cur);
set(handles.Kie, 'Max',   Kie_max);
majorstep =  Kiesld.stmax / (Kie_max-Kie_min);
minorstep =  Kiesld.stmin / (Kie_max-Kie_min);
set(handles.Kie, 'SliderStep', [minorstep majorstep]);

set(handles.thetai0_min, 'Value',  thetai0_min);
set(handles.thetai0_min, 'String', num2str(thetai0_min, '%.1f'));
set(handles.thetai0_cur, 'Value',  thetai0_cur);
set(handles.thetai0_cur, 'String', num2str(thetai0_cur, '%.2f'));
set(handles.thetai0_max, 'Value',  thetai0_max);
set(handles.thetai0_max, 'String', num2str(thetai0_max, '%.1f'));

set(handles.thetai0, 'Min',   thetai0_min);
set(handles.thetai0, 'Value', thetai0_cur);
set(handles.thetai0, 'Max',   thetai0_max);
majorstep = thetai0sld.stmax / (thetai0_max-thetai0_min);
minorstep = thetai0sld.stmin / (thetai0_max-thetai0_min);
set(handles.thetai0, 'SliderStep', [minorstep majorstep]);

% ingressi
IngressoParstr = cell(1);
IngressoParstr{1} = 'Ampiezza [W]';
set(handles.IngressoPar, 'Enable', 'on');
switch IngressoTipo
  case {1, 2}
      set(handles.IngressoPar, 'Enable', 'off');
  case 3
      IngressoParstr{2} = 'Frequenza�[Hz]';
  case 5
      IngressoParstr{2} = 'Frequenza [Hz]';
      IngressoParstr{3} = 'Duty Cycle [%]';
  case {4, 6}
      IngressoParstr{2} = 'Frequenza [Hz]';
end

set(handles.IngressoPar,  'String', IngressoParstr);
set(handles.IngressoTipo, 'Value',  IngressoTipo);
set(handles.IngressoPar,  'Value',  IngressoPar);

set(handles.thetae_min, 'Value',  thetae_min);
set(handles.thetae_min, 'String', num2str(thetae_min, '%.1f'));
set(handles.thetae_cur, 'Value',  thetae_cur);
set(handles.thetae_cur, 'String', num2str(thetae_cur, '%.2f'));
set(handles.thetae_max, 'Value',  thetae_max);
set(handles.thetae_max, 'String', num2str(thetae_max, '%.1f'));

set(handles.thetae, 'Min',   thetae_min);
set(handles.thetae, 'Value', thetae_cur);
set(handles.thetae, 'Max',   thetae_max);
majorstep = thetaesld.stmax / (thetae_max-thetae_min);
minorstep = thetaesld.stmin / (thetae_max-thetae_min);
set(handles.thetae, 'SliderStep', [minorstep majorstep]);

set(handles.q_min, 'Value',  q_min);
set(handles.q_min, 'String', num2str(q_min, '%.1f'));
set(handles.q_cur, 'Value',  q_cur);
set(handles.q_cur, 'String', num2str(q_cur, '%.2f'));
set(handles.q_max, 'Value',  q_max);
set(handles.q_max, 'String', num2str(q_max, '%.1f'));

set(handles.q, 'Min',   q_min);
set(handles.q, 'Value', q_cur);
set(handles.q, 'Max',   q_max);
majorstep = qsld.stmax / (q_max-q_min);
minorstep = qsld.stmin / (q_max-q_min);
set(handles.q, 'SliderStep', [minorstep majorstep]);

set(handles.Uscita, 'Value', Uscita);


% --- Executes on button press in ConfigDelete.
function ConfigDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(handles.ConfigLoadName, 'Value');
string_list = get(handles.ConfigLoadName, 'String');
name = strcat(string_list{val}, '.m');
choice = questdlg(['Delete', ' ', name, '?'], 'Config Deletion', 'Yes', 'No', 'No');
switch choice
  case 'Yes'
    if exist('Sistemi', 'dir') == 0, return; end
    cd('Sistemi');
    delete(name);

    % Aggiunge tutti i nomi delle config trovati nella directory corrente 
    % in una struttura temporanea (rimuovendone l'estensione .m)
    file_list = dir('*.m');
    model_list = {};
    for i = 1 : size(file_list, 1)
        model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
    end
    model_list = vertcat([cellstr('Default')], model_list);

    % Aggiornamento della lista dei nomi delle config nel menu 
    set(handles.ConfigLoadName, 'String', model_list);
    set(handles.ConfigLoadName, 'Value', 1);   
    cd('..');
    set(handles.ConfigDelete, 'Enable', 'off');
    Load_Defaults(handles);
  case 'No'
    % no action
end



function ConfigSaveName_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ConfigSaveName as text
%        str2double(get(hObject,'String')) returns contents of ConfigSaveName as a double


% --- Executes during object creation, after setting all properties.
function ConfigSaveName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ConfigSaveName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ConfigSave.
function ConfigSave_Callback(hObject, eventdata, handles)
% hObject    handle to ConfigSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modified;

name = get(handles.ConfigSaveName, 'String');

% Gestione nomefile vuoto
if isempty(name)
  errordlg('Empty file name!');
  return;
end

if exist('Sistemi', 'dir') == 0
  mkdir('Sistemi');
end
cd('Sistemi'); 

% Controllo se il nome della configurazione da salvare sia gia' presente
namem = [name, '.m'];
if exist(namem, 'file') == 2
  choice = questdlg('Overwrite file?', 'File Overwrite', 'Yes', 'No', 'No');
  switch choice
    case 'Yes'
      delete(namem);
    case 'No'
      cd('..');
      return
  end
end

modified = false;


% Salvataggio di tutti i parametri
% pannello capacit� termica forno
Cf_min = get(handles.Cf_min, 'Value');
Cf_cur = get(handles.Cf_cur, 'Value');
Cf_max = get(handles.Cf_max, 'Value');

% pannello conducibilit� termica
Kie_min = get(handles.Kie_min, 'Value');
Kie_cur = get(handles.Kie_cur, 'Value');
Kie_max = get(handles.Kie_max, 'Value');

% pannello ingressi
IngressoTipo = get(handles.IngressoTipo, 'Value');
IngressoPar = get(handles.IngressoPar, 'Value');

q_min = get(handles.q_min, 'Value');
q_cur = get(handles.q_cur, 'Value');
q_max = get(handles.q_max, 'Value');

thetae_min = get(handles.thetae_min, 'Value');
thetae_cur = get(handles.thetae_cur, 'Value');
thetae_max = get(handles.thetae_max, 'Value');

% pannello stato iniziale
thetai0_min = get(handles.thetai0_min, 'Value');
thetai0_cur = get(handles.thetai0_cur, 'Value');
thetai0_max = get(handles.thetai0_max, 'Value');

Uscita = get(handles.Uscita, 'Value');

% Salvataggio parametri su file
fid = fopen(namem, 'w');
fprintf(fid, 'IngressoTipo = %f;\n', IngressoTipo);
fprintf(fid, 'IngressoPar = %f;\n', IngressoPar);
fprintf(fid, 'Uscita = %f;\n', Uscita);

fprintf(fid, 'Cf_min = %f;\n', Cf_min);
fprintf(fid, 'Cf_cur = %f;\n', Cf_cur);
fprintf(fid, 'Cf_max = %f;\n', Cf_max);

fprintf(fid, 'Kie_min = %f;\n', Kie_min);
fprintf(fid, 'Kie_cur = %f;\n', Kie_cur);
fprintf(fid, 'Kie_max = %f;\n', Kie_max);

fprintf(fid, 'q_min = %f;\n', q_min);
fprintf(fid, 'q_cur = %f;\n', q_cur);
fprintf(fid, 'q_max = %f;\n', q_max);

fprintf(fid, 'thetae_min = %f;\n', thetae_min);
fprintf(fid, 'thetae_cur = %f;\n', thetae_cur);
fprintf(fid, 'thetae_max = %f;\n', thetae_max);

fprintf(fid, 'thetai0_min = %f;\n', thetai0_min);
fprintf(fid, 'thetai0_cur = %f;\n', thetai0_cur);
fprintf(fid, 'thetai0_max = %f;\n', thetai0_max);

fclose(fid);
fclose('all');
clearvars fid;
cd('..');

% Aggiornamento del menu della lista dei modelli
% Aggiunge tutti i nomi dei modelli trovati nella directory corrente in
% una struttura temporanea (rimuovendone l'estensione .m)
global Sistemi;
model_list = {};
file_list = dir(Sistemi);
for i = 1 : size(file_list, 1)
  model_list(i, 1) = cellstr(file_list(i).name(1:size(file_list(i).name, 2)-2));
end
model_list = vertcat([cellstr('Default')], model_list);
set(handles.ConfigLoadName, 'String', model_list);

% Attiva la voce di menu della config attuale
for i = 1 : size(model_list, 1)
  if strcmp(model_list(i, 1), name) == 1
    set(handles.ConfigLoadName, 'Value', i);
    break;
  end
end

set(handles.ConfigDelete, 'Enable', 'on');


% --- Creates and returns a handle to the GUI figure. 
function h1 = Main_export_LayoutFcn(policy)
% policy - create a new figure or use a singleton. 'new' or 'reuse'.

persistent hsingleton;
if strcmpi(policy, 'reuse') & ishandle(hsingleton)
    h1 = hsingleton;
    return;
end

appdata = [];
appdata.GUIDEOptions = struct(...
    'active_h', [], ...
    'taginfo', struct(...
    'figure', 2, ...
    'axes', 4, ...
    'uipanel', 16, ...
    'slider', 7, ...
    'edit', 20, ...
    'popupmenu', 5, ...
    'pushbutton', 6, ...
    'text', 2), ...
    'override', 1, ...
    'release', 13, ...
    'resize', 'none', ...
    'accessibility', 'callback', ...
    'mfile', 1, ...
    'callbacks', 1, ...
    'singleton', 1, ...
    'syscolorfig', 1, ...
    'blocking', 0, ...
    'lastSavedFile', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\02 - Forno.4.1\Main_export.m', ...
    'lastFilename', 'D:\Marchese\Documents\Didattica\DISCO\Robotica\Materiale\MatLab\Esempi\Esempi FM 2016.Mag\02 - Forno.4.1\Main.fig');
appdata.lastValidTag = 'Dialog';
appdata.GUIDELayoutEditor = [];
appdata.initTags = struct(...
    'handle', [], ...
    'tag', 'Dialog');

h1 = figure(...
'Units','characters',...
'PaperUnits',get(0,'defaultfigurePaperUnits'),...
'CloseRequestFcn',@(hObject,eventdata)Main_export('Dialog_CloseRequestFcn',hObject,eventdata,guidata(hObject)),...
'Color',[0.941176470588235 0.941176470588235 0.941176470588235],...
'Colormap',[0 0 0.5625;0 0 0.625;0 0 0.6875;0 0 0.75;0 0 0.8125;0 0 0.875;0 0 0.9375;0 0 1;0 0.0625 1;0 0.125 1;0 0.1875 1;0 0.25 1;0 0.3125 1;0 0.375 1;0 0.4375 1;0 0.5 1;0 0.5625 1;0 0.625 1;0 0.6875 1;0 0.75 1;0 0.8125 1;0 0.875 1;0 0.9375 1;0 1 1;0.0625 1 1;0.125 1 0.9375;0.1875 1 0.875;0.25 1 0.8125;0.3125 1 0.75;0.375 1 0.6875;0.4375 1 0.625;0.5 1 0.5625;0.5625 1 0.5;0.625 1 0.4375;0.6875 1 0.375;0.75 1 0.3125;0.8125 1 0.25;0.875 1 0.1875;0.9375 1 0.125;1 1 0.0625;1 1 0;1 0.9375 0;1 0.875 0;1 0.8125 0;1 0.75 0;1 0.6875 0;1 0.625 0;1 0.5625 0;1 0.5 0;1 0.4375 0;1 0.375 0;1 0.3125 0;1 0.25 0;1 0.1875 0;1 0.125 0;1 0.0625 0;1 0 0;0.9375 0 0;0.875 0 0;0.8125 0 0;0.75 0 0;0.6875 0 0;0.625 0 0;0.5625 0 0],...
'IntegerHandle','off',...
'InvertHardcopy',get(0,'defaultfigureInvertHardcopy'),...
'MenuBar','none',...
'Name','FORNO',...
'NumberTitle','off',...
'PaperPosition',get(0,'defaultfigurePaperPosition'),...
'PaperSize',get(0,'defaultfigurePaperSize'),...
'PaperType',get(0,'defaultfigurePaperType'),...
'Position',[103.571428571429 34.65 164.571428571429 27.1],...
'Resize','off',...
'HandleVisibility','callback',...
'UserData',[],...
'Tag','Dialog',...
'Visible','on',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel1';

h2 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Modello Matematico',...
'Clipping','on',...
'Position',[1.57142857142857 1.45 43 6.1],...
'Tag','uipanel1',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Modello';

h3 = axes(...
'Parent',h2,...
'Units','characters',...
'Position',[1.28571428571429 0.65 40 4.15],...
'Box','on',...
'CameraPosition',[216.5 63 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[15.025974025974 12.1700871691851 10.9805194805195 8.29778670626256],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 432.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 125.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Modello_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Modello',...
'Visible','off');

h4 = get(h3,'xlabel');

set(h4,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[215.728571428571 166.915662650602 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h5 = get(h3,'ylabel');

set(h5,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-60.4428571428571 64.5060240963854 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h6 = get(h3,'zlabel');

set(h6,...
'Parent',h3,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-34.2142857142857 -622.240963855422 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel6';

h7 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabili di Ingresso',...
'Clipping','on',...
'Position',[117.428571428571 8.9 46 14.55],...
'Tag','uipanel6',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel7';

h8 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Temperatura esterna thetae [�C]',...
'Clipping','on',...
'Position',[1.71428571428571 0.5 42 4.55],...
'Tag','uipanel7',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'thetae';

h9 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('thetae_Callback',hObject,eventdata,guidata(hObject)),...
'Max',35,...
'Position',[1.57142857142857 2.15 38 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae');

appdata = [];
appdata.lastValidTag = 'thetae_cur';

h10 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.6 9 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_cur');

appdata = [];
appdata.lastValidTag = 'thetae_max';

h11 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.5714285714286 0.6 9 1],...
'String','35.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_max');

appdata = [];
appdata.lastValidTag = 'thetae_min';

h12 = uicontrol(...
'Parent',h8,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetae_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.6 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetae_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetae_min');

appdata = [];
appdata.lastValidTag = 'uipanel8';

h13 = uipanel(...
'Parent',h7,...
'Units','characters',...
'Title','Flusso di calore q [W]',...
'Clipping','on',...
'Position',[1.57142857142857 5.25 42 8.05],...
'Tag','uipanel8',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'q';

h14 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('q_Callback',hObject,eventdata,guidata(hObject)),...
'Max',300,...
'Position',[1.57142857142857 2.3 38 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',100,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q');

appdata = [];
appdata.lastValidTag = 'q_cur';

h15 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.699999999999999 9 1],...
'String','100.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_cur');

appdata = [];
appdata.lastValidTag = 'q_max';

h16 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.5714285714286 0.699999999999999 9 1],...
'String','300.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_max');

appdata = [];
appdata.lastValidTag = 'q_min';

h17 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('q_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.699999999999999 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('q_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','q_min');

appdata = [];
appdata.lastValidTag = 'IngressoTipo';

h18 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoTipo_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[1.57142857142857 5.5 38 1.3],...
'String',{  'Step'; 'Impulso'; 'Treno di impulsi'; 'Sinusoide'; 'Onda quadra'; 'Onda a dente di sega' },...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoTipo_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoTipo');

appdata = [];
appdata.lastValidTag = 'IngressoPar';

h19 = uicontrol(...
'Parent',h13,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('IngressoPar_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[1.57142857142857 3.8 38 1.3],...
'String','Ampiezza [W]',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('IngressoPar_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','IngressoPar');

appdata = [];
appdata.lastValidTag = 'uipanel9';

h20 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Parametri',...
'Clipping','on',...
'Position',[69.4285714285714 11.9 46 11.65],...
'Tag','uipanel9',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel10';

h21 = uipanel(...
'Parent',h20,...
'Units','characters',...
'Title','Capacit� termica del forno Cf [J/�C]',...
'Clipping','on',...
'Position',[2 5.75 42 4.65],...
'Tag','uipanel10',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Cf';

h22 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Cf_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Min',1,...
'Position',[1.85714285714286 2.2 38 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',2,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cf_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cf');

appdata = [];
appdata.lastValidTag = 'Cf_cur';

h23 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cf_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16.2857142857143 0.55 9 1],...
'String','2.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cf_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cf_cur');

appdata = [];
appdata.lastValidTag = 'Cf_max';

h24 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cf_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.8571428571428 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cf_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cf_max');

appdata = [];
appdata.lastValidTag = 'Cf_min';

h25 = uicontrol(...
'Parent',h21,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Cf_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.85714285714286 0.55 9 1],...
'String','1.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Cf_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Cf_min');

appdata = [];
appdata.lastValidTag = 'uipanel11';

h26 = uipanel(...
'Parent',h20,...
'Units','characters',...
'ShadowColor',[0.501960784313725 0.501960784313725 0.501960784313725],...
'Title','Conducibilit� termica Kie [W/�C]',...
'Clipping','on',...
'Position',[2 0.7 42 4.55],...
'Tag','uipanel11',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Kie';

h27 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('Kie_Callback',hObject,eventdata,guidata(hObject)),...
'Max',10,...
'Position',[1.57142857142857 2.15 38 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',5,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kie_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kie');

appdata = [];
appdata.lastValidTag = 'Kie_cur';

h28 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kie_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.55 9 1],...
'String','5.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kie_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kie_cur');

appdata = [];
appdata.lastValidTag = 'Kie_max';

h29 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kie_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.5714285714286 0.55 9 1],...
'String','10.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kie_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kie_max');

appdata = [];
appdata.lastValidTag = 'Kie_min';

h30 = uicontrol(...
'Parent',h26,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Kie_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Kie_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Kie_min');

appdata = [];
appdata.lastValidTag = 'Run';

h31 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Run_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[130.857142857143 0.45 23.4285714285714 1.65],...
'String','Run',...
'Tag','Run',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel12';

h32 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Variabile di Uscita',...
'Clipping','on',...
'Position',[69.4285714285714 8.2 46 3.35],...
'Tag','uipanel12',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Uscita';

h33 = uicontrol(...
'Parent',h32,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('Uscita_Callback',hObject,eventdata,guidata(hObject)),...
'Enable','off',...
'Position',[2 0.6 41 1.3],...
'String','Thetai',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Uscita_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Uscita');

appdata = [];
appdata.lastValidTag = 'Punto_Eq';

h34 = uicontrol(...
'Parent',h1,...
'Units','characters',...
'BackgroundColor',[1 0 0],...
'Callback',@(hObject,eventdata)Main_export('Punto_Eq_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'FontWeight','bold',...
'ForegroundColor',[1 1 1],...
'Position',[75.4285714285714 1.05 24.4285714285714 1.5],...
'String','Punto di Equilibrio',...
'Tag','Punto_Eq',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel13';

h35 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Punto di Equilibrio',...
'Clipping','on',...
'Position',[62.2857142857143 2.75 53.1428571428571 4.95],...
'Tag','uipanel13',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Punto_Eq_txt';

h36 = uicontrol(...
'Parent',h35,...
'Units','characters',...
'CData',[],...
'HorizontalAlignment','left',...
'Position',[1.14285714285714 0.25 50.7142857142857 3.6],...
'Style','text',...
'UserData',[],...
'Tag','Punto_Eq_txt',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel2';

h37 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Stato iniziale',...
'Clipping','on',...
'Position',[117.428571428571 2.3 46 6.25],...
'Tag','uipanel2',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'uipanel3';

h38 = uipanel(...
'Parent',h37,...
'Units','characters',...
'Title','Temperatura interna thetai0 [�C]',...
'Clipping','on',...
'Position',[1.85714285714286 0.5 42 4.5],...
'Tag','uipanel3',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'thetai0';

h39 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[0.9 0.9 0.9],...
'Callback',@(hObject,eventdata)Main_export('thetai0_Callback',hObject,eventdata,guidata(hObject)),...
'Max',300,...
'Position',[1.57142857142857 2.15 38 1],...
'String',{  'Slider' },...
'Style','slider',...
'Value',20,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0');

appdata = [];
appdata.lastValidTag = 'thetai0_cur';

h40 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_cur_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[16 0.55 9 1],...
'String','20.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_cur_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_cur');

appdata = [];
appdata.lastValidTag = 'thetai0_max';

h41 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_max_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[30.5714285714286 0.55 9 1],...
'String','300.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_max_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_max');

appdata = [];
appdata.lastValidTag = 'thetai0_min';

h42 = uicontrol(...
'Parent',h38,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('thetai0_min_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','right',...
'Position',[1.57142857142857 0.55 9 1],...
'String','0.0',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('thetai0_min_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','thetai0_min');

appdata = [];
appdata.lastValidTag = 'uipanel14';

h43 = uipanel(...
'Parent',h1,...
'Units','characters',...
'FontWeight','bold',...
'Title','Schema',...
'Clipping','on',...
'Position',[1.42857142857143 7.9 66 19.15],...
'Tag','uipanel14',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'Schema';

h44 = axes(...
'Parent',h43,...
'Units','characters',...
'Position',[1.71428571428571 0.700000000000001 62 17.2],...
'Box','on',...
'CameraPosition',[254.5 182.5 9.16025403784439],...
'CameraPositionMode',get(0,'defaultaxesCameraPositionMode'),...
'Color',get(0,'defaultaxesColor'),...
'ColorOrder',get(0,'defaultaxesColorOrder'),...
'Layer','top',...
'LooseInset',[9.54174757281553 2.2388501026694 6.97281553398058 1.5264887063655],...
'XColor',get(0,'defaultaxesXColor'),...
'XLim',[0.5 508.5],...
'XLimMode','manual',...
'YColor',get(0,'defaultaxesYColor'),...
'YDir','reverse',...
'YLim',[0.5 364.5],...
'YLimMode','manual',...
'ZColor',get(0,'defaultaxesZColor'),...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('Schema_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','Schema',...
'Visible','off');

h45 = get(h44,'xlabel');

set(h45,...
'Parent',h44,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[253.914746543779 393.598837209302 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','cap',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h46 = get(h44,'ylabel');

set(h46,...
'Parent',h44,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','center',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-45.7350230414747 184.087209302326 1.00005459937205],...
'Rotation',90,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','bottom',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

h47 = get(h44,'zlabel');

set(h47,...
'Parent',h44,...
'Units','data',...
'FontUnits','points',...
'BackgroundColor','none',...
'Color',[0 0 0],...
'DisplayName',blanks(0),...
'EdgeColor','none',...
'EraseMode','normal',...
'DVIMode','auto',...
'FontAngle','normal',...
'FontName','Helvetica',...
'FontSize',10,...
'FontWeight','normal',...
'HorizontalAlignment','right',...
'LineStyle','-',...
'LineWidth',0.5,...
'Margin',2,...
'Position',[-28.1774193548387 -23.3081395348838 1.00005459937205],...
'Rotation',0,...
'String',blanks(0),...
'Interpreter','tex',...
'VerticalAlignment','middle',...
'ButtonDownFcn',[],...
'CreateFcn', {@local_CreateFcn, [], ''} ,...
'DeleteFcn',[],...
'BusyAction','queue',...
'HandleVisibility','off',...
'HelpTopicKey',blanks(0),...
'HitTest','on',...
'Interruptible','on',...
'SelectionHighlight','on',...
'Serializable','on',...
'Tag',blanks(0),...
'UserData',[],...
'Visible','off',...
'XLimInclude','on',...
'YLimInclude','on',...
'ZLimInclude','on',...
'CLimInclude','on',...
'ALimInclude','on',...
'IncludeRenderer','on',...
'Clipping','off');

appdata = [];
appdata.lastValidTag = 'uipanel15';

h48 = uipanel(...
'Parent',h1,...
'Units','characters',...
'BorderWidth',2,...
'FontWeight','bold',...
'Title','Configurazione',...
'UserData',[],...
'Clipping','on',...
'Position',[69.4285714285714 23.7 94 3.15],...
'Tag','uipanel15',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigLoadName';

h49 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigLoadName_Callback',hObject,eventdata,guidata(hObject)),...
'Position',[2 0.499999999999999 22 1.3],...
'String','Default',...
'Style','popupmenu',...
'Value',1,...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigLoadName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigLoadName');

appdata = [];
appdata.lastValidTag = 'ConfigLoad';

h50 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigLoad_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[25 0.499999999999999 9 1.3],...
'String','Load',...
'Tag','ConfigLoad',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigDelete';

h51 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigDelete_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[35 0.499999999999999 9 1.3],...
'String','Delete',...
'Tag','ConfigDelete',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );

appdata = [];
appdata.lastValidTag = 'ConfigSaveName';

h52 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'BackgroundColor',[1 1 1],...
'Callback',@(hObject,eventdata)Main_export('ConfigSaveName_Callback',hObject,eventdata,guidata(hObject)),...
'HorizontalAlignment','left',...
'Position',[60.4 0.599999999999999 21 1.1],...
'String','nome file',...
'Style','edit',...
'CreateFcn', {@local_CreateFcn, @(hObject,eventdata)Main_export('ConfigSaveName_CreateFcn',hObject,eventdata,guidata(hObject)), appdata} ,...
'Tag','ConfigSaveName');

appdata = [];
appdata.lastValidTag = 'ConfigSave';

h53 = uicontrol(...
'Parent',h48,...
'Units','characters',...
'Callback',@(hObject,eventdata)Main_export('ConfigSave_Callback',hObject,eventdata,guidata(hObject)),...
'FontSize',10,...
'Position',[82.4000000000002 0.499999999999999 9 1.3],...
'String','Save',...
'Tag','ConfigSave',...
'CreateFcn', {@local_CreateFcn, blanks(0), appdata} );


hsingleton = h1;


% --- Set application data first then calling the CreateFcn. 
function local_CreateFcn(hObject, eventdata, createfcn, appdata)

if ~isempty(appdata)
   names = fieldnames(appdata);
   for i=1:length(names)
       name = char(names(i));
       setappdata(hObject, name, getfield(appdata,name));
   end
end

if ~isempty(createfcn)
   if isa(createfcn,'function_handle')
       createfcn(hObject, eventdata);
   else
       eval(createfcn);
   end
end


% --- Handles default GUIDE GUI creation and callback dispatch
function varargout = gui_mainfcn(gui_State, varargin)

gui_StateFields =  {'gui_Name'
    'gui_Singleton'
    'gui_OpeningFcn'
    'gui_OutputFcn'
    'gui_LayoutFcn'
    'gui_Callback'};
gui_Mfile = '';
for i=1:length(gui_StateFields)
    if ~isfield(gui_State, gui_StateFields{i})
        error(message('MATLAB:guide:StateFieldNotFound', gui_StateFields{ i }, gui_Mfile));
    elseif isequal(gui_StateFields{i}, 'gui_Name')
        gui_Mfile = [gui_State.(gui_StateFields{i}), '.m'];
    end
end

numargin = length(varargin);

if numargin == 0
    % MAIN_EXPORT
    % create the GUI only if we are not in the process of loading it
    % already
    gui_Create = true;
elseif local_isInvokeActiveXCallback(gui_State, varargin{:})
    % MAIN_EXPORT(ACTIVEX,...)
    vin{1} = gui_State.gui_Name;
    vin{2} = [get(varargin{1}.Peer, 'Tag'), '_', varargin{end}];
    vin{3} = varargin{1};
    vin{4} = varargin{end-1};
    vin{5} = guidata(varargin{1}.Peer);
    feval(vin{:});
    return;
elseif local_isInvokeHGCallback(gui_State, varargin{:})
    % MAIN_EXPORT('CALLBACK',hObject,eventData,handles,...)
    gui_Create = false;
else
    % MAIN_EXPORT(...)
    % create the GUI and hand varargin to the openingfcn
    gui_Create = true;
end

if ~gui_Create
    % In design time, we need to mark all components possibly created in
    % the coming callback evaluation as non-serializable. This way, they
    % will not be brought into GUIDE and not be saved in the figure file
    % when running/saving the GUI from GUIDE.
    designEval = false;
    if (numargin>1 && ishghandle(varargin{2}))
        fig = varargin{2};
        while ~isempty(fig) && ~ishghandle(fig,'figure')
            fig = get(fig,'parent');
        end
        
        designEval = isappdata(0,'CreatingGUIDEFigure') || isprop(fig,'GUIDEFigure');
    end
        
    if designEval
        beforeChildren = findall(fig);
    end
    
    % evaluate the callback now
    varargin{1} = gui_State.gui_Callback;
    if nargout
        [varargout{1:nargout}] = feval(varargin{:});
    else       
        feval(varargin{:});
    end
    
    % Set serializable of objects created in the above callback to off in
    % design time. Need to check whether figure handle is still valid in
    % case the figure is deleted during the callback dispatching.
    if designEval && ishghandle(fig)
        set(setdiff(findall(fig),beforeChildren), 'Serializable','off');
    end
else
    if gui_State.gui_Singleton
        gui_SingletonOpt = 'reuse';
    else
        gui_SingletonOpt = 'new';
    end

    % Check user passing 'visible' P/V pair first so that its value can be
    % used by oepnfig to prevent flickering
    gui_Visible = 'auto';
    gui_VisibleInput = '';
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        % Recognize 'visible' P/V pair
        len1 = min(length('visible'),length(varargin{index}));
        len2 = min(length('off'),length(varargin{index+1}));
        if ischar(varargin{index+1}) && strncmpi(varargin{index},'visible',len1) && len2 > 1
            if strncmpi(varargin{index+1},'off',len2)
                gui_Visible = 'invisible';
                gui_VisibleInput = 'off';
            elseif strncmpi(varargin{index+1},'on',len2)
                gui_Visible = 'visible';
                gui_VisibleInput = 'on';
            end
        end
    end
    
    % Open fig file with stored settings.  Note: This executes all component
    % specific CreateFunctions with an empty HANDLES structure.

    
    % Do feval on layout code in m-file if it exists
    gui_Exported = ~isempty(gui_State.gui_LayoutFcn);
    % this application data is used to indicate the running mode of a GUIDE
    % GUI to distinguish it from the design mode of the GUI in GUIDE. it is
    % only used by actxproxy at this time.   
    setappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]),1);
    if gui_Exported
        gui_hFigure = feval(gui_State.gui_LayoutFcn, gui_SingletonOpt);

        % make figure invisible here so that the visibility of figure is
        % consistent in OpeningFcn in the exported GUI case
        if isempty(gui_VisibleInput)
            gui_VisibleInput = get(gui_hFigure,'Visible');
        end
        set(gui_hFigure,'Visible','off')

        % openfig (called by local_openfig below) does this for guis without
        % the LayoutFcn. Be sure to do it here so guis show up on screen.
        movegui(gui_hFigure,'onscreen');
    else
        gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        % If the figure has InGUIInitialization it was not completely created
        % on the last pass.  Delete this handle and try again.
        if isappdata(gui_hFigure, 'InGUIInitialization')
            delete(gui_hFigure);
            gui_hFigure = local_openfig(gui_State.gui_Name, gui_SingletonOpt, gui_Visible);
        end
    end
    if isappdata(0, genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]))
        rmappdata(0,genvarname(['OpenGuiWhenRunning_', gui_State.gui_Name]));
    end

    % Set flag to indicate starting GUI initialization
    setappdata(gui_hFigure,'InGUIInitialization',1);

    % Fetch GUIDE Application options
    gui_Options = getappdata(gui_hFigure,'GUIDEOptions');
    % Singleton setting in the GUI M-file takes priority if different
    gui_Options.singleton = gui_State.gui_Singleton;

    if ~isappdata(gui_hFigure,'GUIOnScreen')
        % Adjust background color
        if gui_Options.syscolorfig
            set(gui_hFigure,'Color', get(0,'DefaultUicontrolBackgroundColor'));
        end

        % Generate HANDLES structure and store with GUIDATA. If there is
        % user set GUI data already, keep that also.
        data = guidata(gui_hFigure);
        handles = guihandles(gui_hFigure);
        if ~isempty(handles)
            if isempty(data)
                data = handles;
            else
                names = fieldnames(handles);
                for k=1:length(names)
                    data.(char(names(k)))=handles.(char(names(k)));
                end
            end
        end
        guidata(gui_hFigure, data);
    end

    % Apply input P/V pairs other than 'visible'
    for index=1:2:length(varargin)
        if length(varargin) == index || ~ischar(varargin{index})
            break;
        end

        len1 = min(length('visible'),length(varargin{index}));
        if ~strncmpi(varargin{index},'visible',len1)
            try set(gui_hFigure, varargin{index}, varargin{index+1}), catch break, end
        end
    end

    % If handle visibility is set to 'callback', turn it on until finished
    % with OpeningFcn
    gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
    if strcmp(gui_HandleVisibility, 'callback')
        set(gui_hFigure,'HandleVisibility', 'on');
    end

    feval(gui_State.gui_OpeningFcn, gui_hFigure, [], guidata(gui_hFigure), varargin{:});

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        % Handle the default callbacks of predefined toolbar tools in this
        % GUI, if any
        guidemfile('restoreToolbarToolPredefinedCallback',gui_hFigure); 
        
        % Update handle visibility
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);

        % Call openfig again to pick up the saved visibility or apply the
        % one passed in from the P/V pairs
        if ~gui_Exported
            gui_hFigure = local_openfig(gui_State.gui_Name, 'reuse',gui_Visible);
        elseif ~isempty(gui_VisibleInput)
            set(gui_hFigure,'Visible',gui_VisibleInput);
        end
        if strcmpi(get(gui_hFigure, 'Visible'), 'on')
            figure(gui_hFigure);
            
            if gui_Options.singleton
                setappdata(gui_hFigure,'GUIOnScreen', 1);
            end
        end

        % Done with GUI initialization
        if isappdata(gui_hFigure,'InGUIInitialization')
            rmappdata(gui_hFigure,'InGUIInitialization');
        end

        % If handle visibility is set to 'callback', turn it on until
        % finished with OutputFcn
        gui_HandleVisibility = get(gui_hFigure,'HandleVisibility');
        if strcmp(gui_HandleVisibility, 'callback')
            set(gui_hFigure,'HandleVisibility', 'on');
        end
        gui_Handles = guidata(gui_hFigure);
    else
        gui_Handles = [];
    end

    if nargout
        [varargout{1:nargout}] = feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    else
        feval(gui_State.gui_OutputFcn, gui_hFigure, [], gui_Handles);
    end

    if isscalar(gui_hFigure) && ishghandle(gui_hFigure)
        set(gui_hFigure,'HandleVisibility', gui_HandleVisibility);
    end
end

function gui_hFigure = local_openfig(name, singleton, visible)

% openfig with three arguments was new from R13. Try to call that first, if
% failed, try the old openfig.
if nargin('openfig') == 2
    % OPENFIG did not accept 3rd input argument until R13,
    % toggle default figure visible to prevent the figure
    % from showing up too soon.
    gui_OldDefaultVisible = get(0,'defaultFigureVisible');
    set(0,'defaultFigureVisible','off');
    gui_hFigure = openfig(name, singleton);
    set(0,'defaultFigureVisible',gui_OldDefaultVisible);
else
    gui_hFigure = openfig(name, singleton, visible);  
    %workaround for CreateFcn not called to create ActiveX
    if feature('HGUsingMATLABClasses')
        peers=findobj(findall(allchild(gui_hFigure)),'type','uicontrol','style','text');    
        for i=1:length(peers)
            if isappdata(peers(i),'Control')
                actxproxy(peers(i));
            end            
        end
    end
end

function result = local_isInvokeActiveXCallback(gui_State, varargin)

try
    result = ispc && iscom(varargin{1}) ...
             && isequal(varargin{1},gcbo);
catch
    result = false;
end

function result = local_isInvokeHGCallback(gui_State, varargin)

try
    fhandle = functions(gui_State.gui_Callback);
    result = ~isempty(findstr(gui_State.gui_Name,fhandle.file)) || ...
             (ischar(varargin{1}) ...
             && isequal(ishghandle(varargin{2}), 1) ...
             && (~isempty(strfind(varargin{1},[get(varargin{2}, 'Tag'), '_'])) || ...
                ~isempty(strfind(varargin{1}, '_CreateFcn'))) );
catch
    result = false;
end


