%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                      STR_COMPLEX                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% function str_polynomial = str_polynomial(poly, var)
%
% poly           polynomial coefficients expressed as a vector nx1
%
% var            letter to use as incognite variable
%
% str_polynomial    return displayable rappresentation of the polynomial
%       
%
% by G. Scarpellini (2018)
%
% Tested under MatLab R2013b
%

function str_polynomial = str_polynomial(poly, var)
    poly = round(cell2mat(poly) .* 10) ./ 10;
    str_polynomial = '';
    for i = 1 : length(poly)    
        value = poly(i);
        if (value  ~= 0)
            if (value == 1)
                value = '+';
            end
            if (i == length(poly))
                str = sprintf(' %+.1f', value);
            elseif (i == ( length(poly) -1))
                str = sprintf(' %+.1f %s', value, var);
            else
                str = sprintf(' %+.1f %s^%d', value, var,length(poly) - i);
            end
            str_polynomial = sprintf(strcat(str_polynomial, str));
        end
    end
end